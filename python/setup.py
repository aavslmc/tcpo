from setuptools import setup
import os

# Analyses Packages which must be installed
packages = [f[0] for f in os.walk('pytcpo')]
packages.append('pytcpotests') # For the Testing Script

# Call Setup
setup(
    name='tcpo',
    version='1.0',
    packages=packages,
    url='https://bitbucket.org/aavslmc/tcpo',
    license='',
    author='Michael Camilleri',
    author_email='michael.camilleri@ascent.software',
    description='TCPO Library for AAVS',
    requires=['zope.event', 'matplotlib', 'astropy', 'scipy', 'numpy', 'abc', 'redis', 'rwlock', 'lockfile']
   )