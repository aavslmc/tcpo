import logging
import os

import numpy as np

from pytcpo.Core.PipeLine import PipelineManager
from pytcpo.Core.Common import string, TCPOCode
from pytcpo.Modules.Archivers import FilePersistor

import pytcpo.Modules.IO
import pytcpo.Modules.Archivers
import pytcpo.Modules.RawFilters
import pytcpo.Modules.BandPassModel
import pytcpo.Modules.AnomalyDetectors

# Telescope Definitions
Samples = (0,20000)                                 # Sample Range to read... (min,max)
Antennas = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)  # Which are the valid Antennas
Polarisations = (0,1)                               # Number of Polarisations
Channels = np.arange(74,448)                        # Total channel range to be fitter over
Tile = 1                                            # What tile to operate on
DownSample = 200                                    # Down-Sample Factor (skipping values)

# Permanent RFI Cleaning
replace = True                                      # Selects if permenent RFI cleaning is to be carried out
RFI_Channels = [np.arange(305, 348),                # Known permanent RFI channels within the entire 0-512 channel range
                np.arange(351, 366),
                np.arange(436, 448)]

# Fitting Parameters
model = 'PiecewisePolyFitter'
PolyFits = [[5,np.arange(74,130)],                  # What regions to fit over with what order. Regions must overlap by
            [7,np.arange(120,448)]]                 # exactly 10 channels. Channels must be within Channel range above

# Outlier Detection Scheme Setup
# Which modules are being used within scheme
outliers = ['DerivativeSignsWithDifferenceRejection',
            'PolynomialDeviations']
aggregator = 'OutlierAggregator'
agg_length = 0                                      # Aggregator setup - set to 0 to not use morphological filter
# What parameters are required
outlier_std = 5.0                                   # Number of standard deviations threshold - used by PD
outlier_cut = 0.1                                   # Standard deviation cutoff parameter - used by PD
min_cutoff = 0.5                                    # Gradient cutoff parameter - used by DSWDR
envelope_cutoff = 1.0                               # Envelope parameter - used by DSWDR

# Anomalous Antenna Detection Scheme Setup
# Which modules are being used within scheme
anomalies = ['InterAntennaDeviationDetector',
             'InterAntennaOverlapDetector',
             'IndependentOverlapDetector',
             'OutlierThresholdDetector',
             'OutlierWeightedOverlapDetector',
             'PerAntennaDeviationDetector']
# What parameters are required
rejection_std = 5.0
max_rate = 1.0
avg_length = 15
padd_excessive = np.array([4.0, 4.0])
iadd_excessive = np.array([6.0, 6.0])
overlap_form = False                                # Consider also the mean (offset)
overlap_cutoff = 0.95
similarity_thresh = 0.75                            # At least must be similar to 0.75 of other antennas
outlier_thresh = 0.1                                # 10% outliers is the cutoff for detecting anomalies based on number
                                                    # of outliers in bandpass
max_outlier = 0.1                                   # Maximum number of outliers for moving average to consider sample
fits = len(PolyFits)
fit_orders = np.array([PolyFits[0][0], PolyFits[1][0]])

# Start Vectors For Each Piecewise Fit
model_i = np.array([np.zeros(fit_orders[0]+1), np.zeros(fit_orders[1]+1)])         # All 0s
# Weighting Vectors For Each Piecewise Fit
weightings = np.array([np.array([0.0001,0.001,0.1,1,1,1]), np.array([0.0001,0.001,0.1,1,1,1,1,1])])  # All 1s

# Other Options
log_scale = True                                    # Operate on log-scale or not
perform_detrending = False                          # Flag to allow /disable de-trending

# Source Data Location
src_location = '/home/xandru/Documents/Data/NewData2'
# Destination Data Location
dst_root_loc = '/home/xandru/Documents/ScratchPiecewise/TestDetectors2'

outly_types = [string(outlier) for outlier in outliers]
anomaly_types = [string(anomaly) for anomaly in anomalies]

if __name__=='__main__':

    # Create Manager - The main instance
    manager = PipelineManager()

    # First set the archiver
    sources = {}

    model_pth = string(model).acronym() + ('_log/' if log_scale else '_lin/') + '/'

    # Create Folder if inexistent
    try:
        os.makedirs(dst_root_loc + '/' + model_pth)
    except OSError:
        if not os.path.isdir(dst_root_loc + '/' + model_pth):
            raise

    # Add Key(s)
    # First The Polynomial Fit
    sources[model_pth + 'polyfit'] = None   # No need for type at writing stage

    # Now the Outliers
    for outlier in outly_types:
        sources[model_pth + 'out_' + outlier.acronym()] = None

    # And the aggregator
    sources[model_pth + 'out_agg'] = None

    # Anomaly data
    for anomaly in anomaly_types:
        sources[model_pth + 'anom_' + anomaly.acronym()] = None

    # Finally if detrending
    if perform_detrending:
        sources[model_pth + 'output'] = None

    manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'source': dst_root_loc, 'keys': sources}, 'append' : False},
                         serialiser={'type': 'CSVSerialiser'})

    # Now build the pipeline...

    # Reader
    manager.add_root('ChannelStageReader', {'path':src_location, 'time':None, 'tiles':Tile, 'sample_rate':0,
                                            'block_rate':2048, 'samples':Samples, 'stop_on_end':True})
    # Down-Sampler
    manager.add_module('AliasingDownSampler', {'rate':DownSample, 'start':Samples[0]})

    if replace:
        manager.add_module('PermanentRFIFiller', {'antennas': Antennas, 'polarisations': Polarisations,
                                                  'perm_rfi_chans': RFI_Channels})

    # Antenna Selector
    manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas':np.asarray(Antennas)})

    # Polarisation Selector
    manager.add_module('PolarisationSelector', {'in_polarisations': np.arange(2), 'out_polarisations': np.asarray(Polarisations)})

    # ChannelSelector
    manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels':Channels})

    # CScaler
    manager.add_module('CScaler', {'input':(0,65535), 'output':(0,65535), 'type':float})

    # Power Log (Optional)
    if log_scale:
        manager.add_module('PowerLog', {'multiplier':10, 'log':'10'})

    # Keep Track of the fanout-tile
    fanout = manager.Last

    # The BandPass Piecewise Polynomial Fitter

    fitter = manager.add_module(model, {'antennas': len(Antennas), 'polarisations': len(Polarisations), 'channels': Channels,
                                        'fits': PolyFits}, parents=(fanout,), archive_key=model_pth + 'polyfit')

    evaluator = manager.add_module('PiecewisePolyEvaluator', {'antennas': len(Antennas), 'polarisations': len(Polarisations),
                                   'fit_channels': Channels}, parents=(fitter,))

    # The Outlier Detector(s)
    outly_model = None
    all_outliers = []

    for o_i, outlier in enumerate(outly_types):

        if outlier not in ['DerivativeSignsWithPeakRejection']:

            manager.add_module(outlier, {'antennas':Antennas, 'polarisations':Polarisations,'channels':Channels,
                                         'model':evaluator, 'outlier_cut':outlier_cut, 'envelope':envelope_cutoff,
                                         'outlier_std':outlier_std, 'min_cutoff':min_cutoff},
                               (fanout, evaluator), archive_key=model_pth + 'out_' + outlier.acronym())

            if outlier == 'PolynomialDeviations': outly_model = manager.Last

        else:

            manager.add_module(outlier, {'min_cutoff': min_cutoff,'antennas':Antennas, 'polarisations':Polarisations,
                                         'channels':Channels, 'model':evaluator,'poly_outly':outly_model},
                               (fanout, evaluator, outly_model), archive_key=model_pth + 'out_' + outlier.acronym())

        all_outliers.append(manager.Last)

    # The Aggregator
    aggregat_outlier = manager.add_module(aggregator, {'antennas':Antennas, 'polarisations':Polarisations,'channels':Channels,
                                                       'length': agg_length, 'others': all_outliers},
                                          all_outliers, archive_key=model_pth + 'out_agg')

    # The Anomaly Detector(s)
    for a_i, anomaly in enumerate(anomaly_types):

        if anomaly == 'OutlierWeightedOverlapDetector':

            parent = (fitter, aggregat_outlier)

        elif anomaly == 'OutlierThresholdDetector':

            parent = aggregat_outlier

        else:
            parent = fitter

        manager.add_module(anomaly,
                           {'antennas':Antennas, 'polarisations':Polarisations, 'channels':Channels, 'ignore':True,
                            'start':model_i, 'length':avg_length, 'weight':weightings, 'form':overlap_form,
                            'deviation':iadd_excessive, 'excessive':padd_excessive, 'min_overlap':overlap_cutoff,
                            'threshold':outlier_thresh, 'similarity':similarity_thresh, 'outlier_gen':aggregat_outlier,
                            'max_outlier':max_outlier, 'fits':fits, 'fit_orders':fit_orders},
                           parent, archive_key=model_pth + 'anom_' + anomaly.acronym())

    # Now the De-Trender (and associated, if de-trending)
    if perform_detrending:

        manager.add_module('PolyDeTrender', {'model':evaluator}, (fanout, evaluator))
        # Optional Log Scale convertor
        if log_scale: manager.add_module('Exponentiator', {'divider':10, 'power':'10'})
        # finally the de-scaler
        manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': np.uint16},
                           archive_key=model_pth + 'output')

    # Now Build
    manager.build()

    # Now start the reader, doing all
    logging.getLogger().setLevel(logging.INFO)
    assert (manager.start() == TCPOCode.Status.ACTIVE)

    # Wait for completion
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)