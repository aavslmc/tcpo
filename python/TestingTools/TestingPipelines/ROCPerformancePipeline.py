import logging
import os

import numpy as np

from pytcpo.Core.Common import string, TCPOCode
from pytcpo.Core.PipeLine import PipelineManager

import pytcpo.Modules.IO
import pytcpo.Modules.Archivers
import pytcpo.Modules.RawFilters
import pytcpo.Modules.BandPassModel
import pytcpo.Modules.AnomalyDetectors

import TestingTools.TestingModules.ROCModules
import TestingTools.TestingModules.RawDataManipulators

# ----- Module Parameter Setups To Consider -----

# Outlier Detectors
dsd = {'min_cutoff': np.linspace(0.1, 40.0, num=2, endpoint=False, dtype=np.float32)}

pd = {'outlier_std': np.linspace(0.1, 15.0, num=2, endpoint=False, dtype=np.float32),
      'min_cutoff': np.linspace(0.1, 8.0, num=2, endpoint=False, dtype=np.float32)}

dswpr_pd = {'outlier_std': np.linspace(2.0, 3.0, num=2, endpoint=False, dtype=np.float32),
      'min_cutoff': np.linspace(0.1, 8.0, num=2, endpoint=False, dtype=np.float32)}  # For DSWDR paired with the PD

dswpr = {'min_cutoff': np.linspace(0.1, 10.0, num=2, endpoint=False, dtype=np.float32)}

dswdr = {'min_cutoff': np.linspace(0.1, 20.0, num=2, endpoint=False, dtype=np.float32),
         'envelope': np.linspace(1.0, 2.0, num=2, endpoint=False, dtype=np.float32)}

# Anomaly Detectors
iadd_dev = np.linspace(0.1, 10.0, num=20, endpoint=False, dtype=np.float32)
iadd = {'deviation': np.transpose([np.tile(iadd_dev, len(iadd_dev)), np.repeat(iadd_dev, len(iadd_dev))])}

iaod = {'min_overlap': np.linspace(0.1,1.0, num=20, endpoint=False, dtype=np.float32),
        'similarity': np.linspace(0.1,1.0, num=20, endpoint=False, dtype=np.float32),
        'form': np.array([True,False])}

padd_exc = np.linspace(0.1, 10.0, num=20, endpoint=False, dtype=np.float32)
padd = {'excessive': np.transpose([np.tile(padd_exc, len(padd_exc)), np.repeat(padd_exc, len(padd_exc))]),
        'length': np.linspace(1, 16, num=15, endpoint=False, dtype=int),
        'form': np.array([True, False])}

iod = {'min_overlap': np.linspace(0.1,1.0, num=20, endpoint=False, dtype=np.float32),
       'length': np.linspace(1, 16, num=15, endpoint=False, dtype=int),
       'form': np.array([True, False])}

otd = {'threshold': np.linspace(0.0, 0.1, num=100, endpoint=False, dtype=np.float32)}

owod = {'min_overlap': np.linspace(0.1,1.0, num=2, endpoint=False, dtype=np.float32),
        'max_outlier': np.linspace(0.1, 0.1, num=2, endpoint=False, dtype=np.float32),
        'length': np.linspace(5, 16, num=2, endpoint=False, dtype=int),
        'form': np.array([True, False])}

# ----- ROC Module Setup -----
file_mode = True

# ----- Aggregated Outlier Detector -----
aggregator = 'OutlierAggregator'
agg_length = 0                                      # Aggregator setup - set to 0 to not use morphological filter

# ----- Pipeline Setup -----

# Telescope Definitions
Samples = (0,20000)                                 # Sample Range to read... (min,max)
Antennas = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)  # Which are the valid Antennas
Polarisations = (0,1)                               # Number of Polarisations
Channels = np.arange(74,448)                        # Total channel range to be fitter over
Tile = 1                                            # What tile to operate on
DownSample = 200                                    # Down-Sample Factor (skipping values)

# Permanent RFI Cleaning
replace = True                                      # Selects if permanent RFI cleaning is to be carried out
RFI_Channels = [np.arange(305, 348),                # Known permanent RFI channels within the entire 0-512 channel range
                np.arange(351, 366),
                np.arange(436, 448)]

# Fitting Parameters
model = 'PiecewisePolyFitter'
PolyFits = [[5,np.arange(74,130)],                  # What regions to fit over with what order. Regions must overlap by
            [6,np.arange(120,448)]]                 # Exactly 10 channels. Channels must be within Channel range above

# Outlier Detection Scheme Setup
# Which modules are being used within scheme
outliers = [] #'DerivativeSignsWithDifferenceRejection','PolynomialDeviations'
# Which modules are being used within the detection scheme used by anomalous detectors down the line
anom_outliers = ['DerivativeSignsWithDifferenceRejection','PolynomialDeviations']
# Which parameters are required for the modules being used within the detection scheme above
outlier_std = 5.0                                   # Number of standard deviations threshold - used by PD
outlier_cut = 0.1                                   # Standard deviation cutoff parameter - used by PD
min_cutoff = 0.5                                    # Gradient cutoff parameter - used by DSWDR
envelope_cutoff = 1.0                               # Envelope parameter - used by DSWDR

# Anomalous Antenna Detection Scheme Setup
# Which modules are being used within scheme
anomalies = ['OutlierWeightedOverlapDetector']

# What parameters are required
fits = len(PolyFits)
fit_orders = np.array([PolyFits[0][0], PolyFits[1][0]])
# Specify if raw data inverting is to be used
inverting = True
invert_mdl = np.random.choice([1,0], ((len(Antennas)), len(Polarisations)), p=[1-0.95,0.95])
# Specify if pre-pipeline is required for anom data model generation
pre_pipeline = False

# Start Vectors For Each Piecewise Fit
model_i = np.array([np.zeros(fit_orders[0]+1), np.zeros(fit_orders[1]+1)])         # All 0s
# Weighting Vectors For Each Piecewise Fit
weightings = np.array([[1e-3, 1e-2, 1e-1, 1, 1, 1], [1e-3, 1e-2, 1, 1, 1, 1, 1]])  # All 1s
#  weightings = np.array([[1e-3, 1e-2, 1e-1, 1, 1, 1], [1e-2, 1e-2, 1, 1, 1, 1, 1, 1]])

# Other Options
log_scale = True                                    # Operate on log-scale or not

# Source Data Location
src_location = '/home/xandru/Documents/Data/NewData2'
anom_src_location = '/Data_Models'
rfi_src_location = '/Data_Models'
# Destination Data Location
dst_root_loc = '/home/xandru/Documents/Scratch/ROCTest'

outly_types = [string(outlier) for outlier in outliers]
outly_anom_types = [string(outlier) for outlier in anom_outliers]
anomaly_types = [string(anomaly) for anomaly in anomalies]

if __name__=='__main__':

    # ----- Initial Pipeline -----

    # Create Manager - The main instance
    manager = PipelineManager()

    # First set the archiver
    sources = {}

    model_pth = string(model).acronym() + ('_log/' if log_scale else '_lin/') + '/'

    # Create Folder if inexistent
    try:
        os.makedirs(dst_root_loc + '/' + model_pth)
    except OSError:
        if not os.path.isdir(dst_root_loc + '/' + model_pth):
            raise

    # Add Key(s)
    # First The Polynomial Fit
    sources[model_pth + 'polyfit'] = None   # No need for type at writing stage

    # Now the Outliers

    for outlier in outly_types:
        sources[model_pth + 'ROCData/roc_' + outlier.acronym()] = None

    # Outlier ROC for anomalous detector scheme
    sources[model_pth + 'ROCData/roc_anom_outlier_scheme'] = None

    # Anomaly data
    for anomaly in anomaly_types:
        sources[model_pth + 'ROCData/roc_anom_' + anomaly.acronym()] = None

    manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'source': dst_root_loc, 'keys': sources}, 'append' : False},
                         serialiser={'type': 'CSVSerialiser'})

    # Now build the pipeline...

    # Sub pipeline is necessary
    if pre_pipeline:
        pre_manager = PipelineManager()

        pre_sources = {}
        pre_sources[anom_src_location + '/anom_ap'] = None

        pre_manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'source': dst_root_loc, 'keys': pre_sources}, 'append': False},
                                 serialiser={'type': 'CSVSerialiser'})

        # Reader
        pre_manager.add_root('ChannelStageReader', {'path':src_location, 'time':None, 'tiles':Tile, 'sample_rate':0,
                                                    'block_rate':2048, 'samples':Samples, 'stop_on_end':True})
        # Down-Sampler
        pre_manager.add_module('AliasingDownSampler', {'rate':DownSample, 'start':Samples[0]})

        if replace:
            pre_manager.add_module('PermanentRFIFiller', {'antennas': Antennas, 'polarisations': Polarisations,
                                                          'perm_rfi_chans': RFI_Channels})

        # Antenna Selector
        pre_manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas':np.asarray(Antennas)})

        # Polarisation Selector
        pre_manager.add_module('PolarisationSelector', {'in_polarisations': np.arange(2), 'out_polarisations': np.asarray(Polarisations)})

        # ChannelSelector
        pre_manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels':Channels})

        # CScaler
        pre_manager.add_module('CScaler', {'input':(0,65535), 'output':(0,65535), 'type':float})

        # Power Log (Optional)
        if log_scale:
            pre_manager.add_module('PowerLog', {'multiplier':10, 'log':'10'})

        pre_manager.add_module('AnomalousAntennaManipulator', {'antennas': Antennas, 'polarisations': Polarisations,
                                                               'invert_model': invert_mdl, 'inter_similarity': 0.5,
                                                               'sample_rng': Samples}, archive_key=anom_src_location + '/anom_ap')

        # Now Build
        pre_manager.build()

        # Now start the reader, doing all
        logging.getLogger().setLevel(logging.INFO)
        assert (pre_manager.start() == TCPOCode.Status.ACTIVE)

        # Wait for completion
        assert (pre_manager.wait_until_ready() == TCPOCode.Status.FILE_END)

    # Reader
    manager.add_root('ChannelStageReader', {'path': src_location, 'time': None, 'tiles': Tile, 'sample_rate': 0,
                                                'block_rate': 2048, 'samples': Samples, 'stop_on_end': True})
    # Down-Sampler
    manager.add_module('AliasingDownSampler', {'rate': DownSample, 'start': Samples[0]})

    if replace:
        manager.add_module('PermanentRFIFiller', {'antennas': Antennas, 'polarisations': Polarisations,
                                                      'perm_rfi_chans': RFI_Channels})

    # Antenna Selector
    manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas': np.asarray(Antennas)})

    # Polarisation Selector
    manager.add_module('PolarisationSelector',
                           {'in_polarisations': np.arange(2), 'out_polarisations': np.asarray(Polarisations)})

    # ChannelSelector
    manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels': Channels})

    # CScaler
    manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': float})

    # Power Log (Optional)
    if log_scale:
        manager.add_module('PowerLog', {'multiplier': 10, 'log': '10'})

    if inverting:
        manager.add_module('AnomalousAntennaManipulator', {'antennas': Antennas, 'polarisations': Polarisations,
                                                           'invert_model': invert_mdl, 'inter_similarity': 0.5,
                                                           'sample_rng': Samples})

    # Keep Track of the fanout-tile
    fanout = manager.Last

    # The BandPass Piecewise Polynomial Fitter

    fitter = manager.add_module(model, {'antennas': len(Antennas), 'polarisations': len(Polarisations), 'channels': Channels,
                                        'fits': PolyFits}, parents=(fanout,), archive_key=model_pth + 'polyfit')

    evaluator = manager.add_module('PiecewisePolyEvaluator', {'antennas': len(Antennas), 'polarisations': len(Polarisations),
                                   'fit_channels': Channels}, parents=(fitter,))

    # ----- Singular Outlier Module ROC Curves -----

    for o_i, outlier in enumerate(outly_types):

        if outlier == 'DerivativeSignsDetector':

            for m_cut, m_c in enumerate(dsd['min_cutoff']):
                dsd_module = manager.add_module(outlier, {'antennas': Antennas, 'polarisations': Polarisations,
                                                          'channels': Channels, 'model': evaluator, 'min_cutoff': m_c},
                                                (fanout, evaluator))

                param_str = 'DSD_cutoff: ' + "%.3f" % m_c

                manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                               'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                               'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'OUT',
                                               'parameter_str': param_str, 'source': dst_root_loc + rfi_src_location},
                                   parents=dsd_module, archive_key=model_pth + 'ROCData/roc_DSD')

        elif outlier == 'DerivativeSignsWithDifferenceRejection':

            for env_cut, env in enumerate(dswdr['envelope']):
                for m_cut, m_c in enumerate(dswdr['min_cutoff']):
                    manager.add_module(outlier, {'antennas': Antennas, 'polarisations': Polarisations,
                                                 'channels': Channels, 'model': fitter, 'envelope': env,
                                                 'min_cutoff': m_c}, (fanout, evaluator))

                    param_str = 'Cutoff: ' + "%.3f" % m_c + ' Envelope: ' + "%.3f" % env

                    manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                   'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                   'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'OUT',
                                                   'parameter_str': param_str, 'source': dst_root_loc + rfi_src_location},
                                       archive_key=model_pth + 'ROCData/roc_DSWDR')

        elif outlier == 'PolynomialDeviations':

            for o_std, o_s in enumerate(pd['outlier_std']):
                for m_cut, m_c in enumerate(pd['min_cutoff']):
                    manager.add_module(outlier, {'antennas': Antennas, 'polarisations': Polarisations,
                                                 'channels': Channels, 'model': evaluator, 'outlier_std': o_s,
                                                 'min_cutoff': m_c}, (fanout, evaluator))

                    param_str = 'PD_Std: ' + "%.3f" % o_s + ' PD_cutoff: ' + "%.3f" % m_c

                    manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                   'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                   'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'OUT',
                                                   'parameter_str': param_str, 'source': dst_root_loc + rfi_src_location},
                                       archive_key=model_pth + 'ROCData/roc_PD')

        elif outlier == 'DerivativeSignsWithPeakRejection':

            for po_std, po_s in enumerate(dswpr_pd['outlier_std']):
                for pm_cut, pm_c in enumerate(dswpr_pd['min_cutoff']):
                    for m_cut, m_c in enumerate(dswpr['min_cutoff']):
                        manager.add_module('PolynomialDeviations', {'antennas': Antennas,
                                                                    'polarisations': Polarisations,
                                                                    'channels': Channels, 'model': fitter,
                                                                    'outlier_std': po_s, 'min_cutoff': pm_c},
                                           (fanout, evaluator))

                        outly_model = manager.Last

                        dswpr_module = manager.add_module(outlier,
                                                          {'antennas': Antennas, 'polarisations': Polarisations,
                                                           'channels': Channels, 'model': fitter, 'min_cutoff': m_c,
                                                           'poly_outly': outly_model}, (fanout, fitter, outly_model))

                        agg = manager.add_module(aggregator,
                                                 {'antennas': Antennas, 'polarisations': Polarisations,
                                                  'channels': Channels, 'length': agg_length, 'others': [outly_model]},
                                                 (dswpr_module, outly_model))

                        param_str = 'PD_Std: ' + "%.3f" % po_s + ' PD_cutoff: ' + "%.3f" % pm_c + ' DSWPR_cutoff: ' + "%.3f" % m_c

                        manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                       'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                       'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'OUT',
                                                       'parameter_str': param_str, 'source': dst_root_loc + rfi_src_location},
                                           parents=agg, archive_key=model_pth + 'ROCData/roc_DSWPR')

    # ----- Outlier Detection Scheme for Anomalous Antenna Detectors which are based on Outlier Detection

    outly_model = None
    all_outliers = []

    for o_i, outlier in enumerate(outly_anom_types):

        if outlier not in ['DerivativeSignsWithPeakRejection']:

            manager.add_module(outlier, {'antennas': Antennas, 'polarisations': Polarisations, 'channels': Channels,
                                         'model': evaluator, 'outlier_cut': outlier_cut, 'envelope': envelope_cutoff,
                                         'outlier_std': outlier_std, 'min_cutoff': min_cutoff}, (fanout, evaluator))

            if outlier == 'PolynomialDeviations': outly_model = manager.Last

        else:

            manager.add_module(outlier, {'min_cutoff': min_cutoff, 'antennas': Antennas, 'polarisations': Polarisations,
                                         'channels': Channels, 'model': evaluator, 'poly_outly': outly_model},
                               (fanout, evaluator, outly_model))

        all_outliers.append(manager.Last)

    aggregate_outlier = manager.add_module(aggregator, {'antennas': Antennas, 'polarisations': Polarisations,
                                                        'channels': Channels, 'length': agg_length,
                                                        'others': all_outliers}, all_outliers)

    # manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
    #                                'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
    #                                'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'OUT',
    #                                'parameter_str': ' ', 'source': rfi_src_location},
    #                    parents=aggregate_outlier, archive_key=model_pth + 'ROCData/roc_anom_outlier_scheme')

    # ----- Singular Anomaly Module ROC Curves -----

    for a_i, anomaly in enumerate(anomaly_types):

        if anomaly == 'OutlierWeightedOverlapDetector':

            parent = (fitter, aggregate_outlier)

        elif anomaly == 'OutlierThresholdDetector':

            parent = aggregate_outlier

        else:
            parent = fitter

        if anomaly == 'InterAntennaDeviationDetector':
            for dev in iadd['deviation']:
                manager.add_module(anomaly,{'antennas':Antennas, 'polarisations':Polarisations, 'fit_orders':fit_orders,
                                            'deviation':dev, 'weight':weightings}, parent)
                param_str = 'Devs:'
                for d in dev[:-1]:
                    param_str = param_str + "%.3f" % d + '-'
                param_str = param_str + "%.3f" % dev[-1]

                manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                               'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                               'down_sample': DownSample, 'file_mode': file_mode, 'test_mode': 'ANOM',
                                               'parameter_str':param_str, 'source': dst_root_loc + anom_src_location},
                                   archive_key=model_pth + 'ROCData/roc_anom_IADD')

        elif anomaly == 'InterAntennaOverlapDetector':
            for f in iaod['form']:
                for s in iaod['similarity']:
                    for o in iaod['min_overlap']:
                        manager.add_module(anomaly,{'antennas':Antennas, 'polarisations':Polarisations, 'fit_orders':fit_orders,
                                                    'form':f, 'similarity':s, 'min_overlap':o}, parent)

                        param_str = 'Form:' + str(f) + ' Sim:' + "%.3f" % s + ' OvLap: ' + "%.3f" % o

                        manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                       'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                       'down_sample': DownSample, 'file_mode': file_mode,
                                                       'test_mode': 'ANOM', 'parameter_str': param_str,
                                                       'source': dst_root_loc + anom_src_location},
                                           archive_key=model_pth + 'ROCData/roc_anom_IAOD')

        elif anomaly == 'PerAntennaDeviationDetector':
            for f in padd['form']:
                for b in padd['length']:
                    for e in padd['excessive']:
                        manager.add_module(anomaly, {'antennas':Antennas, 'polarisations':Polarisations, 'fits':fits,
                                                     'form':f, 'excessive':e, 'length':b, 'start':model_i,
                                                     'weight':weightings}, parent)

                        param_str = 'Form:' + str(f) + ' Exc:'
                        for ex in e[:-1]:
                            param_str = param_str + "%.3f" % ex + ','
                        param_str = param_str + "%.3f" % e[-1] + ' Len:' + str(b)

                        manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                       'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                       'down_sample': DownSample, 'file_mode': file_mode,
                                                       'test_mode': 'ANOM', 'parameter_str': param_str,
                                                       'source': dst_root_loc + anom_src_location, 'buffer_length': b},
                                           archive_key=model_pth + 'ROCData/roc_anom_PADD')

        elif anomaly == 'IndependentOverlapDetector':
            for f in iod['form']:
                for b in iod['length']:
                    for o in iod['min_overlap']:
                        manager.add_module(anomaly, {'antennas':Antennas, 'polarisations':Polarisations, 'fits':fits,
                                                     'form':f, 'min_overlap':o, 'length':b, 'start':model_i}, parent)

                        param_str = 'Form:' + str(f) + ' Exc:' + "%.3f" % o + ' Len:' + str(b)

                        manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                       'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                       'down_sample': DownSample, 'file_mode': file_mode,
                                                       'test_mode': 'ANOM', 'parameter_str': param_str,
                                                       'source': dst_root_loc + anom_src_location, 'buffer_length': b},
                                           archive_key=model_pth + 'ROCData/roc_anom_IOD')

        elif anomaly == 'OutlierThresholdDetector':
            for t in otd['threshold']:
                manager.add_module(anomaly, {'antennas':Antennas, 'polarisations':Polarisations, 'channels':Channels,
                                             'threshold': t}, parent)

                param_str = 'Threshold:' + "%.3f" % t

                manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                               'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                               'down_sample': DownSample, 'file_mode': file_mode,
                                               'test_mode': 'ANOM', 'parameter_str': param_str,
                                               'source': dst_root_loc + anom_src_location},
                                   archive_key=model_pth + 'ROCData/roc_anom_OTD')

        elif anomaly == 'OutlierWeightedOverlapDetector':
            for f in owod['form']:
                for b in owod['length']:
                    for ov in owod['min_overlap']:
                        for ot in owod['max_outlier']:
                            manager.add_module(anomaly, {'antennas':Antennas, 'polarisations':Polarisations,
                                                         'channels':Channels, 'start': model_i, 'fits':fits,
                                                         'length':b, 'min_overlap':ov, 'max_outlier':ot,
                                                         'form':f, 'ignore':True, 'outlier_gen':aggregate_outlier}, parent)

                            param_str = 'Form:' + str(f) + ' Len:' + str(b) + ' Out:' + "%.3f" % ot + ' OvLap:' +\
                                        "%.3f" % ov

                            manager.add_module('ROCData', {'antennas': Antennas, 'polarisations': Polarisations,
                                                           'channels': Channels, 'sample_rng': (Samples[0], Samples[1]),
                                                           'down_sample': DownSample, 'file_mode': file_mode,
                                                           'test_mode': 'ANOM', 'parameter_str': param_str,
                                                           'source': dst_root_loc + anom_src_location, 'buffer_length': b},
                                               archive_key=model_pth + 'ROCData/roc_anom_OWOD')

    # Custom Outlier Detection Schemes Go Here

    # Custom Anomalous Antenna Detection Scheme Go Here

    # Now Build
    manager.build()

    # Now start the reader, doing all
    logging.getLogger().setLevel(logging.INFO)
    assert (manager.start() == TCPOCode.Status.ACTIVE)

    # Wait for completion
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)