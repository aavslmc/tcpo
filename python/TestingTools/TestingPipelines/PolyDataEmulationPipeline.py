'''
PolyDataEmulationPipeline

The following pipeline takes a generated matrix of emulated data from PolyGen (the generator), then provides the following
options to process the data and add additional 'features' to the simulated data :
* Addition of Gaussian Noise using NoiseGen
* Addition of RFI noise using RFIGen
* Simulation of antennas failures using FailGen

The data is then processed through the processor modules Exponentiator and CScalar (in that order). Finally the generated
data is written by the CSWriter processor module.
'''


import time as tm
import logging
import numpy as np

from pytcpo.Core.PipeLine import PipelineManager
from pytcpo.Core.Common import directory

import pytcpo.Modules.DataEmulators
import pytcpo.Modules.RawFilters
import pytcpo.Modules.IO
import pytcpo.Modules.Archivers

write_loc = '/home/xandru/Documents/Data/TestDataFromPolyGen'

# Provides options to select what modules to use
modules = {'noise_module' : False,
           'rfi_module' : True,
           'failure_module' : False}

# Defines the shape of the matrix to which the simulated data is to be written
telescope_model = {'antenna_count' : 16,
                   'pol_count' : 2,
                   'channel_count' : 512}

# Provides options for PolyGen module
polygen = {'base_coeff' : [0.0000000002072,-0.00000000266,-0.000037,0.00000074,25],
           'similarity_percentage' : 0,
           'sample_count' : 50}

# Provides options for NoiseGen module
noisegen = {'alpha' : 0,
            'beta' : 0.5}

rfigen = {'rfi_chans' : 80,
          'rfi_variability' : 0.75}

# Provides options for FailGen module
failgen = {'fail_probability' : 0.1,
           'extremes_maximum' : 40,
           'fail_min_sample' : 0,
           'fail_max_sample' : 50}

if __name__ == '__main__':

    # Ensure that the directory exists and start logging
    directory(write_loc)
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # Create and build pipeline
    manager = PipelineManager()

    sources = {}
    sources['/rfi'] = None

    manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'source': write_loc, 'keys': sources}, 'append': False},
                        serialiser={'type': 'CSVSerialiser'})

    assert(manager.add_root('PolyGen', {'antenna_count' : telescope_model['antenna_count'],
                                        'pol_count' : telescope_model['pol_count'],
                                        'channel_count' : telescope_model['channel_count'],
                                        'base_coeff' : polygen['base_coeff'],
                                        'similarity_percentage' : polygen['similarity_percentage'],
                                        'sample_count' : polygen['sample_count']}))

    if modules['noise_module'] is True :
        assert(manager.add_module('NoiseGen',{'antenna_count': telescope_model['antenna_count'],
                                              'pol_count': telescope_model['pol_count'],
                                              'channel_count': telescope_model['channel_count'],
                                              'alpha': noisegen['alpha'],
                                              'beta': noisegen['beta']}))

    if modules['rfi_module'] is True :
        assert (manager.add_module('RFIGen', {'antenna_count': telescope_model['antenna_count'],
                                              'pol_count': telescope_model['pol_count'],
                                              'channel_count': telescope_model['channel_count'],
                                              'rfi_chans' : rfigen['rfi_chans'],
                                              'rfi_variability' : rfigen['rfi_variability']},
                                   archive_key= '/rfi'))

    if modules['failure_module'] is True :
        assert(manager.add_module('FailGen',{'antenna_count': telescope_model['antenna_count'],
                                             'pol_count' : telescope_model['pol_count'],
                                             'channel_count' : telescope_model['channel_count'],
                                             'fail_probability' : failgen['fail_probability'],
                                             'extremes_maximum' : failgen['extremes_maximum'],
                                             'fail_min_sample' : failgen['fail_min_sample'],
                                             'fail_max_sample' : failgen['fail_max_sample']}))

    assert(manager.add_module('Exponentiator', {'power' : '10'}))

    assert(manager.add_module('CScaler',{'input' : (0,2**16),
                                         'type' : np.uint16,
                                         'output' : (0,2**16)}))

    assert(manager.add_module('ChannelStageWriter',{'path': write_loc,
                                                    'type': 'uint16',
                                                    'antennas':telescope_model['antenna_count'],
                                                    'polaris':telescope_model['pol_count'],
                                                    'channels':telescope_model['channel_count'],
                                                    'time':tm.time()}))

    assert(manager.build())

    # Start and run pipeline
    assert(manager.start())
    assert(manager.wait_until_ready())