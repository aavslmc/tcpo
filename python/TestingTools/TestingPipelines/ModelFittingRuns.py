# This Script runs multiple iterations of the band-pass pipeline...
import logging
import os
import warnings

import numpy as np

from pytcpo.Core.PipeLine import PipelineManager
from pytcpo.Core.Common import string, TCPOCode
from pytcpo.Modules.Archivers import FilePersistor

import pytcpo.Modules.IO
import pytcpo.Modules.Archivers
import pytcpo.Modules.RawFilters
import pytcpo.Modules.BandPassModel
import pytcpo.Modules.AnomalyDetectors

# Calculations
Freq_Offs = 0
Freq_Scale = 0.78125

# Telescope Definitions
Samples = (0,2047)                    # Sample Range to read... (min,max)
Frequencies = (0, 400)                 # Frequency Range to consider in fit: must lie between 0 and 400
Antennas = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)  #  Which are the valid Antennas
Polarisations = (0,1)                  # Number of Polarisations
Tiles = 1                               # Number of Tiles
DownSample = 1                          # Down-Sample Factor (skipping values)

# Fitting Parameters
models = ['DiffPolyFit']
outliers = [] # 'DerivativeSignsWithDifferenceRejection','DerivativeSignsDetector','DerivativeSignsDetectorNew',,'DerivativeSignsDetector'
anomalies = [] #'InterAntennaDeviationDetector','InterAntennaOverlapDetector','IndependentOverlapDetector', 'OutlierThresholdDetector','OutlierWeightedOverlapDetector','PerAntennaDeviationDetector'
aggregator = 'OutlierAggregator'
orders = [7,8,9,10,11]        # Polynomial orders to test
rejection_std = 5.0
max_rate = 1.0
outlier_std = 1.0
outlier_cut = 0.1             # Absolute Value - if less than this, does not flag as outlier
min_cutoff = 5
avg_length = 5
padd_excessive = 4.0
iadd_excessive = 6.0
overlap_form = False # Consider also the mean (offset)
overlap_cutoff = 0.95
env_cut = 1
similarity_thresh = 0.75 # At least must be similar to 0.75 of other antennas
outlier_thresh = 0.1     # 10% outliers is the cutoff for detecting anomalies based on number of outliers in bandpass
max_outlier = 0.1        # Maximum number of outliers for moving average to consider sample
log_scale = True              # Operate on log-scale or not
agg_length = 5

perform_detrending = False    # Flag to allow /disable de-trending

model_types = [string(model) for model in models]
outly_types = [string(outlier) for outlier in outliers]
anomaly_types = [string(anomaly) for anomaly in anomalies]

# Change this to suit your needs... i.e. were the data is stored
# src_location = '/media/michael/Elements/MichaelC/Data/SKA-Run (March 2017)/raw'
# dst_root_loc = '/media/michael/Elements/MichaelC/Data/Scratch/Test'
# src_location = '/home/michael/Downloads'
# dst_root_loc = '/home/michael/Downloads/Scratch/Test'
src_location = '/home/xandru/Documents/Data/NewData'
dst_root_loc = '/home/xandru/Documents/Scratch/Test'

start_model = np.zeros(20)   # Up to Order 20
weightings = np.array((0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5e6, 1e6, 1000, 20.0, 2.0, 2.0))

if __name__=='__main__':

    # Supress warnings
    #warnings.simplefilter('ignore')

    # Precompute some Stuff...
    Channels = np.arange(int(np.rint((Frequencies[0] - Freq_Offs) / Freq_Scale)),int(np.rint((Frequencies[1] - Freq_Offs) / Freq_Scale)))
    Channels_Fit = np.concatenate((np.arange(64, 307), np.arange(347, 351), np.arange(366, 434)))

    # Create Manager - The main instance
    manager = PipelineManager()

    # First set the archiver
    sources = {}
    for model in model_types:
        for pol_order in orders:
            model_pth = model.acronym() + ('_log/' if log_scale else '_lin/') + 'ORD_' + str(pol_order) + '/'
            # Create Folder if inexistent
            try: os.makedirs(dst_root_loc + '/' + model_pth)
            except OSError:
                if not os.path.isdir(dst_root_loc + '/' + model_pth): raise
            # Add Key(s)
            # First The Polynomial Fit
            sources[model_pth + 'polyfit'] = None   # No need for type at writing stage
            # Now the Outliers
            for outlier in outly_types: sources[model_pth + 'out_' + outlier.acronym()] = None
            # And the aggregator
            sources[model_pth + 'out_agg'] = None
            # Anomaly data
            for anomaly in anomaly_types: sources[model_pth + 'anom_' + anomaly.acronym()] = None
            # Finally if detrending
            if perform_detrending: sources[model_pth + 'output'] = None

    manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'source': dst_root_loc, 'keys': sources}, 'append' : False},
                         serialiser={'type': 'CSVSerialiser'})

    # Now build the pipeline...
    # Reader
    manager.add_root('ChannelStageReader', {'path':src_location, 'time':None, 'tiles':0, 'sample_rate':0,
                                            'block_rate':2048, 'samples':Samples, 'stop_on_end':True})
    # Down-Sampler
    manager.add_module('AliasingDownSampler', {'rate':DownSample, 'start':Samples[0]})
    # Antenna Selector
    manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas':np.asarray(Antennas)})
    # Polarisation Selector
    manager.add_module('PolarisationSelector', {'in_polarisations': np.arange(2), 'out_polarisations': np.asarray(Polarisations)})
    # ChannelSelector
    manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels':Channels})
    # CScaler
    manager.add_module('CScaler', {'input':(0,65535), 'output':(0,65535), 'type':float})
    # Power Log (Optional)
    if log_scale: manager.add_module('PowerLog', {'multiplier':10, 'log':'10'})
    # Keep Track of the fanout-tile
    fanout = manager.Last

    # Now the Replicated portion of the pipeline:
    for m_i, model in enumerate(model_types):
        for idx, pol_order in enumerate(orders):
            model_pth = model.acronym() + ('_log/' if log_scale else '_lin/') + 'ORD_' + str(pol_order) + '/'
            weights = weightings[len(weightings) - pol_order - 1:]
            model_i = start_model[len(start_model) - pol_order - 1:]

            manager.add_module('ChannelSelector', {'in_channels': Channels, 'out_channels': Channels_Fit}, (fanout,))
            # The BandPass Fitter
            manager.add_module(model,
                               {'antennas': len(Antennas), 'polaris': len(Polarisations), 'channels': Channels_Fit,
                                'order': pol_order, 'max_rate': max_rate, 'reject_std': rejection_std},archive_key=model_pth + 'polyfit')
            fitter = manager.add_module('PolynEvaluator', {'channels': Channels})

            # The Outlier Detector(s)
            outly_model = None
            all_outliers = []
            for o_i, outlier in enumerate(outly_types):
                if outlier not in ['DerivativeSignsWithPeakRejection','DerivativeSignsWithPeakRejectionNew']:
                    manager.add_module(outlier, {'antennas':Antennas, 'polarisations':Polarisations,'channels':Channels,
                                                 'model':fitter, 'outlier_cut':outlier_cut, 'envelope' : env_cut,
                                                 'outlier_std':outlier_std, 'min_cutoff':min_cutoff},
                                       (fanout, fitter), archive_key=model_pth + 'out_' + outlier.acronym())
                    if outlier == 'PolynomialDeviations': outly_model = manager.Last
                else:
                    manager.add_module(outlier, {'min_cutoff': min_cutoff,'antennas':Antennas, 'polarisations':Polarisations,
                                                 'channels':Channels, 'model':fitter,'poly_outly':outly_model},
                                       (fanout, fitter, outly_model), archive_key=model_pth + 'out_' + outlier.acronym())
                all_outliers.append(manager.Last)
            # The Aggregator
            aggregat_outlier = manager.add_module(aggregator, {'antennas':Antennas, 'polarisations':Polarisations,'channels':Channels,
                                                               'length': agg_length, 'others': all_outliers},
                                                  all_outliers, archive_key=model_pth + 'out_agg')
            # The Anomaly Detector(s)
            for a_i, anomaly in enumerate(anomaly_types):
                if anomaly == 'OutlierWeightedOverlapDetector':
                    parent = (fitter, aggregat_outlier)
                elif anomaly == 'OutlierThresholdDetector':
                    parent = aggregat_outlier
                else:
                    parent = fitter
                manager.add_module(anomaly,
                                   {'antennas': Antennas, 'polarisations': Polarisations, 'ignore': True,
                                    'start': model_i, 'length': avg_length, 'weight': weights, 'order': pol_order,
                                    'form': overlap_form, 'deviation': iadd_excessive, 'excessive': padd_excessive,
                                    'min_overlap': overlap_cutoff, 'channels': Channels, 'threshold': outlier_thresh,
                                    'similarity':similarity_thresh, 'outlier_gen':aggregat_outlier,
                                    'max_outlier':max_outlier},
                                   parent, archive_key=model_pth + 'anom_' + anomaly.acronym())

            # Now the De-Trender (and associated, if de-trending)
            if perform_detrending:
                manager.add_module('PolyDeTrender', {'model':fitter}, (fanout, fitter))
                # Optional Log Scale convertor
                if log_scale: manager.add_module('Exponentiator', {'divider':10, 'power':'10'})
                # finally the de-scaler
                manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': np.uint16},
                                   archive_key=model_pth + 'output')

    # Now Build
    manager.build()

    # Now start the reader, doing all
    logging.getLogger().setLevel(logging.INFO)
    assert (manager.start() == TCPOCode.Status.ACTIVE)

    # Wait for completion
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)