import pymap3d as pm
import json
import h5py
import os
import numpy as np
from matplotlib import pyplot as plt
from astropy.coordinates import EarthLocation, Angle
import astropy.units as u
from math import sin, cos, radians

telescope = 'best2'

if telescope == 'best2':

    antennas_in = {'0': [0, 0, [0.0, 0.0, 0.0]],
                     '1': [0, 1, [5.6665, 0.0, 0.0]],
                     '2': [0, 2, [11.333, 0.0, 0.0]],
                     '3': [0, 3, [16.999, 0.0, 0.0]],
                     '4': [0, 4, [0.0, 10.0, 0.0]],
                     '5': [0, 5, [5.6665, 10.0, 0.0]],
                     '6': [0, 6, [11.333, 10.0, 0.0]],
                     '7': [0, 7, [16.999, 10.0, 0.0]],
                     '8': [0, 8, [0.0, 20.0, 0.0]],
                     '9': [0, 9, [5.6665, 20.0, 0.0]],
                     '10': [0, 10, [11.333, 20.0, 0.0]],
                     '11': [0, 11, [16.999, 20.0, 0.0]],
                     '12': [0, 12, [0.0, 30.0, 0.0]],
                     '13': [0, 13, [5.6665, 30.0, 0.0]],
                     '14': [0, 14, [11.333, 30.0, 0.0]],
                     '15': [0, 15, [16.999, 30.0, 0.0]],
                     '16': [0, 16, [0.0, 40.0, 0.0]],
                     '17': [0, 17, [5.6665, 40.0, 0.0]],
                     '18': [0, 18, [11.333, 40.0, 0.0]],
                     '19': [0, 19, [16.999, 40.0, 0.0]],
                     '20': [0, 20, [0.0, 50.0, 0.0]],
                     '21': [0, 21, [5.6665, 50.0, 0.0]],
                     '22': [0, 22, [11.333, 50.0, 0.0]],
                     '23': [0, 23, [16.999, 50.0, 0.0]],
                     '24': [0, 24, [0.0, 60.0, 0.0]],
                     '25': [0, 25, [5.6665, 60.0, 0.0]],
                     '26': [0, 26, [11.333, 60.0, 0.0]],
                     '27': [0, 27, [16.999, 60.0, 0.0]],
                     '28': [0, 28, [0.0, 70.0, 0.0]],
                     '29': [0, 29, [5.6665, 70.0, 0.0]],
                     '30': [0, 30, [11.333, 70.0, 0.0]],
                     '31': [0, 31, [16.999, 70.0, 0.0]]}

    tmp_dir = '/home/josef/Documents/tcpo/'

    # plt.figure()
    # for i in range(32):
    #     plt.scatter(antennas_in[str(i)][2][1], antennas_in[str(i)][2][0], c='black', marker='.')
    # plt.xticks(xrange(0, 70, 25), size=6)
    # plt.yticks(xrange(0, 17, 5), size=6)
    # plt.xlabel('N-S direction (m)', size=7)
    # plt.ylabel('E-W direction (m)', size=7)
    # plt.axes().set_aspect('equal')
    # plt.savefig("/home/josef/Downloads/Thesis_Final/Pics/BEST2.png", dpi=600)
    # exit()

    # Initialize test by creating json file
    # Test json shall contain 8 antennas defined by x,y,z with a reference long-lat-alt position
    test = {"relative_positions": True,
            "antennas": antennas_in,
            "latitudes": None,
            "longitudes": None,
            "altitudes": None,
            "starting_channel": 410.109375e6,
            "end_channel": 410.109375e6,
            "incrementing_channel": 0,
            "num_channels": 1,
            "bandwith": 0.078125e6,
            "absolute_positions": False,
            "longitude_centre": 11.6459889,
            "latitude_centre": 44.523880962,
            "altitude_centre": 0.0,
            "tiles": {'0': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},
            "beams": {'0': [2, 5, 8],
                      '1': [2, 3, 4, 5],
                      '2': [1, 2, 3, 6, 7, 8, 10],
                      '3': [1, 4, 8, 10, 13, 14]},
            "bp_coord": "Horizon",
            "bp_fov": 180,
            "bp_size": 512,
            "img_fov": 180,
            "img_size": 1024,
            "img_type": "I",
            "t_average": 1,
            "tstep_length": 1,
            "obs_length": 64,
            "start_time": "15-02-2018 12:00:00.000",
            "h5_req": False,
            "testing_req": True,
            "PCRA": 250.,
            "PCDec": 58.917,
            "Pol_Mode": "Scalar",
            }

if telescope == 'aavs':
    def obtain_antenna_dict():

        # Antenna mapping
        antenna_preadu_mapping = {0: 1, 1: 2, 2: 3, 3: 4,
                                  8: 5, 9: 6, 10: 7, 11: 8,
                                  15: 9, 14: 10, 13: 11, 12: 12,
                                  7: 13, 6: 14, 5: 15, 4: 16}

        aavs_station_latitude = -26.70408
        aavs_station_longitude = 116.670231

        #aavs_station_latitude = 20
        #aavs_station_longitude = -30

        location = EarthLocation.from_geodetic(Angle(aavs_station_longitude, u.deg),
                                               Angle(aavs_station_latitude, u.deg),
                                               ellipsoid="WGS84")
        station_x, station_y, station_z = location.x.value, location.y.value, location.z.value

        def read_from_google():
            counter = 0
            import urllib2
            response = urllib2.urlopen(
                'https://docs.google.com/spreadsheets/d/e/2PACX-1vQTo60lZmrvBfT0gpa4BwyaB_QkPplqfHga7RCsLDR9J_lv15BQTqb3loBHFhMp6U0X_FIwyByfFCwG/pub?gid=220529610&single=true&output=tsv')
            html = response.read().split('\n')

            lat = np.deg2rad(aavs_station_latitude)
            lon = np.deg2rad(aavs_station_longitude)

            nof_antennas = 256

            mapping = []
            for i in range(16):
                mapping.append([[]] * 16)

            map_out = np.ones((256, 3), dtype=np.float)

            missing = 0
            for i in range(1, nof_antennas + 1):
                items = html[i].split('\t')

                # Convert ENU to ECEF coordinates
                try:
                    tpm, rx = int(items[7]) - 1, int(items[8]) - 1
                    enu_x, enu_y, enu_z = float(items[15].replace(',', '.')), float(items[17].replace(',', '.')), 0.
                except:
                    if missing == 0:
                        tpm, rx = 0, 8
                        enu_x, enu_y, enu_z = 17.525, -1.123, 0.
                    else:
                        tpm, rx = 10, 8
                        enu_x, enu_y, enu_z = 9.701, -14.627, 0.
                    missing += 1
                x = -sin(lon) * enu_x - cos(lon) * sin(lat) * enu_y + cos(lon) * cos(lat) * enu_z + station_x
                y = cos(lon) * enu_x - sin(lon) * sin(lat) * enu_y + cos(lat) * sin(lon) * enu_z + station_y
                z = cos(lat) * enu_y + sin(lat) * enu_z + station_z

                mapping[tpm][rx] = (x, y, z)
                map_out[counter, :] = (enu_x, enu_y, enu_z)
                counter += 1

            rot_matrix = np.matrix([[0, -sin(lat), cos(lat)],
                                    [1, 0, 0],
                                    [0, cos(lat), sin(lat)]])

            antenna_mapping = np.array(np.dot(rot_matrix, map_out.T))

            antenna_mapping = np.reshape(np.array(antenna_mapping), (antenna_mapping.shape[0], 1, antenna_mapping.shape[1]))

            return antenna_mapping.T

        def antenna_base_coordinates():
            import urllib2
            nof_antennas = 256
            antenna_mapping = []

            response = urllib2.urlopen('https://docs.google.com/spreadsheets/d/e/2PACX-1vQTo60lZmrvBfT0gpa4BwyaB_'
                                       'QkPplqfHga7RCsLDR9J_lv15BQTqb3loBHFhMp6U0X_FIwyByfFCwG/pub?gid=2094033346'
                                       '&single=true&output=tsv')

            html = response.read().split('\n')

            lat = radians(aavs_station_latitude)

            #rot_matrix = np.matrix([[0, -sin(lat), cos(lat)],
            #                        [1, 0, 0],
            #                        [0, cos(lat), sin(lat)]])

            for i in range(nof_antennas):
                items = html[i].split('\t')
                enu_x, enu_y, enu_z = float(items[1].replace(',', '.')), float(items[2].replace(',', '.')), 0
                #antenna_mapping.append(np.dot(rot_matrix, np.matrix([enu_x, enu_y, enu_z]).T).T)
                antenna_mapping.append(np.matrix([enu_x, enu_y, enu_z]))

            mapping = [50, 83, 33, 32, 22, 8, 21, 64, 103, 99, 119, 101, 70, 72, 104, 84, 7, 23, 29, 6, 25, 24, 30,
                       51, 53, 26, 52, 31, 63, 61, 62, 27, 59, 58, 56, 57, 85, 55, 86, 28, 98, 88, 87, 89, 60, 91, 54,
                       92, 90, 94, 125, 123,126, 120, 124, 121, 95, 129, 93, 97, 96, 128, 122, 156, 131, 159, 161, 162,
                       160, 157, 127, 163, 135, 136, 158, 132, 134, 130, 133, 165, 195, 167, 190, 194, 191, 153, 166,
                       188, 155, 154, 193, 224, 192, 164, 189, 226, 225, 197, 227, 168, 251, 198, 221, 223, 250, 200,
                       222, 199, 187, 196, 249, 245, 252, 246, 201, 220, 186, 248, 230, 247, 169, 219, 253, 202, 229,
                       228, 218, 10, 11, 2, 37, 9, 19, 5, 18, 36, 66, 47, 16, 1, 4, 35, 34, 79, 68, 43, 78, 44, 77, 15,
                       102, 38, 12, 13, 48, 17, 65, 20, 49, 39, 80, 46, 14, 69, 45, 81, 67, 100, 76, 74, 71, 42, 40, 82,
                       115, 112, 110, 105, 113, 117, 107, 106, 108, 114, 111, 109, 75, 116, 73, 118, 143, 140, 173, 142,
                       176, 138, 174, 175, 177, 145, 144, 147, 146, 139, 178, 141, 213, 212, 207, 149, 235, 208, 180,
                       210, 236, 182, 206, 148, 181, 211, 209, 179, 237, 232, 256, 171, 183, 204, 238, 137, 133, 150,
                       205, 214, 239, 215, 172, 234, 231, 242, 151, 244, 243, 203, 216, 152, 170, 185, 184, 217, 241,
                       254, 240, 255, 3, 41]

            mapping = np.array(mapping)
            mapping -= 1

            antenna_mapping = np.array(antenna_mapping)

            new_positions = antenna_mapping[mapping, :, :]

            return np.array(new_positions)

        def fesoj_antenna_coordinates():
            import urllib2
            antennas_per_tile = 16
            nof_antennas = 256
            antenna_mapping = []

            response = urllib2.urlopen('https://docs.google.com/spreadsheets/d/e/2PACX-1vQTo60lZmrvBfT0gpa4BwyaB_'
                                       'QkPplqfHga7RCsLDR9J_lv15BQTqb3loBHFhMp6U0X_FIwyByfFCwG/pub?gid=2094033346'
                                       '&single=true&output=tsv')

            html = response.read().split('\n')

            for i in range(nof_antennas):
                items = html[i].split('\t')
                enu_x, enu_y, enu_z = float(items[1].replace(',', '.')), float(items[2].replace(',', '.')), 0
                antenna_mapping.append([enu_x, enu_y, enu_z])

            mapping = np.array(
                [50, 83, 33, 32, 22, 8, 21, 64, 3, 103, 99, 119, 101, 70, 72, 104, 84, 7, 23, 29, 6, 25, 24, 30,
                 51, 53, 26, 52, 31, 63, 61, 62, 27, 59, 58, 56, 57, 85, 55, 86, 28, 98, 88, 87, 89, 60, 91, 54,
                 92, 90, 94, 125, 123, 126, 120, 124, 121, 95, 129, 93, 97, 96, 128, 122, 156, 131, 159, 161, 162,
                 160, 157, 127, 163, 135, 136, 158, 132, 134, 130, 133, 165, 195, 167, 190, 194, 191, 153, 166,
                 188, 155, 154, 193, 224, 192, 164, 189, 226, 225, 197, 227, 168, 251, 198, 221, 223, 250, 200,
                 222, 199, 187, 196, 249, 245, 252, 246, 201, 220, 186, 248, 230, 247, 169, 219, 253, 202, 229,
                 228, 218, 10, 11, 2, 37, 9, 19, 5, 18, 36, 66, 47, 16, 1, 4, 35, 34, 79, 68, 43, 78, 44, 77, 15,
                 102, 38, 12, 13, 48, 17, 65, 20, 49, 39, 80, 46, 14, 69, 45, 81, 67, 41, 100, 76, 74, 71, 42, 40, 82,
                 115, 112, 110, 105, 113, 117, 107, 106, 108, 114, 111, 109, 75, 116, 73, 118, 143, 140, 173, 142,
                 176, 138, 174, 175, 177, 145, 144, 147, 146, 139, 178, 141, 213, 212, 207, 149, 235, 208, 180,
                 210, 236, 182, 206, 148, 181, 211, 209, 179, 237, 232, 256, 171, 183, 204, 238, 137, 133, 150,
                 205, 214, 239, 215, 172, 234, 231, 242, 151, 244, 243, 203, 216, 152, 170, 185, 184, 217, 241,
                 254, 240, 255]) - 1

            new_antenna_mapping = []
            for i in range(nof_antennas):
                tile_number = i / antennas_per_tile
                rx_number = antenna_preadu_mapping[i % antennas_per_tile] - 1
                new_antenna_mapping.append(antenna_mapping[mapping[tile_number * 16 + rx_number]])

            return np.array(new_antenna_mapping)

        def antenna_map_new():
            # Antenna mapping placeholder
            antenna_mapping = []
            for i in range(16):
                antenna_mapping.append([[]] * 16)

            # Read antenna location spreadsheet
            import urllib2
            response = urllib2.urlopen('https://docs.google.com/spreadsheets/d/e/2PACX-1vQTo60lZmrvBfT0gpa4BwyaB_QkPplqfHga'
                                      '7RCsLDR9J_lv15BQTqb3loBHFhMp6U0X_FIwyByfFCwG/pub?gid=220529610&single=true&output=tsv')
            html = response.read().split('\n')

            ellipsoid = pm.EarthEllipsoid('wgs84')

            missing = 0
            nof_antennas = 256
            for i in range(1, nof_antennas + 1):
                items = html[i].split('\t')

                # Convert ENU to ECEF coordinates
                try:
                    tpm, rx = int(items[7]) - 1, int(items[8]) - 1
                    east, north, up = float(items[15].replace(',', '.')), float(items[17].replace(',', '.')), 0
                except:
                    if missing == 0:
                        tpm, rx = 0, 8
                        east, north, up = 17.525, -1.123, 0
                    else:
                        tpm, rx = 10, 8
                        east, north, up = 9.701, -14.627, 0
                    missing += 1
                x, y, z = east, north, up
                # x, y, z = pm.enu2ecef(east, north, up, aavs_station_latitude, aavs_station_longitude, 0, ell=ellipsoid)
                # antenna_mapping[tpm][rx] = pm.ecef2enu(x, y, z, aavs_station_latitude, aavs_station_longitude, 0,
                #                                       ell=ellipsoid)
                antenna_mapping[tpm][rx] = x, y, z

            # Create lookup table
            antenna_positions = []
            for i in range(nof_antennas):
                tile_number = i / 16
                rx_number = antenna_preadu_mapping[i % 16] - 1
                antenna_positions.append(antenna_mapping[tile_number][rx_number])

            return np.array(antenna_positions)

        antenna_mapping = fesoj_antenna_coordinates()
        #plt.scatter(antenna_mapping[:, 0], antenna_mapping[:, 1])
        # Create lookup table
        # antenna_positions = []
        # for i in range(256):
        #     tile_number = i / 16
        #     rx_number = antenna_preadu_mapping[i % 16] - 1
        #     antenna_positions.append(antenna_mapping[tile_number][rx_number])

        antenna_positions = antenna_mapping[:, :]
        antenna_dict = {}
        counter = 0

        for i in range(16):
            for j in range(16):
                antenna_dict[str(counter)] = [i, j, [antenna_positions[counter][0],
                                                     antenna_positions[counter][1],
                                                     antenna_positions[counter][2]]]

                counter += 1

        a = np.array(antenna_positions)
        #plt.scatter(a[:, 0], a[:, 1])
        #dec = np.deg2rad(aavs_station_latitude)
        #dec2 = -26.7 + 0
        #rot = [[0, -sin(dec), cos(dec)], [1, 0, 0], [0, cos(dec), sin(dec)]]
        # rot_matrix = [[1, 0, 0], [0, cos(dec), sin(dec)], [0, -sin(dec), cos(dec)]]
        # rot_matrix2 = [[cos(dec), 0, -sin(dec)], [0, 1, 0], [sin(dec), 0, cos(dec)]]
        # rot_matrix3 = [[cos(dec2), sin(dec2), 0], [-sin(dec2), cos(dec2), 0], [0, 0, 1]]
        #xyz = np.dot(rot, a.T)
        #xyz = xyz.T
        xyz = a

        # print(xyz)

        #plt.scatter(xyz[:, 0], xyz[:, 1], marker='.', linewidths=0.001)
        #plt.xlabel('u', fontsize=20)
        #plt.ylabel('v', fontsize=20)
        # plt.show()

        # xyz = np.dot(a, np.linalg.inv(rot_matrix3))
        # xyz = np.dot(xyz, np.linalg.inv(rot_matrix))

        u_values = []
        v_values = []
        w_values = []
        v = 2.998e8
        f = 159.0e6
        lmbd = v / f

        uvw_values = np.ones((xyz.shape[0], xyz.shape[0], 3), dtype=np.float)

        for i in range(xyz.shape[0]):
            for j in range(xyz.shape[0]):
                if i <= j:
                    uvw_values[i, j, 0] = (xyz[j][0] - xyz[i][0])
                    uvw_values[i, j, 1] = (xyz[j][1] - xyz[i][1])
                    uvw_values[i, j, 2] = (xyz[j][2] - xyz[i][2])

        # Plot baseline_distance vs frequency
        # baseline_distance = np.ones(256, dtype=np.float)
        # for i in range(256):
        #     baseline_distance[i] = np.sqrt((uvw_values[0, i, 0]**2) + (uvw_values[0, i, 1]**2))

        baseline_distance = np.ones(136, dtype=np.float)
        counter = 0
        for k in range(16):
            for j in range(k, 16):
                baseline_distance[counter] = np.sqrt((uvw_values[k, j, 0]**2) + (uvw_values[k, j, 1]**2))
                counter += 1

        print np.argsort(baseline_distance)
        print(np.sort(baseline_distance))
        #exit()

        h5 = "/home/josef/Documents/tcpo/RealVis2/correlation_204_20180405_12105_0.hdf5"

        f = h5py.File(h5, "r")
        uncal_data = np.array(f['correlation_matrix']['data'])
        uncal_time = int(np.array(f['sample_timestamps']['data'].shape[0]))
        baselines = int(uncal_data.shape[0]) / uncal_time

        data_out = np.ones((255, uncal_time), dtype=np.float)
        for i in range(uncal_time):
            for j in range(1, 256):
                counter = (i * baselines) + j
                data_out[j - 1, i] = uncal_data[counter, 0]

        # # Smoothing
        # iterator = np.array([100, 102, 111, 112, 117, 118, 122, 125, 181, 192, 202, 212, 233, 234, 241, 251, 252])
        # baseline_distances = baseline_distance[iterator]
        # # baseline_distances = np.sort(baseline_distances)
        # test_data_out = np.ones((iterator.shape[0], data_out.shape[1]), dtype=np.float)
        # counter = 0
        # for iter in iterator:
        #     test_data = data_out[iter, :]
        #     smooth = 300
        #     for i in range(test_data.shape[0]):
        #         if i <= smooth/2:
        #             test_data_out[counter, i] = np.sum(test_data[i:(i+smooth)])/smooth
        #         if (i > smooth/2) and (i < (test_data.shape[0] - smooth/2)):
        #             test_data_out[counter, i] = np.sum(test_data[(i-(smooth/2)):(i+(smooth/2))])/smooth
        #         if (i >= (test_data.shape[0] - (smooth/2))):
        #             test_data_out[counter, i] = np.sum(test_data[(i-smooth):i])/smooth
        #     counter += 1
        #
        # # Determine frequency
        # freq_out = np.ones(baseline_distances.shape[0], dtype=np.float)
        # for i in range(test_data_out.shape[0]):
        #     point_maximum = []
        #     for j in range(test_data_out.shape[1]-1):
        #         if j > 0 and j < test_data_out.shape[1]:
        #             previous_point = test_data_out[i, j] - test_data_out[i, j-1]
        #             next_point = test_data_out[i, j] - test_data_out[i, j+1]
        #             if previous_point > 0 and next_point > 0:
        #                 point_maximum.append(j)
        #     point_maximum = np.array(point_maximum)
        #     freq = []
        #     for k in range(point_maximum.shape[0]-1):
        #         freq.append(point_maximum[k+1] - point_maximum[k])
        #     freq = np.array(freq)
        #     print(i, freq[np.where(freq > (np.mean(freq)))[0]], np.median(freq[np.where(freq > (np.mean(freq)))[0]]))
        #     freq_out[i] = np.median(freq[np.where(freq > (np.mean(freq)))[0]])
        #
        # sorter = np.argsort(baseline_distances)
        # final_out = np.ones((baseline_distances.shape[0], 2), dtype=np.float)
        # for i in range(baseline_distances.shape[0]):
        #     final_out[i, 0] = baseline_distances[sorter[i]]
        #     final_out[i, 1] = freq_out[sorter[i]]
        #
        # plt.figure()
        # plt.scatter(final_out[:, 0], final_out[:, 1])
        # plt.xlabel('Baseline distance (m)')
        # plt.ylabel('Frequency (no. of samples)')
        # plt.savefig("/home/josef/Desktop/FreqvsBaseline.png", dpi=300)
        #exit()

        #plt.plot(test_data_out)
        # plt.plot(test_data)
        #plt.show()
        #exit()

        # uvw_s = np.zeros((len(u_values), 3))
        # uvw_s[:, 0] = u_values[:]
        # uvw_s[:, 1] = v_values[:]
        # uvw_s[:, 2] = w_values[:]

        for i in range(1):
            # ra = 135.+((360./(3600.*24.))*i)
            ha = np.deg2rad(0.)
            dec = np.deg2rad(aavs_station_latitude)

            ra_matrix = np.array([[np.sin(ha), np.cos(ha), 0.], [(-np.sin(dec) * np.cos(ha)),
                                                                 (np.sin(dec) * np.sin(ha)), np.cos(dec)],
                                  [(np.cos(dec) * np.cos(ha)), (-np.cos(dec) * np.sin(ha)), np.sin(dec)]])

            counter = 0
            out = np.ones((3, xyz.shape[0], xyz.shape[0]), dtype=np.float)
            for j in range(len(uvw_values[:, 0])):
                for k in range(len(uvw_values[:, 0])):
                    out[:, j, k] = np.dot((ra_matrix * (1./lmbd)), uvw_values[j, k, :])
                    # out[:, j, k] = uvw_values[j, k, :]
                    counter += 1

        #fig = plt.figure(figsize=(16, 16))
        #plt.scatter(out[0, :, :], out[1, :, :], marker='.', linewidths=0.001)
        #plt.show()
        #plt.xlabel('u', fontsize=20)
        #plt.ylabel('v', fontsize=20)
        #fig.suptitle('UV_upper_plot', fontsize=20)
        # plt.show()
        #plt.savefig('/home/josef/Desktop/UV_plots_data/josef_upper_imag2.png', dpi=600)
        return antenna_dict


    antennas_in = obtain_antenna_dict()

    tmp_dir = '/home/josef/Documents/tcpo/'

    # Initialize test by creating json file
    # Test json shall contain 8 antennas defined by x,y,z with a reference long-lat-alt position
    test = {"relative_positions": True,
            "antennas": antennas_in,
            "latitudes": None,
            "longitudes": None,
            "altitudes": None,
            "starting_channel": 159e6,
            "end_channel": 400e6,
            "incrementing_channel": 400/512,
            "num_channels": 1,
            "bandwith": 1e6,
            "absolute_positions": False,
            "longitude_centre": 116.670231,
            "latitude_centre": -26.70408,
            "altitude_centre": 0.0,
            "tiles": {'0': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},
            "beams": {'0': [2, 5, 8],
                      '1': [2, 3, 4, 5],
                      '2': [1, 2, 3, 6, 7, 8, 10],
                      '3': [1, 4, 8, 10, 13, 14]},
            "bp_coord": "Horizon",
            "bp_fov": 180,
            "bp_size": 512,
            "img_fov": 180,
            "img_size": 4096,
            "img_type": "I",
            "t_average": 1,
            "tstep_length": 1,
            "obs_length": 1,
            "start_time": "05-04-2018 04:00:00.000",
            "h5_req": False,
            "testing_req": True,
            "PCRA": 120,
            "PCDec": -26.70408,
            "Pol_Mode": "Full",
            "skymodel_dir": "/home/josef/Documents/tcpo/python/SkyModel/ParkesFull2.osm"
            }

# Dump json
with open(tmp_dir + 'tm_data.json', 'w') as fp:
    json.dump(test, fp)
