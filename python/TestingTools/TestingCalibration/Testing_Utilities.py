import numpy as np
import h5py


class NoisyVisGen:
    """
    A noisy visibilities generator, for testing purposes only, when real data is not available. The noisy visibilities
    are then written to an HDF5 file.

    """

    def __init__(self):
        """
        Default Initializer

        """
        self.idx = None
        self.noisy_vis = None

    def generator(self, model_h5, multiplier, channel_multiple):
        """
        Noisy visibilities generator
        :param model_h5: File path for hdf5 file containing model visibilities for the observation
        :param multiplier: Noise multiplying factor, in order to easily control level of noise
        :param channel_multiple: Channel grouping multiple, set to 1 if all channels are separately calibrated

        """

        f = h5py.File(model_h5, "r")
        model_vis = np.array(f["Vis"])
        self.idx = model_vis.shape
        self.noisy_vis = np.zeros((self.idx[0] * channel_multiple, self.idx[1], self.idx[2], self.idx[3]),
                                  dtype=np.complex)
        gain_noise = np.random.rand(self.idx[0] * channel_multiple, self.idx[1], self.idx[2], self.idx[3]) * multiplier
        phase_noise = np.random.rand(self.idx[0] * channel_multiple, self.idx[1], self.idx[2], self.idx[3]) * multiplier
        noise_addition = np.zeros((self.idx[0] * channel_multiple, self.idx[1], self.idx[2], self.idx[3]),
                                  dtype=np.complex)
        for i in range(self.idx[0] * channel_multiple):
            for j in range(self.idx[1]):
                for k in range(self.idx[2]):
                    for l in range(self.idx[3]):
                        noise_addition[i, j, k, l] = np.complex(gain_noise[i, j, k, l], phase_noise[i, j, k, l])
                        self.noisy_vis[i, j, k, l] = model_vis[int(i / channel_multiple), j, k, l] + \
                            noise_addition[i, j, k, l]

    def h5_writer(self, h5_out, channel_multiple):
        """
        Write noisy visibilities to h5
        :param h5_out: File path for writing hdf5 file containing noisy visibilities
        :param channel_multiple: Channel grouping multiple, set to 1 if all channels are separately calibrated

        """
        f = h5py.File(h5_out, "w")
        d_set = f.create_dataset("Vis", (self.idx[0] * channel_multiple, self.idx[1], self.idx[2], self.idx[3]),
                                 dtype='c16')
        d_set[:, :, :, :] = self.noisy_vis[:, :, :, :]
        f.flush()
        f.close()


class TestApplyCoeffs:
    """
    Used to apply a set of coefficients to a set of corresponding visibilities, after which the calibrated visibilities
    are written to an HDF5 file.

    """

    def __init__(self):
        """
        Default Initializer

        """

        self.coeffs = None

    def coeff_apply(self, coeffs_file, vis_in, vis_out):
        """
        Apply Coefficients to actual/noisy visibilities, obtain calibrated visibilities

        :param coeffs_file: File path of a text file containing the required coefficients in ordered format, from
        antenna 0 to antenna n.
        :param vis_in: File path for the location of the uncalibrated visibilities HDF5 file.
        :param vis_out: File path where the calibrated visibilities shall be saved.

        """

        self.coeffs = np.loadtxt(coeffs_file, dtype=np.complex)

        coeffs_real = []
        coeffs_imag = []
        for i in range(len(self.coeffs)):
            coeffs_real.append(self.coeffs[i].real)
            coeffs_imag.append(self.coeffs[i].imag)

        coeffs_real = np.array(coeffs_real)
        coeffs_imag = np.array(coeffs_imag)
        coeffs_real /= coeffs_real[0]
        coeffs_imag -= coeffs_imag[0]

        # Open uncalibrated visibilities
        f = h5py.File(vis_in, 'r')
        vis = np.array(f["Vis"])
        basl = np.array(f["Baselines"])

        # Create calibrated visibilities matrix
        vis_calib = np.ones((len(vis[:, 0, 0, 0]), len(vis[0, :, 0, 0]), len(vis[0, 0, :, 0]), len(vis[0, 0, 0, :])),
                            dtype=np.complex)

        # Calibrate visibilities
        for t in range(len(vis[:, 0, 0, 0])):
            for i in range(len(basl[:, 0])):
                a1 = int(basl[i, 1])
                a2 = int(basl[i, 2])
                vis_calib[t, 0, i, 0] = vis[t, 0, i, 0] / (np.exp(np.complex((coeffs_real[a1] + coeffs_real[a2]),
                                                                             (coeffs_imag[a2] - coeffs_imag[a1]))))

        # Write calibrated visibilities to h5 file
        f2 = h5py.File(vis_out, 'w')
        name = 'Vis'
        d_set = f2.create_dataset(name, (len(vis[:, 0, 0, 0]), len(vis[0, :, 0, 0]), len(vis[0, 0, :, 0]),
                                         len(vis[0, 0, 0, :])), dtype='c16')
        d_set[:, :, :, :] = vis_calib[:, :, :, :]
        f2.flush()
        f2.close()
