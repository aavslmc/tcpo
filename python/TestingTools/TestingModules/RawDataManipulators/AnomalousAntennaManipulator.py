import numpy as np
from pytcpo.Core.PipeLine import AbstractProcessor

class AnomalousAntennaManipulator(AbstractProcessor):

    """
    AnomalousAntennaManipulator

    This class implements a processing module that inverts the raw data of a specified percentage of antennas/pols
    (which are randomly selected) across a specified percentage p of samples out of n samples (with the samples simulating
    failure being the last (n*p)/100 samples).

    Required Forwarded Data :
    i) 3D Numpy Array of dimensions Array/Pols/Channels

    Outputted Data :
    i) For Forwarding: 3D Numpy Array of dimensions Array/Pols/Channels with simulated anomalous antennas/polarisations
    ii) For Archiving: 2D Numpy Array of dimension Array/Pols with boolean values indicating is A/P is anomalous or not
    """

    def __init__(self):
        super(AnomalousAntennaManipulator, self).__init__()
        self.antennas = None
        self.polarisations = None
        self.invert_mdl = None
        self.in_sim = None
        self.sample_rng = None
        self.invert_rng = None
        self.rand_ap = None
        self.index = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 6 required parameters :
                            * antennas - [Required]: The Number of Antennas we will be operating on
                            * polarisations - [Required]: The Number of Polarisations we will be operating on
                            * invert_model - [Required] Numpy array with 1s and 0s of dimension Ant/Pols to indicate
                                                         which A/Ps to invert
                            * inter_similarity - [Required]: Ratio of what number of sample per antenna/polarisation are
                                                             to be similar
                            * sample_rng - [Required]: Tuple with the sample range being considered, of the form
                                                       (sample_idx_min, sample_idx_max)

        :return: None
        """

        self.antennas = filter_model['antennas']
        self.polarisations = filter_model['polarisations']
        self.invert_mdl = filter_model['invert_model']
        self.in_sim = filter_model['inter_similarity']
        self.sample_rng = filter_model['sample_rng']
        self.index = 0

        # Determine what sample range are to be inverted:
        self.invert_rng = ((self.sample_rng[1] - self.sample_rng[0]) * self.in_sim, self.sample_rng[1])

    def _will_start(self):
        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """
        self.sample = message.Default

        shift = np.nanmin(self.sample) + np.nanmax(self.sample)

        if message.Index in range(int(self.invert_rng[0]), int(self.invert_rng[1]+1)):
            self.sample[self.invert_mdl == 1,:] = np.add(np.multiply(self.sample[self.invert_mdl == 1,:],-1.0),shift)
            forward[1] = np.asarray(self.invert_mdl)
        else:
            forward[1] = np.zeros((len(self.antennas), len(self.polarisations)))

        forward[0] = self.sample

    def _clean_up(self, reason):

        """
        Empty
        """
        pass