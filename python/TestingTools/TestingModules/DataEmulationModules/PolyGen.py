import numpy as np
import time as tm
from pytcpo.Core.PipeLine import AbstractGenerator, Message
from pytcpo.Core.Common import TCPOCode


class PolyGen(AbstractGenerator):

    """
    PolyGen

    This class implements a generating module producing data based on an input polynomial, generating data for the specified number
    of antennas/pols/channels. Moreover it generates a specified number of samples. The class provides the option to vary slightly the
    polynomial constant of the antennas between each other. This variation functionality is intended to be useful when testing modules
    which compare across antennas.

    Required Forwarded Data :
    i) None - since this is the generating module

    Outputted Data For Forwarding :
    i) Generated 3D Numpy Array of dimensions Antennas/Pols/Channels

    Notes :
    i) Polynomials are generated using poly1d - in other modules the use of poly1d has been replaced with other polynomial function methods.
    """

    def __init__(self):
        super(PolyGen, self).__init__()
        self.telescope_model = None
        self.samples = None
        self.base_coeff = None
        self.similarity = None
        self.range_min = None
        self.index = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 6 required paremeters :
                            * antenna_count - the number of antennas to simulate
                            * pol_count - the number of polarisations to simulate
                            * channel_count - the number of channels to simulate
                            * sample_count - the number of samples to generat
                            * base_coeff - list of coeffients (starting from the highest order)
                            * samilarity_percentage - value between 0 and 1 to indicate how similar antennas should be; use value 1
                                                       to maintain antennas indentical
        :return: None
        """
        self.telescope_model = [filter_model['antenna_count'],filter_model['pol_count'],filter_model['channel_count']]
        self.samples = filter_model['sample_count']
        self.base_coeff = filter_model['base_coeff']
        self.similarity = filter_model['similarity_percentage']
        self.range_min = int(np.floor(self.telescope_model[0] * self.similarity))
        self.index = 0

    def _generator_loop(self):

        """
        Generates one sample at a time and forwards generated data as the Default in a message to the next module down
        the line.
        """

        # Iterating over all samples
        for i in range(self.samples) :

            temp_coeff = np.array(self.base_coeff)
            data_matrix = np.zeros((self.telescope_model[0], self.telescope_model[1], self.telescope_model[2]))

            for a in range(self.telescope_model[0]) :

                # Varying slightly constant term
                if a in range(self.range_min, self.telescope_model[0]) :
                    temp_coeff[-1] += 2*(np.sin((np.pi)/(self.telescope_model[0] - a + 1)))

                # Writing simulated polynomial based data
                data_matrix[a][::][0:self.telescope_model[2]] = np.poly1d(temp_coeff)(np.arange(self.telescope_model[2]))

            msg = Message((Message.SelType.TILE,0),self.index, tm.time(),self.Name)
            msg.Default = data_matrix
            self.index += 1
            self._push(msg,None)

        self._inform(TCPOCode.Request.STOP_FILE)