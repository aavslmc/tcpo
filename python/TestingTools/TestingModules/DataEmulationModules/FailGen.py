import numpy as np
import time as tm
from pytcpo.Core.PipeLine import AbstractProcessor, Message
from pytcpo.Core.Common import TCPOCode

class FailGen(AbstractProcessor):

    """
    FailGen

    This class implements a processing module to simulate antenna failures on the given data. Selection of which antennas to fail
    is entirely probabilistic, which the selection being uniformly distributed such that reptitions are not allowed. Failures are
    simulated by alternating the data across each channel between two values - 0 and extremes_maximum. If extremes_maximum is set
    to 0, module would simulate a dead antenna (flat line at y = 0).

    More over, the module provides the functionality to simulate failures within a particular sample range (rather then failing
    the antenna across all samples).

    Required Forwarded Data :
    i) 3D Numpy Array of dimensions Array/Pols/Channels

    Outputted Data For Forwarding :
    i) 3D Numpy Array of dimensions Array/Pols/Channels with simulated failure is sample happens to be within specified fail range
    """

    def __init__(self):
        super(FailGen, self).__init__()
        self.probability = None
        self.telescope_model = None
        self.failure_model = None
        self.ext_max = None
        self.fail_range = None
        self.index = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 7 required parameters :
                            * fail_probability - value between 0 and 1; defined the probability that an antenna fails
                            * antenna_count - the number of antennas to simulate
                            * pol_count - the number of polarisations to simulate
                            * channel_count - the number of channels to simulate
                            * extremes_maximum - largest possible alternating data value
                            * fail_min_sample - first sample at which an antenna may fail
                            * fail_max_sample - last sample at which an antenna may fail
                            Note - Select fail_min_sample and fail_max_sample to cover the entire sample range to simulate
                                   an antenna failure across all samples.
        :return: None
        """

        self.probability = filter_model['fail_probability']
        self.telescope_model = [filter_model['antenna_count'], filter_model['pol_count'], filter_model['channel_count']]

        # Selects which antennas to fail
        self.failure_model = np.random.choice([0, 1], self.telescope_model[0], p=[1 - self.probability, self.probability])

        self.ext_max = filter_model['extremes_maximum']
        self.fail_range = [filter_model['fail_min_sample'],filter_model['fail_max_sample']]
        self.index = 0

    def _will_start(self):

        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """

        msg = message.deep(self.Name)
        sample = msg.Default

        if self.index in range(self.fail_range[0],self.fail_range[1]):
            for i in range(self.telescope_model[0]):
                if self.failure_model[i] != 0:
                    # Overwriting data with garbled data for failed antennas
                    sample[i, :, :] = 0
                    sample[i, :, 0:(self.telescope_model[2]-1):2] = self.ext_max #

        forward[0] = sample
        self.index += 1

    def _clean_up(self, reason):

        """
        Empty
        """
        pass