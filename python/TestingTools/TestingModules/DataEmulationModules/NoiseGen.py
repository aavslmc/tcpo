import numpy as np
from pytcpo.Core.PipeLine import AbstractProcessor

class NoiseGen(AbstractProcessor):

    """
    NoiseGen

    This class implements a processing module to add simulated Gaussian Noise to inputted data across the channels. The
    added Gaussian Noise is Normally distributed (based on inputted parameters).

    Required Forwarded Data :
    i) 3D Numpy Array of dimensions Array/Pols/Channels

    Outputted Data For Forwarding :
    i) 3D Numpy Array of dimensions Array/Pols/Channels with Gaussian Noise added across channels
    """

    def __init__(self):
        super(NoiseGen, self).__init__()
        self.telescope_model = None
        self.alpha = None
        self.beta = None
        self.index = None


    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 5 required parameters :
                            * antenna_count - the number of antennas to simulate
                            * pol_count - the number of polarisations to simulate
                            * channel_count - the number of channels to simulate
                            * alpha - shape parameter for normal distribution; corresponds to expectation
                            * beta - shape parameter for normal distribution; corresponds to variance

        :return: None
        """

        self.telescope_model = [filter_model['antenna_count'], filter_model['pol_count'], filter_model['channel_count']]
        self.alpha = filter_model['alpha']
        self.beta = filter_model['beta']
        self.index = 0

    def _will_start(self):
        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """
        self.sample = message.Default

        # Adds normally distributed Gaussian Noise if gauss is True
        self.sample = np.add(self.sample,np.random.normal(self.alpha, self.beta, (self.telescope_model[0], self.telescope_model[1], self.telescope_model[2])))

        forward[0] = self.sample

    def _clean_up(self, reason):

        """
        Empty
        """
        pass