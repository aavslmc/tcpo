import numpy as np
from pytcpo.Core.PipeLine import AbstractProcessor

class RFIGen(AbstractProcessor):

    """
    RFIGen

    This class implements a processing module to add simulated RFI Noise to inputted data across the channels. RFI Noise
    maybe be added to either randomly selected channels or to specified channels using an RFI Model (boolean matrix of
    size A/P/C).

    Required Forwarded Data :
    i) 3D Numpy Array of dimensions Array/Pols/Channels

    Outputted Data For Forwarding :
    i) 3D Numpy Array of dimensions Array/Pols/Channels with RFI Noise added across channels
    """

    def __init__(self):
        super(RFIGen, self).__init__()
        self.telescope_model = None
        self.rfi_chans = None
        self.rfi_variability = None
        self.index = None


    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 6 required parameters :
                            * antenna_count - the number of antennas to simulate
                            * pol_count - the number of polarisations to simulate
                            * channel_count - the number of channels to simulate
                            * rfi_chans - number of channels to include RFI in
                            * rfi_variability - value between 0 and 1 to signify how much RFI may vary across channels

        :return: None
        """

        self.telescope_model = [filter_model['antenna_count'], filter_model['pol_count'], filter_model['channel_count']]
        self.rfi_chans = filter_model['rfi_chans']
        self.rfi_variability = filter_model['rfi_variability']
        self.index = 0

    def _will_start(self):
        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """
        self.sample = message.Default

        minima = (0.20) * (100 - (self.rfi_variability))

        # Determining what amount of RFI to add (randomly, uniformly distributed)
        rand_rfi = np.random.uniform(minima, 40,(self.telescope_model[0], self.telescope_model[1], self.telescope_model[2]))

        # Adds RFI to randomly selected channels
        x = float(self.rfi_chans)/float(self.telescope_model[2])

        # Selects which channels to which RFI is to be added
        rand_channels = np.random.choice([0,1],(self.telescope_model[0], self.telescope_model[1], self.telescope_model[2]),p=[1-x, x])

        base_rfi = np.multiply(rand_channels, rand_rfi)

        self.sample[base_rfi != 0] = base_rfi[base_rfi != 0]

        forward[0] = self.sample
        forward[1] = rand_channels

    def _clean_up(self, reason):

        """
        Empty
        """
        pass