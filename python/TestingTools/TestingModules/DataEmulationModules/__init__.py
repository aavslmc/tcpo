from NoiseGen import NoiseGen
from PolyGen import PolyGen
from RFIGen import RFIGen

from TestingTools.TestingModules.DataEmulationModules.FailGen import FailGen
from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(PolyGen)
PipelineManager.registerModule(NoiseGen)
PipelineManager.registerModule(RFIGen)
PipelineManager.registerModule(FailGen)