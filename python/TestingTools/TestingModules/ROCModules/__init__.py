from TestingTools.TestingModules.ROCModules.ROCData import ROCData, ROCStorage
from pytcpo.Core.Archiving import ASerialiser
from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(ROCData)
ASerialiser.registerSerialiseable(ROCStorage)