import numpy as np
from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Archiving.Serialiseable import ASerialiseable
from pytcpo.Modules.Archivers import FilePersistor, CSVSerialiser

class ROCData(AbstractProcessor):

    """
    ROCData is a processing module meant to calculate the True Positive Rate and False Positive Rate of a given sample,
    for a module with a specific parameter set. The module accumulates the TPR and FPR values for each sample, and once
    all samples have been processed the average across all samples is taken and forwarded. A data_model is built as a
    reference model for the received boolean data.

    The module may operate in one of two test modes:
        * OUT mode for outlier detection, where the data_model is of the form Ants/Pols/Chans
        * ANOM mode for anomalous antenna detection, where the data_mode is of the form Ants/Pols

    The data_model maybe built in one of two modes:
        * file_mode = True for reading from a file using the FilePersistor and CSVSerialiser
        * file_mode = False for use of a coded data_model or call to an externally coded data_model

    The module requires specification of any buffers and down sampling rates used in order to ensure correct sample
    comparison between the received and the reference data.

    Outputs:
        * None
    Archives:
        * ROCStorage object containing the TPR/FPR data and a string specifying the detector parameters used.
    """

    def __init__(self):
        super(ROCData, self).__init__()
        self.sample_rng = None
        self.antennas = None
        self.polarisations = None
        self.down_sampling = None
        self.buffer_length = None
        self.data_model = None
        self.param_str = None
        self.source = None
        self.index = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 6 required parameters :
                            * sample_count - tuple of the form (sample_min, sample_max)
                            * down_sample - integer to specify down sampling rate used in pipeline
                            * buffer_length - integer to specify the buffer length (set to 0 iff no buffer is used)
                            * antennas - Numpy array of valid antennas
                            * polarisations - Numpy array of valid polarisations
                            * channels - Numpy array of valid channels
                            * parameter_str - string detailing the parameters being used for current ROC data point
                            * source - string giving the file path of the RFI model
                            * file_mode - boolean to signify if RFI model is to be read from a file
                            * test_mode - string of value either 'OUT' or 'ANOM'
        :return: None
        """

        self.sample_rng = filter_model['sample_rng']
        self.antennas = filter_model['antennas']
        self.polarisations = filter_model['polarisations']
        self.down_sampling = filter_model['down_sample']
        self.buffer_length = filter_model.get('buffer_length', 0)
        self.data = []
        self.param_str = filter_model['parameter_str']
        self.source = filter_model.get('source', 'NA')
        self.index = 0

        if filter_model['file_mode']:
            if self.source is 'NA':
                raise Exception('No file source specified')
            else:
                archiver = FilePersistor()

                # Reading and building data_model stored at the specified source

                if filter_model['test_mode'] is 'OUT':
                    # Setting up archiver
                    assert (archiver.setup(mode=FilePersistor.M_READ, p_setup={'source': self.source,
                                                                               'keys': {'rfi': np.ndarray}},
                                           serialiser='CSVSerialiser', s_setup=None))

                    self.data_model = np.zeros((self.sample_rng[1],len(filter_model['antennas']),
                                                   len(filter_model['polarisations']),len(filter_model['channels'])),dtype=bool)

                    # Reading samples
                    for x in range((self.sample_rng[0] + self.down_sampling) - 1, self.sample_rng[1], self.down_sampling):
                        z = archiver.ReadNext('rfi')[1][np.ix_(filter_model['antennas'], filter_model['polarisations'],
                                                    filter_model['channels'])]
                        self.data_model[x] = z

                    self.data_model = np.asarray(self.data_model, dtype=bool)

                elif filter_model['test_mode'] is 'ANOM':
                    # Setting up archiver
                    assert (archiver.setup(mode=FilePersistor.M_READ, p_setup={'source': self.source,
                                                                               'keys': {'anom_ap': np.ndarray}},
                                           serialiser='CSVSerialiser', s_setup=None))

                    self.data_model = np.zeros((self.sample_rng[1], len(filter_model['antennas']),
                                                len(filter_model['polarisations'])), dtype=bool)

                    # Need to skip buffer length
                    archiver.SkipNext('anom_ap', self.buffer_length)

                    # Reading samples
                    for x in range((self.sample_rng[0] + ((self.buffer_length + 1) * self.down_sampling) - 1),
                                   self.sample_rng[1], self.down_sampling):
                        z = archiver.ReadNext('anom_ap')[1][np.ix_(filter_model['antennas'], filter_model['polarisations'])]
                        self.data_model[x] = z

                    self.data_model = np.asarray(self.data_model, dtype=bool)
                else:
                    raise Exception('test_mode not set to either OUT or ANOM')
        else:
            if filter_model['test_mode'] is 'OUT':
                print('No outlier data_model set')  # Replace with coded model or call to external coded model

            elif filter_model['test_mode'] is 'ANOM':
                print('No anomalous data_model set')  # Replace with coded model or call to external coded model

            else:
                raise Exception('test_mode not set to either OUT or ANOM')

    def _will_start(self):
        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """
        self.sample = message.Default

        if message.Index in range((self.sample_rng[0] + ((self.buffer_length + 1) * self.down_sampling) - 1),
                                  self.sample_rng[1], self.down_sampling):

            data_sum = np.sum(self.data_model[message.Index], dtype=np.float64)
            data_sum_not = np.sum(np.logical_not(self.data_model[message.Index]), dtype=np.float64)

            if data_sum != 0.0 and data_sum_not != 0.0:
                # Carrying out necessary TPR and FPR calculations on a given sample
                is_matching = np.sum(np.logical_and(self.data_model[message.Index],self.sample),dtype=np.float64)
                not_matching = np.sum(self.sample, dtype=np.float64) - is_matching

                tpr = np.divide(is_matching, data_sum)
                fpr = np.divide(not_matching, data_sum_not)

                self.data.append([tpr,fpr])

            self.index = message.Index

        # Accumulation of data until all samples have been read
        if self.index == (self.sample_rng[1] - 1):
            # Forwarding FPR and TPR averaged across all samples
            mean = np.mean(self.data, axis=0,dtype=np.float64)
            forward[1] = ROCStorage()
            forward[1].setup(mean[0], mean[1], self.param_str)

    def _clean_up(self, reason):

        """
        Empty
        """
        pass

class ROCStorage(ASerialiseable):

    """
    ROCStorage is an ASerialisable object used to allow the archiving of TPR/FPR data of a module along with any module
    parameters (as a string). The module also employs the ret_ROC and ret_params to any required data. The use of
    ROCStorage is intended to standardise the way ROC data is stored (for future ROC tests, example for future anomaly
    detector tests).
    """

    def __init__(self):
        self.tpr = None
        self.fpr = None
        self.parameter = None

    def setup(self, tpr, fpr, param):
        self.tpr = tpr
        self.fpr = fpr
        self.parameter = param

    def ret_ROC(self):
        return [self.tpr, self.fpr]

    def ret_params(self):
        return self.parameter

    def serial(self):
        return {'tpr':np.float64, 'fpr':np.float64, 'parameter':str}