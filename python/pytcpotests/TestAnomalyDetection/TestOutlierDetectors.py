import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message
from pytcpo.Modules.AnomalyDetectors.Implementations.OutlierDetectors import DerivativeSignsDetector,\
    PolynomialDeviations, OutlierAggregator, OutlierThinner, DerivativeSignsWithDifferenceRejection

class TestDSDDetection(TCPOTester):

    def test_DSDNoAnomaly(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'DSD_NoAnomaly')
        bandpass_matrix = np.array([[[1.0,2.0,3.0,4.0,5.0],[4.0,5.0,6.0,7.0,8.0]],
                                    [[1.0,2.1,3.2,4.3,5.4],[4.0,5.1,6.2,7.3,8.4]]])
        msg.Default = bandpass_matrix

        dsd_1a = DerivativeSignsDetector()
        dsd_1a.setup('dsd_1a',None, module_opt={'antennas':np.array([0,1]), 'polarisations':np.array([0,1]), 'channels':np.array([0,1,2,3,4]),
                                                  'min_cutoff': 0.1},archiver=(1, 1))
        dsd_1a.start()

        out0 = np.zeros((2,2,5),dtype=bool)

        result = [[], []]
        dsd_1a._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]),0,'No anomaly should have been detected')

class TestDSDNANs(TCPOTester):

    def test_DSDNANExtreme(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'DSD_NANExtreme')
        bandpass_matrix = np.empty((1, 1, 3))
        bandpass_matrix[:] = np.NaN
        msg.Default = bandpass_matrix

        dsd_2a = DerivativeSignsDetector()
        dsd_2a.setup('dsd_2a', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                                 'channels': np.array([0,1,2]), 'min_cutoff': 0.1},archiver=(1,1))
        dsd_2a.start()

        out0 = np.array([[[True,True,True]]])
        out1 = [[0,0]]

        result = [[], []]
        dsd_2a._new_sample(msg, result)

        self.assertEqual((1,1,3), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomaly detected in forward to Archiver not as expected')

class TestDSDIndices(TCPOTester):

    def test_DSDIndices(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'DSD_Indices')
        bandpass_matrix = np.array([[[1.0,2.0,-3.0,4.0,5.0],[4.0,5.0,-6.0,7.0,8.0]],
                                    [[1.0,2.1,3.2,4.3,5.4],[4.0,5.1,-6.2,7.3,8.4]]])
        msg.Default = bandpass_matrix

        dsd_3a = DerivativeSignsDetector()
        dsd_3a.setup('dsd_3a', None, module_opt={'antennas': np.array([2,5]), 'polarisations': np.array([1,4]),
                                                 'channels': np.array([0,3,5,8,10]), 'min_cutoff': 0.1},archiver=(1,1))
        dsd_3a.start()

        out1 = [[2,1],[2,4],[5,4]]

        result = [[], []]
        dsd_3a._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')

        temp1 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp1, out1, 'Indices not as expected')

        temp2 = [x._anomaly for x in result[1]]
        for x in temp2:
            self.assertTrue(np.array_equal(x, np.array([3, 5])), 'Channel indices not as expected')

class TestPDDetection(TCPOTester):

    def test_PDNoAnomaly(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_NoAnomaly')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        msg.add('model', bandpass_matrix)

        pd_1a = PolynomialDeviations()
        pd_1a.setup('pd_1a', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1a.start()

        out0 = np.zeros((2,2,5),dtype=bool)

        result = [[], []]
        pd_1a._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 0, 'No anomaly should have been detected')

    def test_PDAllAnom(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllAnom')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,2,5))
        msg.add('model', model_matrix)

        pd_1b = PolynomialDeviations()
        pd_1b.setup('pd_1b', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 0.2, 'model': 'model'},archiver=(1, 1))
        pd_1b.start()

        out0 = np.ones((2,2,5),dtype=bool)

        result = [[], []]
        pd_1b._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'All antennas/pols should have been detected as anomalies in forward to Archiver')

    def test_PDMixedAnom(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllAnom')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,2,5))
        msg.add('model', model_matrix)

        pd_1c = PolynomialDeviations()
        pd_1c.setup('pd_1c', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1c.start()

        out0 = np.array([[[False,False,False,True,True],[False,False,False,True,True]],
                         [[False,False,False,True,True],[False,False,False,True,True]]])
        out1 = [[0,0, np.array([3,4])],[0,1, np.array([3,4])],[1,0, np.array([3,4])],[1,1, np.array([3,4])]]

        result = [[], []]
        pd_1c._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDMixedAnom_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllAnom_ant')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((1,2,5))
        msg.add('model', model_matrix)

        pd_1d = PolynomialDeviations()
        pd_1d.setup('pd_1d', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1d.start()

        out0 = np.array([[[False,False,False,True,True],[False,False,False,True,True]]])
        out1 = [[0,0, np.array([3,4])],[0,1, np.array([3,4])]]

        result = [[], []]
        pd_1d._new_sample(msg, result)

        self.assertEqual((1, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDMixedAnom_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllAnom_pol')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,1,5))
        msg.add('model', model_matrix)

        pd_1e = PolynomialDeviations()
        pd_1e.setup('pd_1e', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1e.start()

        out0 = np.array([[[False,False,False,True,True]],
                         [[False,False,False,True,True]]])
        out1 = [[0,0, np.array([3,4])],[1,0, np.array([3,4])]]

        result = [[], []]
        pd_1e._new_sample(msg, result)

        self.assertEqual((2, 1, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDNoFit(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_NoFit')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,2,5))
        model_matrix[:,:,:] = np.NaN
        msg.add('model', model_matrix)

        pd_1f = PolynomialDeviations()
        pd_1f.setup('pd_1f', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1f.start()

        out0 = np.ones((2,2,5),dtype=bool)
        out1 = [[0,0, np.array([0,1,2,3,4])],[0,1, np.array([0,1,2,3,4])],[1,0, np.array([0,1,2,3,4])],[1,1, np.array([0,1,2,3,4])]]

        result = [[], []]
        pd_1f._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDNoFit_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_NoFit_ant')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((1,2,5))
        model_matrix[:,:,:] = np.NaN
        msg.add('model', model_matrix)

        pd_1g = PolynomialDeviations()
        pd_1g.setup('pd_1g', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1g.start()

        out0 = np.ones((1,2,5),dtype=bool)
        out1 = [[0,0, np.array([0,1,2,3,4])],[0,1, np.array([0,1,2,3,4])]]

        result = [[], []]
        pd_1g._new_sample(msg, result)

        self.assertEqual((1, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDNoFit_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_NoFit_pol')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,1,5))
        model_matrix[:,:,:] = np.NaN
        msg.add('model', model_matrix)

        pd_1h = PolynomialDeviations()
        pd_1h.setup('pd_1h', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0]),
                                               'channels': np.array([0, 1, 2, 3, 4]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1h.start()

        out0 = np.ones((2,1,5),dtype=bool)
        out1 = [[0,0, np.array([0,1,2,3,4])],[1,0, np.array([0,1,2,3,4])]]

        result = [[], []]
        pd_1h._new_sample(msg, result)

        self.assertEqual((2, 1, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDNoFit_chan(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_NoFit_chan')
        bandpass_matrix = np.array([[[1.0], [4.0]],
                                    [[1.0], [4.0]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2,2,1))
        model_matrix[:,:,:] = np.NaN
        msg.add('model', model_matrix)

        pd_1i = PolynomialDeviations()
        pd_1i.setup('pd_1i', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0]),'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'},archiver=(1, 1))
        pd_1i.start()

        out0 = np.ones((2,2,1),dtype=bool)
        out1 = [[0,0,0],[0,1,0],[1,0,0],[1,1,0]]

        result = [[], []]
        pd_1i._new_sample(msg, result)

        self.assertEqual((2, 2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1]== y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

class TestPDNANS(TCPOTester):

    def test_PDAllNaNs(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllNaNs')
        bandpass_matrix = np.zeros((2, 2, 5))
        bandpass_matrix[:,:,:] = np.NaN
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.add('model', model_matrix)

        pd_2a = PolynomialDeviations()
        pd_2a.setup('pd_2a', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_2a.start()

        out0 = np.ones((2,2,5),dtype=bool)
        out1 = [[0, 0, np.array([0, 1, 2, 3, 4])], [0, 1, np.array([0, 1, 2, 3, 4])], [1, 0, np.array([0, 1, 2, 3, 4])],
                [1, 1, np.array([0, 1, 2, 3, 4])]]

        result = [[], []]
        pd_2a._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDAllNaNs_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllNaNs_ant')
        bandpass_matrix = np.zeros((1, 2, 5))
        bandpass_matrix[:,:,:] = np.NaN
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]]])
        msg.add('model', model_matrix)

        pd_2b = PolynomialDeviations()
        pd_2b.setup('pd_2b', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_2b.start()

        out0 = np.ones((1,2,5),dtype=bool)
        out1 = [[0, 0, np.array([0, 1, 2, 3, 4])], [0, 1, np.array([0, 1, 2, 3, 4])]]

        result = [[], []]
        pd_2b._new_sample(msg, result)

        self.assertEqual((1, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDAllNaNs_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllNaNs_pol')
        bandpass_matrix = np.zeros((2, 1, 5))
        bandpass_matrix[:,:,:] = np.NaN
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0]],
                                 [[1.0, 2.1, 3.2, 4.3, 5.4]]])
        msg.add('model', model_matrix)

        pd_2c = PolynomialDeviations()
        pd_2c.setup('pd_2c', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0]),
                                               'channels': np.array([0, 1, 2, 3, 4]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_2c.start()

        out0 = np.ones((2,1,5),dtype=bool)
        out1 = [[0, 0, np.array([0, 1, 2, 3, 4])], [1, 0, np.array([0, 1, 2, 3, 4])]]

        result = [[], []]
        pd_2c._new_sample(msg, result)

        self.assertEqual((2, 1, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDAllNaNs_chan(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_AllNaNs_chan')
        bandpass_matrix = np.zeros((2, 2, 1))
        bandpass_matrix[:,:,:] = np.NaN
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[1.0], [4.0]],
                                    [[1.0], [4.0]]])
        msg.add('model', model_matrix)

        pd_2d = PolynomialDeviations()
        pd_2d.setup('pd_2d', None, module_opt={'antennas': np.array([0, 1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_2d.start()

        out0 = np.ones((2,2,1),dtype=bool)
        out1 = [[0, 0, 0], [0, 1, 0], [1, 0, 0], [1, 1, 0]]

        result = [[], []]
        pd_2d._new_sample(msg, result)

        self.assertEqual((2, 2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_PDExtremeNaNs(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_ExtremeNaNs')
        bandpass_matrix = np.array([[[np.NaN]]])
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[np.NaN]]])
        msg.add('model', model_matrix)

        pd_2e = PolynomialDeviations()
        pd_2e.setup('pd_2e', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                               'channels': np.array([0]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_2e.start()

        out0 = np.array([[[True]]])

        result = [[], []]
        pd_2e._new_sample(msg, result)

        self.assertEqual((1, 1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

    def test_PDMixedNaNs(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_MixedNaNs')
        bandpass_matrix = np.array([[[1.0, np.NaN, np.NaN, 4.0, 5.0], [np.NaN, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, np.NaN, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, np.NaN]],
                                [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, np.NaN, 7.3, 8.4]]])
        msg.add('model',model_matrix)

        pd_2f = PolynomialDeviations()
        pd_2f.setup('pd_2f', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]),
                                               'channels': np.array([0,1,2,3,4]), 'outlier_cut': 0.1,
                                               'outlier_std': 0.2, 'model': 'model'}, archiver=(1, 1))
        pd_2f.start()

        out0 = np.array([[[False,True,True,False,False],[True,False,False,False,True]],
                         [[False,False,False,False,False],[False,False,True,False,False]]])
        out1 = [[0,0,np.array([1,2])],[0,1,np.array([0,4])],[1,1,2]]

        result = [[], []]
        pd_2f._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

class TestPDIndices(TCPOTester):

    def test_PDMixedIndices(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PD_MixedIndices')
        bandpass_matrix = np.array([[[1.0, 2.0, 3.0, 4.0, 5.0], [4.0, 5.0, 6.0, 7.0, 8.0]],
                                    [[1.0, 2.1, 3.2, 4.3, 5.4], [4.0, 5.1, 6.2, 7.3, 8.4]]])
        msg.Default = bandpass_matrix
        model_matrix = np.zeros((2, 2, 5))
        msg.add('model', model_matrix)

        pd_3a = PolynomialDeviations()
        pd_3a.setup('pd_3a', None, module_opt={'antennas': np.array([2,5]), 'polarisations': np.array([0, 2]),
                                               'channels': np.array([0, 1, 2, 7, 10]), 'outlier_cut': 0.1,
                                               'outlier_std': 1, 'model': 'model'}, archiver=(1, 1))
        pd_3a.start()

        out1 = [[2, 0], [2, 2], [5, 0], [5, 2]]

        result = [[], []]
        pd_3a._new_sample(msg, result)

        temp1 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp1, out1, 'Antenna/pol indices not as expected')

        temp2 = [x._anomaly for x in result[1]]
        for x in temp2 :
            self.assertTrue(np.array_equal(x,np.array([7,10])), 'Channel indices not as expected')

class TestOACases(TCPOTester):

    def test_OAAggregationSingle(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OA_AggregationSingle')
        default_matrix = np.array([[[False,True,False,True,False],[True,False,False,False,True]],
                         [[True,False,False,True,False],[False,False,True,False,False]]])
        msg.Default = default_matrix

        oa_1a = OutlierAggregator()
        oa_1a.setup('oa_1a', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0, 1]),
                                                'channels': np.array([0, 1, 2, 3, 4]), 'length': 4}, archiver=(1,1))

        oa_1a.start()

        out0 = np.array([[[False,True,True,True,False],[True,False,False,False,True]],
                          [[True,True,True,True,False],[False,False,True,False,False]]])
        out1 = [[0,0,np.array([1,2,3])],[0,1,np.array([0,4])],[1,0,np.array([0,1,2,3])],[1,1,2]]

        result = [[], []]
        oa_1a._new_sample(msg,result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_OAAggregationMultiple(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OA_AggregationMultiple')
        outlier0_matrix = np.array([[[True,False,False,True,False,True,False,True],
                                    [False,False,False,False,False,False,False,False]],
                                   [[False,False,False,False,False,False,False,False],
                                    [True,False,False,False,False,False,False,False]]])
        outlier1_matrix = np.array([[[False,False,False,False,False,False,False,False],
                                    [False,True,False,True,False,False,True,False]],
                                   [[False,False,False,False,False,False,False,False],
                                    [False,False,False,False,False,False,False,False]]])
        outlier2_matrix = np.array([[[False,False,False,False,False,False,False,False],
                                    [False,False,False,False,False,False,False,False]],
                                   [[True,False,False,True,True,False,False,True],
                                    [False,False,False,False,False,False,True,False]]])

        msg.Default = outlier0_matrix
        msg.add('outlier1',outlier1_matrix)
        msg.add('outlier2', outlier2_matrix)

        oa_1b = OutlierAggregator()
        oa_1b.setup('oa_1b', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0, 1]),
                                               'channels': np.array([0, 1, 2, 3, 4, 5, 6, 7]), 'length': 4,
                                               'others' : ['outlier1','outlier2']}, archiver=(1,1))

        oa_1b.start()

        out0 = np.array([[[True,True,True,True,True,True,True,True],
                          [False,True,True,True,True,True,True,False]],
                         [[True,True,True,True,True,True,True,True],
                           [True,False,False,False,False,False,True,False]]])

        out1 = [[0,0,np.array([0, 1, 2, 3, 4, 5, 6, 7])],[0,1,np.array([1, 2, 3, 4, 5, 6])],
                [1,0,np.array([0, 1, 2, 3, 4, 5, 6, 7])],[1,1,np.array([0, 6])]]

        result = [[], []]
        oa_1b._new_sample(msg,result)

        self.assertEqual((2, 2, 8), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_OAExtremes(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OA_Extremes')
        outlier0_matrix = np.array([[[False]]])
        msg.Default = outlier0_matrix
        outlier1_matrix = np.array([[[True]]])
        msg.add('outlier1',outlier1_matrix)

        oa_1c = OutlierAggregator()
        oa_1c.setup('oa_1c', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                               'channels': np.array([0]), 'length': 1, 'others':['outlier1']}, archiver=(1, 1))

        oa_1c.start()

        out0 = np.array([[[True]]])

        result = [[], []]
        oa_1c._new_sample(msg, result)

        self.assertEqual((1, 1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to archiver')

class TestOTCases(TCPOTester):

    def test_OTMorphological(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OT_Morphological')
        default_matrix = np.array([[[False,True,False,True,False],[True,True,False,False,True]],
                                   [[True,False,True,True,True],[True,True,True,True,True]],
                                   [[False,True,True,True,False],[True,False,False,False,True]]])
        msg.Default = default_matrix

        ot_1a = OutlierThinner()
        ot_1a.setup('ot_1a', None, module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0, 1]),
                                                'channels': np.array([0, 1, 2, 3, 4])}, archiver=(1,1))

        ot_1a.start()

        out0 = np.array([[[False,True,False,True,False],[True,True,False,False,True]],
                         [[True,False,False,True,False],[False,True,True,True,False]],
                         [[False,False,True,False,False],[True,False,False,False,True]]])
        out1 = [[0,0,np.array([1,3])],[0,1,np.array([0,1,4])],[1,0,np.array([0,3])],[1,1,np.array([1,2,3])],
                [2,0,2], [2,1,np.array([0,4])]]

        result = [[], []]
        ot_1a._new_sample(msg,result)

        self.assertEqual((3, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 6, 'Number of FlaggedAnomaly objects forwarded to Archiver not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]

        for x in out1:
            count = 0
            for y in temp:
                if (x[0] == y[0]) and (x[1] == y[1]):
                    self.assertTrue(np.array_equal(x[2], y[2]), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_OTExtremes(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OT_Extremes')
        outlier0_matrix = np.array([[[False]]])
        msg.Default = outlier0_matrix

        ot_1b = OutlierThinner()
        ot_1b.setup('ot_1b', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                               'channels': np.array([0])}, archiver=(1, 1))

        ot_1b.start()

        out0 = np.array([[[False]]])

        result = [[], []]
        ot_1b._new_sample(msg, result)

        self.assertEqual((1, 1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 0, 'Anomaly detected in forward to archiver when none should have been')

class TestDSWDRNANs(TCPOTester):

    def test_DSWDRNANExtreme(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'DSWDR_NANExtreme')
        bandpass_matrix = np.empty((1, 1, 3))
        bandpass_matrix[:] = np.NaN
        msg.Default = bandpass_matrix

        dswdr_1a = DerivativeSignsDetector()
        dswdr_1a.setup('dswdr_1a', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                                 'channels': np.array([0,1,2]), 'envelope':1.0,'min_cutoff': 0.1},
                       archiver=(1,1))
        dswdr_1a.start()

        out0 = np.array([[[True,True,True]]])
        out1 = [[0,0]]

        result = [[], []]
        dswdr_1a._new_sample(msg, result)

        self.assertEqual((1,1,3), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomaly detected in forward to Archiver not as expected')

class TestDSWDRIndices(TCPOTester):

    def test_DSWDRIndices(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'DSWDR_Indices')
        bandpass_matrix = np.array([[[1.0,2.0,-3.0,4.0,5.0],[4.0,5.0,-6.0,7.0,8.0]],
                                    [[1.0,2.1,3.2,4.3,5.4],[4.0,5.1,-6.2,7.3,8.4]]])
        msg.Default = bandpass_matrix

        dswdr_2a = DerivativeSignsDetector()
        dswdr_2a.setup('dswdr_2a', None, module_opt={'antennas': np.array([2,5]), 'polarisations': np.array([1,4]),
                                                 'channels': np.array([0,3,5,8,10]), 'min_cutoff': 0.1},archiver=(1,1))
        dswdr_2a.start()

        out1 = [[2,1],[2,4],[5,4]]

        result = [[], []]
        dswdr_2a._new_sample(msg, result)

        self.assertEqual((2, 2, 5), result[0].shape, 'Forwarded shape to module is incorrect')

        temp1 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp1, out1, 'Indices not as expected')

        temp2 = [x._anomaly for x in result[1]]
        for x in temp2:
            self.assertTrue(np.array_equal(x, np.array([3, 5])), 'Channel indices not as expected')