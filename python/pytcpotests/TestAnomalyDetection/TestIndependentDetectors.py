import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message
from pytcpo.Modules.AnomalyDetectors.Implementations.IndependentDetectors import PerAntennaDeviationDetector, IndependentOverlapDetector,\
    OutlierThresholdDetector, OutlierWeightedOverlapDetector
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage

class TestPADDNANs(TCPOTester):

    def test_PADDExtremeNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_Extreme')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 0, check_nan=True)
        msg.Default = data_matrix

        padd_1a = PerAntennaDeviationDetector()
        padd_1a.setup('padd_1a',None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]), 'fits': 1,
                                                  'start': np.array([[1.0]]), 'excessive': np.array([1.0]),
                                                  'weight': np.array([[1.0]]), 'length': 3},archiver=(1, 1))
        padd_1a.start()

        out0 = np.array([[True]])
        out1 = [[0,0]]

        result = [[], []]
        padd_1a._new_sample(msg, result)

        self.assertEqual((1,1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomaly detected in forward to Archiver not as expected')

    def test_PADDAllNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_AllNAN')
        data_matrix = np.empty((3, 2, 1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
                data_matrix[a,p,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        padd_1b = PerAntennaDeviationDetector()
        padd_1b.setup('padd_1b', None, module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0,1]), 'fits': 1,
                                                   'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                   'weight': np.array([[1.0,1.0,1.0]]), 'length': 3}, archiver=(1, 1))
        padd_1b.start()

        out0 = np.ones((3, 2), dtype=bool)
        out1 = [[0,0],[0,1], [1,0], [1,1], [2,0], [2,1]]

        result = [[], []]
        padd_1b._new_sample(msg, result)

        self.assertEqual((3,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 6, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')
        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_PADDAllNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_AllNAN_ant')
        data_matrix = np.empty((1, 2, 1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
            data_matrix[0,p,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        padd_1c = PerAntennaDeviationDetector()
        padd_1c.setup('padd_1c', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]), 'fits': 1,
                                                  'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                  'weight': np.array([[1.0,1.0,1.0]]), 'length': 3}, archiver=(1, 1))
        padd_1c.start()

        out0 = np.ones((1, 2), dtype=bool)
        out1 = [[0, 0], [0, 1]]

        result = [[], []]
        padd_1c._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2,
                            'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')
        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_PADDAllNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_AllNAN_pol')
        data_matrix = np.empty((3, 1, 1), dtype=object)
        for a in range(3):
            data_matrix[a,0,0] = FitStorage()
            data_matrix[a,0,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        padd_1d = PerAntennaDeviationDetector()
        padd_1d.setup('padd_1d', None, module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0]), 'fits':1,
                                                   'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                   'weight': np.array([[1.0,1.0,1.0]]), 'length': 3}, archiver=(1, 1))
        padd_1d.start()

        out0 = np.ones((3, 1), dtype=bool)
        out1 = [[0,0], [1,0], [2,0]]

        result = [[], []]
        padd_1d._new_sample(msg, result)

        self.assertEqual((3,1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')
        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

class TestPADDBuffer(TCPOTester):

    def test_PADDBufferNANs(self):
        padd_2a = PerAntennaDeviationDetector()
        padd_2a.setup('padd_2a',None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                 'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                 'weight': np.array([[1.0,1.0,1.0]]), 'length': 3, 'ignore': True},
                      archiver=(1, 1))
        padd_2a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNAN')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        result = [[],[]]
        padd_2a._new_sample(msg, result)
        padd_2a._new_sample(msg, result)
        padd_2a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        padd_2a._new_sample(clean_msg, result)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(),
                        'Anomaly detected when none should have been')

    def test_PADDBufferNANsSingle(self):
        padd_2b = PerAntennaDeviationDetector()
        padd_2b.setup('padd_2b',None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                  'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                  'weight': np.array([[1.0,1.0,1.0]]), 'length': 1, 'ignore': True},
                      archiver=(1, 1))
        padd_2b.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNANSingle')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        result0 = [[],[]]
        padd_2b._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        padd_2b._new_sample(clean_msg, result1)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2,2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2,2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_PADDBufferNANsSingle_ant(self):
        padd_2c = PerAntennaDeviationDetector()
        padd_2c.setup('padd_2c',None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]), 'fits':1,
                                                 'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                 'weight': np.array([[1.0,1.0,1.0]]), 'length': 1, 'ignore': True},
                      archiver=(1, 1))
        padd_2c.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNANSingle_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix

        result0 = [[],[]]
        padd_2c._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNAN_Clean')
        clean_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            clean_matrix[0,p,0] = FitStorage()
            clean_matrix[0,p,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        padd_2c._new_sample(clean_msg, result1)

        out = np.zeros((1, 2), dtype=bool)

        self.assertEqual((1,2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((1,2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_PADDBufferNANsSingle_pol(self):
        padd_2d = PerAntennaDeviationDetector()
        padd_2d.setup('padd_2d',None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0]), 'fits':1,
                                                 'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                 'weight': np.array([[1.0,1.0,1.0]]), 'length': 1, 'ignore': True},
                      archiver=(1, 1))
        padd_2d.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNANSingle_pol')
        data_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        msg.Default = data_matrix

        result0 = [[],[]]
        padd_2d._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferNAN_Clean')
        clean_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            clean_matrix[a,0,0] = FitStorage()
            clean_matrix[a,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        padd_2d._new_sample(clean_msg, result1)

        out = np.zeros((2, 1), dtype=bool)

        self.assertEqual((2,1), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2,1), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_PADDBufferIgnoreFalse(self):
        padd_2e = PerAntennaDeviationDetector()
        padd_2e.setup('padd_2e', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                   'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                   'weight': np.array([[1.0,1.0,1.0]]), 'length': 1}, archiver=(1, 1))
        padd_2e.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferIgnoreFalse')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
               data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        result = [[], []]
        padd_2e._new_sample(msg, result)

        out0 = np.array([[True, False], [True, True]])
        out1 = [[0,0],[1,0],[1,1]]

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')

class TestPADDIndices(TCPOTester):

    def test_PADDIndices(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'PADD_Indices')
        data_matrix = np.empty((4,2,1), dtype=object)
        for a in range(4):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=True)
        data_matrix[2,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[2,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[3,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[3,1,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix

        padd_3a = PerAntennaDeviationDetector()
        padd_3a.setup('padd_3a', None, module_opt={'antennas': np.array([0,1,5,11]), 'polarisations': np.array([2,7]), 'fits':1,
                                                   'start': np.array([[1.0,1.0,1.0]]), 'excessive': np.array([1.0]),
                                                   'weight': np.array([[1.0,1.0,1.0]]), 'length': 1}, archiver=(1, 1))
        padd_3a.start()

        out1 = [[0,2],[1,2],[1,7],[5,2],[11,2],[11,7]]

        result = [[], []]
        padd_3a._new_sample(msg, result)

        self.assertEqual((4, 2), result[0].shape, 'Forwarded shape to module is incorrect')

        temp1 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp1, out1, 'Indices not as expected')

class TestPADDMultiFits(TCPOTester):

    # Tests buffer for when multiple piecewise fits are anomalous
    def test_buffer_multifits(self):
        padd_4a = PerAntennaDeviationDetector()
        padd_4a.setup('padd_4a', None,
                      module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits': 2,
                                  'start': np.array([[1.0,1.0,1.0],[1.0,1.0,1.0]]), 'excessive': np.array([1.0,1.0]),
                                  'weight': np.array([[1.0,1.0,1.0],[1.0,1.0,1.0]]), 'length': 3, 'ignore': True},
                      archiver=(1, 1))
        padd_4a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferFits')
        data_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    data_matrix[a,p,f] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(5), 2, check_nan=True)
        data_matrix[0,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[0,1,2].setup(np.arange(15,20), 2, check_nan=True)
        data_matrix[1,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix

        result = [[], []]
        padd_4a._new_sample(msg, result)
        padd_4a._new_sample(msg, result)
        padd_4a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'PADD_BufferFits_Clean')
        clean_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    clean_matrix[a,p,f] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
                clean_matrix[a,p,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        padd_4a._new_sample(clean_msg, result)

        out = np.zeros((2,2), dtype=bool)

        self.assertEqual((2,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(), 'Anomaly detected when none should have been')

class TestIODNANs(TCPOTester):

    def test_IODExtremeNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_ExtremeNAN')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iod_1a = IndependentOverlapDetector()
        iod_1a.setup('iod_1a', None, module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]), 'fits':1,
                                                 'start': np.array([[1.0]]),'min_overlap': 0.75, 'length': 3},
                     archiver=(1, 1))
        iod_1a.start()

        out0 = np.array([[True]])
        out1 = [[0, 0]]

        result = [[], []]
        iod_1a._new_sample(msg, result)

        self.assertEqual((1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_IODAllNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_AllNAN')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
                data_matrix[a,p,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        iod_1b = IndependentOverlapDetector()
        iod_1b.setup('iod_1b', None,module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]),'min_overlap': 0.75, 'length': 3},
                     archiver=(1, 1))
        iod_1b.start()

        out0 = np.ones((3, 2), dtype=bool)
        out1 = [[0,0],[0,1],[1,0],[1,1],[2,0],[2,1]]

        result = [[], []]
        iod_1b._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 6, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_IODAllNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_AllNAN_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
            data_matrix[0,p,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        iod_1c = IndependentOverlapDetector()
        iod_1c.setup('iod_1c', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]),'min_overlap': 0.75, 'length': 3},
                     archiver=(1, 1))
        iod_1c.start()

        out0 = np.ones((1, 2), dtype=bool)
        out1 = [[0,0],[0,1]]

        result = [[], []]
        iod_1c._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_IODAllNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_AllNAN_pol')
        data_matrix = np.empty((3,1,1), dtype=object)
        for a in range(3):
            data_matrix[a,0,0] = FitStorage()
            data_matrix[a,0,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        iod_1d = IndependentOverlapDetector()
        iod_1d.setup('iod_1d', None,module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 3},
                     archiver=(1, 1))
        iod_1d.start()

        out0 = np.ones((3, 1), dtype=bool)
        out1 = [[0,0],[1,0],[2,0]]

        result = [[], []]
        iod_1d._new_sample(msg, result)

        self.assertEqual((3, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

class TestIODBuffer(TCPOTester):

    def test_IODBufferNANs(self):
        iod_2a = IndependentOverlapDetector()
        iod_2a.setup('iod_2a', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 3,
                                                'ignore': True}, archiver=(1, 1))
        iod_2a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNAN')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        result = [[],[]]
        iod_2a._new_sample(msg, result)
        iod_2a._new_sample(msg, result)
        iod_2a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        iod_2a._new_sample(clean_msg, result)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(),
                        'Anomaly detected when none should have been')

    def test_IODBufferNANsSingle(self):
        iod_2b = IndependentOverlapDetector()
        iod_2b.setup('iod_2b', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]),'min_overlap': 0.75, 'length': 1,
                                                'ignore': True}, archiver=(1, 1))
        iod_2b.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNANSingle')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        result0 = [[],[]]
        iod_2b._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        iod_2b._new_sample(clean_msg, result1)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2,2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2,2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_IODBufferNANsSingle_ant(self):
        iod_2c = IndependentOverlapDetector()
        iod_2c.setup('iod_2c', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 1,
                                                'ignore': True}, archiver=(1, 1))
        iod_2c.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNANSingle_ant')
        data_matrix = np.empty((1, 2, 1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix

        result0 = [[],[]]
        iod_2c._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNAN_Clean')
        clean_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            clean_matrix[0,p,0] = FitStorage()
            clean_matrix[0,p,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        iod_2c._new_sample(clean_msg, result1)

        out = np.zeros((1, 2), dtype=bool)

        self.assertEqual((1,2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((1,2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_IODBufferNANsSingle_pol(self):
        iod_2d = IndependentOverlapDetector()
        iod_2d.setup('iod_2d', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 1,
                                                'ignore': True}, archiver=(1, 1))
        iod_2d.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNANSingle_pol')
        data_matrix = np.empty((2, 1, 1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        msg.Default = data_matrix

        result0 = [[],[]]
        iod_2d._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            clean_matrix[a,0,0] = FitStorage()
            clean_matrix[a,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        result1 = [[], []]
        iod_2d._new_sample(clean_msg, result1)

        out = np.zeros((2, 1), dtype=bool)

        self.assertEqual((2,1), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2,1), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_IODBufferIgnoreFalse(self):
        iod_2e = IndependentOverlapDetector()
        iod_2e.setup('iod_2e', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits':1,
                                                'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 1,
                                                'ignore': False}, archiver=(1, 1))
        iod_2e.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferIgnoreFalse')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        result = [[], []]
        iod_2e._new_sample(msg, result)

        out0 = np.array([[True, False], [True, True]])
        out1 = [[0,0],[1,0],[1,1]]

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')

class TestIODIndices(TCPOTester):

    def test_IODIndices(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'IOD_Indices')
        data_matrix = np.empty((4,2,1), dtype=object)
        for a in range(4):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        data_matrix[2,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[2,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[3,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[3,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix

        iod_3a = IndependentOverlapDetector()
        iod_3a.setup('iod_3a', None, module_opt={'antennas': np.array([0,1,5,11]), 'polarisations': np.array([2,7]), 'fits':1,
                                                 'start': np.array([[1.0,1.0,1.0]]), 'min_overlap': 0.75, 'length': 1},
                     archiver=(1, 1))
        iod_3a.start()

        out1 = [[0,2],[1,2],[1,7],[5,2],[11,2],[11,7]]

        result = [[], []]
        iod_3a._new_sample(msg, result)

        self.assertEqual((4, 2), result[0].shape, 'Forwarded shape to module is incorrect')

        temp1 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp1, out1, 'Indices not as expected')

class TestIODMultiFits(TCPOTester):

    # Tests buffer for when multiple piecewise fits are anomalous
    def test_buffer_multifits(self):
        iod_4a = IndependentOverlapDetector()
        iod_4a.setup('iod_4a', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]), 'fits': 2,
                                                 'start': np.array([[1.0,1.0,1.0],[1.0,1.0,1.0]]), 'min_overlap': 0.75,
                                                 'length': 3, 'ignore': True}, archiver=(1, 1))
        iod_4a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferFits')
        data_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    data_matrix[a,p,f] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(5), 2, check_nan=True)
        data_matrix[0,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[0,1,2].setup(np.arange(15,20), 2, check_nan=True)
        data_matrix[1,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix

        result = [[], []]
        iod_4a._new_sample(msg, result)
        iod_4a._new_sample(msg, result)
        iod_4a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'IOD_BufferFits_Clean')
        clean_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    clean_matrix[a,p,f] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
                clean_matrix[a,p,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix

        iod_4a._new_sample(clean_msg, result)

        out = np.zeros((2,2), dtype=bool)

        self.assertEqual((2,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(), 'Anomaly detected when none should have been')

class TestOTDCases(TCPOTester):

    def test_OTDDetection(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'OTD_Detection')
        data_matrix = np.array([[[True, True, True, False, True],[False, False, False, False, False]],
                                [[True, True, False, False, True],[False, False, True, True, True]],
                                [[True, False, False, False, True],[False, True, False, True, True]]])
        msg.Default = data_matrix

        otd_1a = OutlierThresholdDetector()
        otd_1a.setup('otd_1a',None,module_opt={'antennas': np.array([0, 1, 2]), 'polarisations': np.array([0,1]),
                                              'channels': np.array([0, 1, 2, 3]),'threshold': 0.5},archiver=(1, 1))

        otd_1a.start()

        out0 = np.array([[True,False],[True,True],[False, True]])
        out1 = [[0,0,4],[1,0,3],[1,1,3],[2,1,3]]

        result = [[], []]
        otd_1a._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol, x._anomaly] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')

    def test_OTDIndices(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OTD_Indices')
        data_matrix = np.array([[[True, True, True, False, True], [False, False, False, False, False]],
                                [[True, True, False, False, True], [False, False, True, True, True]],
                                [[True, False, False, False, True], [False, True, False, True, True]]])
        msg.Default = data_matrix

        otd_1b = OutlierThresholdDetector()
        otd_1b.setup('otd_1b', None, module_opt={'antennas': np.array([2,4,5]), 'polarisations': np.array([7,11]),
                                                 'channels': np.array([0, 1, 2, 3]), 'threshold': 0.5}, archiver=(1, 1))

        otd_1b.start()

        out1 = [[2, 7], [4, 7], [4, 11], [5, 11]]

        result = [[], []]
        otd_1b._new_sample(msg, result)

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Indices not as expected')

class TestOWODNANs(TCPOTester):

    def test_OWODExtremeNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_ExtremeNAN')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.array([[[False]]])
        msg.add('test_case_dummy', outlier_matrix)

        owod_1a = OutlierWeightedOverlapDetector()
        owod_1a.setup('owod_1a', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0]),
                                                  'channels': np.arange(0), 'start': np.array([[1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1, 'ignore': False,
                                                  'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_1a.start()

        out0 = np.array([[True]])
        out1 = [[0, 0]]

        result = [[], []]
        owod_1a._new_sample(msg, result)

        self.assertEqual((1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_OWODAllNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_AllNAN')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
                data_matrix[a,p,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.zeros((3,2,2),dtype=bool)
        msg.add('test_case_dummy',outlier_matrix)

        owod_1b = OutlierWeightedOverlapDetector()
        owod_1b.setup('owod_1b', None,module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2),'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1, 'ignore': False,
                                                  'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_1b.start()

        out0 = np.ones((3,2),dtype=bool)
        out1 = [[0,0],[0,1],[1,0],[1,1],[2,0],[2,1]]

        result = [[], []]
        owod_1b._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 6, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_OWODAllNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_AllNAN_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
            data_matrix[0,p,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.zeros((1,2,2),dtype=bool)
        msg.add('test_case_dummy',outlier_matrix)

        owod_1c = OutlierWeightedOverlapDetector()
        owod_1c.setup('owod_1c', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2),'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1, 'ignore': False,
                                                  'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_1c.start()

        out0 = np.ones((1,2),dtype=bool)
        out1 = [[0,0],[0,1]]

        result = [[], []]
        owod_1c._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

    def test_OWODAllNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_AllNAN_pol')
        data_matrix = np.empty((3,1,1), dtype=object)
        for a in range(3):
            data_matrix[a,0,0] = FitStorage()
            data_matrix[a,0,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.zeros((3,1,2),dtype=bool)
        msg.add('test_case_dummy',outlier_matrix)

        owod_1d = OutlierWeightedOverlapDetector()
        owod_1d.setup('owod_1d', None,module_opt={'antennas': np.array([0,1,2]), 'polarisations': np.array([0]),
                                                  'channels': np.arange(2),'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1, 'ignore': False,
                                                  'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_1d.start()

        out0 = np.ones((3,1),dtype=bool)
        out1 = [[0,0],[1,0],[2,0]]

        result = [[], []]
        owod_1d._new_sample(msg, result)

        self.assertEqual((3, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Anomaly not detected in forward to Archiver')

        temp0 = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp0, out1, 'Anomaly detected in forward to Archiver not as expected')

        temp1 = [x._anomaly for x in result[1]]
        self.assertTrue(np.isnan(temp1).all(), 'Anomalies should all be NaNs')

class TestOWODBuffer(TCPOTester):

    def test_OWODBufferNANs(self):
        owod_2a = OutlierWeightedOverlapDetector()
        owod_2a.setup('owod_2a', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2),'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 3, 'length': 3,
                                                  'outlier_gen': 'test_case_dummy', 'ignore': True}, archiver=(1, 1))
        owod_2a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNAN')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.array([[[True,False],[False,False]],
                                   [[True,False],[True,False]]])
        msg.add('test_case_dummy',outlier_matrix)

        result = [[], []]
        owod_2a._new_sample(msg, result)
        owod_2a._new_sample(msg, result)
        owod_2a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        clean_outlier_matrix = np.zeros((2,2,2),dtype=bool)
        msg.add('test_case_dummy', clean_outlier_matrix)
        owod_2a._new_sample(clean_msg, result)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(),
                        'Anomaly detected when none should have been')

    def test_OWODBufferNANsSingle(self):
        owod_2b = OutlierWeightedOverlapDetector()
        owod_2b.setup('owod_2b', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2), 'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1,
                                                  'outlier_gen': 'test_case_dummy', 'ignore': True}, archiver=(1, 1))
        owod_2b.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNANSingle')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.array([[[True,False],[False,False]],
                                   [[True,False],[True,False]]])
        msg.add('test_case_dummy',outlier_matrix)

        result0 = [[], []]
        owod_2b._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                clean_matrix[a,p,0] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        clean_outlier_matrix = np.zeros((2, 2, 2), dtype=bool)
        msg.add('test_case_dummy', clean_outlier_matrix)

        result1 = [[], []]
        owod_2b._new_sample(clean_msg, result1)

        out = np.zeros((2, 2), dtype=bool)

        self.assertEqual((2, 2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2, 2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_OWODBufferNANsSingle_ant(self):
        owod_2c = OutlierWeightedOverlapDetector()
        owod_2c.setup('owod_2c', None,module_opt={'antennas': np.array([0]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2), 'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1,
                                                  'outlier_gen': 'test_case_dummy', 'ignore': True}, archiver=(1, 1))
        owod_2c.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNANSingle_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix
        outlier_matrix = np.array([[[True,False],[False,False]]])
        msg.add('test_case_dummy',outlier_matrix)

        result0 = [[], []]
        owod_2c._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNAN_Clean')
        clean_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            clean_matrix[0,p,0] = FitStorage()
            clean_matrix[0,p,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        clean_outlier_matrix = np.zeros((1, 2, 2), dtype=bool)
        msg.add('test_case_dummy', clean_outlier_matrix)

        result1 = [[], []]
        owod_2c._new_sample(clean_msg, result1)

        out = np.zeros((1, 2), dtype=bool)

        self.assertEqual((1, 2), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((1, 2), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_OWODBufferNANsSingle_pol(self):
        owod_2d = OutlierWeightedOverlapDetector()
        owod_2d.setup('owod_2d', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0]),
                                                  'channels': np.arange(2), 'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1,
                                                  'outlier_gen': 'test_case_dummy', 'ignore': True}, archiver=(1, 1))
        owod_2d.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNANSingle_pol')
        data_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        msg.Default = data_matrix
        outlier_matrix = np.array([[[True,False]],
                                   [[True,False]]])
        msg.add('test_case_dummy',outlier_matrix)

        result0 = [[], []]
        owod_2d._new_sample(msg, result0)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferNAN_Clean')
        clean_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            clean_matrix[a,0,0] = FitStorage()
            clean_matrix[a,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        clean_outlier_matrix = np.zeros((2, 1, 2), dtype=bool)
        msg.add('test_case_dummy', clean_outlier_matrix)

        result1 = [[], []]
        owod_2d._new_sample(clean_msg, result1)

        out = np.zeros((2, 1), dtype=bool)

        self.assertEqual((2, 1), result0[0].shape, 'Forwarded shape to module in 1st sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result0[0], out)).all(),
                        'Anomaly detected in 1st sample when none should have been')

        self.assertEqual((2, 1), result1[0].shape, 'Forwarded shape to module in 2nd sample is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result1[0], out)).all(),
                        'Anomaly detected in 2nd sample when none should have been')

    def test_OWODBufferIgnoreFalse(self):
        owod_2e = OutlierWeightedOverlapDetector()
        owod_2e.setup('owod_2e', None,module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]),
                                                  'channels': np.arange(2), 'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                  'min_overlap': 0.75, 'max_outlier': 1, 'length': 1, 'ignore': False,
                                                  'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_2e.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferIgnoreFalse')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.array([[[True, False], [False, False]],
                                   [[True, False], [True, False]]])
        msg.add('test_case_dummy', outlier_matrix)

        result = [[], []]
        owod_2e._new_sample(msg, result)

        out0 = np.array([[True, False], [True, True]])
        out1 = [[0, 0], [1, 0], [1, 1]]

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver is not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Anomalies detected in forward to Archiver not as expected')

class TestOWODIndices(TCPOTester):

    def test_OWODIndices(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_Indices')
        data_matrix = np.empty((4,2,1), dtype=object)
        for a in range(4):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[0,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(2), 2, check_nan=True)
        data_matrix[2,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,1.0]))
        data_matrix[2,1,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[3,0,0].setup(np.arange(2), 2, check_nan=False, coeff=np.array([np.NaN,1.0,np.NaN]))
        data_matrix[3,1,0].setup(np.arange(2), 2, check_nan=True)
        msg.Default = data_matrix
        outlier_matrix = np.zeros((4,2,3),dtype=bool)
        msg.add('test_case_dummy',outlier_matrix)

        owod_3a = OutlierWeightedOverlapDetector()
        owod_3a.setup('owod_3a', None, module_opt={'antennas': np.array([0,1,5,11]), 'polarisations': np.array([2,7]),
                                                   'channels': np.arange(3), 'start': np.array([[1.0,1.0,1.0]]), 'fits':1,
                                                   'min_overlap': 0.75, 'max_outlier': 1, 'length': 1,
                                                   'outlier_gen': 'test_case_dummy'}, archiver=(1, 1))
        owod_3a.start()

        out1 = [[0,2],[1,2],[1,7],[5,2],[11,2],[11,7]]

        result = [[], []]
        owod_3a._new_sample(msg, result)

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Indices not as expected')

class TestOWODMultiFits(TCPOTester):

    # Tests buffer for when multiple piecewise fits are anomalous
    def test_buffer_multifits(self):
        owod_4a = OutlierWeightedOverlapDetector()
        owod_4a.setup('owod_4a', None, module_opt={'antennas': np.array([0,1]), 'polarisations': np.array([0,1]),
                                                   'channels': np.arange(20), 'start': np.array([[1.0,1.0,1.0],[1.0,1.0,1.0]]),
                                                   'fits': 2, 'min_overlap': 0.75, 'max_outlier': 3, 'length': 3,
                                                   'outlier_gen': 'test_case_dummy', 'ignore': True}, archiver=(1, 1))
        owod_4a.start()

        msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferFits')
        data_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    data_matrix[a,p,f] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,0.0]))
        data_matrix[0,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([np.NaN,0.0,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(5), 2, check_nan=True)
        data_matrix[0,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[0,1,2].setup(np.arange(15,20), 2, check_nan=True)
        data_matrix[1,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        data_matrix[1,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        msg.Default = data_matrix
        outlier_matrix = np.array([[np.concatenate((np.ones((10,), dtype=bool), np.zeros((10,), dtype=bool))).tolist(),
                                    np.zeros((20,), dtype=bool).tolist()],
                                   [np.concatenate((np.ones((10,), dtype=bool), np.zeros((10,), dtype=bool))).tolist(),
                                    np.concatenate((np.ones((10,), dtype=bool), np.zeros((10,), dtype=bool))).tolist()]])
        msg.add('test_case_dummy', outlier_matrix)

        result = [[], []]
        owod_4a._new_sample(msg, result)
        owod_4a._new_sample(msg, result)
        owod_4a._new_sample(msg, result)

        clean_msg = Message((Message.SelType.NONE, 0), 0, 0, 'OWOD_BufferFits_Clean')
        clean_matrix = np.empty((2,2,3), dtype=object)
        for a in range(2):
            for p in range(2):
                for f in [0,2]:
                    clean_matrix[a,p,f] = FitStorage()
                clean_matrix[a,p,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
                clean_matrix[a,p,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1.0,1.0,1.0]))
        clean_msg.Default = clean_matrix
        clean_outlier_matrix = np.zeros((2,2,20), dtype=bool)
        msg.add('test_case_dummy', clean_outlier_matrix)
        owod_4a._new_sample(clean_msg, result)

        out = np.zeros((2,2), dtype=bool)

        self.assertEqual((2,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out)).all(), 'Anomaly detected when none should have been')