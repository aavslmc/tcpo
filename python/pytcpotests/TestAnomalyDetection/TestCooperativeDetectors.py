import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message
from pytcpo.Modules.AnomalyDetectors.Implementations.CooperativeDetectors import InterAntennaDeviationDetector,\
    InterAntennaOverlapDetector
from pytcpo.Modules.AnomalyDetectors.Common import FlaggedAnomaly
from pytcpo.Core.Common import npalt
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage

class TestIADDExtreme(TCPOTester):

    def test_extremeNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_extremeNAN')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 0, check_nan=True)
        msg.Default = data_matrix

        iadd_1a = InterAntennaDeviationDetector()
        iadd_1a.setup('iadd_1a', None, module_opt={'antennas': (0,), 'polarisations': (0,), 'deviation': np.array([6.0]),
                                                   'fit_orders': np.array([0]), 'weight': np.array([[1.0]])}, archiver = (1,1))
        iadd_1a.start()

        out0 = np.array([[True]])
        out1 = FlaggedAnomaly().set(0, 0, np.array([np.NaN]))

        result = [[], []]
        iadd_1a._new_sample(msg, result)

        self.assertEqual((1,1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0],out0)).all(), 'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')
        self.assertTrue(npalt.array_nan_equal(out1._anomaly, result[1][0]._anomaly), 'Anomaly not as expected in forward to Archiver')

    def test_extremeDeviation(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_extremeDeviation')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 0, check_nan=False, coeff=np.array([0]))
        msg.Default = data_matrix

        iadd_1b = InterAntennaDeviationDetector()
        iadd_1b.setup('iadd_1b', None, module_opt={'antennas': (0,), 'polarisations': (0,), 'deviation': np.array([0.0]),
                                                   'fit_orders': np.array([0]), 'weight': np.array([[1.0]])}, archiver = (1,1))
        iadd_1b.start()

        out0 = np.array([[False]])

        result = [[], []]
        iadd_1b._new_sample(msg, result)

        self.assertEqual((1,1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 0,'Number of anomalies detected in forward to Archiver not as expected - No anomalies should have been detected')

class TestIADDallNan(TCPOTester):

    def test_allNAN(self):
        msg = Message((Message.SelType.NONE, 0),0,0,'IADD_allNAN')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
                data_matrix[a,p,0].setup(np.arange(10), 1, check_nan=True)
        msg.Default = data_matrix

        iadd_2 = InterAntennaDeviationDetector()
        iadd_2.setup('iadd_2', None, module_opt = {'antennas':(0,1), 'polarisations':(0,1),'deviation' : np.array([6.0]),
                                                   'fit_orders': np.array([1]), 'weight': np.array([[1.0,1.0]])}, archiver = (1,1))
        iadd_2.start()

        out0 = np.ones((2,2),dtype=bool)
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(0, 1, np.array([np.NaN])),
                FlaggedAnomaly().set(1, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(1, 1, np.array([np.NaN]))]

        result = [[],[]]
        iadd_2._new_sample(msg, result)

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]),4, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(4):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(npalt.array_nan_equal(out1[x]._anomaly, y._anomaly), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_allNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_allNAN')
        data_matrix = np.empty((1, 2, 1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
            data_matrix[0,p,0].setup(np.arange(10), 1, check_nan=True)
        msg.Default = data_matrix

        iadd_2a = InterAntennaDeviationDetector()
        iadd_2a.setup('iadd_2a', None, module_opt={'antennas': (0,), 'polarisations': (0,1),'deviation': np.array([6.0]),
                                                   'fit_orders': np.array([1]), 'weight': np.array([[1.0,1.0]])}, archiver = (1,1))
        iadd_2a.start()

        out0 = np.array([[True,True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(0, 1, np.array([np.NaN]))]

        result = [[], []]
        iadd_2a._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(npalt.array_nan_equal(out1[x]._anomaly, y._anomaly), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_allNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_allNAN')
        data_matrix = np.empty((2, 1, 1), dtype=object)
        for a in range(2):
            data_matrix[a, 0, 0] = FitStorage()
            data_matrix[a, 0, 0].setup(np.arange(10), 1, check_nan=True)
        msg.Default = data_matrix

        iadd_2p = InterAntennaDeviationDetector()
        iadd_2p.setup('iadd_2p', None, module_opt={'antennas': (0,1), 'polarisations':(0,), 'deviation': np.array([6.0]),
                                                   'fit_orders': np.array([1]), 'weight': np.array([[1.0,1.0]])}, archiver = (1,1))
        iadd_2p.start()

        out0 = np.array([[True],[True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(1, 0, np.array([np.NaN]))]

        result = [[], []]
        iadd_2p._new_sample(msg, result)

        self.assertEqual((2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(npalt.array_nan_equal(out1[x]._anomaly, y._anomaly), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count, 1, 'Either one anomaly is duplicate or missing in forward to Archiver')

class TestIADDmixedNAN(TCPOTester):

    def test_mixedNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_mixedNAN')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,2,0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,1,np.NaN]))
        data_matrix[2,0,0].setup(np.arange(10), 2, check_nan=True)
        data_matrix[2,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,0]))
        msg.Default = data_matrix

        iadd_3 = InterAntennaDeviationDetector()
        iadd_3.setup('iadd_3', None, module_opt={'antennas': (0,1,2), 'polarisations': (0,1),'deviation': np.array([6.0]),
                                                 'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_3.start()

        out0 = np.array([[False,False],[True,True],[True,False]])
        out1 = [FlaggedAnomaly().set(1, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(1, 1, np.array([np.NaN])),
                FlaggedAnomaly().set(2, 0, np.array([np.NaN]))]

        result = [[], []]
        iadd_3._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(3):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_mixedNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_mixedNAN_ant')
        data_matrix = np.empty((1, 2, 1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1, np.NaN]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,1, np.NaN]))
        msg.Default = data_matrix

        iadd_3a = InterAntennaDeviationDetector()
        iadd_3a.setup('iadd_3a', None, module_opt={'antennas': (0,), 'polarisations': (0,1),'deviation': np.array([6.0]),
                                                   'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_3a.start()

        out0 = np.array([[True,True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.NaN])),
                FlaggedAnomaly().set(0, 1, np.array([np.NaN]))]

        result = [[], []]
        iadd_3a._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_mixedNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_mixedNAN_pol')
        data_matrix = np.empty((2, 1, 1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,np.NaN]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,1,np.NaN]))
        msg.Default = data_matrix

        iadd_3p = InterAntennaDeviationDetector()
        iadd_3p.setup('iadd_3p', None, module_opt={'antennas': (0,1), 'polarisations': (0,), 'deviation': np.array([6.0]),
                                                   'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_3p.start()

        out0 = np.array([[True],[True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.NaN])),
               FlaggedAnomaly().set(1, 0, np.array([np.NaN]))]

        result = [[], []]
        iadd_3p._new_sample(msg, result)

        self.assertEqual((2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

class TestIADDDeviation(TCPOTester):

    def test_Deviation(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_Deviation')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,2,0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,0]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,1,0]))
        data_matrix[2,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([0,0,0]))
        data_matrix[2,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,-1]))
        msg.Default = data_matrix

        iadd_4 = InterAntennaDeviationDetector()
        iadd_4.setup('iadd_4', None, module_opt={'antennas': (0,1,2), 'polarisations': (0,1), 'deviation': np.array([0.75]),
                                                 'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_4.start()

        out0 = np.array([[True,True],[False,False],[True,False]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.sqrt(366)/18.0])),
                FlaggedAnomaly().set(0, 1, np.array([np.sqrt(186)/18.0])),
                FlaggedAnomaly().set(2, 0, np.array([np.sqrt(258)/18.0]))]

        result = [[], []]
        iadd_4._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(3):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_Deviation_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_Deviation_ant')
        data_matrix = np.empty((1, 2, 1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,2,0]))
        msg.Default = data_matrix

        iadd_4a = InterAntennaDeviationDetector()
        iadd_4a.setup('iadd_4a', None, module_opt={'antennas': (0,), 'polarisations': (0,1), 'deviation': np.array([0.7]),
                                                   'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_4a.start()

        out0 = np.array([[True,True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.sqrt(2)/2.0])),
                FlaggedAnomaly().set(0, 1, np.array([np.sqrt(2)/2.0]))]

        result = [[], []]
        iadd_4a._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

    def test_Deviation_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_Deviation_pol')
        data_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,2,0]))
        msg.Default = data_matrix

        iadd_4p = InterAntennaDeviationDetector()
        iadd_4p.setup('iadd_4p', None, module_opt={'antennas': (0,1), 'polarisations': (0,), 'deviation': np.array([0.7]),
                                                   'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_4p.start()

        out0 = np.array([[True],[True]])
        out1 = [FlaggedAnomaly().set(0, 0, np.array([np.sqrt(2)/2.0])),
               FlaggedAnomaly().set(1, 0, np.array([np.sqrt(2)/2.0]))]

        result = [[], []]
        iadd_4p._new_sample(msg, result)

        self.assertEqual((2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        for x in range(2):
            count = 0
            for y in result[1]:
                if (out1[x]._ant == y._ant) and (out1[x]._pol == y._pol):
                    self.assertTrue(np.allclose(out1[x]._anomaly, y._anomaly, equal_nan=True), 'Anomaly not as expected in forward to Archiver')
                    count += 1
            self.assertEqual(count,1, 'Either one anomaly is duplicate or missing in forward to Archiver')

class TestIADDIndices(TCPOTester):

    def test_IADDIndices(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_Indices')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,2,0]))
        data_matrix[1,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,0]))
        data_matrix[1,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([2,1,0]))
        data_matrix[2,0,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([0,0,0]))
        data_matrix[2,1,0].setup(np.arange(10), 2, check_nan=False, coeff=np.array([1,1,-1]))
        msg.Default = data_matrix

        iadd_5a = InterAntennaDeviationDetector()
        iadd_5a.setup('iadd_5a', None, module_opt={'antennas': (0,2,5), 'polarisations': (1,2), 'deviation': np.array([0.75]),
                                                   'fit_orders': np.array([2]), 'weight': np.array([[1.0,1.0,1.0]])}, archiver = (1,1))
        iadd_5a.start()

        out1 = [[0, 1], [0, 2], [5, 1]]

        result = [[], []]
        iadd_5a._new_sample(msg, result)

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1,'Indices not as expected')

class TestIADDMultiFits(TCPOTester):

    # Tests flagging when only one part of a piecewise fit is anomalous
    def test_single_anomalous_fit(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IADD_SingleAnomFit')
        data_matrix = np.empty((3,2,3), dtype=object)
        for a in range(3):
            for p in range(2):
                for f in [0,2]:
                    data_matrix[a,p,f] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([2,2,0]))
        data_matrix[1,0,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1,1,np.NaN]))
        data_matrix[1,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([2,1,np.NaN]))
        data_matrix[2,0,0].setup(np.arange(5), 2, check_nan=True)
        data_matrix[2,1,0].setup(np.arange(5), 2, check_nan=False, coeff=np.array([1,1,0]))
        data_matrix[0,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1,1,2]))
        data_matrix[0,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([2,2,0]))
        data_matrix[1,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1,1,0]))
        data_matrix[1,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([2,1,0]))
        data_matrix[2,0,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([0,0,0]))
        data_matrix[2,1,2].setup(np.arange(15,20), 2, check_nan=False, coeff=np.array([1,1,-1]))
        msg.Default = data_matrix

        iadd_6a = InterAntennaDeviationDetector()
        iadd_6a.setup('iadd_6a', None, module_opt={'antennas': (0,1,2), 'polarisations': (0,1), 'deviation': np.array([2,2]),
                                 'fit_orders': np.array([2,2]), 'weight': np.array([[1.0,1.0,1.0],[1.0,1.0,1.0]])}, archiver=(1, 1))
        iadd_6a.start()

        out0 = np.array([[False,False],[True,True],[True,False]])

        result = [[], []]
        iadd_6a._new_sample(msg, result)

        self.assertEqual((3,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

class TestIAODExtreme(TCPOTester):

    # Note that we are not interested in the fact that the anomaly is correct, only that it is detected. The anomaly is
    # determined by the similarity ratio, calculated in a seperately tested module.

    def test_extremeNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_extremeNAN')
        data_matrix = np.empty((1,1,1), dtype=object)
        data_matrix[0,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_1 = InterAntennaOverlapDetector()
        iaod_1.setup('iaod_1', None, module_opt={'antennas': (0,), 'polarisations': (0,), 'fit_orders': np.array([0]),
                                                 'min_overlap': 0.95, 'similarity':0.75}, archiver = (1,1))
        iaod_1.start()

        out0 = np.array([[True]])

        result = [[], []]
        iaod_1._new_sample(msg, result)

        self.assertEqual((1, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Anomaly not detected in forward to Archiver')

class TestIAODallNAN(TCPOTester):

    # Note that we are not interested in the fact that the anomaly is correct, only that it is detected. The anomaly is
    # determined by the similarity ratio, calculated in a seperately tested module.

    def test_allNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_allNAN')
        data_matrix = np.empty((2,2,1), dtype=object)
        for a in range(2):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
                data_matrix[a,p,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_2 = InterAntennaOverlapDetector()
        iaod_2.setup('iaod_2', None, module_opt={'antennas': (0,1), 'polarisations': (0,1), 'fit_orders': np.array([0]),
                                                 'min_overlap': 0.95, 'similarity': 0.75}, archiver=(1,1))
        iaod_2.start()

        out0 = np.ones((2,2),dtype=bool)
        out1 = [[0,0],[0,1],[1,0],[1,1]]

        result = [[], []]
        iaod_2._new_sample(msg, result)

        self.assertEqual((2, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 4, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

    def test_allNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_allNAN_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
            data_matrix[0,p,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_2a = InterAntennaOverlapDetector()
        iaod_2a.setup('iaod_2a', None, module_opt={'antennas': (0,), 'polarisations': (0,1), 'fit_orders': np.array([0]),
                                                   'min_overlap': 0.95, 'similarity': 0.75}, archiver=(1,1))
        iaod_2a.start()

        out0 = np.array([[True,True]])
        out1 = [[0,0],[0,1]]

        result = [[], []]
        iaod_2a._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

    def test_allNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_allNAN_pol')
        data_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
            data_matrix[a,0,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_2p = InterAntennaOverlapDetector()
        iaod_2p.setup('iaod_2p', None, module_opt={'antennas': (0,1), 'polarisations': (0,), 'fit_orders': np.array([0]),
                                                   'min_overlap': 0.95, 'similarity': 0.75}, archiver = (1,1))
        iaod_2p.start()

        out0 = np.array([[True],[True]])
        out1 = [[0,0],[1,0]]

        result = [[], []]
        iaod_2p._new_sample(msg, result)

        self.assertEqual((2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 2, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

class TestIAODmixedNAN(TCPOTester):

    def test_mixedNAN(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_mixedNAN')
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[0,1,0].setup(np.arange(2), 0, check_nan=True)
        data_matrix[1,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,1,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[2,0,0].setup(np.arange(2), 0, check_nan=True)
        data_matrix[2,1,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_3 = InterAntennaOverlapDetector()
        iaod_3.setup('iaod_3', None, module_opt={'antennas': (0,1,2), 'polarisations': (0,1), 'fit_orders': np.array([0]),
                                                 'min_overlap': 0.95, 'similarity': 0.75}, archiver = (1,1))
        iaod_3.start()

        out0 = np.array([[False,True],[False,False],[True,True]])
        out1 = [[0,1],[2,0],[2,1]]

        result = [[], []]
        iaod_3._new_sample(msg, result)

        self.assertEqual((3, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

    def test_mixedNAN_ant(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_mixedNAN_ant')
        data_matrix = np.empty((1,2,1), dtype=object)
        for p in range(2):
            data_matrix[0,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[0,1,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_3a = InterAntennaOverlapDetector()
        iaod_3a.setup('iaod_3a', None, module_opt={'antennas': (0,), 'polarisations': (0,1), 'fit_orders': np.array([0]),
                                                   'min_overlap': 0.95, 'similarity': 0.75}, archiver = (1,1))
        iaod_3a.start()

        out0 = np.array([[False,True]])
        out1 = [[0,1]]

        result = [[], []]
        iaod_3a._new_sample(msg, result)

        self.assertEqual((1, 2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

    def test_mixedNAN_pol(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_mixedNAN_pol')
        data_matrix = np.empty((2,1,1), dtype=object)
        for a in range(2):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,0,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_3p = InterAntennaOverlapDetector()
        iaod_3p.setup('iaod_3p', None, module_opt={'antennas': (0,1), 'polarisations': (0,), 'fit_orders': np.array([0]),
                                                   'min_overlap': 0.95, 'similarity': 0.75}, archiver = (1,1))
        iaod_3p.start()

        out0 = np.array([[False],[True]])
        out1 = [[1, 0]]

        result = [[], []]
        iaod_3p._new_sample(msg, result)

        self.assertEqual((2, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 1, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

class TestIAODThresholds(TCPOTester):

    def test_Thresholds(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_Thresholds')
        data_matrix = np.empty((5,1,1), dtype=object)
        for a in range(5):
            data_matrix[a,0,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([10.0]))
        data_matrix[1,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([9.0]))
        data_matrix[2,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([8.0]))
        data_matrix[3,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([7.0]))
        data_matrix[4,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([6.0]))
        msg.Default = data_matrix

        iaod_4 = InterAntennaOverlapDetector()
        iaod_4.setup('iaod_4', None, module_opt={'antennas': (0,1,2,3,4), 'polarisations': (0,), 'fit_orders': np.array([0]),
                                                 'min_overlap': 0.87, 'similarity': 0.6}, archiver = (1,1))
        iaod_4.start()

        out0 = np.array([[True],[False],[False],[True],[True]])
        out1 = [[0,0],[3,0], [4,0]]

        result = [[],[]]
        iaod_4._new_sample(msg, result)

        self.assertEqual((5, 1), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)

class TestIAODIndices(TCPOTester):

    def test_IAODIndices(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_Indices')
        data_matrix = np.array([[[5], [np.NaN]],
                                [[5], [5]],
                                [[np.NaN], [np.NaN]]])
        data_matrix = np.empty((3,2,1), dtype=object)
        for a in range(3):
            for p in range(2):
                data_matrix[a,p,0] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[0,1,0].setup(np.arange(2), 0, check_nan=True)
        data_matrix[1,0,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,1,0].setup(np.arange(2), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[2,0,0].setup(np.arange(2), 0, check_nan=True)
        data_matrix[2,1,0].setup(np.arange(2), 0, check_nan=True)
        msg.Default = data_matrix

        iaod_5 = InterAntennaOverlapDetector()
        iaod_5.setup('iaod_5', None, module_opt={'antennas': (2, 3, 8), 'polarisations': (1, 3), 'fit_orders': np.array([0]),
                                 'min_overlap': 0.95, 'similarity': 0.75}, archiver=(1, 1))
        iaod_5.start()

        out1 = [[2, 3], [8, 1], [8, 3]]

        result = [[], []]
        iaod_5._new_sample(msg, result)

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1, 'Indices not as expected')

class TestIAODMultiFits(TCPOTester):

    # Tests flagging when only one part of a piecewise fit is anomalous
    def test_single_anomalous_fit(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'IAOD_SingleAnomFit')
        data_matrix = np.empty((3,2,3), dtype=object)
        for a in range(3):
            for p in range(2):
                for f in [0,2]:
                    data_matrix[a,p,f] = FitStorage()
        data_matrix[0,0,0].setup(np.arange(5), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[0,1,0].setup(np.arange(5), 0, check_nan=True)
        data_matrix[1,0,0].setup(np.arange(5), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,1,0].setup(np.arange(5), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[2,0,0].setup(np.arange(5), 0, check_nan=True)
        data_matrix[2,1,0].setup(np.arange(5), 0, check_nan=True)
        data_matrix[0,0,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[0,1,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,0,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[1,1,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[2,0,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        data_matrix[2,1,2].setup(np.arange(15,20), 0, check_nan=False, coeff=np.array([5]))
        msg.Default = data_matrix

        iaod_6a = InterAntennaOverlapDetector()
        iaod_6a.setup('iaod_6a', None, module_opt={'antennas': (0,1,2), 'polarisations': (0,1), 'fit_orders': np.array([0,0]),
                                 'min_overlap': 0.95, 'similarity': 0.75}, archiver=(1, 1))
        iaod_6a.start()

        out0 = np.array([[False,True],[False,False],[True,True]])
        out1 = [[0,1], [2,0], [2,1]]

        result = [[], []]
        iaod_6a._new_sample(msg, result)

        self.assertEqual((3,2), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out0)).all(),
                        'Anomaly array forwarded to module not as expected')
        self.assertEqual(len(result[1]), 3, 'Number of anomalies detected in forward to Archiver not as expected')

        temp = [[x._ant, x._pol] for x in result[1]]
        self.checkElementOf(temp, out1)