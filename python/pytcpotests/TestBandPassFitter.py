import numpy as np
import Queue as Qu

from pytcpotests import TCPOTester
from pytcpo.Core.Common import npalt
from pytcpo.Core.PipeLine import Message
from pytcpo.Modules.BandPassModel import PolynFitter, TwoPassPolyFit, PiecewiseMeanFitter, PiecewisePolyFitter

class TestPiecewisePolyFitter(TCPOTester):

    def test_correct_channels_multifits(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PPF_Channels_MultiFits')
        data_matrix = np.array([[range(80), range(0,160,2)],
                                [range(80), range(0,160,2)]])
        msg.Default = data_matrix

        ppf_a = PiecewisePolyFitter()
        ppf_a.setup('ppf_a',None, module_opt={'antennas':2, 'polarisations':2, 'channels':np.arange(80),
                                              'fits':[[1,np.arange(15)],[1,np.arange(5,30)],[1,np.arange(20,80)]]},
                    archiver=(1, 1))
        ppf_a.start()

        x = [np.arange(5),np.arange(5,15),np.arange(15,20),np.arange(20,30),np.arange(30,80)]
        out = np.array([[x,x],[x,x]])

        result = [[],[]]
        ppf_a._new_sample(msg,result)

        self.assertEqual((2,2,5), result[0].shape, 'Shape forwarded to next module not as expected')
        self.assertEqual((2,2,5), result[1].shape, 'Shape forwarded to archiver not as expected')

        for a in range(2):
            for p in range(2):
                for f in range(5):
                    self.checkEqual(out[a,p,f], result[0][a,p,f].ret_chans(), 'Channels forwarded to next module in FitStorage not as expected')
                    self.checkEqual(out[a,p,f], result[1][a,p,f].ret_chans(), 'Channels forwarded to archiver in FitStorage not as expected')

    def test_nan_handling(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PPF_NaN_Handling')
        data_matrix = np.array([[[1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,np.NaN,10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0],
                                 [1.0,2.0,5.0,8.0,np.NaN,10.0,11.0,13.0,15.0,17.0,np.NaN,np.NaN,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0]],
                                [[np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,
                                  np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN,np.NaN],
                                 [1.0,2.0,5.0,8.0,9.0,10.0,11.0,13.0,15.0,17.0,19.0,20.0,21.0,22.0,23.0,24.0,25.0,26.0,27.0,28.0]]])
        msg.Default = data_matrix

        ppf_c = PiecewisePolyFitter()
        ppf_c.setup('ppf_c',None, module_opt={'antennas':2, 'polarisations':2, 'channels':np.arange(20),
                                              'fits':[[5,np.arange(15)],[7,np.arange(5,20)]]},archiver=(1, 1))
        ppf_c.start()

        out0 = np.array([[[np.arange(5),np.arange(5,15),np.arange(15,20)],[np.arange(5),np.arange(5,15),np.arange(15,20)]],
                         [[np.arange(5),np.arange(5,15),np.arange(15,20)],[np.arange(5),np.arange(5,15),np.arange(15,20)]]])

        out1 = np.array([[[True,True,True],[True,True,True]],[[True,True,True],[False,False,False]]])

        result = [[],[]]
        ppf_c._new_sample(msg,result)

        self.assertEqual((2,2,3), result[0].shape, 'Shape forwarded to next module not as expected')
        self.assertEqual((2,2,3), result[1].shape, 'Shape forwarded to archiver not as expected')

        for a in range(2):
            for p in range(2):
                for f in range(3):
                    self.checkEqual(out0[a,p,f], result[0][a,p,f].ret_chans(), 'Channels forwarded to next module in FitStorage not as expected')
                    self.checkEqual(out0[a,p,f], result[1][a,p,f].ret_chans(), 'Channels forwarded to archiver in FitStorage not as expected')
                    self.assertEqual(out1[a,p,f], result[0][a,p,f].ret_isnan(), 'Incorrect FitStorage isnan flagging in forward to next module')
                    self.assertEqual(out1[a,p,f], result[0][a,p,f].ret_isnan(), 'Incorrect FitStorage isnan flagging in forward to archiver')

        wrong_coeff_module = 'Coefficients not as expected in forward to next module'
        wrong_coeff_archiver = 'Coefficients not as expected in forward to archiver'
        nan2 = np.ones((2,), dtype=bool)
        nan6 = np.ones((6,), dtype=bool)
        nan8 = np.ones((8,), dtype=bool)

        for x in [(0,0),(0,1),(1,0)]:
            self.checkEqual(np.isnan(result[0][x][0].ret_coeff()), nan6, wrong_coeff_module)
            self.checkEqual(np.isnan(result[1][x][0].ret_coeff()), nan6, wrong_coeff_archiver)
            self.checkEqual(np.isnan(result[0][x][1].ret_coeff()), nan2, wrong_coeff_module)
            self.checkEqual(np.isnan(result[1][x][1].ret_coeff()), nan2, wrong_coeff_archiver)
            self.checkEqual(np.isnan(result[0][x][2].ret_coeff()), nan8, wrong_coeff_module)
            self.checkEqual(np.isnan(result[1][x][2].ret_coeff()), nan8, wrong_coeff_archiver)

    def test_overlapping_channels(self):
        ppf_d = PiecewisePolyFitter()

        self.assertRaises(Exception, lambda: ppf_d.setup('ppf_d',None, module_opt={'antennas':2, 'polarisations':2,
                                                                                   'channels':np.arange(20),
                                                                                   'fits':[[1,np.arange(16)],
                                                                                           [1,np.arange(5,20)]]},
                                                         archiver=(1, 1)))

        self.assertRaises(Exception, lambda: ppf_d.setup('ppf_d', None, module_opt={'antennas': 2, 'polarisations': 2,
                                                                            'channels': np.arange(20),
                                                                            'fits': [[1,np.arange(15)],
                                                                                     [1,np.arange(6,20)]]},
                                                         archiver=(1, 1)))

        self.assertRaises(Exception, lambda: ppf_d.setup('ppf_d', None, module_opt={'antennas': 2, 'polarisations': 2,
                                                                                    'channels': np.arange(80),
                                                                                    'fits': [[1,np.arange(15)],
                                                                                             [1,np.arange(6,29)],
                                                                                             [1,np.arange(20,80)]]},
                                                         archiver=(1, 1)))


class TestTwoStepPolyFit(TCPOTester):

    def test_outlier_rejection(self):
        tppf = TwoPassPolyFit()
        pf = PolynFitter()

        # Setup
        self.assertNoException(BaseException, tppf.setup, 'Exception in TPPF Setup', None, Qu.Queue,
                               {'order': 2, 'antennas': 1, 'polaris': 1, 'reject_std':2.0, 'channels': np.arange(20)})
        self.assertNoException(BaseException, pf.setup, 'Exception in PF Setup', None, Qu.Queue,
                               {'order': 2, 'antennas': 1, 'polaris': 1, 'channels': np.arange(20)})

        # Try with a clean input
        a = np.asarray([[np.polyval([2.0, 1.0, -5.0], np.arange(20))]])
        m_tppf = [None, None]
        m_pf = [None, None]
        tppf._new_sample(Message((Message.SelType.NONE, 0), 0, 0.0, default=a), m_tppf)
        pf._new_sample(Message((Message.SelType.NONE, 0), 0, 0.0, default=a), m_pf)

        self.assertTrue(np.allclose(m_tppf[0][0,0,0].ret_coeff(), [-5.0, 1.0, 2.0]))
        self.assertTrue(np.allclose(m_pf[0][0,0,0].ret_coeff(), [-5.0, 1.0, 2.0]))


class TestMeanFitter(TCPOTester):

    def setUp(self):
        super(TestMeanFitter, self).setUp()
        self.pmf = PiecewiseMeanFitter()
        self.assertNoException(BaseException, self.pmf.setup, 'Exception in PMF Setup', 'pmf', Qu.Queue,
                               {'pieces': 4, 'sizes': 6, 'outlier': 'outlier'})
        self.assertNoException(BaseException, self.pmf.start, 'Exception in PMF Start')

    def test_correct_size_handling(self):
        # Forwarding
        forward = [None, None]

        # Create (singleton) Message
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[1,1,24]))
        msg.add('outlier', np.zeros(shape=[1,1,24], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([1,1,4], forward[0].shape))

        # Now create singular Antenna Dimension only
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[1, 5, 24]))
        msg.add('outlier', np.zeros(shape=[1, 5, 24], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([1, 5, 4], forward[0].shape))

        # Now create singular Polarisation Dimension only
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[3, 1, 24]))
        msg.add('outlier', np.zeros(shape=[3, 1, 24], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([3, 1, 4], forward[0].shape))

        # Finally create wrong channel data - should not raise exceptions but give nan for extension off the end
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[1, 1, 10]))
        msg.add('outlier', np.zeros(shape=[1, 1, 10], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(npalt.array_nan_equal(np.asarray([[[1,1,np.NaN, np.NaN]]]), forward[0]))

    def test_correct_means(self):
        # Forward
        forward = [None, None]
        outlier_data = np.zeros(shape=[1, 1, 24], dtype=bool)

        # No Outliers detected)
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[np.arange(24.0)]]))
        msg.add('outlier', outlier_data)
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[2.5, 8.5, 14.5, 20.5]]], forward[0]))

        # Outliers detected
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[np.arange(24.0)]]))
        outlier_data[0, 0, 3] = True
        outlier_data[0, 0, 9] = True
        msg.add('outlier', outlier_data)
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[2.4, 8.4, 14.5, 20.5]]], forward[0]))

    def test_invalid_means(self):
        # Forward
        forward = [None, None]
        outlier_data = np.zeros(shape=[1, 1, 24], dtype=bool)

        # All Outliers in one range
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[np.arange(24.0)]]))
        outlier_data[0,0,0:6] = True
        msg.add('outlier', outlier_data)
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(npalt.array_nan_equal([[[np.nan, 8.5, 14.5, 20.5]]], forward[0]))

        # NaN in channel data
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[np.arange(24.0)]]))
        msg.Default[0,0,3] = np.NaN
        msg.add('outlier', np.zeros(shape=[1, 1, 24], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[2.4, 8.5, 14.5, 20.5]]], forward[0]))

        # All NaN in one group of channel data
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[np.arange(24.0)]]))
        msg.Default[0, 0, 6:12] = np.NaN
        msg.add('outlier', np.zeros(shape=[1, 1, 24], dtype=bool))
        self.assertNoException(BaseException, self.pmf._new_sample, 'Exception in PMF _new_sample', msg, forward)
        self.assertTrue(npalt.array_nan_equal([[[2.5, np.NaN, 14.5, 20.5]]], forward[0]))