# Test Cases for the Channelizer-output stage reader.
import os
import glob
import time as tm
import numpy as np
import Queue as Qu

from pydaq.persisters import ChannelFormatFileManager as CFFM
from pydaq.persisters import FileDAQModes

from pytcpotests import TCPOTester, ThreadReturner
from pytcpo.Modules.RawFilters import BaseSelector
from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode, TCPOException
from pytcpo.Modules.IO import ChannelStageReader as CSAR
from pytcpo.Core.PipeLine import Message

# Use Prime Numbers
ANTENNAS = 7
POLARIS = 2
CHANNELS = 11
SAMPLES = 13
TILES = 5


class SampleSummary:

    def __init__(self, tile, idx, ts, data, consumer=-1):
        self._tile = tile
        self._idx = idx
        self._ts = ts
        self._data = data
        self._cons = consumer


class DummyConsumer(AbstractProcessor):

    def __init__(self, sleep_time = None):
        super(DummyConsumer, self).__init__()
        self.samples = [] # Samples Read so Far: a list of SampleSummary objects
        self.done_code = 0
        self.delay = sleep_time # Optionally sleep to delay
        self.tiles = None

    def _setup(self, filter_model):
        return True

    def _will_start(self):
        return True

    def _new_sample(self, message, forward):
        self.samples.append(SampleSummary(message.Selector, message.Index, message.Time, message.Default))
        if self.delay is not None: tm.sleep(self.delay);
        return True

    def _clean_up(self, reason):
        self.done_code = self.done_code*10 + reason #If called multiple times, will be caught

    def byTile(self, num):
        if self.tiles is not None: return self.tiles
        else:
            self.tiles = [[] for _ in range(num)]
            for sample in self.samples: self.tiles[sample._tile].append(sample)
            return self.tiles


# Like the Dummy Consumer, but appends instead to a global sample list: will preserve and show ordering
class GlobalConsumer(AbstractProcessor):

    Samples = []

    def __init__(self, id):
        super(GlobalConsumer, self).__init__()
        self.ID = id
        self.done_code = 0

    def _setup(self, filter_model):
        return True

    def _will_start(self):
        return True

    def _new_sample(self, message, forward):
        GlobalConsumer.Samples.append(SampleSummary(message.Selector, message.Index, message.Time, message.Default, self.ID))
        return True

    def _clean_up(self, reason):
        self.done_code = self.done_code * 10 + reason  # If called multiple times, will be caught


# Test the Basic Functionality (error Checking etc...) mainly in a static setting
class TestCSRBasics(TCPOTester):

    def __init__(self, methodName='runTest'):
        super(TestCSRBasics, self).__init__(methodName=methodName)
        self.dcsar = None

    def setUp(self):
        super(TestCSRBasics, self).setUp()
        self.dcsar = CSAR()

    def tearDown(self):
        self.dcsar.stop(TCPOCode.Error.GENERIC)
        for f in glob.glob(os.path.join(".", "*.lock")): os.remove(f)
        for f in glob.glob(os.path.join(".", "*.hdf5")): os.remove(f)
        super(TestCSRBasics, self).tearDown()

    def test_error_handling_setup(self):
        self.assertRaises(BaseException, self.dcsar.setup, 'dcsar', Qu.Queue())
        self.assertRaises(KeyError, self.dcsar.setup, 'dcsar', Qu.Queue(), {'path': '.', 'samples': None})
        self.assertFalse(self.dcsar.Active)

    def test_simple_run(self):
        dc1 = DummyConsumer()
        dc2 = DummyConsumer()
        queue = Qu.Queue()

        cffm = CFFM(root_path=".", data_type="uint16", daq_mode=FileDAQModes.Integrated)
        cffm.set_metadata(n_chans=2, n_antennas=3, n_pols=5, n_samples=1)
        cffm.ingest_data(np.ones((2, 3, 5)), 1000, False, 1, 0)

        # Setup
        dc1.done_code = 0
        dc2.done_code = 0
        self.assertTrue(self.dcsar.setup('dcsar', queue, {'path': '.', 'time': 1000, 'tiles': 0}))

        # Register Consumer(s)
        self.assertTrue(self.dcsar.registerConsumer(dc1))
        self.assertTrue(self.dcsar.registerConsumer(dc2))
        self.assertTrue(dc1.setup('dc1', queue))
        self.assertTrue(dc2.setup('dc1', queue))

        # Now Run (in non-static mode)
        self.assertTrue(dc1.start())
        self.assertTrue(dc2.start())
        self.assertTrue(self.dcsar.start())
        self.assertTrue(self.dcsar.Active)
        tm.sleep(0.2)
        self.dcsar.stop(TCPOCode.Request.STOP_USER)
        self.assertFalse(self.dcsar.Active)

        # Check that data retrieved
        self.assertEqual(len(dc1.samples), 1)
        self.assertEqual(len(dc2.samples), 1)

    def test_singular_dimensions(self):
        tcsr = CSAR()
        dc = DummyConsumer()
        arg_list = [7,5,3] # Channels, Antennas, Pols
        cffm = CFFM(root_path=".", data_type="uint32", daq_mode=FileDAQModes.Integrated)
        queue = Qu.Queue()

        for i in range(3):
            tmp = arg_list[i]
            arg_list[i] = 1
            # Create File with specifications
            cffm.set_metadata(n_chans=arg_list[0], n_antennas=arg_list[1], n_pols=arg_list[2], n_samples=2)
            #Create and Write Data
            data = np.empty((arg_list[0], 2, arg_list[1], arg_list[2]), dtype=np.uint32)
            for chn in range(arg_list[0]):
                for smp in range(2):
                    for ant in range(arg_list[1]):
                        for pol in range(arg_list[2]):
                            data[chn, smp, ant, pol] = 100000 + chn * 1000 + smp * 100 + ant * 10 + pol
            cffm.ingest_data(data, 1000 + i, append=False, sampling_time=1, tile_id=0)
            # Now Attempt read
            self.assertTrue(tcsr.setup('tcsr', queue, {'path': '.', 'time': 1000+i, 'tiles':0, 'stop_on_end': True}))
            self.assertTrue(tcsr.registerConsumer(dc))
            self.assertTrue(dc.setup('dc', queue, None))
            self.assertTrue(dc.start())
            self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_FILE)

            # Now Ensure read successfully
            self.assertEqual(len(dc.samples), 2)
            for sample in dc.samples:
                self.assertEqual(np.shape(sample._data), (arg_list[1], arg_list[2], arg_list[0]))
                self.assertEqual(sample._ts, 1000 + i + sample._idx)
                for ant in range(arg_list[1]):
                    for pol in range(arg_list[2]):
                        for chn in range(arg_list[0]):
                            self.assertEqual(sample._data[ant, pol, chn], (sample._tile + 1) * 100000 + chn * 1000 +
                                             sample._idx * 100 + ant * 10 + pol)
            # Now Prepare for next iteration
            dc = DummyConsumer()
            arg_list[i] = tmp


class TestCSRStatic(TCPOTester):

    # Global Fixture Setup (done once)
    @classmethod
    def setUpClass(cls):

        # Create File Manager
        try:
            os.makedirs('./TCSR') # Create Folder if inexistent
        except OSError:
            if not os.path.isdir('./TCSR'): raise

        cffm = CFFM(root_path="./TCSR", data_type="uint32", daq_mode=FileDAQModes.Integrated)
        cffm.set_metadata(n_chans=CHANNELS, n_antennas=ANTENNAS, n_pols=POLARIS, n_samples=SAMPLES)

        # Iterate over tiles
        for tile in range(TILES):
            _data = np.zeros((CHANNELS, SAMPLES, ANTENNAS, POLARIS), dtype=np.uint32)
            for chn in range(CHANNELS):
                for smp in range(SAMPLES):
                    for ant in range(ANTENNAS):
                        for pol in range(POLARIS):
                            _data[chn, smp, ant, pol] = (tile + 1) * 1000000 + chn * 10000 + smp * 100 + ant * 10 + pol
            cffm.ingest_data(data_ptr=_data, append=True, timestamp=1000, sampling_time=1,
                             buffer_timestamp=1000, tile_id=tile)

    @classmethod
    def tearDownClass(cls):
        for f in glob.glob(os.path.join("./TCSR", "*.lock")): os.remove(f)
        for f in glob.glob(os.path.join("./TCSR", "*.hdf5")): os.remove(f)
        os.rmdir("./TCSR")

    # Test that the data is output and read in the correct manner
    def test_correct_data(self):
        tcsr = CSAR()
        dc = DummyConsumer()
        queue = Qu.Queue()

        # Create new file to check against it reading this
        cffm = CFFM(root_path="./TCSR", data_type="uint16", daq_mode=FileDAQModes.Integrated)
        cffm.set_metadata(n_chans=2, n_antennas=3, n_pols=5, n_samples=1)
        cffm.ingest_data(np.ones((2, 3, 5)), 2000, False, sampling_time=1, tile_id=0)
        cffm.ingest_data(np.ones((2, 3, 5)), 2000, False, sampling_time=1, tile_id=1)

        # Perform readout
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 1000, 'tiles': range(TILES),
                                                   'stop_on_end': True}))
        self.assertTrue(tcsr.registerConsumer(dc))
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_FILE)   # Start Static - This also tests that it is
                                                                            # able to stop on file end...
        dc.stop(-1)

        # Now Assert Global Size
        self.assertEqual(len(dc.samples), SAMPLES*TILES)

        # For each element in samples
        for sample in dc.samples:
            self.assertEqual(np.shape(sample._data), (ANTENNAS, POLARIS, CHANNELS))
            for ant in range(ANTENNAS):
                for pol in range(POLARIS):
                    for chn in range(CHANNELS):
                        self.assertEqual(sample._data[ant,pol,chn], (sample._tile + 1)*1000000 + chn*10000 +
                                         sample._idx*100 + ant*10 + pol)

        # Now perform readout from second file to ensure that can specify (and this was not a fluke)
        dc = DummyConsumer() # Reset
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 2000, 'tiles': range(2),
                                                   'stop_on_end': True}))
        self.assertTrue(tcsr.registerConsumer(dc))  # Should register for all automatically
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertEqual(tcsr.start(), TCPOCode.Status.ACTIVE)  # Start
        tm.sleep(0.2)
        tcsr.stop(-1)
        dc.stop(-1)
        self.assertEqual(len(dc.samples), 2)
        for sample in dc.samples: self.assertEqual(np.shape(sample._data), (3, 5, 2))

        # Now create last file and ensure that default is ok
        cffm.set_metadata(n_chans=7, n_antennas=3, n_pols=11, n_samples=2)
        cffm.ingest_data(np.ones((7, 2, 3, 11)), 3000, False, sampling_time=1, tile_id=0)
        cffm.ingest_data(np.ones((7, 2, 3, 11)), 3000, False, sampling_time=1, tile_id=1)
        cffm.ingest_data(np.ones((7, 2, 3, 11)), 3000, False, sampling_time=1, tile_id=2)
        cffm.ingest_data(np.ones((7, 2, 3, 11)), 3000, False, sampling_time=1, tile_id=3)
        dc = DummyConsumer()  # Reset
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 3000, 'tiles': range(5),
                                                   'stop_on_end': True}))  # Will attempt 5 tiles, but should only get 4
        self.assertTrue(tcsr.registerConsumer(dc))
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertEqual(tcsr.start(), TCPOCode.Status.ACTIVE)  # Start (non-static since more tiles than actual)
        tm.sleep(0.3)
        tcsr.stop(-1)
        dc.stop(-1)
        self.assertEqual(len(dc.samples), 8)
        for sample in dc.samples: self.assertEqual(np.shape(sample._data), (3, 11, 7))

    # Test that ordering of data is correct
    def test_correct_ordering(self):
        tcsr = CSAR()
        dc = DummyConsumer()
        queue = Qu.Queue()

        # Readout
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 1000, 'tiles': range(TILES + 1),
                                                   'stop_on_end': True}))  # Add one tile, although inexistent, to test
        self.assertTrue(tcsr.registerConsumer(dc))  # Should register for all automatically
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertEqual(tcsr.start(), TCPOCode.Status.ACTIVE)  # Start (non-static)

        # Sleep to wait for samples to be processed
        tm.sleep(0.2)
        tcsr.stop(-1)
        dc.stop(-1)

        # Initialise data storage
        ts_list = np.ones((TILES, SAMPLES), dtype=float) * -1

        # Iterate through the samples
        for sample in dc.samples:
            # Ensure that not already referenced: Should be -1
            self.assertEqual(ts_list[sample._tile, sample._idx], -1)

            # Ensure that we are not writing to somewhere in between
            if sample._idx < SAMPLES - 1:
                self.assertEqual(ts_list[sample._tile, sample._idx + 1], -1)
            if sample._idx > 0:
                self.assertNotEqual(ts_list[sample._tile, sample._idx - 1], -1)  # Again, enforce it...
                self.assertGreater(sample._ts, ts_list[sample._tile, sample._idx - 1])
            ts_list[sample._tile, sample._idx] = sample._ts

    # Test that the correct samples are read, when not full read...
    def test_sample_indexing_tuple(self):
        tcsr = CSAR()
        dc = DummyConsumer()
        queue = Qu.Queue()

        # Ensure that fails if negative:
        self.assertRaises(TCPOException, tcsr.setup, 'tcsr', queue, {'path': './TCSR', 'time': 1000,
                                                                     'tiles': range(TILES), 'samples': (-2, 7),
                                                                     'stop_on_end': True})
        self.assertRaises(TCPOException, tcsr.setup, 'tcsr', queue, {'path': './TCSR', 'time': 1000,
                                                                     'tiles': range(TILES), 'samples': (2, -7),
                                                                     'stop_on_end': True})
        # Prepare and start up
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 1000, 'tiles':range(TILES),
                                                   'samples': (2, 7), 'stop_on_end': False}))
        self.assertTrue(tcsr.registerConsumer(dc))  # Should register for all automatically
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_SMPL)  # Start Static - Will also test that despite
                                                                           # not reaching file end, still stops
        dc.stop(-1)

        # Ensure correct
        self.assertEqual(len(dc.samples), 5*TILES) # End is non-inclusive
        for sample in dc.samples:
            self.assertEqual(np.shape(sample._data), (ANTENNAS, POLARIS, CHANNELS))
            self.assertGreaterEqual(sample._idx, 2)
            self.assertLess(sample._idx, 7)
            for ant in range(ANTENNAS):
                for pol in range(POLARIS):
                    for chn in range(CHANNELS):
                        self.assertEqual(sample._data[ant,pol,chn], (sample._tile + 1)*1000000 + chn*10000 +
                                         sample._idx*100 + ant*10 + pol)

    def test_sample_indexing_single(self):

        tcsr = CSAR()
        dc = DummyConsumer()
        queue = Qu.Queue()

        # Setup the
        self.assertNoException(BaseException, dc.setup, 'dc.setup', 'dc', queue, None)

        # Test single index (positive)
        self.assertNoException(BaseException, tcsr.setup, 'tcsr.setup', 'tcsr', queue,
                               {'path': './TCSR', 'time': 1000, 'tiles': range(TILES), 'stop_on_end':True,
                                'samples': 5})
        self.assertNoException(BaseException, tcsr.registerConsumer, 'tcsr.registerConsumer', dc)

        # Start
        self.assertNoException(BaseException, dc.start, 'dc.start')
        self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_FILE)  # Start Static
        dc.stop(-1)

        # Check that correct data read...
        self.assertEqual(len(dc.samples), (SAMPLES-5) * TILES)
        for sample in dc.samples:
            self.assertEqual(np.shape(sample._data), (ANTENNAS, POLARIS, CHANNELS))
            self.assertGreaterEqual(sample._idx, 5)
            self.assertLess(sample._idx, SAMPLES)
            for ant in range(ANTENNAS):
                for pol in range(POLARIS):
                    for chn in range(CHANNELS):
                        self.assertEqual(sample._data[ant,pol,chn], (sample._tile + 1)*1000000 + chn*10000 +
                                         sample._idx*100 + ant*10 + pol)

    # Check single indexing with negative indices
    def test_sample_indexing_negative(self):

        tcsr = CSAR()
        dc = DummyConsumer()
        queue = Qu.Queue()

        # Setup the
        self.assertNoException(BaseException, dc.setup, 'dc.setup', 'dc', queue, None)

        # Test single index (positive)
        self.assertNoException(BaseException, tcsr.setup, 'tcsr.setup', 'tcsr', queue,
                               {'path': './TCSR', 'time': 1000, 'tiles': range(TILES), 'stop_on_end': True,
                                'samples': -1})
        self.assertNoException(BaseException, tcsr.registerConsumer, 'tcsr.registerConsumer', dc)

        # Start
        self.assertNoException(BaseException, dc.start, 'dc.start')
        self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_FILE)  # Start Static
        dc.stop(-1)

        # Check that correct data read...
        self.assertEqual(len(dc.samples), TILES)
        for sample in dc.samples:
            self.assertEqual(np.shape(sample._data), (ANTENNAS, POLARIS, CHANNELS))
            self.assertEqual(sample._idx, SAMPLES-1)
            for ant in range(ANTENNAS):
                for pol in range(POLARIS):
                    for chn in range(CHANNELS):
                        self.assertEqual(sample._data[ant, pol, chn],
                                         (sample._tile + 1) * 1000000 + chn * 10000 + (SAMPLES-1) * 100 + ant * 10 +
                                         pol)

    # Test that registration is handled correctly
    def test_correct_registration_and_callbacks(self):
        tcsr = CSAR()
        dc_list = [DummyConsumer() for _ in range(4)]
        dc_tile = ((0, 1, 2, 3, 4), (0, 1, 2, 3, 4), (0, 1, 4), (3, 2))
        detiler = [BaseSelector() for _ in range(4)]
        queue = Qu.Queue()

        # Setup
        self.assertTrue(tcsr.setup('tcsr', queue, {'path': './TCSR', 'time': 1000, 'tiles': range(TILES),
                                                   'stop_on_end': True}))
        for idx, dc in enumerate(dc_list):
            self.assertTrue(detiler[idx].setup('de_tiler_' + str(idx), queue,
                                               {'type':Message.SelType.TILE, 'select':dc_tile[idx]}))
            self.assertTrue(dc.setup('dc_' + str(idx), queue))
            self.assertTrue(tcsr.registerConsumer(detiler[idx]))
            self.assertTrue(detiler[idx].registerConsumer(dc))

        # Perform Static Readout
        for dc in dc_list: self.assertTrue(dc.start())
        for dt in detiler: self.assertTrue(dt.start())
        self.assertEqual(tcsr.start_static(), TCPOCode.Request.STOP_FILE)  # Start Static
        self.assertFalse(tcsr.Active)

        # Ensure correct shapes...
        for idx, dc in enumerate(dc_list): self.assertEqual(len(dc.samples), len(dc_tile[idx]) * SAMPLES)

        # Ensure correct tiles registered...
        for idx, dc in enumerate(dc_list):
            for t in range(TILES):
                if t in dc_tile[idx]:
                    self.assertEqual(len(dc.byTile(TILES)[t]), SAMPLES)
                else:
                    self.assertEqual(len(dc.byTile(TILES)[t]), 0)

        # Ensure equality between all registered... and all samples delivered in order
        for t in range(TILES):
            for idx, dc in enumerate(dc_list[1:4], start=1):
                if t in dc_tile[idx]:
                    for s in range(SAMPLES):
                        self.assertTrue(np.all(dc.byTile(TILES)[t][s]._data == dc_list[0].byTile(TILES)[t][s]._data))


class TestCSRDynamic(TCPOTester):

    def __init__(self, methodName = 'runTest'):
        super(TestCSRDynamic, self).__init__(methodName=methodName)
        self.tcsr = None

    def setUp(self):
        super(TestCSRDynamic, self).setUp()
        self.tcsr = CSAR()
        pass

    def tearDown(self):
        self.tcsr.stop(TCPOCode.Error.GENERIC)
        for f in glob.glob(os.path.join(".", "*.lock")): os.remove(f)
        for f in glob.glob(os.path.join(".", "*.hdf5")): os.remove(f)
        super(TestCSRDynamic, self).tearDown()

    # Used to test starting/stopping the producer etc out of sync with the CSR
    def test_dynamic_handling(self):
        dc = DummyConsumer()
        queue = Qu.Queue()

        prod = CFFM(root_path='.', data_type="uint16", daq_mode=FileDAQModes.Integrated)
        prod.set_metadata(n_chans=3, n_antennas=2, n_pols=1, n_samples=2)
        data = np.ones((3, 2, 2, 1), dtype=np.uint16)

        # Start Consumer, before producer
        self.assertTrue(self.tcsr.setup('tcsr', queue, {'path':'.', 'time':1000, 'tiles':range(1), 'sample_rate':0.001}))
        self.assertTrue(self.tcsr.registerConsumer(dc))
        self.assertTrue(dc.setup('dc', queue))
        self.assertTrue(dc.start())
        self.assertTrue(self.tcsr.start())
        tm.sleep(0.05)
        self.assertEqual(len(dc.samples), 0)

        # Ensure that actually started (i.e. cannot start another)
        self.assertRaises(TCPOException, self.tcsr.start)

        # Attempt to stop and ensure that stops
        self.tcsr.stop(TCPOCode.Status.USER_STOP)
        self.assertFalse(self.tcsr.Active)
        dc.done_code = 0    # Refresh

        # Attempt to restart (again without producer)
        self.assertTrue(self.tcsr.setup('tcsr', queue, {'path': '.', 'time': 1000, 'tiles': (0,), 'sample_rate': 0.01}))
        self.assertTrue(self.tcsr.registerConsumer(dc))
        self.assertTrue(self.tcsr.start())

        # Ensure still waiting
        tm.sleep(0.1)
        self.assertEqual(len(dc.samples), 0)

        # Now Add Producer:
        prod.ingest_data(data, 1000, False, sampling_time=1, tile_id=0)
        tm.sleep(0.2)

        # Ensure that read ... and then nothing else
        self.assertEqual(len(dc.samples), 2)
        tm.sleep(0.1)
        self.assertEqual(len(dc.samples), 2)

        # Write and ensure read again...
        prod.ingest_data(data, 1000, True, sampling_time=1, tile_id=0)
        tm.sleep(0.15)
        self.assertEqual(len(dc.samples), 4)

        # Finally Stop again
        self.tcsr.stop(TCPOCode.Status.USER_STOP)
        self.assertFalse(self.tcsr.Active)

    def test_no_return_until_sample(self):
        dc = DummyConsumer()
        queue = Qu.Queue()

        prod = CFFM(root_path='.', data_type="uint16", daq_mode=FileDAQModes.Integrated)
        prod.set_metadata(n_chans=3, n_antennas=2, n_pols=1, n_samples=2)
        data = np.ones((3, 2, 2, 1), dtype=np.uint16)

        # Setup the consumer
        self.assertNoException(BaseException, dc.setup, 'dc.setup', 'dc', queue, None)

        # Test 'stop-on-end'
        self.assertNoException(BaseException, self.tcsr.setup, 'tcsr.setup', 'tcsr', queue,
                               {'path': '.', 'time': 1000, 'tiles': range(TILES), 'stop_on_end': True,
                                'sample_rate': 0.001})
        self.assertNoException(BaseException, self.tcsr.registerConsumer, 'tcsr.registerConsumer', dc)

        # Start statically on a different thread
        static_thread = ThreadReturner(target=lambda: self.tcsr.start_static())
        self.assertNoException(BaseException, dc.start, 'dc.start')
        static_thread.start()
        tm.sleep(0.1)

        # Check that still not ended
        self.assertIsNone(static_thread.getResult())

        # Write one sample to all but one tile...
        for i in range(TILES-1):
            prod.ingest_data(data*i, 1000, True, sampling_time=1, tile_id=i)
            tm.sleep(0.01)   # Give it time to start ingesting
        tm.sleep(0.1)

        # Check that still not ended
        self.assertIsNone(static_thread.getResult())

        # Finally write last Tile
        prod.ingest_data(data, 1000, True, sampling_time=1, tile_id=TILES-1)
        tm.sleep(0.2)

        # Now should have terminated
        self.assertEqual(static_thread.getResult(), TCPOCode.Request.STOP_FILE)

    def test_sample_indexing_tuple(self):
        dc = DummyConsumer()
        queue = Qu.Queue()

        prod = CFFM(root_path='.', data_type="uint16", daq_mode=FileDAQModes.Integrated)
        prod.set_metadata(n_chans=3, n_antennas=2, n_pols=1, n_samples=2)
        data = np.ones((3, 2, 2, 1), dtype=np.uint16)

        # Setup the consumer
        self.assertNoException(BaseException, dc.setup, 'dc.setup', 'dc', queue, None)

        # Test Positive Tuples
        self.assertNoException(BaseException, self.tcsr.setup, 'tcsr.setup', 'tcsr', queue,
                               {'path': '.', 'time': 1000, 'tiles': range(TILES), 'stop_on_end': False,
                                'samples': (2, 5), 'sample_rate': 0.001})
        self.assertNoException(BaseException, self.tcsr.registerConsumer, 'tcsr.registerConsumer', dc)

        # Start
        self.assertNoException(BaseException, dc.start, 'dc.start')
        self.assertNoException(BaseException, self.tcsr.start, 'tcsr.start')

        # Write samples up till sample 7 (8 samples, 4 blocks)
        for i in range(4):
            prod.ingest_data(data*i, 1000, True, sampling_time=1, tile_id=0)
            tm.sleep(0.05)   # Give it time to start ingesting

        # Sleep a bit to wait for samples to be read.
        tm.sleep(0.25)

        # Stop
        self.tcsr.stop(-1)
        dc.stop(-1)

        # Check no of samples
        self.assertEqual(len(dc.samples), 3)
        for _i, _data in enumerate(dc.samples):
            self.assertEqual(_data._idx, _i + 2)

    def test_sample_indexing_negative(self):
        dc = DummyConsumer()
        queue = Qu.Queue()

        prod = CFFM(root_path='.', data_type="uint16", daq_mode=FileDAQModes.Integrated)
        prod.set_metadata(n_chans=2, n_antennas=2, n_pols=2, n_samples=1)
        data = np.ones((2, 2, 2), dtype=np.uint16)

        # Setup the consumer
        self.assertNoException(BaseException, dc.setup, 'dc.setup', 'dc', queue, None)

        # Test Positive Tuples
        self.assertNoException(BaseException, self.tcsr.setup, 'tcsr.setup', 'tcsr', queue,
                               {'path': '.', 'time': 1000, 'tiles': range(TILES), 'stop_on_end': False,
                                'samples': -1, 'sample_rate': 0.001})
        self.assertNoException(BaseException, self.tcsr.registerConsumer, 'tcsr.registerConsumer', dc)

        # Now write 3 samples in each tile up to 3
        for _t in range(TILES-2):
            for _s in range(3):
                prod.ingest_data(data, 1000, True, sampling_time=1, tile_id=_t)

        # Now Start CSR and Consumer
        self.assertNoException(BaseException, dc.start, 'dc.start')
        self.assertNoException(BaseException, self.tcsr.start, 'tcsr.start')

        # Sleep a bit to give time to stabilise
        tm.sleep(0.2)

        # Now write a further 3 samples in all tiles!
        for _t in range(TILES):
            for _s in range(3):
                prod.ingest_data(data, 1000, True, sampling_time=1, tile_id=_t)
                tm.sleep(0.05) # To give time for the CSR to process...

        # Now sleep a bit to give time for CSR to injest any additional data...
        tm.sleep(0.2)

        # Now Stop

        self.tcsr.stop(-1)
        dc.stop(-1)

        # Ensure that data ok
        for _t in range(TILES):
            _samples_t = dc.byTile(TILES)[_t]
            limit = 4 if _t < TILES-2 else 3
            start = 2 if _t < TILES-2 else 0
            self.assertEqual(len(_samples_t), limit,
                            msg='# Samples not equal for tile %d : %d != %d' % (_t, len(_samples_t), limit))
            for s in range(limit):
                self.checkEqual(_samples_t[s]._idx, s + start,
                                msg='# Idx %d not in order for tile %d' % (s, _t))