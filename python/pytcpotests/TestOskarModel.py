import numpy as np
import tempfile as tmp
import shutil

from pytcpotests import TCPOTester
from pytcpo.Modules.Simulators import OskarModel


class TestOskarModel(TCPOTester):

    def test_telescope_builder(self):

        om = OskarModel()

        class TM(object):
            def __init__(self):
                self.antennas = np.array([[0, 1, 2], [2, 3, 4], [4, 5, 6]])

        tm = TM()
        filter_model = dict()
        filter_model['tmp_dir'] = tmp.tempdir
        filter_model['tm'] = tm
        filter_model['model_vis_file'] = None

        om._setup(filter_model)

        self.assertTrue(np.array_equal(om.telbuild.antennas, tm.antennas))

        shutil.rmtree(om._dir, ignore_errors=True)
