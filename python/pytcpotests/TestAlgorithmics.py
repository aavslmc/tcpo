import numpy as np


from pytcpotests import TCPOTester
from pytcpo.Core.Common import MovingAverage, PolynomialCompare, npalt


class TestMovingAverage(TCPOTester):

    def test_average(self):
        orig = np.array(((0.5, 1.0, 2.0), (4.0, 2.0, 1.0)))
        fin = np.array(((4.0, 4.0, 4.0), (4.0, 4.0, 4.0)))
        average = MovingAverage(orig, 5)

        self.assertTrue((average.Average == orig).all())
        for i in range(7):
            ave = average.add(fin)
            com = orig + (i+1)*0.2*np.subtract(fin, orig) if i < 5 else fin
            self.assertTrue(np.allclose(ave, com))

        com = fin
        for i in range(5):
            ave = average.add(np.zeros(orig.shape))
            com = np.subtract(com, 0.8*np.ones(orig.shape))
            self.assertTrue(np.allclose(ave, com))

    # Test a singular sample average...
    def test_singularities(self):

        average = MovingAverage(np.zeros((3,2)), 1)
        for i in range(3):
            rnd = np.random.rand(3,2)
            self.assertTrue(np.allclose(rnd, average.add(rnd)))

        orig = np.array((5.0,))
        fin = np.array((10.0,))
        average = MovingAverage(orig, 10)
        for i in range(15):
            ave = average.add(fin)
            com = orig + (i+1)*0.1*np.subtract(fin, orig) if i < 10 else fin
            self.assertTrue(np.allclose(ave, com))


class TestPolynomialCompare(TCPOTester):

    def test_single_compare_valid(self):
        # Define the Polynomials
        pol1 = np.array((0, 2, 1))
        pol2 = np.array((2, 2, 1))
        pol3 = np.array((0, -2, -1))

        # First Compare with all coefficients
        comparator = PolynomialCompare((0, 1), ignore_constant=False, numerical=False)
        self.assertEqual(comparator.compare(pol1, pol2), 78.0/178.0)
        self.assertEqual(comparator.compare(pol1, pol3), -1.0)
        self.assertEqual(comparator.compare(pol2, pol3), -78.0 / 178.0)

        comparator = PolynomialCompare((0, 1), ignore_constant=False, numerical=True, steps=1000)
        self.assertAlmostEqual(comparator.compare(pol1, pol2), 78.0 / 178.0, places=3)
        self.assertEqual(comparator.compare(pol1, pol3), -1.0)
        self.assertAlmostEqual(comparator.compare(pol2, pol3), -78.0 / 178.0, places=3)

        # Now Compare with higher order coefficients
        comparator = PolynomialCompare((0, 1), ignore_constant=True, numerical=False)
        self.assertEqual(comparator.compare(pol1, pol2), 1.0)
        self.assertEqual(comparator.compare(pol1, pol3), -1.0)
        self.assertEqual(comparator.compare(pol2, pol3), -1.0)

        comparator = PolynomialCompare((0, 1), ignore_constant=True, numerical=True, steps=1000)
        self.assertEqual(comparator.compare(pol1, pol2), 1.0)
        self.assertEqual(comparator.compare(pol1, pol3), -1.0)
        self.assertEqual(comparator.compare(pol2, pol3), -1.0)

    def test_single_compare_invalid(self):
        # Define the Polynomials
        pol1 = np.array((0, 2, 1))
        pol2 = np.array((np.nan, 2, 1))

        # First compare with all coefficients
        comparator = PolynomialCompare((0, 1), False, numerical=False)
        self.assertTrue(np.isnan(comparator.compare(pol1, pol2)))
        comparator = PolynomialCompare((0, 1), False, numerical=True)
        self.assertTrue(np.isnan(comparator.compare(pol1, pol2)))

        # Now Compare with only the higher orders
        comparator = PolynomialCompare((0, 1), True, numerical=False)
        self.assertTrue(np.isnan(comparator.compare(pol1, pol2)))
        comparator = PolynomialCompare((0, 1), True, numerical=True)
        self.assertTrue(np.isnan(comparator.compare(pol1, pol2)))


    def test_recursive_compute_valid(self):
        polynomials = np.array(((0,2), (0,1), (0,0), (0,-1), (0,-2)), np.float)
        resulting   = np.array((( 1.0,  0.5, 0.0, -0.5, -1.0),
                                ( 0.5,  1.0, 0.0, -1.0, -0.5),
                                ( 0.0,  0.0, 1.0,  0.0,  0.0),
                                (-0.5, -1.0, 0.0,  1.0,  0.5),
                                (-1.0, -0.5, 0.0,  0.5,  1.0)))

        comparator = PolynomialCompare([0, 5], False, False)
        self.assertTrue(np.array_equal(resulting, comparator.compare_self(polynomials)))
        comparator = PolynomialCompare([0, 5], True, False)
        self.assertTrue(np.array_equal(resulting, comparator.compare_self(polynomials)))

        comparator = PolynomialCompare([0, 5], False, True)
        self.assertTrue(np.array_equal(resulting, comparator.compare_self(polynomials)))
        comparator = PolynomialCompare([0, 5], True, True)
        self.assertTrue(np.array_equal(resulting, comparator.compare_self(polynomials)))

    def test_recursive_compute_invalid(self):

        polynomials = np.array(((0, 2), (0, 1), (np.nan, 0), (0, 0), (np.nan, np.nan)), np.float)
        resulting = np.array(((1.0,     0.5,    np.nan, 0.0,    np.nan),
                              (0.5,     1.0,    np.nan, 0.0,    np.nan),
                              (np.nan,  np.nan, np.nan, np.nan, np.nan),
                              (0.0,     0.0,    np.nan, 1.0,    np.nan),
                              (np.nan,  np.nan, np.nan, np.nan, np.nan)))

        comparator = PolynomialCompare([0, 5], False, False)
        self.assertTrue(npalt.array_nan_equal(resulting, comparator.compare_self(polynomials)))
        comparator = PolynomialCompare([0, 5], True, False)
        self.assertTrue(npalt.array_nan_equal(resulting, comparator.compare_self(polynomials)))

        comparator = PolynomialCompare([0, 5], False, True)
        self.assertTrue(npalt.array_nan_equal(resulting, comparator.compare_self(polynomials)))
        comparator = PolynomialCompare([0, 5], True, True)
        self.assertTrue(npalt.array_nan_equal(resulting, comparator.compare_self(polynomials)))
