from pytcpo.Core.Archiving import ASerialiseable as AS
from pytcpo.Core.Archiving import APersistor as AA
from pytcpo.Core.Archiving import ASerialiser as ASer


class DumDumSerialiser(ASer):

    def _setup(self, mode, params):
        pass

    def _serialise(self, obj):
        pass

    def _deserialise(self, otype, sample):
        pass


class DumDumArchiver(AA):

    @property
    def SupportsRandomReads(self):
        return True

    @property
    def SupportsRandomWrites(self):
        return False

    def __init__(self):
        super(DumDumArchiver, self).__init__()

    def _initialise(self, source, mode, structure):
        return True

    def _writeSample(self, key, timestamp, sample):
        return True

    def _readSample(self, oType, idx = None):
        return (0,0)

    def _close(self):
        pass


class SerE(AS): # Used to test relative imports with absolute registration

    def __init__(self):
        self.e = 4

    def serial(self):
        return {'e':int}

ASer.registerSerialiseable(SerE)

class SerF(AS): # Used to test relative imports with derived registration

    def __init__(self):
        self.f = 5

    def serial(self):
        return {'f':int}

DumDumSerialiser.registerSerialiseable(SerF)