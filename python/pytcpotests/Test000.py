# First Test run by default: basically checks that environment is ok

import sys
import redis
import importlib

from pytcpotests import TCPOTester

requirements = ['pydaq', 'matplotlib', 'astropy', 'scipy', 'numpy', 'abc', 'redis', 'rwlock']


class TestEnvironment(TCPOTester):

    # Check that Required Python Version is available
    def test_correct_python_version(self):
        self.assertTupleEqual(sys.version_info[0:2], (2,7), msg="Python Environment must be 2.7 compliant")

    # Test that the required libraries are installed and visible to the project.
    # The required modules are automatically inferred from the requirements.pip file of the project
    def test_libraries_are_visible(self):
        for package in requirements:        #Parse all rows, ignoring double-comments (which are actual comments
            self.assertNoException(BaseException, lambda : importlib.import_module(package),
                                   msg="Library {} not found".format(package))

    def test_redis_service_running(self):
        """
        pytcpotests that the Redis Service is up and running
        """
        self.assertNoException(BaseException, redis.StrictRedis().ping, msg='Redis Service Not Running (port 6379)')