import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message
from pytcpo.Modules.RawFilters.Selectors import AntennaSelector, ChannelSelector, PolarisationSelector

model_data = np.array([[[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                        [0.3, 10.3, 20.3, 30.3, 40.3, 50.3, 60.3, 70.3, 80.3, 90.3]],
                       [[9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0, 0.0],
                        [90.0, 80.0, 70.0, 60.0, 50.0, 40.0, 30.0, 20.0, 10.0, 0.0]],
                       [[0.0, 11.0, 22.0, 33.0, 44.0, 55.0, 66.0, 77.0, 88.0, 99.0],
                        [2.0, 12.0, 22.0, 32.0, 42.0, 52.0, 62.0, 72.0, 82.0, 92.0]],
                       [[9.5, 8.5, 7.5, 6.5, 5.5, 4.5, 3.5, 2.5, 1.5, 0.5],
                        [90.3, 80.3, 70.3, 60.3, 50.3, 40.3, 30.3, 20.3, 10.3, 0.3]]])

class TestAntennaSelector(TCPOTester):

    def test_AntennaSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'AntSel_Selection')
        msg.Default = model_data

        antsel_a = AntennaSelector()
        antsel_a.setup('antsel_a',None,module_opt={'in_antennas':np.arange(4), 'out_antennas':np.array([0,2])})

        out_0 = np.array([[[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0],
                           [0.3, 10.3, 20.3, 30.3, 40.3, 50.3, 60.3, 70.3, 80.3, 90.3]],
                          [[0.0, 11.0, 22.0, 33.0, 44.0, 55.0, 66.0, 77.0, 88.0, 99.0],
                           [2.0, 12.0, 22.0, 32.0, 42.0, 52.0, 62.0, 72.0, 82.0, 92.0]]])

        result = [[],[]]
        antsel_a._new_sample(msg,result)

        self.assertEqual((2, 2, 10),result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(), 'Data matrix forwarded to module not as expected')

    def test_AntennaNoSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'AntSel_NoSelection')
        msg.Default = model_data

        antsel_b = AntennaSelector()
        antsel_b.setup('antsel_b', None,module_opt={'in_antennas': np.arange(4), 'out_antennas': np.array([5, 8])})

        out_0 = np.empty((0, 2, 10))

        result = [[], []]
        antsel_b._new_sample(msg, result)

        self.assertEqual((0, 2, 10), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(),'Data matrix forwarded to module not as expected')

class TestChannelSelector(TCPOTester):

    def test_ChannelSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'ChanSel_Selection')
        msg.Default = model_data

        chansel_a = ChannelSelector()
        chansel_a.setup('chansel_a',None,module_opt={'in_channels':np.arange(10), 'out_channels':np.array([0, 2, 7, 9])})

        out_0 = np.array([[[0.0, 2.0, 7.0, 9.0], [0.3, 20.3, 70.3, 90.3]],
                          [[9.0, 7.0, 2.0, 0.0], [90.0, 70.0, 20.0, 0.0]],
                          [[0.0, 22.0, 77.0, 99.0], [2.0, 22.0, 72.0, 92.0]],
                          [[9.5, 7.5, 2.5, 0.5], [90.3, 70.3, 20.3, 0.3]]])

        result = [[],[]]
        chansel_a._new_sample(msg,result)

        self.assertEqual((4, 2, 4),result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(), 'Data matrix forwarded to module not as expected')

    def test_ChannelNoSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'ChanSel_NoSelection')
        msg.Default = model_data

        chansel_b = ChannelSelector()
        chansel_b.setup('chansel_b', None,module_opt={'in_channels': np.arange(10), 'out_channels': np.array([10, 12])})

        out_0 = np.empty((4, 2, 0))

        result = [[], []]
        chansel_b._new_sample(msg, result)

        self.assertEqual((4, 2, 0), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(),'Data matrix forwarded to module not as expected')

class TestPolarisationSelector(TCPOTester):

    def test_PolarisationSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PolSel_Selection')
        msg.Default = model_data

        polsel_a = PolarisationSelector()
        polsel_a.setup('polsel_a',None,module_opt={'in_polarisations':np.arange(2), 'out_polarisations':np.array([1])})

        out_0 = np.array([[[0.3, 10.3, 20.3, 30.3, 40.3, 50.3, 60.3, 70.3, 80.3, 90.3]],
                          [[90.0, 80.0, 70.0, 60.0, 50.0, 40.0, 30.0, 20.0, 10.0, 0.0]],
                          [[2.0, 12.0, 22.0, 32.0, 42.0, 52.0, 62.0, 72.0, 82.0, 92.0]],
                          [[90.3, 80.3, 70.3, 60.3, 50.3, 40.3, 30.3, 20.3, 10.3, 0.3]]])

        result = [[],[]]
        polsel_a._new_sample(msg,result)

        self.assertEqual((4, 1, 10),result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(), 'Data matrix forwarded to module not as expected')

    def test_PolarisationNoSelection(self):
        msg = Message((Message.SelType.NONE, 0), 0, 0, 'PolSel_NoSelection')
        msg.Default = model_data

        polsel_b = PolarisationSelector()
        polsel_b.setup('polsel_b', None,module_opt={'in_polarisations': np.arange(2), 'out_polarisations': np.array([2])})

        out_0 = np.empty((4, 0, 10))

        result = [[], []]
        polsel_b._new_sample(msg, result)

        self.assertEqual((4, 0, 10), result[0].shape, 'Forwarded shape to module is incorrect')
        self.assertTrue(np.asarray(np.array_equal(result[0], out_0)).all(),'Data matrix forwarded to module not as expected')