from pytcpo.Core.Archiving import ASerialiseable as AS
from pytcpo.Core.Archiving import ASerialiser as ASer


class SerG(AS): # Used to test absolute imports

    def __init__(self):
        self.g = 6

    def serial(self):
        return {'g':int}

ASer.registerSerialiseable(SerG)