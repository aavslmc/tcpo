import numpy as np
import Queue as Qu

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message, AbstractProcessor
from pytcpo.Core.Common import TCPOCode
from pytcpo.Modules.RawFilters import CScaler


class DummySTP(AbstractProcessor):

    def __init__(self):
        super(DummySTP, self).__init__()
        self.data = None

    def _setup(self, filter_model):
        return TCPOCode.Status.SETUP

    def _will_start(self):
        pass

    def _new_sample(self, message, forward):
        self.data = message.Default
        forward[0] = message.Default

    def _clean_up(self, reason):
        pass


class TestCScaler(TCPOTester):

    def test_float_scaling(self):
        inp = np.arange(-3, 66, dtype=np.int8)
        inp_r = np.arange(-1,5, dtype=np.int8)
        out_f = np.linspace(4,10,num=69,endpoint=False)
        out_i = np.arange(-11,541,8, dtype=np.int16)
        out_r = np.array((-7,-3,0,4,7,11), dtype=np.int8) # Should be, in real, (-7, -3.4, 0.2, 3.8, 7.4, 11)

        scaler = CScaler()
        dummy = DummySTP()
        queue = Qu.Queue()

        # First Try with Float Scaling
        self.assertTrue(scaler.setup(0, queue, {'input':(-3,66), 'output':(4,10), 'type':float}))
        self.assertTrue(dummy.setup(0, queue, None))
        scaler.registerConsumer(dummy)

        self.assertTrue(scaler.start())
        self.assertTrue(dummy.start())
        scaler.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', inp))
        scaler.stop(0)
        dummy.stop(0)
        self.assertTrue(np.allclose(dummy.data, out_f))

        # Now with Integer up-Scaling (perfect multiple)
        self.assertTrue(scaler.setup(0, queue, {'input': (-3, 66), 'output': (-11, 541), 'type': np.int16}))
        self.assertTrue(dummy.setup(0, queue, None))
        scaler.registerConsumer(dummy)

        self.assertTrue(scaler.start())
        self.assertTrue(dummy.start())
        scaler.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', inp))
        scaler.stop(0)
        dummy.stop(0)
        self.assertTrue(np.equal(dummy.data, out_i).all())

        # Now with Integer up-Scaling (real multiple)
        self.assertTrue(scaler.setup(0, queue, {'input': (-1, 4), 'output': (-7, 11), 'type': np.int8}))
        self.assertTrue(dummy.setup(0, queue, None))
        scaler.registerConsumer(dummy)

        self.assertTrue(scaler.start())
        self.assertTrue(dummy.start())
        scaler.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', inp_r))
        scaler.stop(0)
        dummy.stop(0)
        self.assertTrue(np.equal(dummy.data, out_r).all())
