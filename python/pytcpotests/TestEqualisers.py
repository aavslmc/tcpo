import numpy as np
import Queue as Qu

from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import AbstractProcessor, Message

from pytcpo.Modules.Equalisers import PolyDeTrender, BitShiftEqualiser

INV = BitShiftEqualiser.INVALID_INDICATOR

class DummySTP(AbstractProcessor):

    def __init__(self):
        super(DummySTP, self).__init__()
        self.data = None

    def _setup(self, filter_model):
        return True

    def _will_start(self):
        return True

    def _new_sample(self, message, forward):
        self.data = message.Default
        forward[0] = message.Default
        return True

    def _clean_up(self, reason):
        pass


# class TestSampleEqualiser(base_test):
#
#     def test_setup_errors(self):
#         equaliser = SampleEqualiser()
#         front = DummySTP()
#         sibling = DummySTP()
#         queue = Qu.Queue()
#
#         # Setup Data
#         data = np.empty((5, 2, 3), dtype=np.float)
#         copy = np.copy(data)
#
#         # Test some bad inputs
#         self.assertRaises(AttributeError, equaliser.setup, 'equaliser', queue, 1)
#         self.assertRaises(TCPOException, equaliser.setup, 'equaliser', queue, {'type':'d'})
#         self.assertRaises(TCPOException, equaliser.setup, 'equaliser', queue, {'type': 'pp'})
#         self.assertRaises(TCPOException, equaliser.setup, 'equaliser', queue, {'type': True})
#
#         # Test some correct inputs
#         self.assertTrue(equaliser.setup('equaliser', queue, None))
#         self.assertTrue(equaliser.setup('equaliser', queue, {}))
#         self.assertTrue(equaliser.setup('equaliser', queue, {'type':'A'}))
#         self.assertTrue(equaliser.setup('equaliser', queue, {'type':'a'}))
#         self.assertTrue(equaliser.setup('equaliser', queue, {'type':'P'}))
#         self.assertTrue(equaliser.setup('equaliser', queue, {'type':'p'}))
#
#         # Setup System Components
#         self.assertTrue(front.setup('front', queue, None, None))
#         self.assertTrue(front.setup('front', queue, {'type':None}, None))
#         self.assertTrue(equaliser.setup('equaliser', queue, None))
#         self.assertTrue(sibling.setup('sibling', queue, None, None))
#
#         # Setup Structure
#         self.assertTrue(front.registerConsumer(equaliser))
#         self.assertTrue(front.registerConsumer(sibling))
#
#         # Start
#         self.assertTrue(front.start())
#         self.assertTrue(equaliser.start())
#         self.assertTrue(sibling.start())
#
#         # Test that array not modified...
#         front.onSample(Message((Message.SelType.NONE, 0), 0, 1, 'default', data))
#         self.assertTrue((copy == sibling.data).all())
#
#     def test_archive_values(self):
#         equaliser = SampleEqualiser()
#         archiver = TFA()
#         queue = Qu.Queue()
#
#         self.assertEqual(archiver.setup(TFA.M_WRITE, {'source':'test', 'keys': {'a': None}}, 'CSVSerialiser', None),
#                          TCPOCode.Status.SETUP)
#         self.assertTrue(equaliser.setup(0, queue, {'type':'a'}, (archiver, 'a')))
#         self.assertTrue(equaliser.start())
#
#         #Setup Data
#         data = np.empty((5, 2, 3), dtype=np.float)
#         val = 1
#         for a in range(5):
#             for p in range(2):
#                 for c in range(3):
#                     data[a, p, c] = val
#                     val += 1
#
#         # Pass on
#         equaliser.onSample(Message((Message.SelType.NONE, 0), 0, -2, 'default', data))
#         archiver.Close()
#
#         reader = TFA()
#         self.assertEqual(reader.setup(TFA.M_READ, {'source': 'test', 'keys': {'a': SampleEqualiser.ShiftArchive}},
#                                       'CSVSerialiser', None), TCPOCode.Status.SETUP)
#         ts, sa = reader.ReadNext('a')
#         self.assertEqual(sa._axes, 'a')
#         self.assertTrue((sa._shift == np.array((-1.5,-1.5,-1.5,-1.5,-1.5))).all())
#
#         # Remove the file which was created
#         shutil.rmtree('./test')
#
#     def test_calculation_values(self):
#         equaliser = SampleEqualiser()
#         dummy = DummySTP()
#         queue = Qu.Queue()
#
#         # Create Array
#         data = np.empty((5,2,3), dtype=np.float)
#         val = 1
#         for a in range(5):
#             for p in range(2):
#                 for c in range(3):
#                     data[a,p,c] = val
#                     val += 1
#
#         full_ = np.ones((5,2,3)) * np.array((14.5, 15.5, 16.5))
#         ant_ = np.ones((5,2,3))
#         for a in range(5): ant_[a,:,:] = np.array((2.5,3.5,4.5)) + 6*a
#         pol_ = np.ones((5,2,3))
#         for p in range(2): pol_[:,p,:] = np.array((13,14,15)) + 3*p
#
#         # First let us try to find a global mean
#         self.assertTrue(equaliser.setup(0, queue, None, None))
#         self.assertTrue(dummy.setup(0, queue, None, None))
#         self.assertTrue(equaliser.registerConsumer(dummy))
#         self.assertTrue(equaliser.start())
#         self.assertTrue(dummy.start())
#         equaliser.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', data))
#         equaliser.stop(2)
#         dummy.stop(-1)
#         self.assertTrue((dummy.data == full_).all())
#
#         # Now let us try with independence between antennas
#         self.assertTrue(equaliser.setup(0, queue, {'type':'a'}, None))
#         self.assertTrue(dummy.setup(0, queue, None, None))
#         self.assertTrue(equaliser.registerConsumer(dummy))
#         self.assertTrue(equaliser.start())
#         self.assertTrue(dummy.start())
#         equaliser.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', data))
#         equaliser.stop(2)
#         dummy.stop(-1)
#         self.assertTrue((dummy.data == ant_).all())
#
#         # Finally we will try with independence between Polarisations
#         self.assertTrue(equaliser.setup(0, queue, {'type':'p'}, None))
#         self.assertTrue(dummy.setup(0, queue, None, None))
#         self.assertTrue(equaliser.registerConsumer(dummy))
#         self.assertTrue(equaliser.start())
#         self.assertTrue(dummy.start())
#         equaliser.onSample(Message((Message.SelType.NONE, 0), 0, 0, 'default', data))
#         equaliser.stop(2)
#         dummy.stop(-1)
#         self.assertTrue((dummy.data == pol_).all())


class TestDeTrender(TCPOTester):

    def test_setup_errors(self):
        detrender = PolyDeTrender()
        queue = Qu.Queue()

        self.assertRaises(TypeError, detrender.setup, 'DeTrend', queue)
        self.assertRaises(TypeError, detrender.setup, 'DeTrend', queue, 'AA')
        self.assertRaises(KeyError, detrender.setup, 'DeTrend', queue, {})
        self.assertTrue(detrender.setup('DeTrend', queue, {'model':'AA'}))

    def test_calculation_values(self):
        detrender = PolyDeTrender()
        sampler = DummySTP()
        queue = Qu.Queue()

        # Setup
        self.assertTrue(detrender.setup('DeTrend', queue, {'model': 'AA'}))
        self.assertTrue(sampler.setup('Sampler', queue))
        self.assertTrue(detrender.registerConsumer(sampler))
        self.assertTrue(detrender.start())
        self.assertTrue(sampler.start())

        # Generate Data
        clean = np.empty((5,2,23), dtype=np.float) # 5 Antennas, 2 Polarisations, 23 Channels
        polyn = np.empty((5,2), dtype=np.object)
        for a in range(5):
            for p in range(2):
                polyn[a,p] = np.arange(7) + a + p
                clean[a,p] = np.polyval(polyn[a,p], np.arange(23))

        # Now add Noise
        noise = np.random.binomial(3, 0.2, (5,2,23))
        dirty = clean + noise

        # Create Message
        message = Message((Message.SelType.NONE, 0), 1, 1.0, 'raw', dirty)
        message.add('AA', clean)
        detrender.onSample(message)
        self.assertTrue(np.alltrue(sampler.data == noise))


class TestBitShiftEqualiser(TCPOTester):

    def test_correct_size_handling(self):
        # Forwarding
        forward = [None, None]

        # Create (singleton) Message
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([1, 1, 1], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[1,1,1]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([1, 1, 1], forward[0].shape))

        # Now create singular Antenna Dimension only
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([1, 5, 22], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[1, 5, 22]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([1, 5, 22], forward[0].shape))

        # Now create singular Polarisation Dimension only
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([3, 1, 10], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[3, 1, 10]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([3, 1, 10], forward[0].shape))

        # Singular Channel Dimensions only
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([2, 7, 1], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[2, 7, 1]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([2, 7, 1], forward[0].shape))

        # Finally create fully-general matrix
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([5, 3, 24], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.ones(shape=[5, 3, 24]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([5, 3, 24], forward[0].shape))

    def test_correct_shifts(self):
        # Prepare
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([1, 1, 1], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        forward = [None, None]
        input = [256, 512, 128, 128, 512, 2048, 512, 4096, 4096, 16, 512]
        output = [ 1,   1,  -1,  -3,  -3,   -1,  -1,    2,    5,  0,   0]

        # No Outliers detected)
        for i in range(len(input)):
            msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[[input[i]]]]))
            self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
            self.assertTrue(np.array_equal([[[output[i]]]], forward[0]))

    def test_invalid_shifts(self):
        # Forward
        forward = [None, None]
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([1, 1, 1], 5)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        input = [  4, 0,  4, np.NaN, 512, 1024, np.NaN, np.NaN, 0, 0.5, 4100]
        output = [-2, 0, -7,    INV,   0,    1,    INV,    INV, 0,   0,    3]

        # No Outliers detected)
        for i in range(len(input)):
            msg = Message((Message.SelType.NONE, 0), 0, 0.0, default=np.asarray([[[input[i]]]]))
            self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
            self.assertTrue(np.array_equal([[[output[i]]]], forward[0]))

    def test_external_inputs(self):
        # Prepare
        ccg = BitShiftEqualiser()
        self.assertNoException(BaseException, ccg.setup, 'Exception in CCG Setup', 'ccg', Qu.Queue,
                               {'desired': 2048, 'select_offset': 3, 'integration': 16, 'division': 8,
                                'scaling': np.full([1, 1, 1], 2)})
        self.assertNoException(BaseException, ccg.start, 'Exception in PMF Start')
        forward = [None, None]

        # Update with time-stamp after 1st sample:
        ccg.update_scale(np.full([1, 1, 1], -3), 100.0)
        msg = Message((Message.SelType.NONE, 0), 0, 50.0, default=np.asarray([[[256]]]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[1]]], forward[0]))

        # Now try with new message after time...
        msg = Message((Message.SelType.NONE, 0), 0, 150.0, default=np.asarray([[[2048]]]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[-1]]], forward[0]))

        # Now try with new message again after...
        msg = Message((Message.SelType.NONE, 0), 0, 250.0, default=np.asarray([[[128]]]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[-3]]], forward[0]))

        # Now update at earliest
        ccg.update_scale(np.full([1, 1, 1], 5), -1)
        msg = Message((Message.SelType.NONE, 0), 0, 350.0, default=np.asarray([[[16]]]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[0]]], forward[0]))

        # Now try with new message again after...
        msg = Message((Message.SelType.NONE, 0), 0, 450.0, default=np.asarray([[[512]]]))
        self.assertNoException(BaseException, ccg._new_sample, 'Exception in CCG _new_sample', msg, forward)
        self.assertTrue(np.array_equal([[[0]]], forward[0]))

