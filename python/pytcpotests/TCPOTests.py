# This is the Global Test Module, which contains the base class for inheriting some common functionality

import threading
import unittest
import logging
import glob
import sys
import os

import numpy as np


# Class to wrap a thread and provide support for returning
class ThreadReturner(threading.Thread):

    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
        super(ThreadReturner, self).__init__(group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
        self.__return = None

    # This is copied from the threading.py implementation, albeit adding return value
    def run(self):
        try:
            if self._Thread__target:
                self.__return = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)
        finally:
            del self._Thread__target, self._Thread__args, self._Thread__kwargs

    def getResult(self):
        return self.__return


# Global Parent Test Case from which all test cases should inherit
class TCPOTester(unittest.TestCase):

    def setUp(self):
        super(TCPOTester, self).setUp()
        self.failures = []

    def tearDown(self):
        # Print any failures...
        self.assertEqual(len(self.failures), 0, 'Soft Failure(s) - \n' + self.__stringify(self.failures))

        # Call base Class finally
        super(TCPOTester, self).tearDown()

    def assertNoException(self, exception, callable, msg, *args):
        try:
            callable(*args)
        except BaseException as be:
            self.fail('Caught %s : %s' % (type(be).__name__, msg))

    # A Suite of Assertions which do not immediately fail
    def checkTrue(self, expr, msg=None):
        if not expr:
            self.failures.append(self._formatMessage(msg, "%s is not true" % str(expr)))

    # Can deal with Arrays or scalars
    def checkEqual(self, first, second, msg=None):
        if isinstance(first, np.ndarray):
            if not np.array_equal(first, second):
                self.failures.append(self._formatMessage(msg, '%s != %s' % (str(first), str(second))))
        elif not first == second:
            self.failures.append(self._formatMessage(msg, '%s != %s' % (str(first), str(second))))

    def checkElementOf(self, first, second, msg=None):
        for x in first :
            if not x in second :
                self.failures.append(self._formatMessage(msg, '%s not in %s' % (str(x), str(second))))

    def checkAlmostEqual(self, first, second, places=None, msg=None, delta=None):
        """
        Modification of the original function to handle checks rather than assertions, and also support numpy arrays
        """
        if isinstance(first, np.ndarray):
            if places is not None: raise TypeError("you can only specify delta for numpy arrays")
            if np.allclose(first, second, atol=1e-8 if delta is None else delta): return
            standardMsg = 'Some elements not equal (within %s delta) between %s and %s' % (str(delta), str(first), str(second))
        else:
            if first == second: return
            if delta is not None and places is not None: raise TypeError("specify delta or places not both")
            if delta is not None:
                if abs(first - second) <= delta: return
                standardMsg = '%s != %s within %s delta' % (str(first), str(second), str(delta))
            else:
                if places is None: places = 7
                if round(abs(second-first), places) == 0: return
                standardMsg = '%s != %s within %r places' % (str(first), str(second), places)
        self.failures.append(self._formatMessage(msg, standardMsg))

    def __stringify(self, l):
        s = ""
        for elem in l: s += elem + '\n'
        return s

if __name__ == "__main__":

    # Log to File to avoid tempering with screen output
    for f in glob.glob(os.path.join(".", "logs.log")): os.remove(f)  # Remove any previous ones...
    fh = logging.FileHandler('logs.log')
    fh.setFormatter(logging.Formatter('[%(asctime)s - %(levelname)s] {%(module)s:%(lineno)d} - %(message)s'))
    logging.getLogger().addHandler(fh)
    logging.getLogger().setLevel(logging.DEBUG)

    # Redirect Warnings/Exceptions which go uncaught
    sys.stderr = open('./warn.log', 'w')

    # Build pytcpotests Suite
    test_suite = unittest.TestLoader().discover("", pattern='Test*.py')

    # Now Run using default text runner
    unittest.TextTestRunner(verbosity=2).run(test_suite)

    # Print Goodbye
    print ''
    print '=========================================================='
    print '      All Logging output has been dumped to logs.log'
    print '     Any Warnings/Exceptions can be found in warn.log'
    print '          Redis Logs are available in redis.log'
    print '==========================================================\n'