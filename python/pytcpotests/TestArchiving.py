import numpy as np
import time as tm
import shutil
import redis
import os

import Import_script_2 as TI2
from Import_script_1 import SerE, SerF
from pytcpotests import TCPOTester
from pytcpo.Core.Archiving import ASerialiseable, ASerialiser, APersistor
from pytcpo.Core.Common import TCPOException, TCPOCode

import pytcpo.Modules.Archivers


class SimpleSer(ASerialiseable):

    def __init__(self):
        self.U = np.int16(5)
        self.V = np.float128(6.34)
        self.W = np.complex(1+2j)
        self.X = 1.0
        self.Y = 2
        self.Z = True

    def set(self, u, v, w, x, y, z):
        self.U = np.int16(u)
        self.V = np.float128(v)
        self.W = np.complex(w)
        self.X = float(x)
        self.Y = int(y)
        self.Z = bool(z)
        return self

    def serial(self):
        return {'U':np.int16, 'V':np.float128, 'W':np.complex, 'X':float, 'Y':int, 'Z':bool}


class CompSer(ASerialiseable):

    def __init__(self):
        self.SS = 9 # SimpleSer
        self.AI = 8 # Array of Integers
        self.AS = 7 # Array of Strings
        self.AA = 6 # Array of ASerialiseables
        self.W  = 5 # Single Word
        self.S  = 4 # Full String
        self.O  = 3 # Empty Array
        self.IO = 2 # Single-Entry Array of Objects
        self.IP = 1 # Single-Entry Array of Primitives

    def serial(self):
        return  {
                    'AI': np.ndarray,
                    'SS': SimpleSer,
                    'AS': np.ndarray,
                    'AA': np.ndarray,
                    'W' : str,
                    'S' : str,
                    'O' : np.ndarray,
                    'IO': np.ndarray,
                    'IP': np.ndarray
                }

    def init(self):
        self.SS = SimpleSer()
        self.AI = np.zeros((2,3,5), dtype=np.int)
        for i in range(2):
            for j in range(3):
                for k in range(5):
                    self.AI[i,j,k] = i*100 + j*10 + k
        self.AA = np.empty((5,2,3), dtype=object)
        for i in range(5):
            for j in range(2):
                for k in range(3):
                    self.AA[i,j,k] = SimpleSer()
                    self.AA[i, j, k].set(np.int16(i*j-k), np.float128(i*1.5 + j * 4.33 + k/2.0), np.complex(i,j), float(i), int(j), (k%2==0))
        self.AS = np.array(('Testing', 'Functionality of the', 'AS'))
        self.W  = "Still"
        self.S  = "Last but not least: Hello World!"
        self.O  = np.empty(0, dtype=float)
        self.IO = np.empty(1, dtype=np.object)
        self.IO[0] = SimpleSer()
        self.IP = np.empty(1, dtype=np.int16)
        self.IP[0] = 2222
        return self


class DummySerialiser(ASerialiser):

    def __init__(self):
        super (DummySerialiser, self).__init__()

    def _setup(self, mode, params):
        if not params: raise TCPOException(TCPOCode.Error.GENERIC)

    def _serialise(self, obj):
        return obj

    def _deserialise(self, sample, otype):
        return sample


class DummyArchiver(APersistor):

    random_access = False
    read_write = False

    @property
    def SupportsRandomAccess(self):
        return self.random_access

    @property
    def SupportsReadWrite(self):
        return self.read_write

    def __init__(self):
        super(DummyArchiver, self).__init__()
        self._data = []
        self._ts = []
        self._type = None
        self._idx = None
        self.write_ok = False
        self.read_ok = False

    def _initialise(self, mode, structure):
        self._data = []
        self._ts = []
        self._idx = 0
        if not structure: raise TCPOException(TCPOCode.Error.GENERIC)
        return True

    def _writeSample(self, key, timestamp, sample):
        if type(sample) not in ASerialiser.PRIMITIVES:
            raise TypeError
        else:
            self._ts.append(timestamp)
            self._data.append(sample)
            if not self.write_ok: raise TCPOException(TCPOCode.Error.GENERIC)
            return True

    def _seek(self, key, idx = -1):
        self._idx = idx

    def _skipSample(self, key, num = 1):
        self._idx += num
        return

    def _readSample(self, key, idx = None):
        if not self.read_ok:
            raise TCPOException(TCPOCode.Error.GENERIC)
        else:
            if idx is None:
                self._idx += 1
                return self._ts[self._idx-1], type(self._data[self._idx-1]), self._data[self._idx-1]
            else:
                return self._ts[idx], type(self._data[idx]), self._data[idx]

    def _close(self):
        pass

DummySerialiser.registerSerialiseable(SimpleSer)
DummySerialiser.registerSerialiseable(CompSer)

DummyArchiver.registerSerialiser(DummySerialiser)


class SerA(ASerialiseable): # Used as the base instance which should always be present!

    def __init__(self):
        self.a = 0

    def serial(self):
        return {'a':int}

ASerialiser.registerSerialiseable(SerA)


class SerB(ASerialiseable): # Used to test that using derived class also works

    def __init__(self):
        self.b = 1

    def serial(self):
        return {'b':int}

ASerialiser.registerSerialiseable(SerB)


class SerC(ASerialiseable): # Used to test that using instance of derived class also works

    def __init__(self):
        self.c = 2

    def serial(self):
        return {'c':int}


class SerD(ASerialiseable): # Used to test relative import with derived name

    def __init__(self):
        self.d = 3

    def serial(self):
        return {'d':int}
SD = SerD
ASerialiser.registerSerialiseable(SD)


class TestASerialiseable(TCPOTester):

    def test_equatability_simple(self):
        ss_1 = SimpleSer()
        ss_2 = SimpleSer()
        # from the get-go should work
        self.assertEqual(ss_1, ss_2)
        # Now, start changing bits and pieces
        ss_1.U = np.int16(3); self.assertNotEqual(ss_1, ss_2); ss_1.U = np.int16(5)
        ss_1.V = np.float128(2.25); self.assertNotEqual(ss_1, ss_2); ss_1.V = np.float128(6.34)
        ss_1.W = np.complex(-4-2.5j); self.assertNotEqual(ss_1, ss_2); ss_1.W = np.complex(1+2j)
        ss_1.X = 3.0; self.assertNotEqual(ss_1, ss_2); ss_1.X = 1.0
        ss_1.Y = 500; self.assertNotEqual(ss_1, ss_2); ss_1.Y = 2
        ss_1.Z = False; self.assertNotEqual(ss_1, ss_2); ss_1.Z = True
        # Now ensure again equal
        self.assertEqual(ss_1, ss_2)


class TestAbstractSerialiser(TCPOTester):

    def test_setup_failures(self):
        pass # TODO


class TestAbstractPersistor(TCPOTester):

    def test_error_handling_setup(self):
        dummy = DummyArchiver()

        # Ensure that fails if not setup
        self.assertRaises(TCPOException, dummy.WriteSample, None, 100, 5)
        self.assertRaises(TCPOException, dummy.ReadNext, None)
        self.assertRaises(TCPOException, dummy.ReadSample, None, 0)
        self.assertRaises(TCPOException, dummy.SkipNext, None, 0)

        # Ensure that fails if serialiseable is not correct or is not setup but otherwise ok
        self.assertRaises(KeyError, dummy.setup, dummy.M_WRITE, True, 'buq', True)
        self.assertRaises(TCPOException, dummy.setup, dummy.M_WRITE, True, 'DummySerialiser', False)
        self.assertEqual(dummy.setup(dummy.M_WRITE | dummy.M_APPEND, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)

        # Ensure that setup fails only if underlying function fails (but is otherwise successful)
        self.assertRaises(TCPOException, dummy.setup, dummy.M_WRITE, False, 'DummySerialiser', True)
        self.assertEqual(dummy.setup(dummy.M_WRITE, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        self.assertEqual(dummy.setup(dummy.M_WRITE | dummy.M_APPEND, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        self.assertRaises(TCPOException, dummy.setup, dummy.M_READ, False, 'DummySerialiser', True)
        self.assertEqual(dummy.setup(dummy.M_READ, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        self.assertRaises(TCPOException, dummy.setup, dummy.M_RDWRT, True, 'DummySerialiser', True)
        dummy.read_write = True
        self.assertRaises(TCPOException, dummy.setup, dummy.M_RDWRT, False, 'DummySerialiser', True)
        self.assertEqual(dummy.setup(dummy.M_RDWRT, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        self.assertEqual(dummy.setup(dummy.M_RDWRT | dummy.M_APPEND, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)

    def test_error_handling_write(self):
        dummy = DummyArchiver()
        self.assertEqual(dummy.setup(dummy.M_WRITE, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)

        #Should fail
        self.assertRaises(TCPOException, dummy.WriteSample, None, 0, 5)  # Handle Failure at end [Data will be stored]
        self.assertRaises(TypeError, dummy.WriteSample, None, 0, None) # Handle Failure at start [Data will be lost]

        # Now attempt successful writes
        dummy.write_ok = True
        self.assertEqual(dummy.WriteSample(None, 1, 10), TCPOCode.Status.OK)
        self.assertEqual(dummy.WriteSample(None, 2, 15), TCPOCode.Status.OK)

        # Ensure that written successfully...
        self.assertEqual(dummy._ts, [0, 1, 2])
        self.assertEqual(dummy._data, [5, 10, 15])

        # Should not be able to read (since in write-only mode)
        self.assertRaises(TCPOException, dummy.ReadNext, None)
        self.assertRaises(TCPOException, dummy.ReadSample, None, 0)
        self.assertRaises(TCPOException, dummy.SkipNext, None, 0)

    def test_error_handling_read(self):
        dummy = DummyArchiver()
        self.assertEqual(dummy.setup(dummy.M_READ, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        dummy._ts = [0, 1, 2, 3]
        dummy._data = [0.0, 5.0, 10.0, 15.0]
        dummy.write_ok = True

        # Should not be able to write...
        self.assertRaises(TCPOException, dummy.WriteSample, None, 0, 5)  # Cannot Write in read-only mode

        # Read will fail due to flag
        self.assertRaises(TCPOException, dummy.ReadNext, None)
        self.assertRaises(TCPOException, dummy.ReadSample, None, 1)

        # Now Should succeed on Serial Reads but not Random
        dummy.read_ok = True
        self.assertEqual((0, 0.0), dummy.ReadNext(None))
        self.assertRaises(TCPOException, dummy.ReadSample, None, 1)

        # Now allow random reads
        dummy.random_access = True
        self.assertEqual((1, 5.0), dummy.ReadNext(None))
        self.assertEqual((0, 0.0), dummy.ReadSample(None, 0))

    def test_error_handling_read_write(self):
        dummy = DummyArchiver()
        dummy.read_write = True
        self.assertEqual(dummy.setup(dummy.M_RDWRT, True, 'DummySerialiser', True), TCPOCode.Status.SETUP)
        dummy.write_ok = True
        dummy.read_ok = True

        # Should not be able to write...
        self.assertEqual(dummy.WriteSample(None, 0, 5.0), TCPOCode.Status.OK)
        self.assertEqual(dummy.WriteSample(None, 1, 10.0), TCPOCode.Status.OK)

        # Now Should succeed on Serial Reads but not Random
        dummy.read_ok = True
        self.assertEqual((0, 5.0), dummy.ReadNext(None))
        self.assertRaises(TCPOException, dummy.ReadSample, None, 1)

        # Now allow random reads
        dummy.random_access = True
        self.assertEqual((1, 10.0), dummy.ReadNext(None))
        self.assertEqual((0, 5.0), dummy.ReadSample(None, 0))

    def test_importing(self):
        dummy = DummySerialiser()
        dummy.registerSerialiseable(SerC)

        # Check first importing from the Abstract Class
        self.assertEqual(ASerialiser.getSerialiseable('SerA'), SerA)
        self.assertEqual(ASerialiser.getSerialiseable('SerB'), SerB)
        self.assertEqual(ASerialiser.getSerialiseable('SerC'), SerC)
        self.assertEqual(ASerialiser.getSerialiseable('SerD'), SerD)
        self.assertEqual(ASerialiser.getSerialiseable('SerE'), SerE)
        self.assertEqual(ASerialiser.getSerialiseable('SerF'), SerF)
        self.assertEqual(ASerialiser.getSerialiseable('SerG'), TI2.SerG)
        self.assertRaises(KeyError, ASerialiser.getSerialiseable, 'a')

        # Now check import from derived class
        self.assertEqual(DummySerialiser.getSerialiseable('SerA'), SerA)
        self.assertEqual(DummySerialiser.getSerialiseable('SerB'), SerB)
        self.assertEqual(DummySerialiser.getSerialiseable('SerC'), SerC)
        self.assertEqual(DummySerialiser.getSerialiseable('SerD'), SerD)
        self.assertEqual(DummySerialiser.getSerialiseable('SerE'), SerE)
        self.assertEqual(DummySerialiser.getSerialiseable('SerF'), SerF)
        self.assertEqual(DummySerialiser.getSerialiseable('SerG'), TI2.SerG)
        self.assertRaises(KeyError, DummySerialiser.getSerialiseable, 'a')

        # Finally, check import from instantiated object
        self.assertEqual(dummy.getSerialiseable('SerA'), SerA)
        self.assertEqual(dummy.getSerialiseable('SerB'), SerB)
        self.assertEqual(DummySerialiser().getSerialiseable('SerC'), SerC)
        self.assertEqual(dummy.getSerialiseable('SerD'), SerD)
        self.assertEqual(dummy.getSerialiseable('SerE'), SerE)
        self.assertEqual(dummy.getSerialiseable('SerF'), SerF)
        self.assertEqual(dummy.getSerialiseable('SerG'), TI2.SerG)
        self.assertRaises(KeyError, dummy.getSerialiseable, 'a')


class TestSerialisers(TCPOTester):

    def test_error_handling(self):
        dummy = DummySerialiser()
        # Assert Setup Failures
        self.assertRaises(TCPOException, DummySerialiser().setup, True, False)  # Failure due to derived failure
        self.assertEqual(TCPOCode.Status.SETUP, dummy.setup(True, True))        # Success
        self.assertRaises(TCPOException, dummy.setup, True, True)               # Failure due to resetup attempt

        # Assert Usage Failures
        self.assertRaises(TCPOException, DummySerialiser().serialise, 5.0)
        self.assertRaises(TCPOException, DummySerialiser().deserialise, 5.0, float)

    def test_simple_storage(self):

        # Iterate over all Registered Serialisers
        for serialiser in APersistor._APersistor__Serialisers:
            if serialiser == 'DummySerialiser': continue

            # Store the Type
            stype = APersistor.getSerialiser(serialiser)

            # Create Data Types
            s_data = SimpleSer()
            s_writer = stype(); s_writer.setup(ASerialiser.M_WRITE, APersistor.getTestSetup(serialiser))
            s_reader = stype(); s_reader.setup(ASerialiser.M_READ, APersistor.getTestSetup(serialiser))

            # First Try
            s_obj = s_writer.serialise(s_data)
            self.assertIsNotNone(s_obj)
            r_data = s_reader.deserialise(s_obj, SimpleSer)
            self.assertEqual(s_data, r_data)

            # Second Try
            s_obj = s_writer.serialise(s_data.set(5, 2.3, -4.3 + 5.6j, 2.5, -1, False))
            self.assertIsNotNone(s_obj)
            r_data = s_reader.deserialise(s_obj, SimpleSer)
            self.assertEqual(s_data, r_data)

            # Final Try
            s_obj = s_writer.serialise(s_data.set(np.int16(355), np.float128(10.45), np.complex(456.4 + 543j), -2, 8, False))
            self.assertIsNotNone(s_obj)
            r_data = s_reader.deserialise(s_obj, SimpleSer)
            self.assertEqual(s_data, r_data)

    def test_complex_storage(self):

        # Iterate over all Registered Serialisers
        for serialiser in APersistor._APersistor__Serialisers:
            if serialiser == 'DummySerialiser': continue

            # Store the Type
            stype = APersistor.getSerialiser(serialiser)

            # Complex Storage object
            s_data = CompSer(); s_data.init()
            s_writer = stype(); s_writer.setup(ASerialiser.M_WRITE, APersistor.getTestSetup(serialiser))
            s_reader = stype(); s_reader.setup(ASerialiser.M_READ, APersistor.getTestSetup(serialiser))

            # Do First Write-out
            s_obj = s_writer.serialise(s_data)
            self.assertIsNotNone(s_obj)
            r_data = s_reader.deserialise(s_obj, CompSer)
            self.assertEqual(s_data, r_data)

            # Second Try
            s_data.AS = np.array(("Still",))
            s_data.W  = "Testing"
            s_data.SS.set(-67, 23.333, -4.3 + 5.6j, 2.5, -10, False)
            s_data.AA[4,1,2].set(-34, 1e5, 5.0, -1.2, 100, False)
            s_data.O = np.empty(0, np.object)
            s_data.IP[0] = -2222
            s_data.IO[0] = SimpleSer().set(-67, 23.10, -4.3 + 0.6j, 2.25, -12, False)
            s_obj = s_writer.serialise(s_data)
            self.assertIsNotNone(s_obj)
            r_data = s_reader.deserialise(s_obj, CompSer)
            self.assertEqual(s_data, r_data, 'Equality Failure for %s Serialiser' % serialiser)

    def test_singular_dimension_storage(self):
        
        # Iterate over all Registered Serialisers
        for serialiser in APersistor._APersistor__Serialisers:
            if serialiser == 'DummySerialiser': continue
            
            # Store the Type
            stype = APersistor.getSerialiser(serialiser)
            
            # Now the data to be stored ...
            s_data = [np.zeros(0, dtype=np.uint16),
                      np.zeros(1, dtype=np.float),
                      np.ones((1, 1), dtype=np.int),
                      np.empty(0, dtype=np.object)]

            # ... and the writers/readers
            s_writer = stype(); s_writer.setup(ASerialiser.M_WRITE, APersistor.getTestSetup(serialiser))
            s_reader = stype(); s_reader.setup(ASerialiser.M_READ, APersistor.getTestSetup(serialiser))

            # Write out the Data
            for i in range(len(s_data)):
                s_obj = s_writer.serialise(s_data[i])
                self.assertIsNotNone(s_obj)
                r_data = s_reader.deserialise(s_obj, np.ndarray)
                self.assertEqual(s_data[i].dtype, r_data.dtype)
                self.assertEqual(s_data[i].size, r_data.size)
                self.assertTrue(np.array_equal(s_data[i].shape, r_data.shape))
                self.assertTrue(np.array_equal(s_data[i], r_data))


class TestFilePersistor(TCPOTester):

    test_dir = '/tmp/test_tfa'

    def setUp(self):
        super(TestFilePersistor, self).setUp()

    def tearDown(self):
        shutil.rmtree(self.test_dir, ignore_errors=True)
        super(TestFilePersistor, self).tearDown()

    # Check that simple key structure is handled
    def test_key_structure_simple(self):
        tfa = pytcpo.Modules.Archivers.FilePersistor()

        # Simple structure handled ok
        self.assertEqual(tfa.setup(APersistor.M_WRITE, {'source': self.test_dir + '/alpha', 'keys': {'a': int}},
                                   'DummySerialiser', True), TCPOCode.Status.SETUP)

        self.assertTrue(os.path.isdir(self.test_dir + '/alpha'))
        self.assertFalse(os.path.isdir(self.test_dir + '/alpha/a.tfa'))
        self.assertTrue(os.path.exists(self.test_dir + '/alpha/a.tfa'))

        # Simple Structure handled ok with extra backslash
        self.assertEqual(tfa.setup(APersistor.M_WRITE, {'source': self.test_dir + '/beta/', 'keys': {'b': int}},
                                   'DummySerialiser', True), TCPOCode.Status.SETUP)

        self.assertTrue(os.path.isdir(self.test_dir + '/beta'))
        self.assertFalse(os.path.isdir(self.test_dir + '/beta/b.tfa'))
        self.assertTrue(os.path.exists(self.test_dir + '/beta/b.tfa'))

    def test_key_structure_complex(self):

        tfa = pytcpo.Modules.Archivers.FilePersistor()

        # Simple structure handled ok
        self.assertEqual(tfa.setup(APersistor.M_WRITE, {'source': self.test_dir,
                                                        'keys': {'56/79/a': int, '56/78/b': int, '57/c': int}},
                                   'DummySerialiser', True), TCPOCode.Status.SETUP)
        # Check Directories
        self.assertTrue(os.path.isdir(self.test_dir + '/56/79'))
        self.assertTrue(os.path.isdir(self.test_dir + '/56/78'))
        self.assertTrue(os.path.isdir(self.test_dir + '/57'))
        # Check Files
        self.assertFalse(os.path.isdir(self.test_dir + '/56/79/a.tfa'))
        self.assertTrue(os.path.exists(self.test_dir + '/56/79/a.tfa'))
        self.assertFalse(os.path.isdir(self.test_dir + '/56/78/b.tfa'))
        self.assertTrue(os.path.exists(self.test_dir + '/56/78/b.tfa'))
        self.assertFalse(os.path.isdir(self.test_dir + '/57/c.tfa'))
        self.assertTrue(os.path.exists(self.test_dir + '/57/c.tfa'))

    def test_key_handling(self):

        tfa = pytcpo.Modules.Archivers.FilePersistor()

        # First Setup and ensure that ok
        self.assertEqual(tfa.setup(APersistor.M_WRITE, {'source':self.test_dir, 'keys': {'a': int, 'b': float}},
                                   'CSVSerialiser', None), TCPOCode.Status.SETUP)

        # Now Check failure to write to non-existent key
        self.assertRaises(KeyError, tfa.WriteSample, 'c', 100, 0.5)

        # Now write to the keys
        self.assertEqual(tfa.WriteSample('a', 1.0, 1), TCPOCode.Status.OK)
        self.assertEqual(tfa.WriteSample('a', 2.0, 5), TCPOCode.Status.OK)
        self.assertEqual(tfa.WriteSample('b', 3.0, 10.5), TCPOCode.Status.OK)
        self.assertEqual(tfa.WriteSample('a', 4.0, 12), TCPOCode.Status.OK)

        # Check failure again to ensure does not corrupt
        self.assertRaises(KeyError, tfa.WriteSample, 'c', 5.0, 0.5)

        # Now Attempt read back
        self.assertEqual(tfa.setup(APersistor.M_READ, {'source':self.test_dir, 'keys': {'a': int, 'b': float}},
                                   'CSVSerialiser', None), TCPOCode.Status.SETUP)

        # Check Failure to read from a non-existent key
        self.assertRaises(KeyError, tfa.ReadNext, 'c')

        # Now read in the samples
        self.assertEqual((3.0, 10.5), tfa.ReadNext('b'))
        self.assertEqual((1.0, 1), tfa.ReadNext('a'))
        self.assertEqual((2.0, 5), tfa.ReadNext('a'))
        self.assertEqual((4.0, 12), tfa.ReadNext('a'))

        # There should be failure in attempt to read more
        self.assertRaises(StopIteration, tfa.ReadNext, 'a')


class TestRedisPersistor(TCPOTester):

    def __init__(self, methodName='runTest'):
        super(TestRedisPersistor, self).__init__(methodName)

    @classmethod
    def setUpClass(cls):
        super(TestRedisPersistor, cls).setUpClass()
        with open('tcpo.test.conf', 'w') as config:
            config.write('daemonize yes\n'
                         'port 1500\n'
                         'bind 127.0.0.1\n'
                         'logfile redis.log\n'
                         'requirepass tcpo.test.2oi7')

    @classmethod
    def tearDownClass(cls):
        os.remove('tcpo.test.conf')
        super(TestRedisPersistor, cls).tearDownClass()

    def setUp(self):
        super(TestRedisPersistor, self).setUp()
        os.system('redis-server tcpo.test.conf')
        tm.sleep(0.1) # Give chance for it to come up

    def tearDown(self):
        client = redis.StrictRedis(host='localhost', port=1500, password='tcpo.test.2oi7', socket_connect_timeout=0.2)
        client.flushall()
        client.shutdown()
        super(TestRedisPersistor, self).tearDown()

    def test_setup_errors(self):

        rpers = pytcpo.Modules.Archivers.RedisPersistor

        # Check inexistent keys
        self.assertRaises(KeyError, rpers().setup, APersistor.M_WRITE,
                          {'host': 'localhost', 'auth':'tcpo.test.2oi7', 'keys': {}},
                          'DummySerialiser', True)
        self.assertRaises(KeyError, rpers().setup, APersistor.M_WRITE,
                          {'port': 1500, 'auth':'tcpo.test.2oi7', 'keys': {}},
                          'DummySerialiser', True)

        # Now invalid Server Connection
        self.assertRaises(redis.ConnectionError, rpers().setup, APersistor.M_WRITE,
                          {'host': '1.1.1.1', 'port': 1500, 'auth':'tcpo.test.2oi7', 'keys': {}},
                          'DummySerialiser', True)
        self.assertRaises(redis.ConnectionError, rpers().setup, APersistor.M_WRITE,
                          {'host': 'localhost', 'port': 101, 'auth':'tcpo.test.2oi7', 'keys': {}},
                          'DummySerialiser', True)

        # Now Password Error
        self.assertRaises(redis.ResponseError, rpers().setup, APersistor.M_WRITE,
                          {'host': 'localhost', 'port': 1500, 'keys': {}},
                          'DummySerialiser', True)

        # Now should succeed
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers().setup(APersistor.M_WRITE, {'host': 'localhost', 'port': 1500, 'auth':'tcpo.test.2oi7',
                                                            'keys': {}}, 'DummySerialiser', True))

    def test_database_selection(self):

        rpers = pytcpo.Modules.Archivers.RedisPersistor()

        # First write something to db 1
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_WRITE, {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7',
                                                          'keys': {'a': int}, 'db': 1}, 'DummySerialiser', True))

        self.assertEqual(TCPOCode.Status.OK, rpers.WriteSample('a', 1.0, 5))

        # Now open db 0 (default) and read - should be nothing...
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_RDWRT, {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7',
                                                          'keys': {'a': int}}, 'DummySerialiser', True))
        self.assertRaises(BaseException, rpers.ReadSample, 'a', 0)  # There should be nothing

        # Write something to check...
        self.assertEqual(TCPOCode.Status.OK, rpers.WriteSample('a', 2.0, 15))

        # Now open db 1 and read - ensure only one value
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_RDWRT, {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7',
                                                          'keys': {'a': int}, 'db': 1}, 'DummySerialiser', True))
        self.assertEqual((1.0, '5'), rpers.ReadNext('a'))
        self.assertRaises(BaseException, rpers.ReadNext, 'a')  # There should be nothing else

        # Now back to db 0 (specified)
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_READ, {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7',
                                                         'keys': {'a': int}, 'db': 0}, 'DummySerialiser', True))
        self.assertEqual((2.0, '15'), rpers.ReadNext('a'))
        self.assertRaises(BaseException, rpers.ReadNext, 'a')  # There should be nothing else

    def test_append_writeon_modes(self):
        rpers = pytcpo.Modules.Archivers.RedisPersistor()

        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_RDWRT, {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7',
                                                          'keys': {'a':int}}, 'DummySerialiser', True))

        self.assertRaises(BaseException, rpers.ReadSample, 'a', 0)  # There should be nothing

        # Write Something
        self.assertEqual(TCPOCode.Status.OK, rpers.WriteSample('a', 1.0, 5))
        self.assertEqual(TCPOCode.Status.OK, rpers.WriteSample('a', 2.0, 10))

        # Ensure there, but no more than 2
        self.assertEqual((1.0, '5'), rpers.ReadSample('a', 0))
        self.assertRaises(BaseException, rpers.ReadSample, 'a', 2)  # There should be nothing else

        # Now re-open in append mode
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_RDWRT | APersistor.M_APPEND,
                                     {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7', 'keys': {'a': int}},
                                     'DummySerialiser', True))

        # Ensure there...
        self.assertEqual((2.0, '10'), rpers.ReadSample('a', 1))

        # Write Something else...
        self.assertEqual(TCPOCode.Status.OK, rpers.WriteSample('a', 3.0, 20))

        # Now Re-open in read mode - not append, but should not erase
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_READ,
                                     {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7', 'keys': {'a': int}},
                                     'DummySerialiser', True))

        self.assertEqual((3.0, '20'), rpers.ReadSample('a', 2))

        # Finally re-open in just write mode
        self.assertEqual(TCPOCode.Status.SETUP,
                         rpers.setup(APersistor.M_WRITE,
                                     {'host': 'localhost', 'port': 1500, 'auth': 'tcpo.test.2oi7', 'keys': {'a': int}},
                                     'DummySerialiser', True))

        self.assertRaises(BaseException, rpers.ReadSample, 'a', 0)  # There should be nothing again

    def test_key_handling(self):

        persistor = pytcpo.Modules.Archivers.RedisPersistor()

        # Set up in read-write mode
        self.assertEqual(TCPOCode.Status.SETUP,
                         persistor.setup(APersistor.M_RDWRT, {'host': 'localhost', 'port': 1500,
                                                              'auth': 'tcpo.test.2oi7', 'keys': {'a': int, 'b': float}},
                                         'DummySerialiser', True))

        # Now Check failure to write to non-existent key
        self.assertRaises(KeyError, persistor.WriteSample, 'c', 100, 0.5)

        # Now write to the keys
        self.assertEqual(persistor.WriteSample('a', 1.0, 1), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('a', 2.0, 5), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('b', 3.0, 10.5), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('a', 4.0, 12), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('b', 5.5, 12.125), TCPOCode.Status.OK)

        # Check failure again to ensure does not corrupt
        self.assertRaises(KeyError, persistor.WriteSample, 'c', 5.0, 0.5)

        # Now read back
        # Check Failure to read from a non-existent key
        self.assertRaises(KeyError, persistor.ReadNext, 'c')

        # Now read in the samples
        self.assertEqual((3.0, '10.5'), persistor.ReadNext('b'))
        self.assertEqual((1.0, '1'), persistor.ReadNext('a'))
        self.assertEqual((3.0, '10.5'), persistor.ReadSample('b', 0))
        self.assertEqual((2.0, '5'), persistor.ReadNext('a'))
        self.assertEqual((4.0, '12'), persistor.ReadNext('a'))
        self.assertEqual((4.0, '12'), persistor.ReadSample('a', 2))
        self.assertEqual((5.5, '12.125'), persistor.ReadNext('b'))

        # Check that reading from negative indices works as expected
        self.assertEqual((4.0, '12'), persistor.ReadSample('a', -1))
        self.assertEqual((1.0, '1'), persistor.ReadSample('a', -3))
        self.assertEqual((3.0, '10.5'), persistor.ReadSample('b', -2))

        # Read in non-existent values
        self.assertRaises(BaseException, persistor.ReadNext, 'a')
        self.assertRaises(BaseException, persistor.ReadNext, 'b')
        self.assertRaises(BaseException, persistor.ReadSample, 'a', 5)
        self.assertRaises(BaseException, persistor.ReadSample, 'b', -3)

        # But can still read arbitrary values
        self.assertEqual((2.0, '5'), persistor.ReadSample('a', 1))
        self.assertEqual((5.5, '12.125'), persistor.ReadSample('b', 1))

        # Now Reset read
        self.assertEqual(TCPOCode.Status.SETUP,
                         persistor.setup(APersistor.M_READ, {'host': 'localhost', 'port': 1500,
                                                             'auth': 'tcpo.test.2oi7', 'keys': {'a': int, 'b': float}},
                                         'DummySerialiser', True))

        self.assertRaises(TCPOException, persistor.SkipNext, 'a', 3)
        self.assertEqual(TCPOCode.Status.OK, persistor.SkipNext('a', 2))
        self.assertEqual((4.0, '12'), persistor.ReadSample('a', 2))

    def test_seeking(self):

        persistor = pytcpo.Modules.Archivers.RedisPersistor()

        # Set up in read-write mode
        self.assertEqual(TCPOCode.Status.SETUP,
                         persistor.setup(APersistor.M_RDWRT, {'host': 'localhost', 'port': 1500, 'name': 'test',
                                                              'auth': 'tcpo.test.2oi7', 'keys': {'a': int}},
                                         'DummySerialiser', True))

        # Now write to the keys
        self.assertEqual(persistor.WriteSample('a', 1.0, 1), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('a', 2.0, 5), TCPOCode.Status.OK)
        self.assertEqual(persistor.WriteSample('a', 3.0, 6), TCPOCode.Status.OK)

        # Now seek to -1 and read (ensuring only 1 read)
        self.assertNoException(BaseException, persistor.Seek, 'Seek Error', 'a', -1)
        self.assertEqual(persistor.ReadNext('a'), (3.0, '6'))
        self.assertRaises(BaseException, persistor.ReadNext, 'Read Error', 'a')

        # Now write another one and should again read ok
        self.assertEqual(persistor.WriteSample('a', 4.0, 8), TCPOCode.Status.OK)
        self.assertEqual(persistor.ReadNext('a'), (4.0, '8'))
        self.assertEqual(persistor.WriteSample('a', 5.0, 10), TCPOCode.Status.OK)
        self.assertEqual(persistor.ReadNext('a'), (5.0, '10'))

        # Now Seek to another point...
        self.assertNoException(BaseException, persistor.Seek, 'Seek Error', 'a', 2)
        self.assertEqual(persistor.ReadNext('a'), (3.0, '6'))
        self.assertNoException(BaseException, persistor.Seek, 'Seek Error', 'a', -5)
        self.assertEqual(persistor.ReadNext('a'), (1.0, '1'))
        self.assertNoException(BaseException, persistor.Seek, 'Seek Error', 'a', 4)
        self.assertEqual(persistor.ReadNext('a'), (5.0, '10'))

        # Check that seeking past the end returns an error
        self.assertRaises(TCPOException, persistor.Seek, 'a', 5)
        self.assertRaises(TCPOException, persistor.Seek, 'a', -6)
