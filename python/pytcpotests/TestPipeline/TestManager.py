import time as tm
import numpy as np

from pytcpotests import TCPOTester

from pytcpo.Core.Common import TCPOCode, TCPOException
from pytcpo.Core.PipeLine import Message, PipelineManager, AbstractProcessor, AbstractGenerator
from pytcpo.Core.Archiving import ASerialiser, APersistor, PersistorFactory


class ManagerWrapper(PipelineManager):

    @property
    def Modules(self):
        return self._PipelineManager__pipeline_d


class DummySerialiser(ASerialiser):

    def __init__(self):
        super (DummySerialiser, self).__init__()

    def _setup(self, mode, params):
        if not params: raise TCPOException(TCPOCode.Error.GENERIC)

    def _serialise(self, obj):
        return obj

    def _deserialise(self, sample, otype):
        return sample


class DummyArchiver(APersistor):

    random_access = False
    read_write = False

    @property
    def SupportsRandomAccess(self):
        return self.random_access

    @property
    def SupportsReadWrite(self):
        return self.read_write

    def __init__(self):
        super(DummyArchiver, self).__init__()
        self._data = []
        self._ts = []
        self._type = None
        self._idx = None
        self.write_ok = False
        self.read_ok = False

    def _initialise(self, mode, metadata):
        self._data = []
        self._ts = []
        self._idx = 0
        if not metadata: raise TCPOException(TCPOCode.Error.GENERIC)
        return True

    def _writeSample(self, key, timestamp, sample):
        if type(sample) not in ASerialiser.PRIMITIVES:
            raise TypeError
        else:
            self._ts.append(timestamp)
            self._data.append(sample)
            if not self.write_ok: raise TCPOException(TCPOCode.Error.GENERIC)
            return True

    def _seek(self, key, idx = -1):
        self._idx = idx
        return

    def _skipSample(self, key, num = 1):
        self._idx += num
        return

    def _readSample(self, key, idx = None):
        if not self.read_ok:
            raise TCPOException(TCPOCode.Error.GENERIC)
        else:
            if idx is None:
                self._idx += 1
                return self._ts[self._idx-1], self._data[self._idx-1]
            else:
                return self._ts[idx], self._data[idx]

    def _close(self):
        pass

PersistorFactory.registerPersistor(DummyArchiver)

DummyArchiver.registerSerialiser(DummySerialiser)


class DummyProc(AbstractProcessor):

    @property
    def Consumers(self):
        return self._consumers

    def __init__(self):
        super(DummyProc, self).__init__()
        # Control
        self._allow_start = True
        self._fail_sample = -1
        self._operation = None # Operation to perform on the data
        self._operand = None
        # Diagnostic Parameters
        self._data = []
        self._start_times = []
        self._stop_times = []
        self._stop_codes = []

    def _setup(self, filter_model):
        if not filter_model:
            raise TCPOException(TCPOCode.Error.GENERIC)
        return filter_model

    def _will_start(self):
        self._start_times.append(tm.time())
        if not self._allow_start:
            raise TCPOException(TCPOCode.Error.GENERIC)
        return self._allow_start

    def _new_sample(self, message, forward):
        if self._fail_sample < 0:
            self._data.append(message.Default)
            if self._operation == 'mult':
                forward[0] = message.Default * self._operand
            elif self._operation == 'add':
                forward[0] = message.Default + self._operand
            elif self._operation == 'sum':
                forward[0] = message.Default + message[self._operand]
            else:
                forward[0] = message.Default
            return True

        elif self._fail_sample > 0:
            self._fail_sample -= 1
            self._data.append(message.Default)
            forward[0] = message.Default
            return True

        else:
            self._data.append(message.Default) # Still append, but fail...
            raise TCPOException(TCPOCode.Error.DUPLICATE)

    def _clean_up(self, reason):
        self._stop_times.append(tm.time())
        self._stop_codes.append(reason)
        return True

    def _invokenoargs(self):
        return 1

    def _invokeargs(self, arg):
        return arg+2


PipelineManager.registerModule(DummyProc)


class DummyGenerator(AbstractGenerator):

    def __init__(self):
        super(DummyGenerator, self).__init__()
        self._data = []
        self._length = None
        self._rate = None
        self._start_times=[]
        self._stop_times=[]
        self._stop_codes=[]

    def _setup(self, filter_model):
        return True

    def _generator_loop(self):
        self._start_times.append(tm.time())
        code_ret = TCPOCode.Status.FILE_END

        # Loop
        for i in range(self._length):
            if self._UserStop.wait(self._rate):
                code_ret = TCPOCode.Status.USER_STOP
                break
            self._data.append(np.random.randint(-100, 100))
            if not self._push(Message((Message.SelType.NONE, 0), i, tm.time(), self.Name, self._data[i]), None):
                code_ret = TCPOCode.Error.ARCHIVE
                break

        # Store Time
        self._stop_times.append(tm.time())
        self._stop_codes.append(code_ret)

        # Inform that terminating if not user-stopped...
        if not self._UserStop.is_set(): self._inform(TCPOCode.Request.STOP_FILE)

PipelineManager.registerModule(DummyGenerator)


class TestManagerBasicErrorHandling(TCPOTester):

    def test_no_root(self):
        self.assertRaises(TCPOException, ManagerWrapper().add_module, 'DummyProc', True, ('a',))
        self.assertRaises(TCPOException, ManagerWrapper().build)
        self.assertEqual(ManagerWrapper().start(), TCPOCode.Error.STATE)
        self.assertEqual(ManagerWrapper().stop(), TCPOCode.Warning.STATE)
        self.assertEqual(ManagerWrapper().wait_until_ready(1), TCPOCode.Warning.STATE)

    def test_no_build(self):
        self.assertIsNotNone(ManagerWrapper().add_root('DummyProc', True))
        self.assertEqual(ManagerWrapper().start(), TCPOCode.Error.STATE)
        self.assertEqual(ManagerWrapper().stop(), TCPOCode.Warning.STATE)
        self.assertEqual(ManagerWrapper().wait_until_ready(1), TCPOCode.Warning.STATE)

    def test_no_start(self):
        manager = ManagerWrapper()
        self.assertIsNotNone(manager.add_root('DummyProc', True))
        self.assertEqual(manager.build(), TCPOCode.Status.OK)
        self.assertEqual(ManagerWrapper().stop(), TCPOCode.Warning.STATE)
        self.assertEqual(ManagerWrapper().wait_until_ready(1), TCPOCode.Warning.STATE)

    def test_start_ok(self):
        manager = ManagerWrapper()
        self.assertIsNotNone(manager.add_root('DummyProc', True))
        self.assertEqual(manager.build(), TCPOCode.Status.OK)
        self.assertEqual(manager.start(), TCPOCode.Status.ACTIVE)
        self.assertEqual(manager.stop(), TCPOCode.Status.USER_STOP)

    def test_module_setup_failures(self):
        manager = ManagerWrapper()
        self.assertRaises(TCPOException, manager.add_root, 'DummyProc', False)
        self.assertEqual(len(manager.Modules), 0)
        root = manager.add_root('DummyProc', True)
        self.assertIsNotNone(root)
        self.assertRaises(KeyError, manager.add_module, 'Dummy', True, (root,))
        self.assertRaises(TCPOException, manager.add_module, 'DummyProc', False, (root,))
        leaf = manager.add_module('DummyProc', True, (root,))
        self.assertIsNotNone(leaf)
        self.assertRaises(TCPOException, manager.add_module, 'DummyProc', True, ('a',))
        self.assertEqual(len(manager.Modules), 2)
        self.assertEqual(len(manager.Modules[root].Consumers), 1)
        self.assertEqual(manager.Modules[root].Consumers[0], manager.Modules[leaf])

        # Check with Archiver Setup
        self.assertRaises(TCPOException, manager.add_module, 'DummyProc', True, (root,), 'archiver') # Cannot add module without archiver
        self.assertEqual(manager.set_archiver(persistor={'type':'DummyArchiver', 'source':None, 'setup':True},
                                              serialiser={'type':'DummySerialiser', 'setup':True}), TCPOCode.Status.OK) # Success in adding archiver
        self.assertRaises(TCPOException, manager.set_archiver,
                          persistor={'type':'DummyArchiver', 'source':None, 'setup':True},
                          serialiser={'type': 'DummySerialiser', 'setup': True}) # Cannot add another archiver once one is setup
        self.assertIsNotNone(manager.add_module('DummyProc', True, (root,), 'archiver')) # Able to add module
        self.assertEqual(len(manager.Modules), 3)   # Check that indeed module was added...
        self.assertEqual(len(manager.Modules[root].Consumers), 2) #  ... and to correct location

    def test_archiver_start_failure(self):
        manager = ManagerWrapper()

        # First Set Wrongly
        self.assertNoException(TCPOException, manager.add_root, 'Should Succeed in Setting Root', 'DummyProc', True)
        self.assertEqual(manager.set_archiver(persistor={'type':'DummyArchiver', 'source':None, 'setup':True},
                                              serialiser={'type':'WrongName', 'setup': True}), TCPOCode.Status.OK)
        self.assertEqual(manager.build(), TCPOCode.Status.OK)
        self.assertEqual(manager.start(), TCPOCode.Error.MODULE)
        self.assertNoException(TCPOException, manager.reset, msg='Reset Did Not Succeed')

        # Now Set ok
        self.assertNoException(TCPOException, manager.add_root, 'Should Succeed in Setting Root', 'DummyProc', True)
        self.assertEqual(manager.set_archiver(persistor={'type': 'DummyArchiver', 'source': None, 'setup': True},
                                              serialiser={'type': 'DummySerialiser', 'setup': True}),
                         TCPOCode.Status.OK)
        self.assertEqual(manager.build(), TCPOCode.Status.OK)
        self.assertEqual(manager.start(), TCPOCode.Status.ACTIVE)

    def test_invoke_capability(self):
        manager = ManagerWrapper()
        self.assertNoException(TCPOException, manager.add_root, 'Should Succeed in Setting Root', 'DummyProc', True)

        # First try to invoke when not built
        self.assertRaises(TCPOException, manager.invoke, manager.Last, '_invokenoargs')

        # Now build
        self.assertEqual(manager.build(), TCPOCode.Status.OK)

        # Now try to invoke with wrong method or wrong module
        self.assertRaises(KeyError, manager.invoke, 'wrong', '_invokenoargs')
        self.assertRaises(AttributeError, manager.invoke, manager.Last, 'wrong')

        # Finally correct invocation
        self.assertEqual(1, manager.invoke(manager.Last, '_invokenoargs'))
        self.assertEqual(5, manager.invoke(manager.Last, '_invokeargs', 3))


class TestManagerStructureDefinition(TCPOTester):

    # Class Attribute
    callback_exit_code = None

    @staticmethod
    def _static_cb(exitcode):
        TestManagerStructureDefinition.callback_exit_code = exitcode

    def setUp(self):
        super(TestManagerStructureDefinition, self).setUp()
        self.manager = ManagerWrapper()
        self.proc_list = []
        # Build Structure
        self.G = self.manager.Modules[self.manager.add_root('DummyGenerator', None)]
        self.proc_list.append(self.G)
        self.A = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.G.Name,))]
        self.proc_list.append(self.A)
        self.B = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.A.Name,))]
        self.proc_list.append(self.B)
        self.C = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.A.Name,))]
        self.proc_list.append(self.C)
        self.D = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.B.Name,))]
        self.proc_list.append(self.D)
        self.E = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.D.Name, self.C.Name))]
        self.proc_list.append(self.E)
        self.F = self.manager.Modules[self.manager.add_module('DummyProc', True, (self.E.Name,))]
        self.proc_list.append(self.F)
        # Perform some basic configuration
        self.G._length = 10
        self.G._rate = 0.01
        # Ensure build happens ok
        self.assertEqual(TCPOCode.Status.OK, self.manager.build())
        # Clean up Class Method
        TestManagerStructureDefinition.callback_exit_code = None

    def tearDown(self):
        self.manager.stop()
        super(TestManagerStructureDefinition, self).tearDown()

    # Test that the start order is from leaves to parents...
    def test_start_order_success(self):
        # Ensure started
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start())
        # Ensure none of the starts get called more than once...
        for proc in self.proc_list: self.assertEqual(1, len(proc._start_times))
        # Ensure order is followed:
        self.assertGreater(self.E._start_times[0], self.F._start_times[0])
        self.assertGreater(self.D._start_times[0], self.E._start_times[0])
        self.assertGreater(self.B._start_times[0], self.D._start_times[0])
        self.assertGreater(self.C._start_times[0], self.E._start_times[0])
        self.assertGreater(self.A._start_times[0], self.B._start_times[0])
        self.assertGreater(self.A._start_times[0], self.C._start_times[0])
        self.assertGreater(self.G._start_times[0], self.A._start_times[0])

    def test_stop_order_file_ended(self):
        # Ensure started...
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start(TestManagerStructureDefinition._static_cb))
        self.assertEqual(TCPOCode.Status.FILE_END, self.manager.wait_until_ready(0.5)) # Wait at most half a second

        # Ensure no stop happens more than once, and also correct code
        for proc in self.proc_list:
            self.assertEqual(1, len(proc._stop_times))
            self.assertEqual(TCPOCode.Status.FILE_END, proc._stop_codes[0])
            self.assertEqual(10, len(proc._data))

        # Ensure stop happens in the opposite order
        self.assertGreater(self.A._stop_times[0], self.G._stop_times[0])
        self.assertGreater(self.B._stop_times[0], self.A._stop_times[0])
        self.assertGreater(self.C._stop_times[0], self.A._stop_times[0])
        self.assertGreater(self.D._stop_times[0], self.B._stop_times[0])
        self.assertGreater(self.E._stop_times[0], self.D._stop_times[0])
        self.assertGreater(self.E._stop_times[0], self.C._stop_times[0])
        self.assertGreater(self.F._stop_times[0], self.E._stop_times[0])

        # Ensure that callback is called, and that wait_until_stop returns since already terminated
        self.assertEqual(TestManagerStructureDefinition.callback_exit_code, TCPOCode.Status.FILE_END)
        self.assertEqual(self.manager.wait_until_ready(0.2), TCPOCode.Status.FILE_END)

    def test_stop_file_end_informed(self):
        """
        Test that if the file ends and I was not waiting for it, wait_until_ready still works as expected if called
        afterwards.
        """
        # Start and wait too little, to ensure that still active
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start(TestManagerStructureDefinition._static_cb))
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.wait_until_ready(0.005))  # Wait 5 ms (too little)

        # Sleep a bit
        tm.sleep(0.2)
        self.assertEqual(TestManagerStructureDefinition.callback_exit_code, TCPOCode.Status.FILE_END)
        self.assertEqual(TCPOCode.Status.FILE_END, self.manager.wait_until_ready(0.1))  # Wait at most 0.1 s

    def test_stop_order_user_stopped(self):
        self.G._length = 1000
        # Ensure started
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start(TestManagerStructureDefinition._static_cb))
        tm.sleep(0.1)
        self.assertEqual(TCPOCode.Status.USER_STOP, self.manager.stop())

        # Ensure no stop happens more than once, and also correct code
        len_gen = len(self.G._data)
        for proc in self.proc_list:
            self.assertEqual(1, len(proc._stop_times))
            self.assertEqual(TCPOCode.Status.USER_STOP, proc._stop_codes[0])
            self.assertLessEqual(len_gen, len(proc._data))

        # Ensure callback called...
        self.assertEqual(TestManagerStructureDefinition.callback_exit_code, TCPOCode.Status.USER_STOP)

        # Ensure stop happens in the opposite order
        self.assertGreater(self.A._stop_times[0], self.G._stop_times[0])
        self.assertGreater(self.B._stop_times[0], self.A._stop_times[0])
        self.assertGreater(self.C._stop_times[0], self.A._stop_times[0])
        self.assertGreater(self.D._stop_times[0], self.B._stop_times[0])
        self.assertGreater(self.E._stop_times[0], self.D._stop_times[0])
        self.assertGreater(self.E._stop_times[0], self.C._stop_times[0])
        self.assertGreater(self.F._stop_times[0], self.E._stop_times[0])

        # ReRequesting wait until ready or stop
        self.assertEqual(self.manager.wait_until_ready(0.2), TCPOCode.Status.USER_STOP)
        self.assertEqual(self.manager.stop(), TCPOCode.Status.USER_STOP)

    def test_start_order_failure(self):
        self.D._allow_start = False
        # Ensure not started (and hence callback does not get called)
        self.assertEqual(TCPOCode.Error.GENERIC, self.manager.start(TestManagerStructureDefinition._static_cb))
        self.assertIsNone(TestManagerStructureDefinition.callback_exit_code)

        # Ensure start called for every stop...
        for proc in self.proc_list:
            self.assertEqual(len(proc._start_times), len(proc._stop_times))
            if len(proc._stop_times) > 0:
                self.assertEqual(TCPOCode.Error.GENERIC, proc._stop_codes[0])

        # Ensure wait until ready warns
        self.assertEqual(self.manager.wait_until_ready(0.2), TCPOCode.Warning.STATE)

        # Now attempt restart to ensure that ok...
        self.D._allow_start = True
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start())
        self.assertEqual(TCPOCode.Status.FILE_END, self.manager.wait_until_ready(0.5))  # Wait at most half a second

    def test_data_trickles_correctly(self):
        # Setup scheme:
        self.A._operation = 'mult'; self.A._operand = 2
        self.B._operation = 'add'; self.B._operand = 3
        self.C._operation = 'add'; self.C._operand = -3
        self.D._operation = 'mult'; self.D._operand = 5
        self.E._operation = 'sum'; self.E._operand = self.C.Name
        # Ensure started...
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start())
        self.assertEqual(TCPOCode.Status.FILE_END, self.manager.wait_until_ready(0.5))  # Wait at most half a second
        # Check that data is correct:
        for i in range(10):
            self.assertEqual(self.G._data[i], self.A._data[i])
            self.assertEqual(12*(self.G._data[i] + 1), self.F._data[i])

    def test_data_trickle_failure(self):
        # Configure unit D for failure
        self.D._fail_sample = 4  # Fail on the 5th sample (4, 3, 2, 1, [0])
        # Wait until failure happens (hardcoded to be a Duplicate Error)
        self.assertEqual(TCPOCode.Status.ACTIVE, self.manager.start())
        self.assertEqual(TCPOCode.Error.DUPLICATE, self.manager.wait_until_ready(0.5))  # Wait at most half a second
        # Now ensure that stop called for all...
        for idx, proc in enumerate(self.proc_list):
            self.assertEqual(1, len(proc._stop_times))
            # Compare exit codes
            if idx == 0: self.assertEqual(proc._stop_codes[0], TCPOCode.Status.USER_STOP) # Reason for this is due to stop logic in generator using the one event
            else: self.assertEqual(proc._stop_codes[0], TCPOCode.Error.DUPLICATE)
            # Compare data
            if idx < 4: self.assertGreaterEqual(len(proc._data), 5)
            elif idx == 4: self.assertEqual(len(proc._data), 5)
            else: self.assertLessEqual(len(proc._data), 5)




