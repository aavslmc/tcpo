import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Modules.Calibrators import SelfCal


class TestSelfCal(TCPOTester):

    def test_selfcal_setup(self):

        sl = SelfCal.SelfCalRun()

        filter_model = dict()

        filter_model['no_of_antennas'] = 4
        filter_model['transit_run'] = True
        filter_model['gaincal'] = False
        filter_model['phasecal'] = True
        filter_model['stefcal'] = False
        filter_model['pointing_ra'] = 100
        filter_model['pointing_dec'] = 30
        filter_model['calibration_dec'] = 30
        filter_model['antennas'] = [[0, 1, 2], [1, 2, 3], [2, 3, 4], [3, 4, 5]]
        filter_model['longitude'] = 100
        filter_model['latitude'] = 20
        filter_model['frequency'] = 200
        filter_model['bandwith'] = 1
        filter_model['baseline_no'] = 6
        filter_model['main_dir'] = "home/"

        sl._setup(filter_model)

        self.assertTrue(np.array_equal(filter_model["no_of_antennas"], sl._no_of_antennas))
        self.assertTrue(np.array_equal(filter_model["transit_run"], sl._transit_observation))
        self.assertTrue(np.array_equal(filter_model["gaincal"], sl._gaincal))
        self.assertTrue(np.array_equal(filter_model["phasecal"], sl._phasecal))
        self.assertTrue(np.array_equal(filter_model["stefcal"], sl._stefcal))
        self.assertTrue(np.array_equal(filter_model["pointing_ra"], sl._pointing_ra))
        self.assertTrue(np.array_equal(filter_model["pointing_dec"], sl._pointing_dec))
        self.assertTrue(np.array_equal(filter_model["calibration_dec"], sl._calibration_dec))
        self.assertTrue(np.array_equal(filter_model["antennas"], sl._antennas))
        self.assertTrue(np.array_equal(filter_model["longitude"], sl._longitude))
        self.assertTrue(np.array_equal(filter_model["latitude"], sl._latitude))
        self.assertTrue(np.array_equal(filter_model["frequency"], sl._frequency))
        self.assertTrue(np.array_equal(filter_model["bandwith"], sl._bandwith))
        self.assertTrue(np.array_equal(filter_model["baseline_no"], sl._baseline_no))
        self.assertTrue(np.array_equal(filter_model["main_dir"], sl._main_dir))

    def test_selfcal_phasecal(self):

        sl = SelfCal.SelfCalRun()

        vis_in = np.array([1.+0.1j, 1.1+0.2j, 1.+0.2j, 1.2+0.1j, 1.+0.j, 1.1+0.j, 1.2+0.2j, 1.+0.j, 1.2+0.j, 1.+0.j])
        vis_in = np.expand_dims(vis_in, axis=1)

        no_of_antennas = 5

        sl.selfcal.phasecal_run(vis_in, no_of_antennas)

    def test_selfcal_gaincal(self):

        sl = SelfCal.SelfCalRun()

        vis_in = np.array([1.+0.1j, 1.1+0.2j, 1.+0.2j, 1.2+0.1j, 1.+0.j, 1.1+0.j, 1.2+0.2j, 1.+0.j, 1.2+0.j, 1.+0.j])
        vis_in = np.expand_dims(vis_in, axis=1)

        no_of_antennas = 5

        sl.selfcal.gaincal_run(vis_in, no_of_antennas)
