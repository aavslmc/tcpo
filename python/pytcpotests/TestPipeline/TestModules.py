import threading as td
import Queue as Qu
import numpy as np
import time as tm

from pytcpo.Core.PipeLine.PipelineModule import AbstractPipelineModule
from pytcpo.Core.PipeLine import Message, AbstractProcessor, AbstractGenerator
from pytcpo.Core.Common import TCPOCode, TCPOException

from pytcpotests import TCPOTester


class DummyArchiver(object):

    def __init__(self):
        self.data = []
        self.ts = []

    def WriteSample(self, key, timestamp, datum):
        self.ts.append(timestamp)
        self.data.append(datum)
        return TCPOCode.Status.OK


class DummyConsumer(object):

    def __init__(self):
        self.data = []

    def onSample(self, message):
        self.data.append(message)


class DummyAPM(AbstractPipelineModule):

    def __init__(self):
        super(DummyAPM, self).__init__()
        self._data = None

    def _setup(self, filter_model):
        self._data = []
        if not filter_model:
            raise TCPOException(TCPOCode.Error.VALUE)
        return True

    def start(self):
        return True

    def stop(self, reason):
        pass


class DummyProc(AbstractProcessor):

    def __init__(self):
        super(DummyProc, self).__init__()
        # Control PArameters
        self._allow_start = True
        # Diagnostic Parameters
        self._reason = None
        self._data = None
        self._startup = None
        self._aux = None

    def _setup(self, filter_model):
        self._reason = 0
        self._data = []
        self._startup = 1
        self._aux = []
        if not filter_model:
            raise TCPOException(TCPOCode.Error.SETUP)
        return True

    def _will_start(self):
        self._startup *= 2
        if not self._allow_start:
            raise TCPOException(TCPOCode.Error.GENERIC)
        return True

    def _new_sample(self, message, forward):
        self._data.append(message.Default)
        self._aux.append((message.Index, message.Time))
        if np.all(message.Default >= 0):
            forward[0] = message.Default * 2
            forward[1] = message.Default

    def _clean_up(self, reason):
        self._reason = self._reason*10 + reason


class DummyGen(AbstractGenerator):

    def __init__(self):
        super(DummyGen, self).__init__()
        # Control PArameters
        self._allow_start = True
        self._samples = 10
        self._rate = 0.1    # 100ms
        # Diagnostic Parameters
        self._reason = None
        self._startup = None
        self._started = None
        self._ended = None

    def _setup(self, filter_model):
        self._reason = 0
        self._startup = 1
        self._started = False
        self._ended = False
        if not filter_model:
            raise TCPOException(TCPOCode.Error.SETUP)
        return True

    def _generator_loop(self):
        self._startup *= 2
        if not self._allow_start:
            self._inform(TCPOCode.Error.STATE)
            return
        self._started = True
        while self._samples > 0 and not self._UserStop.wait(self._rate):
            message = Message((Message.SelType.NONE, 0), self._samples, self._samples*10.0, default=self._samples + 5.5)
            self._push(message, None)
            self._samples -= 1
        # If stopping because we timed out...
        if not self._UserStop.is_set():
            self._inform(TCPOCode.Status.FILE_END) # Indicate that terminating...
            self._reason = self._reason * 10 + TCPOCode.Status.FILE_END
        else:
            self._reason = self._reason * 10 + TCPOCode.Status.USER_STOP
        self._ended = True


class NameFilter(AbstractProcessor):

    def __init__(self):
        super(NameFilter, self).__init__()
        self.data = []
        self.name = None

    def _setup(self, filter_model):
        self.name = filter_model
        return True

    def _will_start(self):
        return True

    def _new_sample(self, message, forward):
        if self.name is not None:
            try:
                self.data.append(message[self.name])
            except KeyError:
                self.data.append(None)
        else:
            self.data.append(message.Default)
        forward[0] = message.Default
        return True

    def _clean_up(self, reason):
        pass


class TestAbstractPipelineModule(TCPOTester):

    def test_setup_handling(self):
        dummy = DummyAPM()
        queue = Qu.Queue()

        self.assertRaises(TCPOException, dummy.registerConsumer, DummyAPM())
        self.assertRaises(TCPOException, dummy.setup, 'dummy', queue, False)
        self.assertTrue(dummy.setup('dummy', queue, True))

    def test_consumer_registration(self):
        dummy = DummyAPM()
        dummy1 = DummyAPM()
        queue = Qu.Queue()

        self.assertTrue(dummy.setup('dummy', queue, True))
        self.assertTrue(dummy.registerConsumer(dummy1))
        dummy2 = DummyAPM()
        self.assertTrue(dummy.registerConsumer(dummy2))
        self.assertRaises(TCPOException, dummy.registerConsumer, dummy2)
        self.assertRaises(TCPOException, dummy.registerConsumer, dummy)

        # Now check that they are there...
        self.assertEqual(len(dummy._consumers), 2)
        self.assertTrue(dummy1 in dummy._consumers)
        self.assertTrue(dummy2 in dummy._consumers)

    def test_naming_convention_wrong(self):
        front = DummyProc()
        back  = NameFilter()
        back_back = NameFilter()
        queue = Qu.Queue()

        self.assertTrue(front.setup('front', queue, True))
        self.assertTrue(front.registerConsumer(back))
        self.assertTrue(back.setup('back', queue, 'fron'))
        self.assertTrue(back.registerConsumer(back_back))
        self.assertTrue(back_back.setup('back_back', queue, 'back'))

        # Start
        self.assertTrue(front.start())
        self.assertTrue(back.start())
        self.assertTrue(back_back.start())

        # Write a sample
        msg = Message((Message.SelType.NONE, 0), 5, 100, 'abc')
        msg.Default = 25
        front.onSample(msg)
        front.stop(TCPOCode.Status.FILE_END)

        # Check what has been read
        self.assertEqual(front._data, [25])
        self.assertEqual(back.data, [None])
        self.assertEqual(back_back.data, [50])
        front.default = []

    def test_naming_convention_right(self):
        front = DummyProc()
        back = NameFilter()
        back_back = NameFilter()
        back_back2 = NameFilter()
        queue = Qu.Queue()

        self.assertTrue(front.setup('front', queue, True))
        self.assertTrue(front.registerConsumer(back))
        self.assertTrue(back.setup('back', queue, 'front'))
        self.assertTrue(back.registerConsumer(back_back))
        self.assertTrue(back.registerConsumer(back_back2))
        self.assertTrue(back_back.setup('back_back', queue,  'front'))
        self.assertTrue(back_back2.setup('back_2', queue, 'back'))

        # Start
        self.assertTrue(front.start())
        self.assertTrue(back.start())
        self.assertTrue(back_back.start())
        self.assertTrue(back_back2.start())

        # Write a sample
        msg = Message((Message.SelType.NONE, 0), 5, 100, 'abc')
        msg.Default = 205
        front.onSample(msg)
        front.stop(TCPOCode.Status.FILE_END)

        # Check
        self.assertEqual(front._data, [205])
        self.assertEqual(back.data, [410])
        self.assertEqual(back_back.data, [None]) # Will pass through as None (as convention by NameFilter)
        self.assertEqual(back_back2.data, [410])

    def test_archiving_functionality(self):
        front = DummyProc()
        arch = DummyArchiver()
        msg = Message((Message.SelType.NONE, 0), 0, -1)
        queue = Qu.Queue()

        # Assert that there will be no exception if we attempt to archive when there is no archiver present
        self.assertTrue(front.setup('0', queue, True, None))
        self.assertTrue(front.start())
        msg.Default = 0; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        msg.Default = 1; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        front.stop(TCPOCode.Status.FILE_END)

        # Now re-setup with archiver
        self.assertTrue(front.setup('0', queue, True, (arch, 'a')))
        self.assertTrue(front.start())
        msg.Default = 0; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        msg.Default = 1; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        msg.Default = -3; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        msg.Default = 2; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        msg.Default = 5; self.assertNoException(AttributeError, front.onSample, "Should not Raise", msg)
        self.assertEqual(arch.data, [0, 1, 2, 5])


class TestAbstractProcessor(TCPOTester):

    def setUp(self):
        super(TestAbstractProcessor, self).setUp()
        self.a = DummyProc()
        self.b = DummyProc()
        self.c = DummyProc()
        self.d = DummyProc()
        self.queue = Qu.Queue()

        self.b_arch = DummyArchiver()

        self.assertTrue(self.a.setup('A', self.queue, True))
        self.assertTrue(self.b.setup('B', self.queue, True, (self.b_arch, 'B')))
        self.assertTrue(self.c.setup('C', self.queue, True))
        self.assertTrue(self.d.setup('D', self.queue, True))

        self.assertTrue(self.a.registerConsumer(self.b))
        self.assertTrue(self.a.registerConsumer(self.c))
        self.assertTrue(self.b.registerConsumer(self.d))

    def test_out_of_order_calls_no_setup(self):
        # Test Start
        dummy = DummyProc(); dummy._startup = 1
        self.assertRaises(TCPOException, dummy.start)
        self.assertEqual(dummy._startup, 1)

        # Test Message
        dummy = DummyProc(); dummy._data = []
        self.assertNoException(TCPOException, dummy.onSample, '', Message((Message.SelType.NONE, 0),0,0)) # No Exception Raised however
        self.assertEqual(len(dummy._data), 0)

        # Test Stop
        dummy = DummyProc(); dummy._reason = 0
        dummy.stop(TCPOCode.Status.USER_STOP)
        self.assertEqual(dummy._reason, 0) # Does not get called

    def test_out_of_order_calls_no_start(self):
        # Test Setup itself
        dummy = DummyProc()
        self.assertRaises(TCPOException, dummy.setup, 'dummy', self.queue, False)
        self.assertTrue(dummy.setup('dummy', self.queue, True))
        self.assertTrue(dummy.setup('dummy2', self.queue, True))

        # Test Message
        dummy = DummyProc(); dummy.setup('dummy', self.queue, True)
        dummy.onSample(Message((Message.SelType.NONE, 0),0,0.0,default=1))
        dummy.onSample(Message((Message.SelType.NONE, 0),1,2.0,default=-1))
        dummy.start()
        dummy.onSample(Message((Message.SelType.NONE, 0),2,4.0,default=5))
        self.assertEqual(len(dummy._data), 1)
        self.assertEqual(dummy._data[0], 5)

        # Test Stopping
        dummy = DummyProc(); dummy.setup('dummy', self.queue, True)
        dummy.stop(TCPOCode.Status.USER_STOP)
        self.assertEqual(dummy._reason, 0)  # Does not get called

    def test_start_stop_interplay(self):
        dummy = DummyProc(); dummy.setup('dummy', self.queue, True)
        # Call a bunch of stops
        dummy.stop(-1)
        dummy.stop(-2)
        self.assertEqual(dummy._reason, 0)
        # Do not allow starts - should call stop
        dummy._allow_start = False
        self.assertRaises(TCPOException, dummy.start)
        self.assertEqual(dummy._reason, TCPOCode.Error.GENERIC)
        # Now allow start
        dummy._allow_start = True
        self.assertTrue(dummy.start())
        # Now retry start - should not call stop
        self.assertRaises(TCPOException, dummy.start)
        self.assertEqual(dummy._reason, TCPOCode.Error.GENERIC)  # not changed
        # Now call stop
        dummy.stop(-3)
        dummy.stop(-4)
        self.assertEqual(dummy._reason, TCPOCode.Error.GENERIC * 10 - 3) # Code should be previous one minus 3 (amounting to first call only)

    def test_sample_passage_part_failure(self):
        self.assertTrue(self.a.start())
        self.assertTrue(self.c.start())
        self.assertTrue(self.d.start())

        # Send a couple of messages
        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', 1))
        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', 6))
        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', -4))

        # Check that trickled down only onto c
        self.assertEqual(len(self.a._data), 3)
        self.assertEqual(len(self.b._data), 0)
        self.assertEqual(len(self.c._data), 2)
        self.assertEqual(len(self.d._data), 0)

        # Check correct values
        self.assertEqual(self.c._data, [2, 12])

    def test_sample_passage_success(self):
        self.assertTrue(self.a.start())
        self.assertTrue(self.b.start())
        self.assertTrue(self.c.start())
        self.assertTrue(self.d.start())

        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 2.0, 'def', np.array((2,1,4))))
        self.a.onSample(Message((Message.SelType.NONE, 0), 2, 4.0, 'def', np.array((1,1,1))))
        self.a.onSample(Message((Message.SelType.NONE, 0), 3, 6.0, 'def', np.array((-1,5,6))))

        # Check that all lengths ok
        self.assertEqual(len(self.a._data), 3)
        self.assertEqual(len(self.b._data), 2)
        self.assertEqual(len(self.c._data), 2)
        self.assertEqual(len(self.d._data), 2)

        # Check that indices ok...
        self.assertEqual(self.a._aux, [(1, 2.0), (2, 4.0), (3, 6.0)])
        self.assertEqual(self.b._aux, [(1, 2.0), (2, 4.0)])
        self.assertEqual(self.c._aux, [(1, 2.0), (2, 4.0)])
        self.assertEqual(self.d._aux, [(1, 2.0), (2, 4.0)])

        # check that data is correct
        self.assertTrue(np.all(self.a._data[0] == np.array((2, 1, 4))))
        self.assertTrue(np.all(self.a._data[1] == np.array((1,1,1))))
        self.assertTrue(np.all(self.a._data[2] == np.array((-1,5,6))))

        self.assertTrue(np.all(self.b._data[0] == np.array((4, 2, 8))))
        self.assertTrue(np.all(self.b._data[1] == np.array((2, 2, 2))))

        self.assertTrue(np.all(self.c._data[0] == np.array((4, 2, 8))))
        self.assertTrue(np.all(self.c._data[1] == np.array((2, 2, 2))))

        self.assertTrue(np.all(self.d._data[0] == np.array((8, 4, 16))))
        self.assertTrue(np.all(self.d._data[1] == np.array((4, 4, 4))))

        # Check modification guards... i.e. that data is different
        # Modification in A
        self.a._data[0][0] = -1
        self.assertNotEqual(self.b._data[0][0], -1)
        self.assertNotEqual(self.c._data[0][0], -1)
        self.assertNotEqual(self.d._data[0][0], -1)
        # Modification in second row (B/C)
        self.b._data[1][0] = -1
        self.assertNotEqual(self.a._data[1][0], -1)
        self.assertEqual(self.c._data[1][0], -1)
        self.assertNotEqual(self.d._data[1][0], -1)
        # Modification in third row (D)
        self.d._data[0][2] = -1
        self.assertNotEqual(self.a._data[0][2], -1)
        self.assertNotEqual(self.b._data[0][2], -1)
        self.assertNotEqual(self.c._data[0][2], -1)

    def test_archiving_correct(self):
        self.assertTrue(self.a.start())
        self.assertTrue(self.b.start())
        self.assertTrue(self.c.start())
        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', -1))
        self.assertEqual(len(self.b_arch.data), 0)

        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', 3))
        self.a.onSample(Message((Message.SelType.NONE, 0), 1, 1000, 'def', -6))
        self.assertEqual(len(self.b_arch.data), 1)
        self.assertEqual(self.b_arch.data[0], 6)


class TestAbstractGenerator(TCPOTester):

    def test_ooo_execution_handling(self):
        # Test Start
        dummy = DummyGen(); dummy._startup = 1; dummy._reason = 0
        queue = Qu.Queue()

        self.assertRaises(TCPOException, dummy.start)
        self.assertEqual(dummy._startup, 1)
        self.assertEqual(dummy._reason, 0)  # Does not get called
        self.assertFalse(dummy._started)

        # Successful setup - start failure
        dummy = DummyGen(); dummy._allow_start = False
        self.assertTrue(dummy.setup('dummy', queue, True))
        self.assertTrue(dummy.start())
        self.assertEqual(dummy._startup, 2)
        self.assertFalse(dummy._started)
        self.assertEqual(queue.get().Code, TCPOCode.Error.STATE)

        # Test Stop
        dummy = DummyGen(); dummy._reason = 0
        dummy.stop(TCPOCode.Status.USER_STOP)
        self.assertEqual(dummy._reason, 0)  # Does not get called

    def test_start_until_end(self):
        # Setup
        gen = DummyGen(); con = DummyConsumer()
        queue = Qu.Queue()

        self.assertTrue(gen.setup('dummy', queue, True))
        self.assertTrue(gen.registerConsumer(con))
        gen._rate = 0.01
        gen._samples = 5

        # Start
        curr_threads = td.active_count()
        self.assertTrue(gen.start())

        tm.sleep(0.01) # Give some time to kick in
        self.assertEqual(td.active_count(), curr_threads + 1)
        self.assertTrue(gen._started)
        self.assertFalse(gen._ended)

        tm.sleep(0.1) # Give time to execute all
        self.assertEqual(td.active_count(), curr_threads)
        self.assertTrue(gen._ended)
        self.assertEqual(len(con.data), 5)
        for i in [5,4,3,2,1]:
            self.assertEqual(con.data[5-i].Index, i)
        self.assertEqual(queue.get().Code, TCPOCode.Status.FILE_END)

    def test_start_stop_dynamics(self):        # Setup
        gen = DummyGen(); con = DummyConsumer()
        queue = Qu.Queue()

        self.assertTrue(gen.setup('dummy', queue, True))
        self.assertTrue(gen.registerConsumer(con))
        gen._rate = 0.01
        gen._samples = 500

        # Start
        curr_threads = td.active_count()
        self.assertTrue(gen.start())

        tm.sleep(0.01)  # Give some time to kick in
        self.assertEqual(td.active_count(), curr_threads + 1)
        self.assertTrue(gen._started)
        self.assertFalse(gen._ended)

        tm.sleep(0.1)
        gen.stop(TCPOCode.Status.USER_STOP)
        self.assertEqual(td.active_count(), curr_threads)
        self.assertTrue(gen._ended)
        self.assertLess(len(con.data), 12)
        self.assertEqual(gen._reason, TCPOCode.Status.USER_STOP)
        self.assertTrue(queue.empty()) # Since this was user-initiated, it should not be communicated.
