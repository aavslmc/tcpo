import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Modules.Calibrators import Calibrator


class TestCalibrator(TCPOTester):

    def test_calibrator_setup(self):

        cb = Calibrator.CalibratorRun()

        filter_model = dict()

        no_of_antennas = 32
        cr = 0
        no_of_baselines = np.int(0.5 * ((no_of_antennas ** 2) - no_of_antennas))
        bas_ant_no = np.zeros((no_of_baselines, 2), dtype=np.int)
        for i in range(no_of_antennas):
            for j in range(no_of_antennas):
                if i < j:
                    bas_ant_no[cr, 0] = i
                    bas_ant_no[cr, 1] = j
                    cr += 1

        filter_model["baseline_no"] = bas_ant_no
        filter_model["selfcal"] = True
        filter_model["stefcal"] = False
        filter_model["transit_run"] = True

        cb._setup(filter_model)

        self.assertTrue(np.array_equal(filter_model["baseline_no"][:, 0], cb._antennas_list1))
        self.assertTrue(np.array_equal(filter_model["baseline_no"][:, 1], cb._antennas_list2))
        self.assertTrue(np.array_equal(filter_model["selfcal"], cb._selfcal))
        self.assertTrue(np.array_equal(filter_model["stefcal"], cb._stefcal))
        self.assertTrue(np.array_equal(filter_model["transit_run"], cb._transit_run))

    def test_calibrator_triangulizer_model(self):

        model_vis = np.array([1.+0.1j, 1.1+0.2j, 1.+0.2j, 1.2+0.1j, 1.+0.j, 1.1+0.j, 1.2+0.2j, 1.+0.j, 1.2+0.j, 1.+0.j])
        model_vis = np.expand_dims(model_vis, axis=1)
        antenna1 = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3]
        antenna2 = [1, 2, 3, 4, 2, 3, 4, 3, 4, 4]
        transit = True

        cb = Calibrator.CalibratorRun()

        cb.calibrator.detriangulizer_model(model_vis, antenna1, antenna2, transit)

        expected = np.zeros((5, 5, model_vis.shape[1]))
        expected_shape = expected.shape
        self.assertTrue(np.array_equal(cb.calibrator.model_vis_matrix.shape, expected_shape))

    def test_calibrator_triangulizer_real(self):

        real_vis = np.array([1.+0.1j, 1.1+0.2j, 1.+0.2j, 1.2+0.1j, 1.+0.j, 1.1+0.j, 1.2+0.2j, 1.+0.j, 1.2+0.j, 1.+0.j])
        real_vis = np.expand_dims(real_vis, axis=1)
        antenna1 = [0, 0, 0, 0, 1, 1, 1, 2, 2, 3]
        antenna2 = [1, 2, 3, 4, 2, 3, 4, 3, 4, 4]
        transit = True

        cb = Calibrator.CalibratorRun()

        cb.calibrator.detriangulizer_real(real_vis, antenna1, antenna2, transit)

        expected = np.zeros((5, 5, real_vis.shape[1]))
        expected_shape = expected.shape
        self.assertTrue(np.array_equal(cb.calibrator.real_vis_matrix.shape, expected_shape))

    def test_transit_peak_find(self):

        obs_in = np.array([1.+0.1j, 1.1+0.2j, 1.+0.2j, 1.0+0.1j, 1.+0.j, 1.1+0.j, 10.5+0.2j, 1.+0.j, 1.2+0.j, 1.+0.j])

        power = np.ones(obs_in.shape[0], dtype=np.complex)
        for i in range(obs_in.shape[0]):
            power[i] = np.sqrt(obs_in[i].real**2 + obs_in[i].imag**2)

        obs_in = np.expand_dims(obs_in, axis=1)

        cb = Calibrator.CalibratorRun()

        cb.calibrator.transit_peak_find(obs_in)

        self.assertTrue(np.array_equal(cb.calibrator.vis_peak, np.where(power == np.max(power))[0][0]))
