import numpy as np
import Queue as Qu

from pytcpotests import TCPOTester

from pytcpo.Core.PipeLine import AggregatorModule, AbstractProcessor, Message
from pytcpo.Core.Common import TCPOCode, TCPOException


class DummyAPM(AbstractProcessor):

    def __init__(self):
        super(DummyAPM, self).__init__()
        # self._data = None
        self._reason = None

    def _setup(self, filter_model):
        self._reason = 0
        # self._data = []
        if not filter_model: raise TCPOException(TCPOCode.Error.GENERIC)
        return filter_model

    def _will_start(self):
        return True

    def _new_sample(self, message, forward):
        if np.all(message.Default >= 0):
            forward[0] = message.Default * 2
            forward[1] = message.Default
        return True

    def _clean_up(self, reason):
        self._reason = self._reason*10 + reason
        return True


class EvenFilter(AbstractProcessor):

    def __init__(self):
        super(EvenFilter, self).__init__()

    def _setup(self, filter_model):
        pass

    def _will_start(self):
        pass

    def _new_sample(self, message, forward):
        if message.Index % 2 == 0:
            forward[0] = message.Default

    def _clean_up(self, reason):
        pass


class DataStorage(AbstractProcessor):
    def __init__(self):
        super(DataStorage, self).__init__()
        self.data = None

    def _setup(self, filter_model):
        pass

    def _will_start(self):
        self.data = []
        pass

    def _new_sample(self, message, forward):
        self.data.append(message)

    def _clean_up(self, reason):
        pass


class NameFilter(object):

    def __init__(self):
        self.data = []
        self.name = None
        self.consumers = []
        self.Name = None

    def registerConsumer(self, consumer):
        self.consumers.append(consumer)

    def onSample(self, message):
        if self.name is not None:
            try: self.data.append(message[self.name])
            except KeyError: self.data.append(None)
        else:
            self.data.append(message.Default)
        if len(self.consumers) > 0:
            new_message = message.part(self.Name)
            new_message.Default = message.Default
            for cons in self.consumers:
                cons.onSample(new_message)


class TestAggregatorModule(TCPOTester):

    def test_setup_failures(self):
        am = AggregatorModule()
        back = NameFilter()
        queue = Qu.Queue()

        # Setup Fails
        self.assertRaises(TypeError, am.setup, 'am', queue)
        self.assertRaises(KeyError, am.setup, 'am', queue, {})

        # Setup ok...
        self.assertTrue(am.setup('am', queue, {'names': ['front_1', 'front_2']}))
        self.assertTrue(am.registerConsumer(back))
        back.name = 'front'
        self.assertTrue(am.start())

        # Ensure raises when irrelevant value
        self.assertNoException(BaseException, am.onSample, None, Message((Message.SelType.NONE, 0), 1, 2, 'front', 'check'))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.VALUE)
        self.assertEqual(back.data, [])

    def test_duplicate_failure(self):
        am = AggregatorModule()
        back_1 = NameFilter()
        back_2 = NameFilter()
        queue = Qu.Queue()

        # Setup
        self.assertTrue(am.setup('am', queue, {'names': ['front_1', 'front_2']}))
        self.assertTrue(am.registerConsumer(back_1))
        self.assertTrue(am.registerConsumer(back_2))
        back_1.name = 'front_1'
        back_2.name = 'front_2'
        self.assertTrue(am.start())

        # Ensure that duplicate fails...
        self.assertNoException(BaseException, am.onSample, 'onSample should work', Message((Message.SelType.NONE, 0), 1, 2, 'front_1', 'check'))
        self.assertNoException(TCPOException, am.onSample, 'Should not Raise, only inform', Message((Message.SelType.NONE, 0), 1, 2, 'front_1', 'check2')) # No Exception raised but...
        self.assertEqual(queue.get().Code, TCPOCode.Warning.DUPLICATE)
        self.assertTrue(queue.empty())

        self.assertNoException(BaseException, am.onSample, 'onSample should work', Message((Message.SelType.NONE, 0), 1, 2, 'front_2', 'check3')) # Once this comes, will be forwarded, and hence no longer raise duplicates...
        self.assertNoException(BaseException, am.onSample, 'onSample should work', Message((Message.SelType.NONE, 0), 1, 2, 'front_2', 'check4'))
        self.assertNoException(TCPOException, am.onSample, 'Should not Raise, only inform', Message((Message.SelType.NONE, 0), 1, 2, 'front_2', 'check5'))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.DUPLICATE)
        self.assertTrue(queue.empty())

        self.assertNoException(BaseException, am.onSample, 'onSample should work', Message((Message.SelType.NONE, 0), 1, 2, 'front_1', 'check6'))
        self.assertTrue(queue.empty())

        # Ensure correctly sent messages:
        self.assertEqual(back_1.data, ['check', 'check6'])
        self.assertEqual(back_2.data, ['check3', 'check4'])

    def test_inexistent(self):
        am = AggregatorModule()
        back_a = NameFilter()
        back_b = NameFilter()
        back_c = NameFilter()
        queue = Qu.Queue()

        # Setup
        self.assertTrue(am.setup('am', queue, {'names': ['a', 'b']}))
        self.assertTrue(am.registerConsumer(back_a))
        self.assertTrue(am.registerConsumer(back_b))
        self.assertTrue(am.registerConsumer(back_c))
        back_a.name = 'a'
        back_b.name = 'b'
        back_c.name = 'c'
        self.assertTrue(am.start())

        # Send Messages
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'a', 1))
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'b', 2))
        self.assertTrue(queue.empty())
        self.assertNoException(BaseException, am.onSample, 'Should not raise, only inform', Message((Message.SelType.NONE, 0), 1, 2, 'c', 3))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.VALUE)
        self.assertTrue(queue.empty())

        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 2, 3, 'a', 4))
        self.assertNoException(TCPOException, am.onSample, 'Should not raise, only inform', Message((Message.SelType.NONE, 0), 2, 3, 'c', 5))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.VALUE)
        self.assertTrue(queue.empty())
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 2, 3, 'b', 6))

        # Ensure correctly sent messages:
        self.assertEqual(back_a.data, [1, 4])
        self.assertEqual(back_b.data, [2, 6])
        self.assertEqual(back_c.data, [None, None])

    def test_default_naming(self):
        am = AggregatorModule()
        back_default = NameFilter()
        back_a = NameFilter()
        back_b = NameFilter()
        queue = Qu.Queue()

        self.assertTrue(am.setup('am', queue, {'names': ['a', 'b'], 'default':'a'}))
        self.assertTrue(am.registerConsumer(back_default))
        self.assertTrue(am.registerConsumer(back_a))
        self.assertTrue(am.registerConsumer(back_b))
        back_default.name = None
        back_a.name = 'a'
        back_b.name = 'b'
        self.assertTrue(am.start())

        # First Value Error
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'a', 1))
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'b', 2))
        self.assertNoException(TCPOException, am.onSample, 'Should not Raise, only inform', Message((Message.SelType.NONE, 0), 1, 2, 'default', 3))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.VALUE)
        self.assertTrue(queue.empty())
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'b', 4))
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'a', 5))
        self.assertEqual(back_a.data, [1, 5])
        self.assertEqual(back_b.data, [2, 4])
        self.assertEqual(back_default.data, [1, 5])

    def test_non_default_naming(self):
        am = AggregatorModule()
        back_default = NameFilter()
        back_a = NameFilter()
        back_b = NameFilter()
        queue = Qu.Queue()

        self.assertTrue(am.setup('am', queue, {'names': ['a'], 'default': 'b'}))
        self.assertTrue(am.registerConsumer(back_default))
        self.assertTrue(am.registerConsumer(back_a))
        self.assertTrue(am.registerConsumer(back_b))
        back_default.name = None
        back_a.name = 'a'
        back_b.name = 'b'
        self.assertTrue(am.start())
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'a', 6))
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'b', 7))
        self.assertNoException(TCPOException, am.onSample, 'Should not Raise, only Inform', Message((Message.SelType.NONE, 0), 1, 2, 'default', 8))
        self.assertEqual(queue.get().Code, TCPOCode.Warning.VALUE)
        self.assertTrue(queue.empty())
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'b', 9))
        self.assertNoException(BaseException, am.onSample, 'Should Work', Message((Message.SelType.NONE, 0), 1, 2, 'a', 10))
        self.assertEqual(back_a.data, [6, 10])
        self.assertEqual(back_b.data, [7, 9])
        self.assertEqual(back_default.data, [7, 9])

    def test_simple_passage(self):
        front = DummyAPM()
        am = AggregatorModule()
        back_ok = NameFilter()
        back_err = NameFilter()
        queue = Qu.Queue()

        # Setup
        self.assertTrue(front.setup('front', queue, True))
        self.assertTrue(front.registerConsumer(am))
        self.assertTrue(am.setup('am', queue, {'names':['front']}))
        self.assertTrue(am.registerConsumer(back_ok))
        self.assertTrue(am.registerConsumer(back_err))
        back_ok.name = 'front'
        back_err.name = 'am'

        # Start
        self.assertTrue(front.start())
        self.assertTrue(am.start())

        # Put some data in...
        self.assertNoException(Exception, front.onSample, 'Should Succeed', Message((Message.SelType.NONE, 0), 1, 2, 'default', 5.0))
        self.assertNoException(Exception, front.onSample, 'Should Succeed', Message((Message.SelType.NONE, 0), 1, 3, 'am', 10.0)) # Should not raise because sent on already
        self.assertNoException(Exception, front.onSample, 'Should Succeed', Message((Message.SelType.NONE, 0), 2, 4, 'am', 15.0))
        front.stop(TCPOCode.Status.USER_STOP)

        # Now check data throughput...
        self.assertEqual(back_ok.data, [10.0, 20.0, 30.0])
        self.assertEqual(back_err.data, [None, None, None])

    # A Much more complex structure...
    #   front (DummySTP) ---> middle_1 (DummySTP) ---> middle_2 (DummySTP) ---> aggregator --> back_1
    #       |---------------> middle_3 (DummySTP) ----------------------------> |     |------> back_2
    #       |---------------> middle_4 (NameFilter) --------------------------> |     |------> back_3
    def test_complex_structure(self):
        front = DummyAPM()
        middle_1 = DummyAPM()
        middle_2 = DummyAPM()
        middle_3 = DummyAPM()
        middle_4 = NameFilter()
        aggregator = AggregatorModule()
        back_1 = NameFilter()
        back_2 = NameFilter()
        back_3 = NameFilter()
        queue = Qu.Queue()

        # Setup
        self.assertTrue(front.setup('front', queue, True))
        self.assertTrue(middle_1.setup('middle_1', queue, True))
        self.assertTrue(middle_2.setup('middle_2', queue, True))
        self.assertTrue(middle_3.setup('middle_3', queue, True))
        middle_4.name = 'front'
        middle_4.Name = 'middle_4'
        self.assertTrue(aggregator.setup('am', queue, {'names':['middle_3', 'middle_4'], 'default':'middle_2'}))
        back_1.name=None
        back_2.name = 'middle_3'
        back_3.name = 'middle_4'

        # Connect the pipeline
        self.assertTrue(front.registerConsumer(middle_1))
        self.assertTrue(front.registerConsumer(middle_3))
        self.assertTrue(front.registerConsumer(middle_4))

        self.assertTrue(middle_1.registerConsumer(middle_2))

        self.assertTrue(middle_2.registerConsumer(aggregator))
        self.assertTrue(middle_3.registerConsumer(aggregator))
        middle_4.registerConsumer(aggregator)

        self.assertTrue(aggregator.registerConsumer(back_1))
        self.assertTrue(aggregator.registerConsumer(back_2))
        self.assertTrue(aggregator.registerConsumer(back_3))

        # Start and send data
        self.assertTrue(front.start())
        self.assertTrue(middle_1.start())
        self.assertTrue(middle_2.start())
        self.assertTrue(middle_3.start())
        self.assertTrue(aggregator.start())

        for i in range(1, 6):
            front.onSample(Message((Message.SelType.NONE, 0), i, i+2, 'blabla', i))
        self.assertEqual(0, len(aggregator._AggregatorModule__index_ptr)) # Should be empty
        front.stop(TCPOCode.Status.USER_STOP)

        # Check that correctly transmitted all data...
        self.assertEqual(back_1.data, [8, 16, 24, 32, 40])
        self.assertEqual(back_2.data, [4, 8, 12, 16, 20])
        self.assertEqual(back_3.data, [2, 4, 6, 8, 10])

    def test_out_of_order_arrival(self):
        # Structure:
        #  front (DummyAPM) ---> allpass (DummyAPM) -----> combine (Aggregator) --> collect (DataStorage)
        #       |--------------> evenpass (EvenFilter) -->|
        modules = [DummyAPM(), DummyAPM(), EvenFilter(), AggregatorModule(), DataStorage()]
        queue = Qu.Queue()

        # Setup
        self.assertNoException(BaseException, modules[0].setup, 'Setup Failure', 'front', queue, True)
        self.assertNoException(BaseException, modules[1].setup, 'Setup Failure', 'allpass', queue, True)
        self.assertNoException(BaseException, modules[2].setup, 'Setup Failure', 'evenpass', queue, None)
        self.assertNoException(BaseException, modules[3].setup, 'Setup Failure', 'front', queue,
                               {'names': ['allpass', 'evenpass']})
        self.assertNoException(BaseException, modules[4].setup, 'Setup Failure', 'collect', queue, None)

        # Connect
        self.assertNoException(BaseException, modules[0].registerConsumer, 'Register Error', modules[1])
        self.assertNoException(BaseException, modules[0].registerConsumer, 'Register Error', modules[2])
        self.assertNoException(BaseException, modules[1].registerConsumer, 'Register Error', modules[3])
        self.assertNoException(BaseException, modules[2].registerConsumer, 'Register Error', modules[3])
        self.assertNoException(BaseException, modules[3].registerConsumer, 'Register Error', modules[4])

        # Start all
        for _module in modules: self.assertNoException(BaseException, _module.start, 'Start')

        # Now do a couple of samples
        for i in range(10):
            self.assertNoException(BaseException, modules[0].onSample, 'OnSample',
                                   Message((None, None), i, 0.0, default=i*2))

        # Now check that received correctly
        self.assertEqual(len(modules[4].data), 5)
        for _i, _data in enumerate(modules[4].data):
            self.assertEqual(_data.Index, _i*2)
            self.assertEqual(_data['allpass'], _i*16)   # This has passed through 2 DummyAPMs, which multiply by 4
            self.assertEqual(_data['evenpass'], _i*8)   # This has passed through 1 DummyAPM which multiplies by 2
