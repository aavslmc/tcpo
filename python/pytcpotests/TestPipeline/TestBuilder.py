from StringIO import StringIO

from pytcpotests import TCPOTester

from pytcpo.Core.Common import TCPOCode, TCPOException
from pytcpo.Core.PipeLine import AbstractGenerator, AbstractPipelineBuilder, PipelineManager


class BuilderGenerator(AbstractGenerator):

    Data_Storage = None

    def __init__(self):
        super(BuilderGenerator, self).__init__()

        # Setup Parameters
        self.__count = None

        # State Parameters
        BuilderGenerator.Data_Storage = None

    def _setup(self, filter_model):
        self.__count = filter_model.get('count', 1)
        return True

    def _generator_loop(self):

        # First state start
        BuilderGenerator.Data_Storage = []
        count = 0

        while self.__count > count and not self._UserStop.wait(0.001):
            BuilderGenerator.Data_Storage.append(count)
            count += 1

        # If stopping because we timed out...
        if not self._UserStop.is_set():
            self._inform(TCPOCode.Status.FILE_END) # Indicate that terminating...

PipelineManager.registerModule(BuilderGenerator)


class DummyPipelineBuilder(AbstractPipelineBuilder):

    _spec_type = 0      # 0 - Empty Specification
                        # 1 - Wrong Specification - Multiple occurances of same parameter
                        # 2 - Wrong Specification - Option where one parameter has invalid default type
                        # 3 - Correct Specification (Simple)
                        # 4 - Correct Specification (More Complete)

    _struct_type = 0    # 0 - Simple Default Variable

    def _specification(self):

        if DummyPipelineBuilder._spec_type == 1:
            self._add_option('count', 'Number of samples')
            self._add_option('count', 'Number of samples')

        elif DummyPipelineBuilder._spec_type == 2:
            self._add_option('count', 'Number of Samples', required=False, allow_type=int, default=0.5)

        elif DummyPipelineBuilder._spec_type == 3:
            self._add_option('count', 'Number of Samples')

        elif DummyPipelineBuilder._spec_type == 4:
            self._add_option('gender', 'Gender')  # Required
            self._add_option('name', 'Name', allow_type=str)
            self._add_option('age', 'Age', allow_type=(int, float))
            self._add_option('sibling', 'Any Siblings?', required=False, default=True)
            self._add_option('children', 'Any Offspring', required=False, allow_type=int, default=0)

    def _structure(self, manager):
        manager.add_root('DummyGenerator', {})

    @property
    def archive(self):
        return {}


class TestPipelineBuilderSpecification(TCPOTester):

    def test_call_order(self):
        """
        Ensure that methods are not called before setup
        """
        DummyPipelineBuilder._spec_type = 0
        self.assertRaises(TCPOException, DummyPipelineBuilder().build)
        self.assertNoException(BaseException, DummyPipelineBuilder().help, 'Help Raised Exception', StringIO())
        self.assertNoException(BaseException, lambda : DummyPipelineBuilder().options, msg='Options Raised Exception')

    def test_specification(self):

        # Specifying multiple occurences
        DummyPipelineBuilder._spec_type = 1
        self.assertRaises(TCPOException, DummyPipelineBuilder)

        # Specifying wrong default type
        DummyPipelineBuilder._spec_type = 2
        self.assertRaises(TCPOException, DummyPipelineBuilder)

        # Now Succeed
        DummyPipelineBuilder._spec_type = 3
        self.assertNoException(TCPOException, DummyPipelineBuilder, msg="DPB raised Exception")


class TestPipelineBuilderSetup(TCPOTester):

    def setUp(self):
        super(TestPipelineBuilderSetup, self).setUp()

        DummyPipelineBuilder._spec_type = 4
        self.dpb = DummyPipelineBuilder()
        self.default = {'gender': 'M',
                        'name': 'Peter',
                        'age': 48,
                        'sibling': True,
                        'children': 3}

    def test_correct_setup(self):
        # Base Correct
        self.assertNoException(BaseException, self.dpb.setup, 'Setup Raised Exception', self.default)

        # Other possible types
        self.default['age'] = 6.6
        self.assertNoException(BaseException, self.dpb.setup, 'Setup Raised Exception', self.default)

        # Missing Optional Types
        del self.default['sibling']; del self.default['children']
        self.assertNoException(BaseException, self.dpb.setup, 'Setup Raised Exception', self.default)

    def test_missing_required(self):
        no_gender = self.default.copy(); del no_gender['gender']
        self.assertRaises(TCPOException, self.dpb.setup, no_gender)

        no_name = self.default.copy(); del no_name['name']
        self.assertRaises(TCPOException, self.dpb.setup, no_name)

        no_age = self.default.copy(); del no_age['age']
        self.assertRaises(TCPOException, self.dpb.setup, no_age)

    def test_wrong_type(self):
        bad_name = self.default.copy(); bad_name['name'] = 5
        self.assertRaises(TCPOException, self.dpb.setup, bad_name)

        bad_age = self.default.copy(); bad_age['age'] = 'Hello'
        self.assertRaises(TCPOException, self.dpb.setup, bad_age)

        bad_children = self.default.copy(); bad_children['children'] = 6.6
        self.assertRaises(TCPOException, self.dpb.setup, bad_children)
