import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Modules.Calibrators import Consumer


class TestConsumer(TCPOTester):

    def test_consumer_setup(self):

        cn = Consumer.ConsumerRun()

        filter_model = dict()

        filter_model['actual_vis_directory'] = "/home/"
        filter_model['model_vis_directory'] = "/home/"
        filter_model['model_vis_list_path'] = "/home/"
        filter_model['total_time_samples'] = 10
        filter_model['transit_run'] = True
        filter_model['transit_file'] = "/home/"

        cn._setup(filter_model)

        self.assertTrue(np.array_equal(filter_model["actual_vis_directory"], cn._uncal_vis_dir))
        self.assertTrue(np.array_equal(filter_model["model_vis_directory"], cn._model_vis_dir))
        self.assertTrue(np.array_equal(filter_model["model_vis_list_path"], cn._model_vis_list_path))
        self.assertTrue(np.array_equal(filter_model["total_time_samples"], cn._total_obs_samples))
        self.assertTrue(np.array_equal(filter_model["transit_run"], cn._transit_run))
        self.assertTrue(np.array_equal(filter_model["transit_file"], cn._transit_obs_file))
