from pytcpotests import TCPOTester
from pytcpo.Core.PipeLine import Message


class TestMessagingInitialisation(TCPOTester):

    @staticmethod
    def setDefault(message, value):
        message.Default = value

    def test_correct_types(self):
        # Check correct types
        msg = Message((Message.SelType.NONE, 0), 1, 100.4)
        self.assertEqual(msg.Selector, 0)
        self.assertEqual(msg.Index, 1)
        self.assertEqual(msg.Time, 100.4)
        self.assertRaises(KeyError, lambda: msg.Default) # Has not been set...
        self.assertRaises(KeyError, lambda : msg['H'])

    def test_added_data(self):
        # Add Data and check that works
        msg = Message((Message.SelType.NONE, 0), 1, 100.4, 'abc')
        msg.Default = 5
        msg.add('A', 12)
        msg.add('B', 16.0)
        self.assertEqual(msg['A'], 12)
        self.assertEqual(msg.Data['B'], 16.0)
        self.assertRaises(KeyError, lambda : msg['default'])
        self.assertEqual(msg['abc'], 5)
        self.assertEqual(msg.Default, 5)

    def test_with_no_data(self):
        msg = Message((Message.SelType.NONE, 0), 1, 100.4, None, 5)
        self.assertRaises(AttributeError, lambda: msg.Default) # Will be an attribute error, because Message has no default!
        self.assertRaises(AttributeError, self.setDefault, msg, 7)


class TestMessagingPartCopy(TCPOTester):

    def setUp(self):
        super(TestMessagingPartCopy, self).setUp()
        self.msg = Message((Message.SelType.NONE, 0), 1, 100.4, 'test')
        self.msg.Data['abc'] = 10.25
        self.msg.Default = 'test'
        self.msg_p = Message.part(self.msg, 'part')

    def test_part_copy(self):
        self.assertEqual(self.msg_p.Selector, 0)
        self.assertEqual(self.msg_p.Index, 1)
        self.assertEqual(self.msg_p.Time, 100.4)
        self.assertEqual(self.msg_p._Message__name, 'part')
        self.assertRaises(KeyError, lambda : self.msg_p.Default) # Data not copied

    def test_changes_not_carried(self):
        # Make changes to original and ensure that they do not follow
        self.msg.Selector = -4
        self.msg.Default = 'testing'
        self.assertEqual(self.msg_p.Selector, 0)
        self.assertRaises(KeyError, lambda : self.msg_p.Default) # This is because Default was still set up, however, the entry still does not exist.


class TestMessagingShallowCopy(TCPOTester):

    def setUp(self):
        super(TestMessagingShallowCopy, self).setUp()
        self.msg = Message((Message.SelType.NONE, 0), 1, 100.4, 'msg', ['test'])
        self.msg.add('aux', [5.0])
        self.msg.add('dict', {1: 2.0, 2: 4.0})
        self.msg_sc = self.msg.shallow()  # Use same Name
        self.msg_sr = self.msg.shallow('msg_sr')

    def test_equateability(self):
        self.assertEqual(self.msg.Index, self.msg_sc.Index)
        self.assertEqual(self.msg.Index, self.msg_sr.Index)
        self.assertEqual(self.msg_sc.Default, ['test'])
        self.assertEqual(self.msg_sr.Default, ['test'])
        self.assertEqual(self.msg_sc['aux'], [5.0])
        self.assertEqual(self.msg_sr['aux'], [5.0])
        self.assertEqual(self.msg_sc['dict'][1], 2)
        self.assertEqual(self.msg_sc['dict'][2], 4)
        self.assertEqual(self.msg_sr['dict'][1], 2)
        self.assertEqual(self.msg_sr['dict'][2], 4)

    def test_names_are_correct(self):
        # For Copied and not
        self.assertEqual(self.msg_sc._Message__name, 'msg')
        self.assertEqual(self.msg_sr._Message__name, 'msg_sr')
        self.assertRaises(KeyError, lambda : self.msg_sc['msg_sr'])
        self.assertRaises(KeyError, lambda : self.msg_sr['msg'])

    def test_correct_after_change(self):
        self.msg.Index = -2
        self.msg.Default.append('twice')
        self.assertEqual(self.msg_sc.Index, 1)
        self.assertEqual(self.msg_sr.Index, 1)
        self.assertEqual(self.msg_sc.Default, ['test', 'twice'])
        self.assertEqual(self.msg_sr.Default, ['test', 'twice'])

    def test_renaming_dict_key(self):
        self.msg.Data['tcid'] = self.msg.Data.pop('dict')
        self.assertRaises(KeyError, lambda : self.msg['dict'])
        self.assertEqual(self.msg_sc['dict'][1], 2)
        self.assertEqual(self.msg_sc['dict'][2], 4)
        self.assertEqual(self.msg_sr['dict'][1], 2)
        self.assertEqual(self.msg_sr['dict'][2], 4)

    def test_delete_dict_data(self):
        self.msg.Default.append('twice')
        del(self.msg.Data['msg'])
        self.assertRaises(KeyError, lambda: self.msg.Default)
        self.assertEqual(self.msg_sc.Default, ['test', 'twice'])
        self.assertEqual(self.msg_sr.Default, ['test', 'twice'])


class TestMessagingDeepCopy(TCPOTester):

    def setUp(self):
        super(TestMessagingDeepCopy, self).setUp()
        self.msg = Message((Message.SelType.NONE, 0), 1, 100.4, 'msg', ['test'])
        self.msg.add('aux', [5.0])
        self.msg.add('dict', {1: 2.0, 2: 4.0})
        self.msg_sc = self.msg.deep()  # Use same Name
        self.msg_sr = self.msg.deep('msg_sr')

    def test_equating_correct(self):
        self.assertEqual(self.msg.Index, self.msg_sc.Index)
        self.assertEqual(self.msg.Index, self.msg_sr.Index)
        self.assertEqual(self.msg_sc.Default, ['test'])
        self.assertEqual(self.msg_sr.Default, ['test'])
        self.assertEqual(self.msg_sc['aux'], [5.0])
        self.assertEqual(self.msg_sr['aux'], [5.0])
        self.assertEqual(self.msg_sc['dict'][1], 2)
        self.assertEqual(self.msg_sc['dict'][2], 4)
        self.assertEqual(self.msg_sr['dict'][1], 2)
        self.assertEqual(self.msg_sr['dict'][2], 4)

    def test_names_are_correct(self):
        self.assertEqual(self.msg_sc._Message__name, 'msg')
        self.assertEqual(self.msg_sr._Message__name, 'msg_sr')
        self.assertRaises(KeyError, lambda : self.msg_sc['msg_sr'])
        self.assertRaises(KeyError, lambda : self.msg_sr['msg'])

    def test_correct_after_change(self):
        self.msg.Index = -2
        self.msg.Default.append('twice')
        self.msg.Data['dict'][2] = -4
        self.assertEqual(self.msg_sc.Index, 1)
        self.assertEqual(self.msg_sr.Index, 1)
        self.assertEqual(self.msg_sc.Default, ['test'])
        self.assertEqual(self.msg_sr.Default, ['test'])
        self.assertEqual(self.msg_sc['dict'][1], 2)
        self.assertEqual(self.msg_sc['dict'][2], 4)
        self.assertEqual(self.msg_sr['dict'][1], 2)
        self.assertEqual(self.msg_sr['dict'][2], 4)

    def test_correct_after_dict_key_rename(self):
        self.msg.Default.append('twice')
        self.msg.Data['dict'][2] = -4
        self.msg.Data['tcid'] = self.msg.Data.pop('dict')
        self.assertRaises(KeyError, lambda : self.msg['dict'])
        self.assertEqual(self.msg_sc['dict'][1], 2)
        self.assertEqual(self.msg_sc['dict'][2], 4)
        self.assertEqual(self.msg_sr['dict'][1], 2)
        self.assertEqual(self.msg_sr['dict'][2], 4)

    def test_deletion(self):
        self.msg.Default.append('twice')
        self.msg.Data['dict'][2] = -4
        del (self.msg.Data['msg'])
        self.assertRaises(KeyError, lambda : self.msg.Default) # Now no longer exists - but name is still specified!
        self.assertEqual(self.msg_sc.Default, ['test'])
        self.assertEqual(self.msg_sr.Default, ['test'])