import numpy as np

from pytcpotests import TCPOTester
from pytcpo.Modules.Calibrators import StEFCal


class TestStEFCal(TCPOTester):

    def test_stefcal_setup(self):

        sf = StEFCal.StEFCalRun()

        filter_model = dict()

        filter_model['no_of_antennas'] = 5
        filter_model['selfcal'] = False
        filter_model['transit_run'] = True

        sf._setup(filter_model)

        self.assertTrue(np.array_equal(filter_model["no_of_antennas"], sf._no_of_antennas))
        self.assertTrue(np.array_equal(filter_model["selfcal"], sf._selfcal_check))
        self.assertTrue(np.array_equal(filter_model["transit_run"], sf._transit_run))

    def test_stefcal_gainsolv(self):

        sf = StEFCal.StEFCalRun()

        model_covariance_matrix = np.ones((5, 5), dtype=np.complex)
        covariance_matrix = np.ones((5, 5), dtype=np.complex)
        gains_estimate = np.ones(5, dtype=np.complex)

        sf._stefcal.solve(model_covariance_matrix, covariance_matrix, gains_estimate)
