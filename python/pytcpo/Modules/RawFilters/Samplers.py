import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode


# An aliasing down-sampler... basically, just returns every nth sample
# Since the down-sampling is based only on the index, can be used before a tile-selector!
class AliasingDownSampler(AbstractProcessor):
    """
    This Module is a simple downsampler which just picks every nth sample it receives. It makes use of the index entry
    in the message directly (no internal state) and hence can also be used before a tile-selector.

    Requires:
        * None (Any Message is supported)
    Outputs:
        * Input Message if index occurs on the down-sample, otherwise nothing. Note that while the data is not
            explicitly copied, it is wrapped in a new message (appropriately named).
    Archives:
        * None

    Limitations:
        a) Only the Default Data item is considered - any other information is lost.
    """
    def __init__(self):
        """
        Default Initialiser
        """
        super(AliasingDownSampler, self).__init__()

        # Setup Parameters
        self.__rate = None  # Rate at which to forward samples.
        self.__start = None # Start Index

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python dict with 2 (1 Required, 1 Optional) Parameters
            * rate - [Required]: The Rate at which to forward samples. For rate = 1, every sample is passed on.
            * start - [Optional]: Typically for offline readout scenarios, indicates the start sample so that the
                downsampling is offset appropriately. Defaults to 0
        :return: Status.SETUP
        """
        # Setup Parameters
        self.__rate = filter_model['rate']
        self.__start = filter_model.get('start', 0)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        (Usual Parameters)
        """
        # Return something only iff at required offset...
        if (message.Index-self.__start+1) % self.__rate == 0:
            forward[0] = message.Default

    def _clean_up(self, reason):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass


class SmoothingDownSampler(AbstractProcessor):
    """
    This Downsampling module handles numerical data and performs an averaging operation on the samples to be downsampled
    in a bid to reduce aliasing effects.

    Requires:
        * ND Numpy Array (any shape/type, but care must be taken with overflow/underflow if an integral type) [Default]
    Outputs:
        * Numpy Array of elementwise mean over the specified number of samples if at the correct offset, else None.
    Archives:
        * Numpy Array of mean (as above)

    Limitations:
        a) Due to the use of the numpy add/divide functions, the type of the array must be a numeric one.
        b) Only the Default Data item is considered - any other information is lost.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(SmoothingDownSampler, self).__init__()
        # Setup Parameters
        self.__rate = None
        self.__start = None

        # State Parameters
        self.__sample = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python dict with 2 (1 Required, 1 Optional) Parameters
            * rate - [Required]: The Rate at which to forward samples. For rate = 1, every sample is passed on.
            * start - [Optional]: Typically for offline readout scenarios, indicates the start sample so that the
                downsampling is offset appropriately. Defaults to 0
        :return: Status.SETUP
        """
        # Setup
        self.__rate = filter_model['rate']
        self.__start = filter_model.get('start', 0)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        :return: None
        """
        self.__sample = None
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """
        # First Add this message's default
        self.__sample = np.copy(message.Default) if self.__sample is None else np.add(self.__sample, message.Default)

        # Now if at rate
        if (message.Index - self.__start + 1) % self.__rate == 0:
            forward[0] = np.divide(self.__sample, self.__rate)
            self.__sample = None

        # Archive - will remain None if forward[0] is None
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleaning Method

        Empty - State Handled by '_will_start'
        """
        pass