# This Package contains the set of 'simple' pre-filters which equalise etc.. the data
from Casts import CScaler
from Exponentials import PowerLog, Exponentiator
from Samplers import AliasingDownSampler, SmoothingDownSampler
from Selectors import BaseSelector, AntennaSelector, ChannelSelector, PolarisationSelector
from Statistics import MovingAverageModule
from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(CScaler)
PipelineManager.registerModule(AliasingDownSampler)
PipelineManager.registerModule(SmoothingDownSampler)
PipelineManager.registerModule(BaseSelector)
PipelineManager.registerModule(AntennaSelector)
PipelineManager.registerModule(PolarisationSelector)
PipelineManager.registerModule(ChannelSelector)
PipelineManager.registerModule(PowerLog)
PipelineManager.registerModule(Exponentiator)
PipelineManager.registerModule(MovingAverageModule)

__author__ = 'Michael Camilleri'
