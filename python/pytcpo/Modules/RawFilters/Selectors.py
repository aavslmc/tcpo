from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import tupler, TCPOCode, TCPOException

import numpy as np

class BaseSelector(AbstractProcessor):
    """
    This Module filters messages by their base-selector (usually the tile or beam)

    Requires:
        * None (Any data type is supported)
    Outputs:
        * Input Message.Default if its identifier is in the (configurable) list of allowed values, else None
    Archives:
        * None

    Limitations:
        a) Only the Default Data item is considered - any other information is lost.
    """

    def __init__(self):
        super(BaseSelector, self).__init__()
        # Setup Parameters
        self.__type = None
        self.__select = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python dict with 1 (Required) option:
            * type - [Required]: The type of selection to perform on.
            * select - [Required]: Integer or Tuple of index/indices to filter through.
        :return: Status.SETUP
        """
        # Setup
        self.__type = filter_model['type']
        self.__select = tupler(filter_model['select'])

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """
        if not (message.SelectorMask & self.__type):
            raise TCPOException(TCPOCode.Error.MESSAGE, 'Message does not have %d selection' % self.__type)

        if message.Selector in self.__select:
            forward[0] = message.Default

    def _clean_up(self, reason):
        """
        Cleanup Method

        Empty (Stateless Module)
        """
        pass


class AntennaSelector(AbstractProcessor):
    """
    This module filters the antennas within a message by their absolute index.

    Requires:
        * 3D Numpy Array (of arbitrary shape/type), where the Antenna index is along the first dimension [Default]
    Outputs:
        * 3D Numpy Array subset with only the data corresponding to the specified antennas. Note that a copy is returned
            due to the numpy advanced indexing used.
    Archives:
        * None
    Note:
        * If none of requested output antennas are in the input antennas, an empty Numpy array is returned with size
          (0, Polarisations, Channels).
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(AntennaSelector, self).__init__()
        self.__model = None
        self.__select = None
        self.__indices = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python dict with 2 compulsory parameters:
            * in_antennas - Numpy 1-dim array containing all the absolute indices of the input antennas
            * out_antennas - Numpy 1-dim array containing all the absolute indices of the desired antennas
        :return:
        """
        # Setup
        self.__model = filter_model['in_antennas']
        self.__select = filter_model['out_antennas']
        self.__indices = np.where(np.in1d(self.__model,self.__select))[0]

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """

        forward[0] = message.Default[self.__indices,:,:]

    def _clean_up(self, reason):
        """
        Cleanup Method

        Empty (Stateless Module)
        """
        pass


class ChannelSelector(AbstractProcessor):
    """
    This module filters the channels within a message by their absolute index.

    Requires:
        * 3D Numpy Array (of arbitrary shape/type), where the Channel index is along the third dimension [Default]
    Outputs:
        * 3D Numpy Array subset with only the data corresponding to the specified channels. Note that a copy is returned
            due to the numpy advanced indexing used.
    Archives:
        * None
    Note:
        * If none of requested output channels are in the input channels, an empty Numpy array is returned with size
          (Antennas, Polarisations, 0).
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(ChannelSelector, self).__init__()
        self.__model = None
        self.__select = None
        self.__indices = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python dict with 2 compulsory parameters:
            * in_channels - Numpy 1-dim array containing all the absolute indices of the input channels
            * out_channels - Numpy 1-dim array containing all the absolute indices of the desired channels
        :return:
        """
        # Setup
        self.__model = filter_model['in_channels']
        self.__select = filter_model['out_channels']
        self.__indices = np.where(np.in1d(self.__model,self.__select))[0]

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """

        forward[0] = message.Default[:, :, self.__indices]

    def _clean_up(self, reason):
        """
        Cleanup Method

        Empty (Stateless Module)
        """
        pass


class PolarisationSelector(AbstractProcessor):
    """
    This module filters the polarisations within a message by their absolute index.

    Requires:
        * 3D Numpy Array (of arbitrary shape/type), where the Polarisation index is along the second dimension [Default]
    Outputs:
        * 3D Numpy Array subset with only the data corresponding to the specified polarisations. Note that a copy is returned
            due to the numpy advanced indexing used.
    Archives:
        * None
    Note:
        * If none of requested output polarisations are in the input polarisations, an empty Numpy array is returned
          with size (Antennas, 0, Channels).
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PolarisationSelector, self).__init__()
        self.__model = None
        self.__select = None
        self.__indices = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python dict with 2 compulsory parameters:
            * in_polarisations - Numpy 1-dim array containing all the indices of the input polarisations
            * out_polarisations - Numpy 1-dim array containing all the indices of the desired polarisations
        :return:
        """
        # Setup
        self.__model = filter_model['in_polarisations']
        self.__select = filter_model['out_polarisations']
        self.__indices = np.where(np.in1d(self.__model,self.__select))[0]

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """

        forward[0] = message.Default[:, self.__indices, :]

    def _clean_up(self, reason):
        """
        Cleanup Method

        Empty (Stateless Module)
        """
        pass