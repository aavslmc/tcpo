import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode, npalt


class PowerLog(AbstractProcessor):
    """
    This Module implements a power-log scaler for Power spectra: i.e.
        output = N * log_m (input)
            * where both N and m are configurable.

    Since Log(0) is undefined, this is replaced by the log of a default value.

    Requires:
        * ND Numpy Matrix of type float (any shape, but typically, A/P/Chan) [Default]
    Outputs:
        * ND Numpy Matrix with elements appropriately log scaled (same type/shape as input)
    Archives:
        * ND Numpy Matrix (as above)

    Limitations:
        a) The Module currently does not correctly deal with nan values (which remain nan's)
    """
    # This serves a dual purpose:
    #   * Enumerating the allowable options
    #   * Mapping inputs directly to log function.
    ALLOWABLES = {'e': np.log, 'E': np.log, '2': np.log2, '10': np.log10}

    def __init__(self):
        """
        Default Initialiser
        """
        super(PowerLog, self).__init__()
        # Setup Parameters
        self.__log = None  # Log power to drive to
        self.__mul = None  # Weighting Factor: must be a scaler
        self.__nor = None  # Normaliser to use in the case that the value is 0

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Pythin Dict containing 3 (Optional) Parameters:
            * log - [Optional]: The Logarithm Base to scale to. This must be a string (case insensitive), from the
                following:
                    > 'e' : Natural Logarithm (ln)
                    > '2' : Binary Logarithm (base 2)
                    > '10': Decimal Logarithm (base 10) [Default]
            * multiplier - [Optional]: The postmultiplier in the logarithm scaling operation. Default 10.0
            * normaliser - [Optional]: Value to replace non-positive entries with. Default to 1.0
        :return:
        """
        # Bind logarithm function and set multiplier/normaliser. For the latter, use the precomputed value.
        self.__log = PowerLog.ALLOWABLES[filter_model.get('log', '10')]
        self.__mul = filter_model.get('multiplier', 10.0)
        self.__nor = self.__mul * self.__log(filter_model.get('normaliser', 1.0))

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """
        # Create Forward Map
        forward[0] = self.__mul * self.__log(message.Default)
        forward[0][message.Default <= 0] = self.__nor

        # Copy also to archiving
        forward[1] = forward[0]

    def _clean_up(self, reason):
        pass


class Exponentiator(AbstractProcessor):
    """
    This class performs the counter-operation to the PowerLog above: i.e. it raises the input as follows:
        output = m ^ (input/N)
            * where again, both N and m are configurable.

    Requires:
        * ND Numpy Matrix of type float (any shape, but typically, A/P/Chan) [Default]
    Outputs:
        * ND Numpy Matrix with elements appropriately exponentiated (same type/shape as input)
    Archives:
        * ND Numpy Matrix (as above)

    Limitations:
        a) The Module currently does not correctly deal with nan values (which remain nan's)
    """
    # Map from possible parameters to exponential functions
    ALLOWABLES = {'e': np.exp, 'E': np.exp, '2': np.exp2, '10': npalt.exp10}

    def __init__(self):
        """
        Default Initialiser
        """
        super(Exponentiator, self).__init__()
        # Setup Parameters
        self.__pow = None  # Power to raise to
        self.__div = None  # Dividing Factor: must be a scalar

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python dict with 2 (Optional) parameters
            * divider - [Optional]: The predivider coefficient to divide the input by. Default 10.0
            * power - [Optional]: The base to raise to the input. String (case insensitivie) from the following:
                > 'e' : Euler's number
                > '2' : Two
                > '10': Ten [Default]
        :return: Status.SETUP
        """
        # Bind logarithm function and set multiplier
        self.__pow = Exponentiator.ALLOWABLES[filter_model.get('power', '10')]
        self.__div = filter_model.get('divider', 10)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        Empty (stateless module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        (Usual Parameters)
        """
        # Exponentiate
        forward[0] = self.__pow(message.Default/self.__div)

        # Archive
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleaning Method

        Empty (stateless module)
        """
        pass