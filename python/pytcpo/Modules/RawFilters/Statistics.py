import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import MovingAverage, TCPOCode

# DO NOT USE - OBSOLETE

class MovingAverageModule(AbstractProcessor):
    """
    The Module implements a Moving Average: basically a wrapper around the Moving Average Data Filter implementation.

    Requires:
        * Numpy Array (of arbitrary shape/numeric type, but integral types may cause inaccuracies) [Default]
    Outputs:
        * Numpy Array of the moving average at the current iteration.
    Archives:
        * Numpy Array of the moving average (as above).

    Limitations:
        a) The Current implementation only supports 3D Arrays.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(MovingAverageModule, self).__init__()
        # Setup Parameters
        self.__data_shape = None # Shape of the moving averages
        self.__dimensions = None # Dimensionality of each average

        # State Parameters
        self.__buffers = None    # Running Average Filters

    # Setup the Detector:
    #  Requires six (4) parameters:
    #   'antennas' - Number of antennas in configuration
    #   'polarisations' - Number of polarisations per antenna
    #   'dimensions' - The dimensionality of the data
    #   'length' - The window length over which to average
    def _setup(self, filter_model):
        # Assign Parameters
        self.__buffers = np.empty((filter_model['antennas'], filter_model['polarisations']), np.object)
        # Build Model
        for a in range(filter_model['antennas']):
            for p in range(filter_model['polarisations']):
                self.__buffers[a, p] = MovingAverage(np.zeros(filter_model['dimensions']), int(filter_model['length']))
        # Finally store data shape for convenience...
        self.__data_shape = [filter_model['antennas'], filter_model['polarisations'], filter_model['dimensions']]

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        pass

    def _new_sample(self, message, forward):
        # Setup Averages
        forward[0] = np.empty(self.__data_shape, dtype=message.Default.ravel()[0].dtype)
        for a in range(self.__data_shape[0]):
            for p in range(self.__data_shape[1]):
                forward[0][a,p] = self.__buffers[a, p].add(message.Default[a, p]) # Add Average
        # Set Archiving if necessary
        forward[1] = forward[0]

    def _clean_up(self, reason):
        pass