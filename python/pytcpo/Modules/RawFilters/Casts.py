import numbers

import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode


class CScaler(AbstractProcessor):
    """
    The Class implements an Up/Down Scaling and Type Casting operation on the passed data. It is designed to be
    typically used at the front of the pipeline for converting from restricted to less restricted numeric types. In
    order to achieve the highest accuracy, the scaling is carried out entirely in floating-poing arithmetic, with
    integer casting if required happening at the end of the conversion.

    Requires:
        * ND Numpy Matrix [Default]
    Outputs:
        * ND Numpy Matrix with elements appropriately scaled and of the required type. This is always a copy.
    Archives:
        * ND Numpy Matrix (as above)

    Limitations:
        a) For the sake of efficiency, no bounds checking is performed, and hence it should not be used for down-casting
            to more restrictive types (say from 32-bit integer to 16 bit), as this will almost always cause under/over-
            flow.

    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(CScaler, self).__init__()

        # Setup Parameters
        self.__type = None
        self.__scale = None
        self.__offset = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python Dict containing 3 (Compulsary) parameters:
            * input - [Required]: The Input Full-Scale Range. This need not be the full scale of the input type, but the
                fullscale of the data generation process.
            * output - [Required]: The Output Full-Scale Range to scale to.
            * type - [Required]: The output data type (type object)
        :return: Status.SETUP
        """

        # Pre Calculate some Stuff
        _imin, _imax = filter_model['input']
        _omin, _omax = filter_model['output']
        _iscale = _imax - _imin
        _oscale = _omax - _omin

        # Store Values: use float for accuracy
        self.__type = filter_model['type']
        self.__scale = float(_oscale) / float(_iscale)
        self.__offset = float(_omin - self.__scale * _imin)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        (Empty - Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Hanlder

        Parameters as in Abstract Class
        """
        # Scale and Cast Data
        if issubclass(self.__type, numbers.Integral): # If Integral, enforce rounding
            forward[0] = np.rint(message.Default.astype(self.__type) * self.__scale + self.__offset)
        else:
            forward[0] = message.Default.astype(self.__type) * self.__scale + self.__offset

        # Send forward
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleaning Method

        (Empty - Stateless)
        """
        pass