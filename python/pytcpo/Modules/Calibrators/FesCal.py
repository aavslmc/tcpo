import numpy as np
import logging
from copy import deepcopy

from pytcpo.Core.PipeLine import AbstractProcessor


class FesCalSolver:

    def __init__(self):

        self.coeffs = None
        self.bas_coeffs = None
        self.triangulized = None
        self._logger = logging.getLogger(__name__)

    def _triangulizer(self, matrix, auto_corr):

        corr = 0
        vis_out = np.int(0.5 * ((matrix.shape[0] ** 2) + matrix.shape[0]))
        if auto_corr is False:
            corr = 1
            vis_out = np.int(0.5 * ((matrix.shape[0] ** 2) - matrix.shape[0]))

        self.triangulized = np.ones((vis_out, matrix.shape[2]), dtype=np.complex)

        for pol in range(matrix.shape[2]):
            counter = 0
            for i in range(matrix.shape[0]):
                for j in range(i+corr, matrix.shape[0]):
                    self.triangulized[counter, pol] = matrix[i, j, pol]
                    counter += 1

    def solver1(self, model, uncal, vis_in, auto_corr):

        self.coeffs = np.ones((vis_in.shape[1], model.shape[0]), dtype=np.complex)

        self.bas_coeffs = np.ones((vis_in.shape[0], vis_in.shape[1]), dtype=np.complex)

        self._triangulizer(model, auto_corr)

        # self.triangulized = deepcopy(vis_in) * 0.96

        for pol in range(vis_in.shape[1]):

            with np.errstate(divide='ignore', invalid='ignore'):
                bas_coeffs_real = np.array(self.triangulized.real[:, pol]) / np.array(vis_in.real[:, pol])
                bas_coeffs_real /= bas_coeffs_real[0]
                bas_coeffs_imag = np.array(self.triangulized.imag[:, pol]) - np.array(vis_in.imag[:, pol])
                bas_coeffs_imag -= bas_coeffs_imag[0]
                for i in range(bas_coeffs_real.shape[0]):
                    self.bas_coeffs[i, pol] = np.complex(bas_coeffs_real[i], bas_coeffs_imag[i])

    def solver2(self, model, vis_in):

        self.coeffs = np.ones(((model.shape[0]), model.shape[2]), dtype=np.complex)

        counter = 0
        a_mat = np.zeros((vis_in.shape[0], (model.shape[0])))
        for i in range(0, (model.shape[0]-1)):
            for j in range(i+1, model.shape[0]):
                a_mat[counter, i] = 1
                a_mat[counter, j] = 1
                counter += 1

        for pol in range(vis_in.shape[1]):

            with np.errstate(divide='ignore', invalid='ignore'):
                self.coeffs[:, pol] = 10**(np.linalg.lstsq(a_mat, np.log(self.bas_coeffs[:, pol]))[0])

            self.coeffs[:, pol].real /= self.coeffs[0, pol].real
            self.coeffs[:, pol].imag -= self.coeffs[0, pol].imag

            coeffs_mean = np.mean(self.coeffs[:, pol].real)
            coeffs_std = np.std(self.coeffs[:, pol].real)
            for i in range(1, len(self.coeffs[:, pol])):
                if (coeffs_mean - (2*coeffs_std)) > self.coeffs[i, pol].real:
                    self.coeffs[i, pol] = 1. + 0.j
                if (coeffs_mean + (2*coeffs_std)) < self.coeffs[i, pol].real:
                    self.coeffs[i, pol] = 1. + 0.j

            coeffs_mean = np.mean(self.coeffs[:, pol].imag)
            coeffs_std = np.std(self.coeffs[:, pol].imag)
            for i in range(1, len(self.coeffs[:, pol])):
                if (coeffs_mean - (2*coeffs_std)) > self.coeffs[i, pol].imag:
                    self.coeffs[i, pol] = 1. + 0.j
                if (coeffs_mean + (2*coeffs_std)) < self.coeffs[i, pol].imag:
                    self.coeffs[i, pol] = 1. + 0.j

            print(self.coeffs)

            self._logger.info('FesCal successful')

    def solver3(self, model, vis_in):

        power_in = np.zeros((vis_in.shape[0], vis_in.shape[1]))

        for pol in range(vis_in.shape[1]):
            vis_in[:, pol] /= np.max(vis_in[:, pol])
            for i in range(vis_in.shape[0]):
                power_in[i, pol] = np.sqrt((vis_in[i, pol].real**2) + (vis_in[i, pol].imag**2))

        gain_coeffs = np.ones((vis_in.shape[1], model.shape[0]), dtype=np.complex)

        for pol in range(vis_in.shape[1]):

            a_mat = np.zeros((vis_in.shape[0], model.shape[0]), dtype=np.complex)
            b_vec = np.zeros(vis_in.shape[0], dtype=np.complex)

            counter = 0

            for i in range(0, (model.shape[0]-1)):
                for j in range((i+1), model.shape[0]):
                    a_mat[counter, i] = 1
                    a_mat[counter, j] = 1
                    with np.errstate(divide='ignore', invalid='ignore'):
                        b_vec[counter] = np.log(power_in[counter, pol])
                    counter += 1
            self.coeffs = np.transpose(self.coeffs)

            with np.errstate(divide='ignore', invalid='ignore'):
                log_coeff = np.linalg.lstsq(a_mat, b_vec)[0]
            gain_coeff = 10**log_coeff

            gain_coeffs[pol, :] = gain_coeff / gain_coeff[0]

            coeffs_mean = np.mean(gain_coeffs[pol, :])
            coeffs_std = np.std(gain_coeffs[pol, :])
            for i in range(1, len(gain_coeffs[pol, :])):
                if (coeffs_mean - (2*coeffs_std)) > gain_coeffs[pol, i]:
                    gain_coeffs[pol, i] = 1. + 0.j
                if (coeffs_mean + (2*coeffs_std)) < gain_coeffs[pol, i]:
                    gain_coeffs[pol, i] = 1. + 0.j

        self.coeffs = np.transpose(gain_coeffs)
        print(self.coeffs)
        self._logger.info('FesCal successful')


class FesCalRun(AbstractProcessor):

    def __init__(self):

        super(FesCalRun, self).__init__()
        self._fescal = FesCalSolver()
        self._model_vis = None
        self._actual_vis = None
        self._input_coeffs = None
        self._no_of_antennas = None
        self._auto_corr = None
        self._selfcal_check = False
        self._detriangulized_vis = None
        self._visibilities = None
        self._stefcal_coeffs = None
        self._transit_run = False

    def _setup(self, filter_model):

        self._no_of_antennas = filter_model['no_of_antennas']
        self._auto_corr = filter_model['auto_corr']

    def _will_start(self):

        pass

    def _new_sample(self, message, forward):

        self._detriangulized_vis = message.Default[0]
        self._model_vis = message.Default[1]
        self._actual_vis = message.Default[2]
        self._visibilities = message.Default[5]

        self._fescal.solver1(self._model_vis, self._actual_vis, self._detriangulized_vis, self._auto_corr)
        self._fescal.solver2(self._model_vis, self._detriangulized_vis)

        forward[0] = [self._fescal.coeffs, self._visibilities, message.Default[3], message.Default[4]]

        forward[1] = None

    def _clean_up(self, reason):

        pass
