import numpy as np
import h5py
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import scipy.signal
from astropy.io import fits

image_input = '/home/josef/Documents/tcpo/calib0.h5'
image_read = h5py.File(image_input, 'r')
image_actual = image_read["Vis"]
image_cut2 = image_actual[:, 0, :, 0]

image_out1 = np.ones((image_cut2.shape[0], 16, 16), dtype=np.complex)

for h in range(image_cut2.shape[0]):
    cr = 0
    print(np.float(h)/np.float(image_cut2.shape[0])*100.0)
    for i in range(16):
        for j in range(16):
            if i < j:
                image_out1[h, i, j] = image_cut2[h, cr]
                image_out1[h, j, i] = np.conj(image_cut2[h, cr])
                cr += 1

image_cut = np.ones((image_cut2.shape[0], ((16**2) - 16)), dtype=np.complex)
for h in range(image_cut2.shape[0]):
    cr = 0
    print(np.float(h)/np.float(image_cut2.shape[0])*100.0)
    for i in range(16):
        for j in range(16):
            if i != j:
                image_cut[h, cr] = image_out1[h, i, j]
                cr += 1

print(image_cut2.shape)
print(image_cut.shape)

# Antennas

antennas = [[-12509, 9761, 0.0],
            [-13374, 11332, 0.0],
            [-8176, 8118, 0.0],
            [-12102, 8109, 0.0],
            [-4390, 2647, 0.0],
            [-16075, 6983, 0.0],
            [-9428, 7309, 0.0],
            [-10846, 6036, 0.0],
            [-11236, 11325, 0.0],
            [-14882, 9444, 0.0],
            [-9252, 4468, 0.0],
            [-10845, 9952, 0.0],
            [-8332, 6287, 0.0],
            [-7034, 5400, 0.0],
            [-9911, 8887, 0.0],
            [-14288, 6461, 0.0]]

antennas = np.array(antennas)

ant_x = antennas[:, 0]
ant_y = antennas[:, 1]
ant_z = antennas[:, 2]

# Define List of Antennas for Baselines
counter = 0
antenna1 = np.zeros(((len(ant_x)**2) - len(ant_x)), dtype=np.int)
antenna2 = np.zeros(((len(ant_x)**2) - len(ant_x)), dtype=np.int)
for i in range(len(ant_x)):
    for j in range(len(ant_x)):
        if i != j:
            antenna1[counter] = int(i)
            antenna2[counter] = int(j)
            counter += 1

u_basl = np.zeros(((len(ant_x)**2)-len(ant_x)))
v_basl = np.zeros(((len(ant_x)**2)-len(ant_x)))
w_basl = np.zeros(((len(ant_x)**2)-len(ant_x)))
v = 2.998e8
f = 410.0e8
Lambda = v/f
cr = 0

for i in range(len(ant_x)):
    for j in range(len(ant_x)):
        if i != j:
            u_basl[cr] = (ant_x[j] - ant_x[i])
            v_basl[cr] = (ant_y[j] - ant_y[i])
            w_basl[cr] = (ant_z[j] - ant_z[i])
            cr += 1

# dec_vir = 12.2942
dec = -26.704167
dirty_image = np.zeros(((image_cut.shape[0]*(len(u_basl))), 3), dtype=np.complex)
dirty_beam = np.zeros(((image_cut.shape[0]*(len(u_basl))), 3), dtype=np.complex)
clean_image = np.zeros(((image_cut.shape[0]*(len(u_basl))), 3), dtype=np.complex)
print(dirty_image.shape)
p1 = 0
p2 = 496
cr = 0
plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111)

# for i in range(image_cut.shape[0]):
for i in range(0, 200):
    print(i)
    # ra_vir = 187.9250 + ((360.0/(23.933*60*60))*i*0.84375)
    ra = 0 + ((360.0/(23.933*60*60))*i*1.13246208)
    for j in range(len(u_basl)):
        ra_matrix = np.array([[np.sin(ra), np.cos(ra), 0], [(-np.sin(dec)*np.cos(ra)),
                                                                          (np.sin(dec)*np.sin(ra)), np.cos(dec)],
                                            [(np.cos(dec)*np.cos(ra)), (-np.cos(dec)*np.sin(ra)), np.sin(dec)]])
        delta_vector = np.array([u_basl[j], v_basl[j], w_basl[j]])
        dirty_image[cr, :] = (np.multiply(((np.dot(ra_matrix, delta_vector)) * (1.0/Lambda)), (image_cut[i, j])))
        dirty_beam[cr, :] = (np.dot(ra_matrix, delta_vector) * (1.0/Lambda))
        clean_image[cr, :] = (np.divide((np.convolve(((np.dot(ra_matrix, delta_vector)) * (1.0/Lambda)),
                                        (image_cut[i, j]))), (np.dot(ra_matrix, delta_vector) * (1.0/Lambda))))
        cr += 1

    #for v in range(p1, p2):
    #    for w in range(3):
    #        dirty_image[v, w] = np.sqrt((dirty_image[v, w].real ** 2) + (dirty_image[v, w].imag ** 2))

    dirty_image2 = np.fft.ifft(dirty_image[p1:p2, :])
    dirty_beam2 = np.fft.ifft(dirty_beam[p1:p2, :])
    clean_image2 = np.fft.ifft(clean_image[p1:p2, :])

    image_out = dirty_image2
    print(image_out[0, 0])

    #for v in range(image_out.shape[0]):
    #    for w in range(image_out.shape[1]):
    #        image_out[v, w] = np.sqrt((image_out[v, w].real ** 2) + (image_out[v, w].imag ** 2))

    plt.cla()
    ax.scatter(image_out[:, 0], image_out[:, 1], marker=".", s=0.01)
    if i == 0:
        a = 2 * np.min(image_out[:, 0])
        b = 2 * np.max(image_out[:, 0])
        c = 2 * np.min(image_out[:, 1])
        d = 2 * np.max(image_out[:, 1])
    plt.xlim(a, b)
    plt.ylim(c, d)
    fig.canvas.draw()
    plt.show()
    if i == 0:
        plt.savefig('/home/josef/Documents/tcpo/img2_out.png', dpi=600)
    p1 += 136
    p2 += 136

#plt.figure()
#sns.jointplot(x=dirty_beam2[:, 0], y=dirty_beam2[:, 1], kind='hex', color='xkcd:sky blue')
#plt.show()




