import traceback
import numpy as np
import h5py
import datetime as dt
import logging
try:
    from casacore import tables
except ImportError:
    logging.warning('casacore not found.')

from pytcpo.Core.PipeLine import AbstractGenerator, Message
from pytcpo.Core.Common import TCPOCode

from pydaq.persisters import corr


class Consumer(object):

    """
    Data handler for calibration pipeline. Shall receive message containing true visibility data, for which it shall
    read corresponding model data. It will forward both these sets of matching data to the calibration processor.

    """

    def __init__(self):
        """
        Default Initializer

        """

        self.UVW = None
        self.antenna1 = None
        self.antenna2 = None
        self.model_data = None
        self.uncal_data = None
        self.basl_no = None
        self.logger = logging.getLogger(__name__)

    def ms_reader(self, ms_name):
        """
        MS Reader: A CASA Measurement Set (MS) reader, for loading of OSKAR-generated MS. Visibility data and other
        related parameters are read into numpy arrays using the CASACore package.
        Also transposes MS model data to correct dimensional order, from [basl, chans, cross] to [chans, basl, cross]

        :param ms_name: File path for MS to be loaded

        """
        np.set_printoptions(threshold='nan')
        tb = tables.table(ms_name, ack=False)

        uvw = tb.getcol("UVW")
        self.UVW = np.array(uvw.transpose())
        self.antenna1 = np.array(tb.getcol("ANTENNA1"))
        self.antenna2 = np.array(tb.getcol("ANTENNA2"))
        self.model_data = np.array(tb.getcol("DATA"))
        self.basl_no = np.array(range(0, len(self.antenna1)))

        self.model_data = np.transpose(self.model_data, (1, 0, 2))

    def h5_reader(self, h5):
        """
        Reader for hdf5 files

        :param h5: File path for the hdf5 file containing the visibilities data set named "Vis_Data" to be read.

        """

        f = h5py.File(h5, "r")
        self.uncal_data = np.array(f["Vis"])


class ConsumerRun(AbstractGenerator):

    def __init__(self):

        super(ConsumerRun, self).__init__()
        self.receiver = Consumer()
        self._model_vis = None
        self._uncal_vis = None
        self._uncal_vis_dir = None
        self._model_vis_dir = None
        self._model_vis_list_path = None
        self._model_vis_file = None
        self._total_obs_samples = None
        self._transit_run = False
        self._mod_time_list = list()
        self._model_name = None
        self._transit_obs_file = None
        self._model_gen = False
        self._telescope = None
        self._counter = 0

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary containing format of real visibilities parameter

        """

        self._uncal_vis_dir = filter_model['actual_vis_directory']
        self._model_vis_dir = filter_model['model_vis_directory']
        self._model_vis_list_path = filter_model['model_vis_list_path']
        self._total_obs_samples = filter_model['total_time_samples']
        self._transit_run = filter_model['transit_run']
        self._transit_obs_file = filter_model['transit_file']
        self._telescope = filter_model['telescope']

    def _generator_loop(self):

        """
        Uses DAQ reader to call most recently obtained visibilities data chunk for a particular channel. Loads the
        channel closest in time from the model visibilities folder, passes real and model visibilities to Calibrator.

        """

        if self._transit_run is True:

            h5file = self._transit_obs_file
            self.receiver.h5_reader(h5file)

            if self._telescope == 'best2':

                self._uncal_vis = self.receiver.uncal_data[:, 0, :, 0]

                self._model_vis = np.ones((self._uncal_vis.shape[0], self._uncal_vis.shape[1]), dtype=np.complex)

            if self._telescope == 'aavs':

                self._uncal_vis = self.receiver.uncal_data[:, :, 0]/1e9

                self._model_name = '2018-02-2104:43:30'
                self._model_vis_file = str(self._model_vis_dir) + '/' + str(self._model_name) + '.ms'
                self.receiver.ms_reader(self._model_vis_file)
                self._model_vis = self.receiver.model_data[0, :, :]

            msg = Message((Message.SelType.NONE, 0), idx=0, ts=0)

            msg.Default = [self._model_vis[:, :], self._uncal_vis[:, :], 0, self._model_name]

            self.receiver.logger.info('Consumer Run:{}'.format(self._counter))

            self._push(msg, None)

        if self._transit_run is False:

            cff = corr.CorrelationFormatFileManager(self._uncal_vis_dir)

            while True:

                for i in range(self._total_obs_samples):

                    while True:

                        if self._counter >= self._total_obs_samples:

                            break

                        # Wait for visibility data is necessary
                        counter_read = 0
                        while True:
                            try:
                                data_out = cff.read_data(channel_id=204)
                                if data_out[0] is not []:
                                    break
                            except TypeError:
                                if counter_read == 0:
                                    self.receiver.logger.info('Waiting for visibility data!')
                                    counter_read += 0

                        if data_out[0] is not []:

                            self._model_timestamp_list = np.loadtxt(self._model_vis_list_path, dtype=np.str)
                            for j in range(self._model_timestamp_list.shape[0]):
                                mod_date = self._model_timestamp_list[j, 0]
                                mod_time = self._model_timestamp_list[j, 1]
                                current_mod_date = dt.datetime.strptime(mod_date, '%Y-%m-%d').date()
                                current_mod_time = dt.datetime.strptime(mod_time, '%H:%M:%S').time()
                                self._mod_time_list.append(dt.datetime.combine(current_mod_date, current_mod_time))

                            current_datetime = data_out[1]

                            model_time_req = min(self._mod_time_list, key=lambda d: abs(d - current_datetime))

                            self._model_name = dt.datetime.strftime(model_time_req, '%Y-%m-%d%H:%M:%S')
                            model_vis_file = str(self._model_vis_dir) + '/' + str(self._model_name) + '.ms'
                            self.receiver.ms_reader(model_vis_file)

                            self._model_vis = self.receiver.model_data
                            self._uncal_vis = data_out[0]

                            msg = Message((Message.SelType.NONE, 0), idx=0, ts=0)

                            msg.Default = [self._model_vis[0, :, :], self._uncal_vis[:, :], i, self._model_name]

                            self._push(msg, None)

                            self._counter += 1

                            self.receiver.logger.info('Consumer Run: {}'.format(self._counter))

                            break

                    if self._counter >= self._total_obs_samples:

                        break

                if self._counter >= self._total_obs_samples:

                    break

        self._inform(TCPOCode.Request.STOP_SMPL)
