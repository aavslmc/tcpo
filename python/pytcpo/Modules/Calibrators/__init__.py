# Contains all necessary modules for the second calibration pipeline, which shall be receiving visibilities live.

from pytcpo.Modules.Calibrators.Consumer import ConsumerRun
from pytcpo.Modules.Calibrators.Calibrator import CalibratorRun
from pytcpo.Modules.Calibrators.SelfCal import SelfCalRun
from pytcpo.Modules.Calibrators.StEFCal import StEFCalRun
from pytcpo.Modules.Calibrators.FesCal import FesCalRun
from pytcpo.Modules.Calibrators.CoeffManager import CoeffManagerRun

from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(ConsumerRun)
PipelineManager.registerModule(CalibratorRun)
PipelineManager.registerModule(SelfCalRun)
PipelineManager.registerModule(StEFCalRun)
PipelineManager.registerModule(FesCalRun)
PipelineManager.registerModule(CoeffManagerRun)
