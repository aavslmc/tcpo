# import datetime
# import numpy as np
#
# # Define sky model sources
# sky_model = {"source_names": ['M31', 'M42', 'M43', 'M45', 'Pol', 'SOc', 'Wsp'],
#              "source_RAs": [10.5, 83.8, 83.9, 56.9, 0.0, 0.0, 348.5],
#              "source_Decs": [41.3, -5.4, -5.3, 24.1, 89.9, -89.8, 8.8],
#              }
#
# sky_model["source_T"] = np.zeros(len(sky_model["source_names"]), dtype=float)
#
#
# # Define main parameters
# latitude = 35.9
# time_local = datetime.time(0, 0, 0)
# date_now = datetime.date(2018, 11, 18)
# year_now = date_now.year
# month_now = date_now.month
# day_now = date_now.day
# print 'Date: ' + str(day_now) + '-' + str(month_now) + '-' + str(year_now)
#
# # Determine Zenith RA at location
# RA_daily_change_deg = ((((24./365.25) * 60. * 60.)/(24. * 60. * 60.)) * 360.)
# days_from_Sept_21 = float((date_now - datetime.date(year_now, 9, 21)).days)
#
# zenith_RA = RA_daily_change_deg * days_from_Sept_21
# if zenith_RA < 0.:
#     zenith_RA += 360.
# if zenith_RA > 360.:
#     zenith_RA -= 360.
#
# midnight = datetime.time(0, 0, 0)
# time_observation = datetime.datetime.combine(date_now, time_local)
# time_midnight = datetime.datetime.combine(date_now, midnight)
#
# # Define Zenith Declination as Latitude
# zenith_dec = latitude
#
# print "Zenith Dec: " + str(zenith_dec) + ';', "Zenith RA:" + str(zenith_RA)
#
# # Determine declination range above horizon from location
# dec_zenith_range_above_horizon = [zenith_dec - 90., zenith_dec + 90.]
#
# marker = None
# always_above_horizon = None
# always_below_horizon = None
#
# if dec_zenith_range_above_horizon[0] < -90.:
#     marker = 'Southern_Hemisphere'
#     always_above_horizon = -90. + (abs(dec_zenith_range_above_horizon[0]) - 90.)
#     always_below_horizon = dec_zenith_range_above_horizon[1]
#     dec_zenith_range_above_horizon[0] = -90.
# if dec_zenith_range_above_horizon[1] > 90.:
#     marker = 'Northern_Hemisphere'
#     always_above_horizon = 90. - (abs(dec_zenith_range_above_horizon[1]) - 90.)
#     always_below_horizon = dec_zenith_range_above_horizon[0]
#     dec_zenith_range_above_horizon[1] = 90.
# if dec_zenith_range_above_horizon[0] == -90. and dec_zenith_range_above_horizon[1] == 90:
#     marker = 'Equator'
#     always_above_horizon = 90.0
#     always_below_horizon = -90.0
#
# print('Declination range above horizon: ' + str(dec_zenith_range_above_horizon[0]) + ', ' +
#       str(dec_zenith_range_above_horizon[1]))
#
# print('Declination limit ALWAYS above horizon: ' + str(always_above_horizon))
# print('Declination limit ALWAYS below horizon: ' + str(always_below_horizon))
#
# # Determine length of time of source above horizon
# for i in range(len(sky_model["source_names"])):
#
#     if marker == 'Northern_Hemisphere':
#         if float(sky_model["source_Decs"][i]) >= always_below_horizon:
#             if float(sky_model["source_Decs"][i]) < always_above_horizon:
#                 dec = np.deg2rad(sky_model["source_Decs"][i])
#                 lat = np.deg2rad(latitude)
#                 H = np.arccos((-np.sin(lat)*np.sin(dec))/(np.cos(lat)*np.cos(dec)))
#                 sky_model["source_T"][i] = np.degrees(H)/7.42
#                 continue
#         if float(sky_model["source_Decs"][i]) < always_below_horizon:
#             sky_model["source_T"][i] = 0.
#         if float(sky_model["source_Decs"][i]) >= always_above_horizon:
#             sky_model["source_T"][i] = 24.
#
#     if marker == 'Equator':
#         dec = np.deg2rad(sky_model["source_Decs"][i])
#         lat = np.deg2rad(latitude)
#         H = np.arccos((-np.sin(lat)*np.sin(dec))/(np.cos(lat)*np.cos(dec)))
#         sky_model["source_T"][i] = np.degrees(H)/7.42
#
#     if marker == 'Southern_Hemisphere':
#         if float(sky_model["source_Decs"][i]) <= always_below_horizon:
#             if float(sky_model["source_Decs"][i]) > always_above_horizon:
#                 dec = np.deg2rad(sky_model["source_Decs"][i])
#                 lat = np.deg2rad(latitude)
#                 H = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
#                 sky_model["source_T"][i] = np.degrees(H) / 7.42
#                 continue
#         if float(sky_model["source_Decs"][i]) > always_below_horizon:
#             sky_model["source_T"][i] = 0.
#         if float(sky_model["source_Decs"][i]) <= always_above_horizon:
#             sky_model["source_T"][i] = 24.
#
# print(sky_model["source_T"])
#
# # Determine sources below and above horizon
# above_horizon = []
# below_horizon = []
# print 'Source list rising, setting time computation...'
# for i in range(len(sky_model["source_names"])):
#     # If source is always below horizon
#     if sky_model["source_T"][i] == 0.:
#         below_horizon.append(sky_model["source_names"][i])
#         print sky_model["source_names"][i] + ' Always Below Horizon'
#         continue
#     # If source is always above horizon
#     if sky_model["source_T"][i] == 24.:
#         above_horizon.append(sky_model["source_names"][i])
#         print sky_model["source_names"][i] + ' Always Above Horizon'
#         continue
#     # If source IS `not always above or below horizon, compute source time range above horizon
#     RA_difference = zenith_RA - float(sky_model["source_RAs"][i])
#     time_at_meridian = datetime.datetime(year_now, month_now, day_now, midnight.hour, midnight.minute, midnight.second) \
#                        - datetime.timedelta(seconds=(RA_difference/360. * (24. * 60. * 60.)))
#
#     time_range_above_horizon = [(time_at_meridian - datetime.timedelta(hours=0.5*float(sky_model["source_T"][i]))),
#                                 (time_at_meridian + datetime.timedelta(hours=0.5*float(sky_model["source_T"][i])))]
#
#     # Verify correct date computation, or correct for it, for analysis of whether source is in sky
#     if time_range_above_horizon[1] < time_observation:
#         time_range_above_horizon[0] += datetime.timedelta(hours=24.)
#         time_range_above_horizon[1] += datetime.timedelta(hours=24.)
#
#     # Verify if source is above horizon during observation, or otherwise
#     if time_observation >= time_range_above_horizon[0]:
#         if time_observation <= time_range_above_horizon[1]:
#             above_horizon.append(sky_model["source_names"][i])
#             print sky_model["source_names"][i], time_range_above_horizon[0], time_range_above_horizon[1]
#             continue
#     below_horizon.append(sky_model["source_names"][i])
#     print sky_model["source_names"][i], time_range_above_horizon[0], time_range_above_horizon[1]
#
# print "Above Horizon: " + str(above_horizon)
# print "Below Horizon: " + str(below_horizon)
# exit()

import datetime

time_observation = datetime.datetime(2018, 9, 21, 0, 0, 0)

sun_Dec = None
dec_daily_change_deg = 93.76 / 365.25
days_seconds_from_mar_21 = time_observation - datetime.datetime(time_observation.year, 3, 21, 0, 0, 0)
print(days_seconds_from_mar_21)
if days_seconds_from_mar_21.days < 0:
    sun_Dec = (dec_daily_change_deg * (days_seconds_from_mar_21.days -
                                            (((days_seconds_from_mar_21.seconds / 60.) / 60.) / 24.)))
if days_seconds_from_mar_21.days >= 0:
    sun_Dec = (dec_daily_change_deg * (days_seconds_from_mar_21.days +
                                            (((days_seconds_from_mar_21.seconds / 60.) / 60.) / 24.)))

if sun_Dec < -23.44:
    sun_Dec = -23.44 + (-23.44 - sun_Dec)
if sun_Dec > 23.44:
    sun_Dec = 23.44 - (sun_Dec - 23.44)
    if sun_Dec < -23.44:
        sun_Dec = -23.44 + (-23.44 - sun_Dec)
