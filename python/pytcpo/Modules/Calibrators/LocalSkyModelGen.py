import datetime
import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor


class LocalSkyModelGen(object):

    def __init__(self):

        self.global_sky_model = None
        self.source_RAs = None
        self.source_Decs = None
        self.sky_model = {}
        self.latitude = None
        self.time_local = None
        self.date_now = None
        self.time_observation = None
        self.zenith_RA = None
        self.zenith_Dec = None
        self.position = None
        self.always_above_horizon = None
        self.always_below_horizon = None
        self.above_horizon = None
        self.below_horizon = None
        self.lsm_sources = None

    def gsm_reader(self, sky_model_name):

        self.global_sky_model = np.loadtxt(sky_model_name)
        self.sky_model["source_RAs"] = self.global_sky_model[:, 0]
        self.sky_model["source_Decs"] = self.global_sky_model[:, 1]
        self.sky_model["source_T"] = np.zeros(len(self.sky_model["source_RAs"]), dtype=float)

    def receive_observation_parameters(self, latitude, local_time, date):

        self.latitude = latitude
        self.time_local = local_time
        self.date_now = date
        self.time_observation = datetime.datetime.combine(self.date_now, self.time_local)

    def calculate_zenith_dec_ra(self):

        ra_daily_change_deg = ((((24. / 365.25) * 60. * 60.) / (24. * 60. * 60.)) * 360.)
        days_from_sept_21 = float((self.date_now - datetime.date(self.date_now.year, 9, 21)).days)

        self.zenith_RA = ra_daily_change_deg * days_from_sept_21
        if self.zenith_RA < 0.:
            self.zenith_RA += 360.
        if self.zenith_RA > 360.:
            self.zenith_RA -= 360.

        # Define Zenith Declination as Latitude
        self.zenith_Dec = self.latitude

    def calculate_dec_ranges(self):

        dec_zenith_range_above_horizon = [self.zenith_Dec - 90., self.zenith_Dec + 90.]

        if dec_zenith_range_above_horizon[0] < -90.:
            self.position = 'Southern_Hemisphere'
            self.always_above_horizon = -90. + (abs(dec_zenith_range_above_horizon[0]) - 90.)
            self.always_below_horizon = dec_zenith_range_above_horizon[1]
            dec_zenith_range_above_horizon[0] = -90.
        if dec_zenith_range_above_horizon[1] > 90.:
            self.position = 'Northern_Hemisphere'
            self.always_above_horizon = 90. - (abs(dec_zenith_range_above_horizon[1]) - 90.)
            self.always_below_horizon = dec_zenith_range_above_horizon[0]
            dec_zenith_range_above_horizon[1] = 90.
        if dec_zenith_range_above_horizon[0] == -90. and dec_zenith_range_above_horizon[1] == 90:
            self.position = 'Equator'
            self.always_above_horizon = 90.0
            self.always_below_horizon = -90.0

    def determine_source_duration_above_horizon(self):

        for i in range(len(self.sky_model["source_RAs"])):

            if self.position == 'Northern_Hemisphere':
                if float(self.sky_model["source_Decs"][i]) >= self.always_below_horizon:
                    if float(self.sky_model["source_Decs"][i]) < self.always_above_horizon:
                        dec = np.deg2rad(self.sky_model["source_Decs"][i])
                        lat = np.deg2rad(self.latitude)
                        h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                        self.sky_model["source_T"][i] = np.degrees(h) / 7.42
                        continue
                if float(self.sky_model["source_Decs"][i]) < self.always_below_horizon:
                    self.sky_model["source_T"][i] = 0.
                if float(self.sky_model["source_Decs"][i]) >= self.always_above_horizon:
                    self.sky_model["source_T"][i] = 24.

            if self.position == 'Equator':
                dec = np.deg2rad(self.sky_model["source_Decs"][i])
                lat = np.deg2rad(self.latitude)
                h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                self.sky_model["source_T"][i] = np.degrees(h) / 7.42

            if self.position == 'Southern_Hemisphere':
                if float(self.sky_model["source_Decs"][i]) <= self.always_below_horizon:
                    if float(self.sky_model["source_Decs"][i]) > self.always_above_horizon:
                        dec = np.deg2rad(self.sky_model["source_Decs"][i])
                        lat = np.deg2rad(self.latitude)
                        h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                        self.sky_model["source_T"][i] = np.degrees(h) / 7.42
                        continue
                if float(self.sky_model["source_Decs"][i]) > self.always_below_horizon:
                    self.sky_model["source_T"][i] = 0.
                if float(self.sky_model["source_Decs"][i]) <= self.always_above_horizon:
                    self.sky_model["source_T"][i] = 24.

    def define_sources_above_horizon(self):

        self.above_horizon = []
        self.below_horizon = []
        for i in range(len(self.sky_model["source_RAs"])):
            # If source is always below horizon
            if self.sky_model["source_T"][i] == 0.:
                self.below_horizon.append(i)
                continue
            # If source is always above horizon
            if self.sky_model["source_T"][i] == 24.:
                self.above_horizon.append(i)
                continue
            # If source IS `not always above or below horizon, compute source time range above horizon
            ra_difference = self.zenith_RA - float(self.sky_model["source_RAs"][i])
            time_at_meridian = datetime.datetime(self.date_now.year, self.date_now.month, self.date_now.date, 0, 0, 0) \
                - datetime.timedelta(seconds=(ra_difference / 360. * (24. * 60. * 60.)))

            time_range_above_horizon = [
                (time_at_meridian - datetime.timedelta(hours=0.5 * float(self.sky_model["source_T"][i]))),
                (time_at_meridian + datetime.timedelta(hours=0.5 * float(self.sky_model["source_T"][i])))]

            # Verify correct date computation, or correct for it, for analysis of whether source is in sky
            if time_range_above_horizon[1] < self.time_observation:
                time_range_above_horizon[0] += datetime.timedelta(hours=24.)
                time_range_above_horizon[1] += datetime.timedelta(hours=24.)

            # Verify if source is above horizon during observation, or otherwise
            if self.time_observation >= time_range_above_horizon[0]:
                if self.time_observation <= time_range_above_horizon[1]:
                    self.above_horizon.append(i)
                    continue
            self.below_horizon.append(i)

    def lsm_builder(self):

        self.lsm_sources = []
        for i in self.above_horizon:
            self.lsm_sources.append(self.global_sky_model[i, :])

        self.lsm_sources = np.array(self.lsm_sources, dtype=float)

    def lsm_writer(self, filepath_out):

        text_file = open(filepath_out, "w")
        for i in range(self.lsm_sources.shape[0]):
            for j in range(self.lsm_sources.shape[1]):
                text_file.write(str(self.lsm_sources[i][j]) + ' ')
            text_file.write('\n')
        text_file.close()


class LocalSkyModelGenRun(AbstractProcessor):

    def __init__(self):

        super(LocalSkyModelGenRun, self).__init__()
        self.lsm_gen = LocalSkyModelGen()
        self.sky_model_in = None
        self.latitude = None
        self.time_local = None
        self.date_now = None
        self.sky_model_out = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Dictionary containing required parameters for all LSM generation modules

        """

        self.sky_model_in = filter_model['sky_model_in']
        self.latitude = filter_model['latitude']
        self.time_local = filter_model['time']
        self.date_now = filter_model['date']
        self.sky_model_out = filter_model['sky_model_out']

    def _will_start(self):
        """
        Start Method

        (Empty - Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):

        self.lsm_gen.gsm_reader(self.sky_model_in)
        self.lsm_gen.receive_observation_parameters(self.latitude, self.time_local, self.date_now)
        self.lsm_gen.calculate_zenith_dec_ra()
        self.lsm_gen.calculate_dec_ranges()
        self.lsm_gen.determine_source_duration_above_horizon()
        self.lsm_gen.define_sources_above_horizon()
        self.lsm_gen.lsm_builder()
        self.lsm_gen.lsm_writer(self.sky_model_out)

    def _clean_up(self, reason):
        """
        Clean Function

        (Empty)
        """
        pass







