import numpy as np
import logging

from pytcpo.Core.PipeLine import AbstractProcessor


class StEFCalSolver:

    def __init__(self, tolerance, maximum_iterations):
        """ Constructs a new instance of StefcalSolver
        :param tolerance: Stop criterion on the change in the norm of the estimated gain vectors in consecutive
        iterations
        :param maximum_iterations: The maximum number of iterations to perform, if the convergence criterion has not
        been met by this number of iterations a RuntimeError is raised.
        :return: A new instance of the class.
        """
        self._tolerance = tolerance
        self._maximum_number_of_iterations = maximum_iterations
        self._logger = logging.getLogger(__name__)
        self.sols = None

    def _gainsolv(self, tolerance, model_covariance_matrix, covariance_matrix, gains_estimate):
        """
        This function is a direct port of the MATLAB gainsolv routine written by:
        SS & SJW, March 7, 2012

        Arguments:
        :param tolerance: Stop criterion on the change in the norm of the estimated gain vectors in consecutive
        iterations.
        :param model_covariance_matrix: (number_of_antenna x number_of_antenna) zero gain covariance matrix derived from
        sky model.
        :param covariance_matrix: (number_of_antenna x number_of_antenna) measured covariance matrix.
        :param gains_estimate: (number_of_antenna x 1) vector with initial estimate for gains.
        """

        gains = gains_estimate.copy()

        # Normalize the measured array covariance matrix (weighting)
        column_normalisation_factor = np.sum(np.multiply(np.conj(covariance_matrix), covariance_matrix), axis=0)
        normalised_covariance_matrix = np.divide(covariance_matrix, column_normalisation_factor)

        iteration = []
        gains_last_estimate = []

        # Start iterating
        for iteration in range(1, self._maximum_number_of_iterations+1):
            covariance_matrix_estimate = np.multiply(gains, model_covariance_matrix)

            # Update gain estimate
            gains_inverse_conjugate = np.sum(np.multiply(np.conj(normalised_covariance_matrix),
                                                         covariance_matrix_estimate), axis=0)

            # Inverse of diagonal
            gains = np.divide(1, np.conj(gains_inverse_conjugate))
            gains = np.reshape(gains, (gains.size, 1))

            if iteration % 2 > 0:
                # find new convergence curve
                gains_last_estimate = gains
            else:
                # update calibration results so far and check for convergence
                gains_old = gains
                gains = np.multiply(np.add(gains,  gains_last_estimate), 0.5)

                if iteration >= 2:
                    delta_gains_norm = np.linalg.norm(np.subtract(gains, gains_old))
                    gains_norm = np.linalg.norm(gains)

                    if delta_gains_norm / gains_norm <= tolerance:
                        self._logger.info('Convergence reached after {} iterations'.format(iteration))
                        self._logger.info('Change in norm(g) from the last iteration: '
                                          '{} '.format(delta_gains_norm / gains_norm))
                        break

        if iteration >= self._maximum_number_of_iterations:
            message = 'Failed to reach convergence after {} iterations'.format(iteration)
            self._logger.warning(message)

        self.sols = np.array(gains, dtype=np.complex)

    def solve(self, model_covariance_matrix, covariance_matrix, coeffs_estimate):
        """
        Solve the measurement equation and return the complex gains, using the StEFCal algorithm.
        :param model_covariance_matrix: (number_of_antenna x number_of_antenna) zero gain covariance matrix derived from
        sky model.
        :param covariance_matrix: (number_of_antenna x number_of_antenna) measured covariance matrix.
        :param coeffs_estimate: (number_of_antenna) initial coeff estimates via SelfCal or otherwise.
        """

        model = np.array(model_covariance_matrix, dtype=np.complex)
        observed = np.array(covariance_matrix, dtype=np.complex)

        np.fill_diagonal(model, 0)
        np.fill_diagonal(observed, 0)

        if model.shape != observed.shape:
            message = 'The model_covariance_matrix and covariance_matrix have different shapes: {} and ' \
                      '{}'.format(model.shape, observed.shape)
            self._logger.warning(message)

        # It is possible that there may be a solution if a number of the covariance matrix elements are NaN. Thus, they
        # converted to zeros for computation purposes.
        nan_mask = np.isnan(observed)
        observed[nan_mask] = 0

        nan_mask = np.isnan(model)
        observed[nan_mask] = 0
        model[nan_mask] = 0

        if np.any(np.sum(np.multiply(np.conj(observed), observed), axis=0) == 0):
            message = 'The sum of one or more covariance matrix rows multiplied by its conjugate equals zero.'
            self._logger.warning(message)

        gains_estimate = np.array(coeffs_estimate, dtype=np.complex)

        self._gainsolv(self._tolerance, model, observed, gains_estimate)


class StEFCalRun(AbstractProcessor):

    def __init__(self):

        super(StEFCalRun, self).__init__()
        self._stefcal = StEFCalSolver(0.001, 4096)
        self._model_vis = None
        self._actual_vis = None
        self._input_coeffs = None
        self._no_of_antennas = None
        self._selfcal_check = False
        self._detriangulized_vis = None
        self._stefcal_coeffs = None
        self._transit_run = False

    def _setup(self, filter_model):

        self._no_of_antennas = filter_model['no_of_antennas']
        if filter_model['selfcal'] is True:
            self._selfcal_check = True
        if filter_model['transit_run'] is True:
            self._transit_run = True

    def _will_start(self):

        pass

    def _new_sample(self, message, forward):

        self._detriangulized_vis = message.Default[0]
        self._model_vis = message.Default[1]
        self._actual_vis = message.Default[2]

        if self._transit_run is False:

            self._stefcal_coeffs = np.ones((self._model_vis.shape[0], self._model_vis.shape[2]), dtype=np.complex)

            if self._selfcal_check is True:
                for pol in range(self._detriangulized_vis[0, :].shape[0]):
                    self._input_coeffs = message.Default[3][:, pol]
                    self._stefcal.solve(self._model_vis[:, :, pol], self._actual_vis[:, :, pol], self._input_coeffs)
                    self._stefcal_coeffs[:, pol] = self._stefcal.sols[:, 0] * self._input_coeffs
                    self._stefcal_coeffs[:, pol] /= self._stefcal_coeffs[0, pol]

                forward[0] = [self._stefcal_coeffs, self._detriangulized_vis, message.Default[4], message.Default[5]]

            if self._selfcal_check is False:
                for pol in range(self._detriangulized_vis[0, :].shape[0]):
                    self._input_coeffs = np.ones(self._no_of_antennas, dtype=np.complex)
                    self._stefcal.solve(self._model_vis[:, :, pol], self._actual_vis[:, :, pol], self._input_coeffs)
                    self._stefcal_coeffs[:, pol] = self._stefcal.sols[:, 0]
                    self._stefcal_coeffs[:, pol] /= self._stefcal_coeffs[0, pol]

                forward[0] = [self._stefcal_coeffs, self._detriangulized_vis, message.Default[3], message.Default[4]]

        if self._transit_run is True:

            self._stefcal_coeffs = np.ones((self._model_vis.shape[0], self._model_vis.shape[2]), dtype=np.complex)

            if self._selfcal_check is True:
                self._input_coeffs = message.Default[3][:, 0]
                self._stefcal.solve(self._model_vis[:, :, 0], self._actual_vis[:, :, 0], self._input_coeffs)
                for i in range(len(self._input_coeffs)):
                    stefcal_coeff_real = self._stefcal.sols[i, 0].real * self._input_coeffs[i].real
                    stefcal_coeff_imag = self._stefcal.sols[i, 0].imag * self._input_coeffs[i].imag
                    self._stefcal_coeffs[i, 0] = np.complex(stefcal_coeff_real, stefcal_coeff_imag)

                self._stefcal_coeffs /= self._stefcal_coeffs[0, 0]
                forward[0] = [self._stefcal_coeffs, self._detriangulized_vis, message.Default[4], message.Default[5]]

            if self._selfcal_check is False:
                self._input_coeffs = np.ones(self._no_of_antennas, dtype=np.complex)
                self._stefcal.solve(self._model_vis[:, :, 0], self._actual_vis[:, :, 0], self._input_coeffs)
                self._stefcal_coeffs[:, 0] = self._stefcal.sols[:, 0]

                self._stefcal_coeffs /= self._stefcal_coeffs[0, 0]
                forward[0] = [self._stefcal_coeffs, self._detriangulized_vis, message.Default[3], message.Default[4]]

        # print(self._stefcal_coeffs)
        forward[1] = None

    def _clean_up(self, reason):

        pass
