import numpy as np
import h5py
import logging
import subprocess
try:
    from casacore import tables
except ImportError:
    logging.warning('casacore not found.')

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Modules.Calibrators import FringeImager as fi


class CoeffManager:

    def __init__(self):

        self.coeffs = None
        self.baseline_no = None
        self.coeff_filepath = None
        self.calib_filepath = None
        self._logger = logging.getLogger(__name__)

    def read_coeffs(self, coeffs_in):

        self.coeffs = coeffs_in

    def save_coeffs(self, coeffs_filepath):

        self.coeff_filepath = coeffs_filepath
        text_file = open(self.coeff_filepath, "w")
        for pol in range(self.coeffs.shape[1]):
            for i in range(self.coeffs.shape[0]):
                text_file.write(str(self.coeffs[i, pol]) + '\n')
        text_file.close()

    def coeffs_apply(self, real_vis, baseline_no, main_dir, i, ms_name, ms_dir, model_run):

        # Create empty calibrated visibilities matrix
        vis_calib = np.ones((len(real_vis[:, 0]), len(real_vis[0, :])), dtype=np.complex)

        for pol in range(real_vis.shape[1]):
            coeffs_real = []
            coeffs_imag = []
            for j in range(self.coeffs.shape[0]):
                coeffs_real.append(self.coeffs[j, pol].real)
                coeffs_imag.append(self.coeffs[j, pol].imag)

            coeffs_real = np.array(coeffs_real)
            coeffs_imag = np.array(coeffs_imag)

            #print(real_vis.shape)
            # Calibrate visibilities
            for t in range(len(real_vis[:, 0])):
                a1 = int(baseline_no[t, 0])
                a2 = int(baseline_no[t, 1])
                vis_calib[t, pol] = real_vis[t, pol] * ((np.complex(coeffs_real[a1], coeffs_imag[a1])) *
                                                        np.conj(np.complex(coeffs_real[a2], coeffs_imag[a2])))

        self.calib_filepath = main_dir + '/calib' + str(i) + '.h5'
        # Write calibrated visibilities to h5 file
        f2 = h5py.File(self.calib_filepath, 'w')
        name = 'Vis'
        dset = f2.create_dataset(name, (len(real_vis[:, 0]), len(real_vis[0, :])), dtype='c16')
        dset[:, :] = vis_calib[:, :]
        f2.flush()
        f2.close()

        # Save calibrated visibilities to ms file
        old_ms = str(ms_dir) + '/' + str(ms_name) + '.ms'
        new_ms = str(ms_dir) + '/' + str(ms_name) + '_calib.ms'

        np.set_printoptions(threshold='nan')
        tb = tables.table(old_ms, ack=False)
        tb.copy(new_ms, deep=True)
        tb.close()

        tb = tables.table(new_ms, readonly=False)

        vis_calib_cut = np.array(vis_calib[0, 0, :, 0])
        vis_calib_cut2 = np.reshape(vis_calib_cut, (len(vis_calib_cut), 1, 1))
        tables.table.putcol(tb, "DATA", vis_calib_cut2)
        tb.flush()
        tb.close()

    def coeffs_apply_transit(self, obs_file, baseline_no, main_dir, i, ms_name, ms_dir, model_run):

        with h5py.File(obs_file, "r") as f:
            data = f["Vis"]
            real_vis = data[:, 0, :, 0]

        # Create empty calibrated visibilities matrix
        vis_calib = np.ones((len(real_vis[:, 0]), len(real_vis[0, :])), dtype=np.complex)

        for basl in range(real_vis.shape[1]):
            coeffs_real = []
            coeffs_imag = []
            for j in range(self.coeffs.shape[0]):
                coeffs_real.append(self.coeffs[j, 0].real)
                coeffs_imag.append(self.coeffs[j, 0].imag)

            coeffs_real = np.array(coeffs_real)
            coeffs_imag = np.array(coeffs_imag)

            # Calibrate visibilities
            for t in range(real_vis.shape[0]):
                a1 = int(baseline_no[basl, 0])
                a2 = int(baseline_no[basl, 1])
                vis_calib[t, basl] = real_vis[t, basl] * ((np.complex(coeffs_real[a1], coeffs_imag[a1])) *
                                                          np.conj(np.complex(coeffs_real[a2], coeffs_imag[a2])))

        self.calib_filepath = str(main_dir) + '/calibTau2002' + str(i) + '.h5'
        #print(self.calib_filepath)

        # Write calibrated visibilities to h5 file
        f2 = h5py.File(self.calib_filepath, 'w')
        name = 'Vis'
        dset = f2.create_dataset(name, (len(real_vis[:, 0]), 1, len(real_vis[0, :]), 1), dtype='c16')
        vis_calib = np.reshape(vis_calib, [len(real_vis[:, 0]), 1, len(real_vis[0, :]), 1])
        dset[:, :, :, :] = vis_calib[:, :, :, :]
        f2.flush()
        f2.close()

        if model_run is True:

            # Save calibrated visibilities to ms file
            old_ms = str(ms_dir) + '/' + str(ms_name) + '.ms'
            new_ms = str(ms_dir) + '/' + str(ms_name) + '_cal.ms'
            uncal_ms = str(ms_dir) + '/' + str(ms_name) + '_uncal.ms'

            tb = tables.table(old_ms, ack=False)
            tb.copy(new_ms, deep=True)
            tb.copy(uncal_ms, deep=True)
            tb.close()

            tb = tables.table(new_ms, readonly=False)

            vis_calib_cut = np.array(vis_calib[0, 0, :, 0])
            vis_calib_cut2 = np.reshape(vis_calib_cut, (len(vis_calib_cut), 1, 1))
            tables.table.putcol(tb, "DATA", vis_calib_cut2)
            tb.flush()
            tb.close()

            tb = tables.table(uncal_ms, readonly=False)

            vis_uncal_cut = np.array(real_vis[1000, :])
            vis_uncal_cut2 = np.reshape(vis_uncal_cut, (len(vis_uncal_cut), 1, 1))
            tables.table.putcol(tb, "DATA", vis_uncal_cut2)
            tb.flush()
            tb.close()

    def fringe_imager(self, no_of_antennas, original_vis, calib_check_path):

        fringes = fi.FringeImager(original_vis, self.calib_filepath, no_of_antennas, calib_check_path)
        self._logger.info('Plotting fringes before and after calibration')
        fringes.plotter()


class CoeffManagerRun(AbstractProcessor):

    def __init__(self):
        super(CoeffManagerRun, self).__init__()
        self.coeffs_manager = CoeffManager()
        self.__coeffs_filepath = None
        self.__baseline_no = None
        self.__transit_run = False
        self.__obs_file = None
        self.__transit_file = None
        self.__test_run_check = False
        self.__calib_check_path = None
        self.__no_of_antennas = None
        self.__uncalib_vis = None
        self.__model_generation = False

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Contains filepath to which coefficients will be saved (filepath must point to a text file)
        and an indicator for whether a test run is being effected (True) or otherwise (False). If this is a test run,
        the file path to which the calibrated visibilities will be saved needs to be provided, together with an array of
        the uncalibrated visibilities and of the baseline number.

        """
        self.__coeffs_filepath = filter_model['coeffs_filepath']
        self.__model_generation = filter_model['model_generation']
        self.__test_run_check = filter_model['test_run']
        self.__transit_run = filter_model['transit_run']
        self.__obs_file = filter_model['obs_file']
        self.__transit_file = filter_model['transit_file']
        self.__main_dir = filter_model['main_dir']
        self.__no_of_antennas = filter_model['no_of_antennas']
        self.__calib_check_path = filter_model['calib_check_path']
        self.__ms_dir = filter_model['model_vis_directory']
        if self.__test_run_check is False\
                :
            self.__baseline_no = filter_model['baseline_no']

    def _will_start(self):
        """
        Start Method

        (Empty)
        """
        pass

    def _new_sample(self, message, forward):
        """
        New Sample Method

        Receives message from any calibrator method module containing the coefficients as message default. The coeffs
        are themselves read via the manager reader function, and then subsequently saved to file. In addition, the
        name of the file containing the uncalibrated visibilities for which the coefficients have been derived is also
        passed as part of the message.
        real_vis
        """

        self.coeffs_manager.read_coeffs(message.Default[0])
        self.coeffs_manager.save_coeffs(self.__coeffs_filepath)

        if self.__test_run_check is False:
            if self.__transit_run is False:
                self.coeffs_manager.coeffs_apply(message.Default[1], self.__baseline_no, self.__main_dir,
                                                 message.Default[2], message.Default[3], self.__ms_dir,
                                                 self.__model_generation)

            if self.__transit_run is True:
                self.coeffs_manager.coeffs_apply_transit(self.__obs_file, self.__baseline_no, self.__main_dir,
                                                         message.Default[2], message.Default[3], self.__ms_dir,
                                                         self.__model_generation)

                self.coeffs_manager.fringe_imager(self.__no_of_antennas, self.__transit_file, self.__calib_check_path)

        forward[0] = None

    def _clean_up(self, reason):
        """
        Clean-Up Method

        (Empty)
        """
        pass
