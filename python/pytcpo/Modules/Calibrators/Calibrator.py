import numpy as np
import logging

from pytcpo.Core.PipeLine import AbstractProcessor


class Calibrator:

    """
    Calibrator main module for calibration pipeline. Assorts incoming model and real visibilities, passes them to
    required calibration routine(s). This will allow further calibration schemes to be added further on.
    """

    def __init__(self):

        self.model_vis = None
        self.real_vis = None
        self.selfcal_coeffs = None
        self.coeffs_out = None
        self.model_vis_matrix = None
        self.real_vis_matrix = None
        self.vis_calib = None
        self.baseline_no = None
        self.vis_peak = None
        self._logger = logging.getLogger(__name__)

    def reader(self, model_vis, real_vis):

        self.model_vis = np.array(model_vis)
        self.real_vis = np.array(real_vis)

    def detriangulizer_model(self, model_vis, antenna1, antenna2, transit):

        """
        Used to convert visibilities from a 0.5(n^2-n) list format to a full n^2 matrix. Required mostly for StEFCal,
        which needs both model and observed visibility matrices to contain the full correlation matrix, including
        autocorrelations and repeated visibilities.

        Requirements: Input visibilities list in np.array format, list of corresponding antennas for each visibility
        value, separated into two lists (antenna1 and antenna2).

        :param model_vis: Input model visibilities list in numpy array format.
        :param antenna1: First members of paired lists of corresponding antennas for each visibility value
        :param antenna2: Second members of paired lists of corresponding antennas for each visibility value
        :param transit: Whether observation is of transit type or otherwise (Bool)

        """

        self.baseline_no = len(antenna1)
        antennas = np.max(antenna2) + 1

        if transit is False:
            self.model_vis_matrix = np.ones((antennas, antennas, model_vis.shape[1]), dtype=np.complex)

            for pol in range(model_vis.shape[1]):
                cr = 0
                for i in range(antennas):
                    for j in range(antennas):
                        if i <= j:
                            self.model_vis_matrix[antenna1[cr], antenna2[cr], pol] = model_vis[cr, pol]
                            self.model_vis_matrix[antenna2[cr], antenna1[cr], pol] = np.conj(model_vis[cr, pol])
                            cr += 1

        if transit is True:
            self.model_vis_matrix = np.ones((antennas, antennas, 1), dtype=np.complex)

    def detriangulizer_real(self, real_vis, antenna1, antenna2, transit):

        """
        Used to convert visibilities from a 0.5(n^2-n) list format to a full n^2 matrix. Required mostly for StEFCal,
        which needs both model and observed visibility matrices to contain the full correlation matrix, including
        autocorrelations and repeated visibilities.

        Requirements: Input visibilities list in np.array format, list of corresponding antennas for each visibility
        value, separated into two lists (antenna1 and antenna2).

        :param real_vis: Input real visibilities list in numpy array format.
        :param antenna1: First members of paired lists of corresponding antennas for each visibility value
        :param antenna2: Second members of paired lists of corresponding antennas for each visibility value

        """

        antennas = np.max(antenna2) + 1

        if transit is False:
            self.real_vis_matrix = np.ones((antennas, antennas, real_vis.shape[1]), dtype=np.complex)
            for pol in range(real_vis.shape[1]):
                cr = 0
                for i in range(antennas):
                    for j in range(antennas):
                        if i < j:
                            self.real_vis_matrix[antenna1[cr], antenna2[cr], pol] = real_vis[cr, pol]
                            self.real_vis_matrix[antenna2[cr], antenna1[cr], pol] = np.conj(real_vis[cr, pol])
                            cr += 1

        if transit is True:
            self.real_vis_matrix = np.ones((antennas, antennas, 1), dtype=np.complex)
            cr = 0
            for i in range(antennas):
                for j in range(antennas):
                    if i < j:
                        self.real_vis_matrix[antenna1[cr], antenna2[cr], 0] = real_vis[self.vis_peak, cr]
                        self.real_vis_matrix[antenna2[cr], antenna1[cr], 0] = np.conj(real_vis[self.vis_peak, cr])
                        cr += 1

    def transit_peak_find(self, observation_in):

        """
        Only to be used when the peak visibilities in a transit observation needs to be found, prior to carrying out
        SelfCal on the peak visibilities only. Returns a vector of peak visibilities that can be fed to selfcal_run.
        This is not envisaged to be required for AAVS, except for testing purposes.

        :param observation_in: A transit observation, in the format [timesteps, channels, baselines, pols],
        with only 1 channel and 1 polarization fed at a time

        """

        # Define data
        obs_in = np.array(observation_in)
        data = obs_in[:, :]
        # Define arrays to be filled
        all_peak = np.zeros(data.shape[1])

        # Start loop across baselines
        for i in range(data.shape[1]):
            # Initialize list for taking in final data points with removed outliers
            final_list = list()
            # Find power
            data1 = np.sqrt((np.abs(data[:, i].real) ** 2) + (np.abs(data[:, i].imag) ** 2))
            # Remove zero power padding regions
            data1 = np.trim_zeros(data1)
            # Find mean power
            mn = np.mean(data1, axis=0)
            # Find power std dev
            sd = np.std(data1, axis=0)
            # Start loop per baseline across time
            for j in range(data1.shape[0]):
                # Select data which falls within std dev away from mean
                if (data1[j] > mn - 2 * sd) and (data1[j] < mn + 2 * sd):
                    final_list.append(data1[j])
                # Data not within std dev range is an outlier, change with mean
                if data1[j] > mn + 2 * sd:
                    final_list.append(mn)
                if data1[j] < mn - 2 * sd:
                    final_list.append(mn)
            # Make list of final power values an array
            final_list = np.array(final_list)
            # Find time index of max power for this baseline
            all_peak[i] = np.int(np.where(final_list == np.max(final_list))[0][0])
        # Take mean peak
        self.vis_peak = np.int(np.mean(all_peak))

        self._logger.info('Transit observation peak observed at t={}'.format(self.vis_peak))


class CalibratorRun(AbstractProcessor):
    def __init__(self):
        super(CalibratorRun, self).__init__()
        self.calibrator = Calibrator()
        self._model_vis = None
        self._uncal_vis = None
        self.__coeffs = None
        self._selfcal = True
        self._stefcal = False
        self._fescal = False
        self._transit_run = False
        self._settings = None
        self._antennas_list1 = None
        self._antennas_list2 = None

    def _setup(self, filter_model):
        """
        Setup Method

        """

        self._antennas_list1 = filter_model["baseline_no"][:, 0]
        self._antennas_list2 = filter_model["baseline_no"][:, 1]

        if filter_model["selfcal"] is False:
            self._selfcal = False

        if filter_model["stefcal"] is True:
            self._stefcal = True

        if filter_model["fescal"] is True:
            self._fescal = True

        if filter_model["transit_run"] is True:
            self._transit_run = True

    def _will_start(self):
        """
        Start Method

        (Empty)
        """
        pass

    def _new_sample(self, message, forward):

        if self._selfcal is True:

            forward[0] = [message.Default[1], message.Default[2], message.Default[3]]

        if self._fescal is True:

            if self._transit_run is True:
                self.calibrator.transit_peak_find(message.Default[1])
                vis_out = np.array(message.Default[1][self.calibrator.vis_peak, :])
                vis_out = np.expand_dims(vis_out, axis=1)

            self.calibrator.detriangulizer_model(message.Default[0], self._antennas_list1, self._antennas_list2,
                                                 self._transit_run)
            self.__model_vis = self.calibrator.model_vis_matrix

            self.calibrator.detriangulizer_real(message.Default[1], self._antennas_list1, self._antennas_list2,
                                                self._transit_run)

            self.__uncal_vis = self.calibrator.real_vis_matrix

            forward[0] = [vis_out, self.__model_vis, self.__uncal_vis, message.Default[2],
                          message.Default[3], message.Default[1]]

        if self._stefcal is True:

            if self._transit_run is True:
                self.calibrator.transit_peak_find(message.Default[1])

            self.calibrator.detriangulizer_model(message.Default[0], self._antennas_list1, self._antennas_list2,
                                                 self._transit_run)
            self.__model_vis = self.calibrator.model_vis_matrix

            self.calibrator.detriangulizer_real(message.Default[1], self._antennas_list1, self._antennas_list2,
                                                self._transit_run)

            self.__uncal_vis = self.calibrator.real_vis_matrix

            forward[0] = [message.Default[1], self.__model_vis, self.__uncal_vis, message.Default[2],
                          message.Default[3]]

    def _clean_up(self, reason):
        """
        Clean Function

        (Empty)
        """
        pass
