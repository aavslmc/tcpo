import datetime
import warnings
import numpy as np

from astropy import constants
from astropy import units as u
from astropy.coordinates import Angle, AltAz, SkyCoord, EarthLocation
from astropy.time import TimeDelta
from astropy.time.core import Time
from astropy.utils.exceptions import AstropyWarning

from pytcpo.Core.PipeLine import AbstractProcessor

warnings.simplefilter('ignore', category=AstropyWarning)

class Pointing(AbstractProcessor):

    """
    Note that error handling is to be implemented in the coming weeks when TM/PointingRequester are implemented.

    The following class implements the necessary pointing processes written by Alessio Magro in the modular format
    of the TCPO framework. The class is to be implemented within a pointing pipeline, with the parent module being
    PointingRequester - a generation module that passes onto this module what beam is to be processed and with what
    RA/Dec.

    The Pointing processing module is intended to process one beam at a time. It is planned that a beam selector is
    implemented later on to allow fanning out of the PointingRequester generation module, allowing for parallelisation.

    The module will later on be implemented such that it works hand in hand with the Telescope Model implementation by
    Josef Borg.

    Required Forwarded Data :
    i) List of the form [RA, Dec]

    Outputted Data For Forwarding :
    i) 2D Numpy Array of pointing coefficients for each antenna
    """

    def __init__(self):
        super(Pointing, self).__init__()

        self.ref_lat = None
        self.ref_lon = None
        self.ref_height = None
        self.telescope_model = None
        self.delay = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 1 required parameters :
                            * TelescopeModel - instance of the TelescopeModel singleton

        :return: None
        """

        self.telescope_model = filter_model['TelescopeModel']
        self.ref_lat = self.telescope_model.cen_lat
        self.ref_lon = self.telescope_model.cen_lon
        self.ref_height = self.telescope_model.cen_alt

    def _will_start(self):
        """
        Empty
        :return: True
        """
        return True

    def _new_sample(self, message, forward):

        """
        Handles sample processing.
        """
        self.sample = message.Default
        # The responsibility of correct beam selection falls on the preceding module
        self.beam_vectors = self.telescope_model.beams[message.Selector][:, 2:5]
        self.antenna_indices = self.telescope_model.beams[message.Selector][:, 0:2]

        beam_coefficients = self.point_array(self.sample[0], self.sample[1], message.Time) # forwarding to next module
        forward[0] = np.concatenate((self.antenna_indices, beam_coefficients), axis=1)
        forward[1] = forward[0] # forwarding to archiver

    def _clean_up(self, reason):

        """
        Empty
        """
        pass

    # --------------------- Pointing Functions Based On Alessio Magro's Pointing Implementation ---------------------

    def point_array_static(self, altitude, azimuth):
        """ Calculate the delay given the altitude and azimuth coordinates of a sky object as astropy angles
        :param altitude: altitude coordinates of a sky object as astropy angle
        :param azimuth: azimuth coordinates of a sky object as astropy angles
        :return: The (delay,delay rate) tuple for each antenna
        """

        # Type conversions if required
        if type(altitude) is not Angle:
            if type(altitude) is str:
                altitude = Angle(altitude)
            else:
                altitude = Angle(altitude, u.deg)

        if type(azimuth) is not Angle:
            if type(azimuth) is str:
                azimuth = Angle(azimuth)
            else:
                azimuth = Angle(azimuth, u.deg)

        # Compute the delays
        delay = self._delays_from_altitude_azimuth(altitude.rad, azimuth.rad, self.beam_vectors)
        return delay

    def point_array(self, right_ascension, declination, pointing_time=None):
        """ Calculate the phase shift between two antennas which is given by the phase constant (2 * pi / wavelength)
        multiplied by the projection of the baseline vector onto the plane wave arrival vector
        :param right_ascension: Right ascension of source (astropy angle, or string that can be converted to angle)
        :param declination: Declination of source (astropy angle, or string that can be converted to angle)
        :param pointing_time: Time of observation (in format astropy time)
        :return: The (delay,delay rate) tuple for each antenna
        """

        if pointing_time is None:
            pointing_time = Time(datetime.datetime.utcnow(), scale='utc')

        pointing_time1 = pointing_time + TimeDelta(1.0, format='sec')

        reference_antenna_loc = EarthLocation.from_geodetic(self.ref_lon, self.ref_lat,
                                                            height=self.ref_height,
                                                            ellipsoid='WGS84')
        alt, az = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination),
                                         Time(pointing_time), reference_antenna_loc)
        alt1, az1 = self._ra_dec_to_alt_az(Angle(right_ascension), Angle(declination),
                                           Time(pointing_time1), reference_antenna_loc)

        delay_0 = self.point_array_static(alt, az)
        delay_1 = self.point_array_static(alt1, az1) - delay_0

        delay = np.array([delay_0, delay_1])
        self.delay = delay.transpose()

        # load the delays in beam 0
        # self.set_delay(self.delaytranspose, 0);
        return self.delay

    @staticmethod
    def _ra_dec_to_alt_az(right_ascension, declination, time, location):
        """ Calculate the altitude and azimuth coordinates of a sky object from right ascension and declination and time
        :param right_ascension: Right ascension of source (in astropy Angle on string which can be converted to Angle)
        :param declination: Declination of source (in astropy Angle on string which can be converted to Angle)
        :param time: Time of observation (as astropy Time")
        :param location: astropy EarthLocation
        :return: Array containing altitude and azimuth of source as astropy angle
        """

        # Initialise SkyCoord object using the default frame (ICRS) and convert to horizontal
        # coordinates (altitude/azimuth) from the antenna's perspective.
        sky_coordinates = SkyCoord(ra=right_ascension, dec=declination)
        c_altaz = sky_coordinates.transform_to(AltAz(obstime=time, location=location))

        return c_altaz.alt, c_altaz.az

    @staticmethod
    def _delays_from_altitude_azimuth(altitude, azimuth, displacements):
        """
        Calculate the delay using a target altitude Azimuth
        :param altitude: The altitude of the target astropy angle
        :param azimuth: The azimuth of the target astropy angle
        :param displacements: Numpy array: The displacement vectors between the antennae in East, North, Up
        :return: The delay in seconds for each antenna
        """
        scale = np.array([-np.sin(azimuth) * np.cos(altitude),
                          -np.cos(azimuth) * np.cos(altitude),
                          -np.sin(altitude)])

        path_length = np.dot(scale, displacements.transpose())

        k = 1.0 / constants.c.value
        delay = np.multiply(k, path_length)
        return delay