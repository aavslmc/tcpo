import numpy as np
import numpy.polynomial.polynomial as nppol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import ConnectorFiller, TCPOCode, npalt, tupler
from pytcpo.Core.Common.DataFilters import MorphologicalThinner

from pytcpo.Modules.AnomalyDetectors.Common import FlaggedAnomaly

class PolynomialDeviations(AbstractProcessor):
    """
    Polynomial Outlier Detector Implementation
    Detects outlying channels based on the relative number of standard deviations (in magnitude) from the model fit.

    If any channel is NaN in either the original bandpass or the evaluated one than it is flagged as an Outlier.

    Requires:
      * The Raw Channel Bandpass Data as 3D Numpy Array (real-valued, order A/P/Chan) [Default]
      * The Polynomial Fitted Data as 3D Numpy Array (real-valued, order A/P/Chan)
    Outputs:
      * 3D Boolean Numpy Matrix indicating outlier (or not) per channel (same shape as raw data)
    Archive:
      * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations have outlying channels, with the channels
        being listed (by index) in the _anomaly field.

    Limitations:
        a) Used alone this will generally always detect the same number of outliers, irrespective of their absolute
            magnitude, since the standard deviation is actually computed on the sample itself (deviation being the rms
            error from the model fit). This is alleviated through the cutoff parameter, below which the outliers are
            never signalled.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PolynomialDeviations, self).__init__()

        # Setup Parameters
        self.__dev_thresh = None    # The Number of Standard Deviations above which an outlier is signalled
        self.__min_cutoff = None    # The minimum cutoff (absolute) below which outliers are never signalled
        self.__antennas = None      # The Antenna list used
        self.__polaris = None       # The list of Polarisations used
        self.__channels = None      # The channel range of operation (numeric indices)
        self.__model = None         # The module reference for the polynomial-generated data.

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: must be a dictionary with 6 (5 compulsary, 1 optional) keys-value pairs:
                * antennas - [Required] The ordered list of antennas being considered (with appropriate indices)
                * polarisations - [Required] The ordered list of polarisations per antenna.
                * channels - [Required] The ordered list of channels being considered per antenna/polarisation.
                * outlier_std - [Required] The number of standard deviations (float) beyond which to detect outliers
                * model - [Required] Since this module requires an aggregated message, this informs the key for the
                            polynomial fit (coefficients) message.
                * outlier_cut - [Optional] A minimum value below which outliers are not flagged. Helps to reduce false
                                positives.
        :return: Status.SETUP (on success)
        :raises TypeError, KeyError: If filter_model is not a dictionary, or any of the parameters is missing.
        """
        # Option Parsing
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__min_cutoff = float(filter_model.get('outlier_cut', 0.0))
        self.__dev_thresh = float(filter_model['outlier_std'])
        self.__channels = np.asarray(tupler(filter_model['channels'])) # Ordered list of channels # bug fix - original : tupler(filter_model['channels'])
        self.__model = filter_model['model'] # Model Name (due to aggregation)

        # Return
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        This is a stateless module
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameter list as described
        """

        # Compute Deviation and consequently rms of deviation for each antenna/polarisation
        deviation = np.abs(message.Default - message[self.__model]) # NaN's (in original or computed) will carry over
        rms_dev = npalt.rms(deviation, axis=2, ignore_nan=True, keepdims=True)  # nan's will only appear if all channels

        # Compute Numerical-based anomaly (if deviation is greater than x std_devs and also greater than cutoff)
        num_anom = np.logical_and(deviation > self.__dev_thresh * rms_dev, deviation > self.__min_cutoff)

        # Now logical or with presence of NaN's
        forward[0] = np.logical_or(num_anom, np.logical_or(np.isnan(message.Default), np.isnan(message[self.__model])))

        # Now if Archiving:
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a,p]):
                        chan_list = np.asarray(self.__channels[np.squeeze(np.where(forward[0][a,p]))])
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class DerivativeSignsDetector(AbstractProcessor):
    """
    RFI Spike Detector

    Outlier detector based on 1st derivative. An RFI spike is detected by comparing the signs of the gradients on both
    sides of a given point (a point on the plot corresponds directly to a channel). If the derivative signs differ, then
    the channel is flagged as an outlier. More over if a channel is an NaN, then it is also flagged as an outlier.

    Requires:
      * The Raw Channel (BandPass) Data as a 3D Numpy Array (order A/P/Chan) [Default]
    Outputs:
      * 3D Boolean Numpy Matrix indicating outlier (or not) per channel (same shape as raw data)
    Archive:
      * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations have outlying channels, with the channels
        being listed (by index) in the _anomaly field.

    Limitations:
        a) Requires at least 3 channels per antenna/pol.
        b) The 1st and last channels are not checked for by the current implementation.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(DerivativeSignsDetector, self).__init__()
        self.__min_cutoff = None
        self.__antennas = None
        self.__polaris = None
        self.__channels = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary  with 4 (Compulsary) options:
                * antennas - [Required] The ordered list of antennas being considered (with appropriate indices)
                * polarisations - [Required] The ordered list of polarisations per antenna.
                * channels - [Required] The ordered list of channels being considered per antenna/polarisation.
                * min_cutoff - [Required] Minimum value below which first derivative is zeroed (and ignored)
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or the min_cutoff option is not specified
        """
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__channels = np.asarray(tupler(filter_model['channels']))
        self.__min_cutoff = filter_model['min_cutoff']
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        Stateless
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler.
        Parameter List as described in Abstract Class
        """

        discrete_differences = np.diff(message.Default)
        discrete_differences[np.abs(discrete_differences) < self.__min_cutoff] = 0.0
        discrete_sign_changes = np.diff(np.sign(discrete_differences))
        discrete_sign_changes = np.pad(discrete_sign_changes, ((0, 0), (0, 0), (1, 1)), 'constant',
                                       constant_values=(0,))

        forward[0] = np.logical_or((discrete_sign_changes != 0.0), np.isnan(message.Default))

        # Compute Archiver...
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a, p]):
                        chan_list = np.asarray(self.__channels[np.squeeze(np.where(forward[0][a, p]))])
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass


class DerivativeSignsWithPeakRejection(AbstractProcessor):
    """
    Derivative-Based Outlier Detector

    This module is an extension of the DerivativeSignsDetector, employing the same 1st derivative signs technique.
    However the raw bandpass data is cleaned before hand using polynomial-based channel and outlier data. For polynomial
    specified outliers, the raw bandpass data is replaced with the corresponding polynomial channel data - effectively
    cleaning the signal. 

    Requires:
        * The Raw BandPass Channel Data as 3D real Numpy Array (order A/P/Chan) [Default]
        * The Polynomial-based Channel Data as a 3D real-valued Numpy Array (order A/P/Chan)
        * The Polynomial-based Outlier Data as a 3D bool Numpy Array (order A/P/Chan)
    Outputs:
        * Binary flag indicating the presence of outlying channels (same shape as input)
    Archives:
        * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations have outlying channels, with the
            channels being listed (by index) in the _anomaly field.

    Limitations:
        a) At least 3 channels per pol/ant are required for correct operation.
        b) The 1st and last channels are not checked for by the current implementation.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(DerivativeSignsWithPeakRejection, self).__init__()
        self.__min_cutoff = None # Minimum cutoff, below which the 1st derivative value is zeroed
        self.__peak_outly = None # Polynomial-based outlier rejection data (reference)
        self.__model = None      # Polynomial coefficients data (reference)
        self.__antennas = None   # Antenna List (for correct referencing)
        self.__polaris = None    # Polarisation Indices (for correct referencing)
        self.__channels = None   # List of channels upon which the module operates

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 6 (Required) Parameters
                * antennas - [Required] The ordered list of antennas being considered (with appropriate indices)
                * polarisations - [Required] The ordered list of polarisations per antenna.
                * channels - [Required]: The channels over which the pipeline is operating.
                * min_cutoff - [Required]: Minimum value below which second derivative is zeroed (and ignored)
                * model - [Required]: The name (reference) of the Polynomial-generated Data
                * poly_outly - [Required]: The name (reference) of the Polynomial-based Outlier Component in the message
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified
        """
        # Option Parsing
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__channels = np.asarray(tupler(filter_model['channels']))
        self.__min_cutoff = filter_model['min_cutoff']
        self.__peak_outly = filter_model['poly_outly']
        self.__model = filter_model['model']

        # Return
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        Module is stateless
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler.
        Parameter List as described in Abstract Class
        """
        # Get References for readibility
        _computed = message[self.__model]
        _outliers = message[self.__peak_outly]

        # First replace identified outliers in data with computed polynomial
        _cleaned = np.copy(message.Default)         # Copy Message Default data
        _cleaned[_outliers] = _computed[_outliers]  # Replace as necessary - may still contain NaN's

        discrete_differences = np.diff(_cleaned)
        discrete_differences[np.abs(discrete_differences) < self.__min_cutoff] = 0.0
        discrete_sign_changes = np.diff(np.sign(discrete_differences))
        discrete_sign_changes = np.pad(discrete_sign_changes, ((0,0),(0,0),(1,1)), 'constant', constant_values=(0,))

        forward[0] = np.logical_or((discrete_sign_changes != 0.0), np.isnan(_cleaned))

        # Compute Archiver...
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a, p]):
                        chan_list = np.asarray(self.__channels[np.squeeze(np.where(forward[0][a, p]))])
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class DerivativeSignsWithDifferenceRejection(AbstractProcessor):

    """
        Derivative-Based Outlier Detector

        This module is an extension of the DerivativeSignsDetector, employing the same 1st derivative signs technique.
        However a region ('envelope') is defined by the envelope parameter about the polynomial fit, such that is the
        raw data lies within this region the correponding channel cannot be flagged as an outlier.

        Requires:
            * The Raw BandPass Channel Data as 3D real Numpy Array (order A/P/Chan) [Default]
            * The Polynomial-based Channel Data as a 3D real-valued Numpy Array (order A/P/Chan)
        Outputs:
            * Binary flag indicating the presence of outlying channels (same shape as input)
        Archives:
            * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations have outlying channels, with the
                channels being listed (by index) in the _anomaly field.

        Limitations:
            a) At least 3 channels per pol/ant are required for correct operation.
            b) The 1st and last channels are not checked for by the current implementation.
        """

    def __init__(self):
        """
        Default Initialiser
        """
        super(DerivativeSignsWithDifferenceRejection, self).__init__()
        self.__min_cutoff = None  # Minimum cutoff, below which 1st derivative value is zeroed
        self.__envelope = None # Defines the region about polynomial fit
        self.__model = None  # Polynomial coefficients data (reference)
        self.__antennas = None  # Antenna List (for correct referencing)
        self.__polaris = None  # Polarisation Indices (for correct referencing)
        self.__channels = None  # List of channels upon which the module operates

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 6 (Required) Parameters
                * antennas - [Required] The ordered list of antennas being considered (with appropriate indices)
                * polarisations - [Required] The ordered list of polarisations per antenna.
                * channels - [Required]: The channels over which the pipeline is operating.
                * min_cutoff - [Required]: Minimum value below which second derivative is zeroed (and ignored)
                * envelope - [Required]: Minimum absolute value for which not to flag, defining an envelope about the
                                         polynomial fit.
                * model - [Required]: The name (reference) of the Polynomial-generated Data.
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified
        """
        # Option Parsing
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__channels = np.asarray(tupler(filter_model['channels']))
        self.__min_cutoff = filter_model['min_cutoff']
        self.__envelope = filter_model['envelope']
        self.__model = filter_model['model']

        # Return
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        Module is stateless
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler.
        Parameter List as described in Abstract Class
        """

        poly_data = message[self.__model]

        discrete_differences = np.diff(message.Default)
        discrete_differences[np.abs(discrete_differences) < self.__min_cutoff] = 0.0
        discrete_sign_changes = np.diff(np.sign(discrete_differences))
        discrete_sign_changes = np.pad(discrete_sign_changes, ((0, 0), (0, 0), (1, 1)), 'constant',
                                       constant_values=(0,))

        poly_differences = np.abs(np.subtract(poly_data, message.Default))

        forward[0] = np.logical_or(np.logical_and((poly_differences > self.__envelope), (discrete_sign_changes != 0.0)),
                                    np.isnan(message.Default))

        # Compute Archiver...
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a, p]):
                        chan_list = self.__channels[np.squeeze(np.where(forward[0][a, p]))]
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass


class OutlierAggregator(AbstractProcessor):
    """
    Outlier Consolidator

    This is not strictly speaking an Outlier Detector, but rather encapsulates a morphological filter for joining
    together discontinuous/spurious RFI spikes as one region. It makes use of the ConnecterFiller implementation in
    pytcpo.Core.Common.DataFilters. It also doubles as an aggregator of the output of different outlier detectors into
    one module.

    If the window length is set to 0 then the ConnectorFiller is not used and the module only serves as an aggregator.

    Since this module deals with the outputs of previous outlier detectors, it need not explicitly handle NaN's.

    Requires:
        * Outlier-Detector result matrix (3D Boolean Numpy Array, order A/P/Out) [Default]
        * Other Outlier-Detector results (same as above) [Optional]
    Outputs:
        * Boolean indication of Outliers
    Archives:
        * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations have outlying channels, with the
            channels being listed (by index) in the _anomaly field.
    """
    def __init__(self):
        """
        Default Initialiser
        """
        super(OutlierAggregator, self).__init__()

        # Setup Parameters
        self.__antennas = None
        self.__polaris = None
        self.__channels = None
        self.__filler = None
        self.__morphological = None
        self.__others = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 5 (4 Required, 1 Optional) Parameters
                * antennas - [Required]: The (absolute) indices for the selected Antennas
                * polarisations - [Required]: The (absolute) indices for the selected Polarisations
                * channels - [Required]: The (absolute) indices for the operating Channels per Antenna.
                * length - [Required]: Length of window over which to join flagged values.
                * others - [Optional]: Names (references) of other (apart from default) outlier detectors to consider.
                        Any outlier-data must be specified in order for the module to consider it.
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or the 'length' option is not specified.
        """
        # Parse Options
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__channels = np.asarray(tupler(filter_model['channels'])) # bug fix - original : tupler(filter_model['channels'])

        if filter_model['length'] != 0 :
            self.__morphological = True
            self.__filler = ConnectorFiller(filter_model['length'])
        else :
            self.__morphological = False

        self.__others = filter_model.get('others', []) # Optional

        # Return
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler.
        Parameter List as described in Abstract Class
        """
        # Get the First one
        forward[0] = np.copy(message.Default)

        # Now, if any more, iterate over them..
        for item in self.__others: forward[0] = np.logical_or(forward[0], message[item])

        if self.__morphological :
            # Now perform the actual cumulative filler
            forward[0] = self.__filler.compute(forward[0])

        # Finally perform the Archiving if necessary
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a, p]):
                        chan_list = self.__channels[np.squeeze(np.where(forward[0][a, p]))]
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class OutlierThinner(AbstractProcessor):

    """
    OutlierThinner is an abstract processor module that serves as a wrapper for the MorphologicalThinner implementation.
    The module simply un-flags the end channels of a sequence of contiguously flagged channels (of length > 2).
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(OutlierThinner, self).__init__()

        # Setup Parameters
        self.__antennas = None
        self.__polaris = None
        self.__channels = None
        self.__thinner = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 3 required parameters
                * antennas - [Required]: The (absolute) indices for the selected Antennas
                * polarisations - [Required]: The (absolute) indices for the selected Polarisations
                * channels - [Required]: The (absolute) indices for the operating Channels per Antenna.

        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary
        """
        # Parse Options
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__channels = np.asarray(tupler(filter_model['channels']))
        self.__thinner = MorphologicalThinner()

        # Return
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty
        :return: None
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler.
        Parameter List as described in Abstract Class
        """
        # Get the First one
        sample = message.Default

        # Now perform the actual cumulative filler
        forward[0] = self.__thinner.compute(sample)

        # Finally perform the Archiving if necessary
        if self.ArchiveEnabled:
            anom_list = []
            for a in range(message.Default.shape[0]):
                for p in range(message.Default.shape[1]):
                    if np.any(forward[0][a, p]):
                        chan_list = np.asarray(self.__channels[np.squeeze(np.where(forward[0][a, p]))])
                        anom_list.append(FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], chan_list))

            forward[1] = np.array(anom_list) if (len(anom_list) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Empty
        """
        pass