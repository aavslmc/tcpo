import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import PolynomialCompare, TCPOCode, tupler
from pytcpo.Modules.AnomalyDetectors import FlaggedAnomaly

class InterAntennaDeviationDetector(AbstractProcessor):
    """
    The class implements a state-less (no history) Anomaly Detector based on the deviation per piecewise fit from the
    average model coefficients per piecewise fit of all antennas/polarisations. The module handles invalid model
    coefficients (nan's) by:
        a) Nan-values do not factor into the mean calculation - this means however, that if only some of the model
           entries are nan, the other coefficients will contribute: however this is an unlikely scenario.
        b) Any model with at least one fit containing a coefficient np.nan is flagged as an anomaly (even if there are
           no other antennas to compare with).

    Requires:
        * FitStorage objects as 3D Numpy Array (order A/P/FitStorage), with Coeffs within FitStorage objects [Default]
    Outputs:
        * 2D Numpy Array of type bool flagging anomalous antennas
    Archives:
        * A list of FlaggedAnomaly objects indicating anomalous antennas/polarisations, with the anomaly field being the
          deviation from the mean.

    Limitations:
     a) Because the deviation is computed from the global average, it suffers from systematic shifts in the mean, and
        may cause false positives if a large percentage of the antennas are anomolous.
     b) The polynomial-coefficient norm computation is not very intuitive, especially in how to weight the different
        coefficients together.
     c) np.nanmean ignores nan-values and calculates the mean on the no. of non nan entries.
        For example, np.nanmean([1,2,np.NaN]) returns 1.5 while np.nanmean([1,2,0]) returns 1.0.
        This in turn affects the calculation of the average and thus that of the deviation, and hence
        must be taken into account.
     d) The Current implementation compares both between antennas and between polarisations - this may or may not be
        ideal.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(InterAntennaDeviationDetector, self).__init__()
        # Setup Parameters
        self.__weight = None     # Weighting for calculation of RMS: can be scalar or vector of same size as ideal
        self.__max = None        # Maximum Allowed Absolute Deviation
        self.__antennas = None   # The selected antennas, so they can be mapped back for the purpose of indexing
        self.__polaris = None    # The selected polarisations, so they can be mapped back for indexing
        self.__fits = None       # Orders of each polynomial fit considered

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python dict with 5 (Compulsory) parameters:
                * antennas - [Required]: The indices of selected antennas in the tile
                * polarisations - [Required]: The indices of selected polarisations per antenna
                * fit_orders - [Required]: Numpy array of the orders for each piecewise polynomial fit considered.
                                           Orders must be supplied in the same order that the corresponding FitStorage
                                           objects are supplied
                * deviation - [Required]: Numpy array of the maximum allowed (absolute) deviation in terms of the
                                          weighted norm per piecewise fit from the average for that fit across all
                                          antennas/polarisations
                * weight - [Required]: Numpy array of vectors (one for each fit) of the same size as the corresponding
                                       fit coefficients which serves to weight the individual components. Vectors must
                                       be supplied in the same order that the corresponding FitStorage objects are
                                       supplied
        :return: Status.SETUP if successful.
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any required option is not specified
        """
        # Setup Parameters
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__fits = filter_model['fit_orders']
        self.__max = filter_model['deviation']
        self.__weight = filter_model['weight']

        # Return ok
        return TCPOCode.Status.OK

    def _will_start(self):
        """
        Empty
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameter List as described in Abstract Class
        """

        forward[0] = np.zeros((len(self.__antennas), len(self.__polaris)), dtype=bool)
        rms_dev = np.zeros((len(self.__fits),len(self.__antennas), len(self.__polaris)), dtype=np.float64)

        for f, order in enumerate(self.__fits):
            coeffs_f = np.empty((len(self.__antennas), len(self.__polaris), order+1))

            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    coeffs_f[a,p,:] = message.Default[a,p,2*f].ret_coeff()

            # Average out and find rms deviation: Note that in the extreme case of 1 antenna/1 pol with nan values, it will
            #   give nan for that one antenna (which is desired).

            average = np.nanmean(coeffs_f, axis=(0,1))  # Find mean across antennas/pols, ignoring any nan
            deviate = np.subtract(coeffs_f, average)
            rms_dev[f,:,:] = np.sqrt(np.mean(np.square(np.multiply(deviate, self.__weight[f])), 2))

            forward[0] = np.logical_or(forward[0], np.logical_or(np.isnan(rms_dev[f,:,:]), rms_dev[f,:,:] > self.__max[f]))

        # Assign Anomalies List if archiving
        if self.ArchiveEnabled:
            forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    if forward[0][a, p]:
                        forward[1][nxt] = FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], rms_dev[:,a,p])
                        nxt += 1

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class InterAntennaOverlapDetector(AbstractProcessor):
    """
    This Class implements an Overlap-based Anomaly Detector, similar to the IndependentOverlapDetector. However, in this
    case, the anomaly detection is based on a piecewise fit comparison with all other antennas, and flagging those which
    are deemed dissimilar to more than a specified fraction of the antennas. The similarity is again based on a
    threshold value of the overlap between each pair of antennas (based on the sum of the overlap for each piecewise fit
    considered). In this case, each polarisation is treated separately i.e. comparisons happen for all antennas within
    each polarisation but there is no cross-comparison between polarisations.

    The comparator deals with nan's in the following way:
    a) Antennas which contain at least a single nan value are immediately marked as anomalous, even if there is only one
       antenna in the pipeline.
    b) The comparison ratio for the remaining antennas is adjusted as necessary to indicate that the nan'd antennas do
       not contribute (refer to the sample handler for repercussions of this).

    Requires:
      * FitStorage objects as 3D Numpy Array (order A/P/FitStorage), with Coeffs within FitStorage objects [Default]
    Outputs:
      * 2D Numpy Array of type bool flagging anomalous antennas
    Archives:
      * The List of FlaggedAnomaly Objects indicating anomalous antennas/polarisations, with the anomaly field being an
        array containing the similarity ratios to all other antennas.

    Limitations:
        a) The implementation suffers from false positives when many of the antennas are anomalous.
        b) Due to the overlap computation, the module is restricted to use polynomial modeling
    """
    def __init__(self):
        super(InterAntennaOverlapDetector, self).__init__()
        # Setup Parameters
        self.__antennas = None
        self.__polaris = None
        self.__cutoff = None
        self.__form = None
        self.__sim_ratio = None
        self.__fits = None

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python dict containing 6 (5 Required, 1 optional) parameters:
                * antennas - [Required]: The Indices of selected Antennas
                * polarisations - [Required]: The Indices of polarisations to compute for
                * fit_orders - [Required]: Numpy array of the orders for each piecewise polynomial fit considered.
                                           Orders must be supplied in the same order that the corresponding FitStorage
                                           objects are supplied
                * min_overlap - [Required]: The minimum overlap below which the pair of antennas is flagged as dissimilar
                * similarity - [Required]: The minimum number of similar antennas below which an anomaly is flagged.
                * form - [Optional]: If True (defaults to False) compares only the relative shape by zeroing out the
                                constant term.
        :return:
        """
        # Assign Parameters
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__cutoff = filter_model['min_overlap']
        self.__sim_ratio = filter_model['similarity']
        self.__form = filter_model.get('form', False)
        self.__fits = filter_model['fit_orders']

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Some things to note in the implementation:
            > If an Antenna/Pol is nan, it will have a similarity count of 0, and will contribute 1 less to the remaining
                counts.
            > If there is only 1 non-nan antenna, then the minimum similarity count threshold will be a fraction of or
                equal to 1, and hence for this antenna it will signal ok (since 1 is not less then the fraction or
                itself) but for the others, whose similarity counts will be 0, it will trigger an anomaly.
            > If all are antennas are, then the threshold will be 0, and this must be catered for explicitly to signal
                the anomaly correctly.

        Parameter List as described in Abstract Class
        """

        nan_counts = np.zeros((len(self.__polaris)))
        similarity_matrix = np.empty(shape=(len(self.__antennas), len(self.__polaris), len(self.__fits), len(self.__antennas)))

        for f, order in enumerate(self.__fits):
            coeffs_f = np.empty((len(self.__antennas), len(self.__polaris), order+1))

            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    coeffs_f[a,p,:] = message.Default[a,p,2*f].ret_coeff()

            # Find number of NAN antennas per Polarisation (flag antenna if at least one coeff is nan), and conversely, the
            #   similarity threshold (number of antennas) per polarisation, per fit.
            nan_counts = np.add(nan_counts, np.isnan(coeffs_f).any(axis=(2)).sum(axis=0))

            # Compute Similarity Ratios (per polarisation, per fit)
            for p in range(len(self.__polaris)):
                chans = message.Default[0,p,2*f].ret_chans()  # Channels for fits must be the same across all antennas
                comparator = PolynomialCompare((chans[0],chans[-1]),self.__form)
                similarity_matrix[:,p,f,:] = comparator.compare_self(coeffs_f[:,p,:])

        min_sim_count = self.__sim_ratio * (len(self.__antennas) - nan_counts)  # Vector of counts per antenna

        # Detect Anomalies:
        #   Logical Ops should broadcast successfully to polarisations since these are along 2nd dimension...
        similarity_threshold = np.greater(similarity_matrix, self.__cutoff) # Which Ants/Pols/Fits are similar enough [4D]
        similarity_antennas = np.all(similarity_threshold, axis=2)          # Which Ants/Pols are similar based on
                                                                            # logical AND between all fits [3D]
        similarity_counts = np.sum(similarity_antennas, axis=2)             # What is similarity count per Ant/Pol [2D]
        anomalous = np.less(similarity_counts, min_sim_count)               # Which Ants/Pols fail count threshold [2D]
        forward[0] = np.logical_or(anomalous, min_sim_count == 0)           # Consider if min_count is 0 (all anomalous)

        # Assign Anomalies List if archiving
        if self.ArchiveEnabled:
            forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    if forward[0][a,p]:
                        forward[1][nxt] = FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], similarity_matrix[a,p,:,:])
                        nxt += 1

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class AnomalyAggregator(AbstractProcessor):
    """
    This class serves as a logical or between different anomaly detector outputs so that anomalies can be stored as one
    sample set.

    Requires:
        * Anomaly-Detector result matrix (2D Boolean Numpy Array, order A/P) [Default]
        * Any additional Anomaly-Detector results (same as above) [Optional]
    Outputs:
        * Boolean indication of Anomalies (2D Numpy Array, order A/P)
    Archives:
        * List of FlaggedAnomaly Objects indicating which Antennas/Polarisations are anomalous. The _anomaly list is an
          empty array.
    """
    def __init__(self):
        """
        Default Initialiser
        """
        super(AnomalyAggregator, self).__init__()
        self.__antennas = None
        self.__polaris = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python dict containing 2 (Required) parameters:
                * antennas - [Required]: The Indices of selected Antennas
                * polarisations - [Required]: The Indices of polarisations to compute for
        :return: Success if all parameters passed correctly.
        """
        # Assign indices
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])

        return TCPOCode.Status.SETUP # Return Success

    def _will_start(self):
        """
        Start Method

        Empty (Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Usual Parameters
        """
        # Get the First one
        forward[0] = np.copy(message.Default)

        # Now, if any more, iterate over them..
        for item in message.All: forward[0] = np.logical_or(forward[0], item)

        # If need be set up archive
        if self.ArchiveEnabled:
            alist = []  # Create Empty List
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    if forward[0][a, p]:
                        alist.append(
                            FlaggedAnomaly().set(self.__antennas[a], self.__polaris[p], np.empty(0)))

            # Set Archiving
            forward[1] = np.array(alist) if (len(alist) > 0) else np.empty(0, dtype=np.object)

    def _clean_up(self, reason):
        """
        Cleanup Method

        Empty (Stateless Module)
        """
        pass