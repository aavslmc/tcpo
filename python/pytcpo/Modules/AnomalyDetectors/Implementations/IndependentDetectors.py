import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import MovingAverage, PolynomialCompare, TCPOCode, tupler
from pytcpo.Modules.AnomalyDetectors import FlaggedAnomaly

class PerAntennaDeviationDetector(AbstractProcessor):
    """
    Independent Antenna Deviation-based Anomaly Detector

    This Class implements a detector of anomalies based on deviation from the last moving average of the Model
    Parameters (typically polynomial coefficients) for the same Antenna. The current implementation should be able to
    deal with high-dimensional model coefficients (i.e. those which are stored as an ND Array) stored within FitStorage
    objects. The Moving Average itself is carried out on a per piecewise fit basis and does not update its history with
    anomalous data. The module operates on a single-tile.

    The module handles invalid fits in the following ways:
        a) A model containing at least one FitStorage object set to check_nan=True is treated as an anomaly.
        b) While the buffer for some piecewise fit is filling up (if ignore is set), then if it encounters an nan-model,
           its contribution is replaced with the 'start' vector corresponding to the piecewise fit - the antenna/
           polarisation is not signalled at this stage however.
        c) During normal operation, nan'd models are treated like any other anomaly.

    Requires:
        * FitStorage objects as 3D Numpy Array (order A/P/FitStorage), with Coeffs within FitStorage objects [Default]
    Outputs:
        * Boolean indication of Anomalies (2D Numpy array, order A/P)
    Archives:
        * A list of FlaggedAnomaly objects indicating anomalous antennas/polarisations, with the anomaly field
          indicating the deviation from the mean of the moving average (as a scalar array) for each anomalous
          piecewise fit.

    Limitations:
        a) While this is an easy and efficient method, it does not lend itself to an intuitive interpretation of how the
            individual coefficients should be weighted in applying the norm operator.
        b) If the system starts off in anomalous state, and then it corrects itself just after the first valid sample,
            then the remaining samples will all be flagged as false-positives.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PerAntennaDeviationDetector, self).__init__()

        # Setup Parameters
        self.__antennas = None      # Antenna list (absolute)
        self.__polaris = None       # Polarisation indices
        self.__fits = None          # Number of fits
        self.__weight = None        # Weighting for calculation of RMS: can be scalar or vector of same size as ideal
        self.__max = None           # Maximum Allowed Absolute Deviation
        self.__wait_filling = None  # Used to ignore samples while the buffer is filling
        self.__default = None       # Default value in place of nan's
        self.__ma_length = None     # Moving Average Length

        # State Parameters
        self.__buffers = None   # Running Average Filters
        self.__filling = None   # Used to ignore the first 'length' inputs (until buffer has stabilised)

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 8 (7 Compulsory, 1 Optional) parameters
                * antennas - [Required]: The (ordered, absolute) list of Antennas
                * polarisations - [Required]: The indices of polarisations per antenna
                * fits - [Required]: The number of polynomial fits considered
                * start - [Required]: Numpy array of starting vectors (one for each fit). This will also dictate the
                                      shape of the model coefficients which will be received. It is also used as a
                                      stand-in during the initialisation phase, if an anomalous piecewise fit entry is
                                      detected due to nan's in which case this value is input instead to the moving
                                      average. Vectors must be supplied in the same order that the corresponding
                                      FitStorage objects are supplied
                * excessive - [Required]: Numpy array of the maximum deviation per fit to allow before signaling
                * length - [Required]: The length of the Moving Average over which to smooth comparisons
                * weight - [Required]: Numpy array of vectors (one for each fit) of the same size as the corresponding
                                       fit coefficients which serves to weight the individual components. Vectors must
                                       be supplied in the same order that the corresponding FitStorage objects are
                                       supplied
                * ignore - [Optional]: If True (default False), does not signal anomalies until after the moving average
                                       buffer for each piecewise fit has been filled up completely with real data. This
                                       option is typically used when the start vectors are non-informative (such as all
                                       zero vectors)
        :return: Status.SETUP if successful.
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any required option is not specified
        """
        # Store Start Data
        self.__default = filter_model['start']

        # Assign Data Shape (not including the model coefficients' shape)
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__fits = filter_model['fits']

        # Keep track of the cutoffs and filter parameters
        self.__weight = filter_model['weight']
        self.__ma_length = int(filter_model['length'])
        self.__max = filter_model['excessive']
        self.__wait_filling = filter_model.get('ignore', False)

        # Now Build Model
        self.__buffers = np.empty([len(self.__antennas), len(self.__polaris), self.__fits], np.object)
        for a in range(len(self.__antennas)):
            for p in range(len(self.__polaris)):
                for f in range(self.__fits):
                    self.__buffers[a,p,f] = MovingAverage(self.__default[f], self.__ma_length)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Starting Function

        Sets up the buffers
        """
        # Flush Buffers
        for f in range(self.__fits):
            for _buffer in self.__buffers[:,:,f].ravel():
                _buffer.flush(self.__default[f])

        # Refresh counters
        self.__filling = self.__ma_length if self.__wait_filling else 0

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameter List as described in Abstract Class
        """

        # Initialise Empty 2D Array of anomalies
        forward[0] = np.zeros([len(self.__antennas), len(self.__polaris)], dtype=bool)

        # If Still Filling in...
        if self.__filling > 0:
            # Update Moving Average
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    for f in range(self.__fits):
                        if np.isnan(message.Default[a,p,2*f].ret_coeff()).any():
                            self.__buffers[a,p,f].add(self.__default[f])  # Add the default entry
                        else:
                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff())  # Just add without flagging anomaly...

        # If already filled in:
        else:
            # Temporary placeholder of anomalies
            anom_list = np.empty((len(self.__antennas), len(self.__polaris)), dtype=np.ndarray)
            # Simultaneously detect anomalies and update MA:
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    anom_fits = []
                    for f in range(self.__fits):
                        deviation = message.Default[a,p,2*f].ret_coeff() - self.__buffers[a,p,f].Average  # Some coeffs may be nan's
                        dev_norm = np.sqrt(np.mean(np.square(np.multiply(deviation, self.__weight[f]))))  # May be nan...
                        if (dev_norm > self.__max[f]) or (np.isnan(dev_norm)):
                            anom_fits.append(dev_norm)
                        else:
                            x = message.Default[a,p,2*f].ret_coeff()
                            self.__buffers[a,p,f].add(x)  # Just add without flagging

                    if len(anom_fits) != 0:
                        forward[0][a,p] = True
                        anom_list[a,p] = np.asarray(anom_fits)

            # Assign Anomalies List if archiving
            if self.ArchiveEnabled:
                forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
                for a in range(len(self.__antennas)):
                    for p in range(len(self.__polaris)):
                        if forward[0][a, p]:
                            forward[1][nxt] = FlaggedAnomaly()
                            forward[1][nxt].set(self.__antennas[a], self.__polaris[p], anom_list[a,p])
                            nxt += 1

        # Update Filling flag if need be...
        if self.__filling > 0: self.__filling -= 1  # Decrement for next iteration...

    def _clean_up(self, reason):
        """
        Cleaning

        Nothing to do... since will_start will automatically flush memory...
        """
        pass

class IndependentOverlapDetector(AbstractProcessor):
    """
    Historical Overlap-Based Anomaly Detector

    This Class Detects Anomalies by comparing the overlap areas between the fitting polynomials specified in the sample
    with that indicated by an average over the last N samples. The principle is similar to the PerAntennaDeviationDetector
    previously defined, but uses overlap rather than the actual coefficients, to provide a more intuitive measure.
    Again the module operates on a single tile. The same moving average scheme is employed on a per piecewise fit basis,
    whereby anomalous samples are not compounded. However, since the calculation now deals with overlap, it restricts
    the band-pass modeller to use polynomials specified as 1D arrays, with lower-order coefficients first, retrieved
    from FitStorage objects.

    As before, NAN's are treated as follows:
        a) A model containing at least one FitStorage object set to check_nan=True is treated as an anomaly.
        b) While the buffer for some piecewise fit is filling up (if ignore is set), then if it encounters an nan-model,
           its contribution is replaced with the 'start' vector corresponding to the piecewise fit - the antenna/
           polarisation is not signalled at this stage however.
        c) During normal operation, nan'd models are treated like any other anomaly.

    Requires:
        * FitStorage objects as 3D Numpy Array (order A/P/FitStorage), with Coeffs within FitStorage objects [Default]
    Outputs:
        * Boolean indication of Anomalies (2D Numpy array, order A/P)
    Archives:
        * A list of FlaggedAnomaly objects indicating the antenna/polarisation, with the anomaly field storing the
            overlap ratio with the historical average (as a scalar array)

    Limitations:
        a) If the system starts off in an anomalous state, and then it corrects itself just after the first valid
            sample, then the remaining samples will all be flagged as false-positives.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(IndependentOverlapDetector, self).__init__()
        # Setup Parameters
        self.__antennas = None      # Antenna Indices
        self.__polaris = None       # Polarisation Indices
        self.__cutoff = None        # Minimum cutoff (below which anomaly is flagged)
        self.__wait_filling = None  # Used to ignore samples while the buffer is filling
        self.__default = None       # Default Value for buffer
        self.__ma_length = None     # Moving Average Length
        self.__fits = None

        # State Parameters
        self.__buffers = None       # Running Average Filters
        self.__form = None          # Constant term consideration
        self.__filling = None       # Indicates that the buffer is filling up (and hence, anomalies must be ignored)

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 8 (6 Compulsory, 2 Optional) options:
                * antennas - [Required]: The Indices of antennas in the tile.
                * polarisations - [Required]: The Indices of polarisations per antenna.
                * fits - [Required]: The number of polynomial fits considered
                * start - [Required]: Numpy array of starting vectors (one for each fit). This will also dictate the
                                      shape of the model coefficients which will be received. It is also used as a
                                      stand-in during the initialisation phase, if an anomalous piecewise fit entry is
                                      detected due to nan's in which case this value is input instead to the moving
                                      average. Vectors must be supplied in the same order that the corresponding
                                      FitStorage objects are supplied
                * length - [Required]: The length of the Moving Average over which to smooth comparisons
                * min_overlap - [Required]: Minimum Overlap ratio to consider for a valid antenna (used for all piecewise
                                            polynomial fits)
                * form - [Optional]: When True (defaults to False) indicates that the comparator should only consider
                                     the general shape (form) rather than the overall area. This is achieved effectively by
                                     zeroing out the constant term.
                * ignore - [Optional] : If True (default False), does not signal anomalies until after the moving average
                                        buffer for each piecewise fit has been filled up completely with real data. This
                                        option is typically used when the start vectors are non-informative (such as all
                                        zero vectors)
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any required option is not specified
        """
        # Assign Setup Parameters: although the comparator is technically a state parameter, it itself does not contain
        #   any state while executing, and hence it can serve as a placeholder like the rest of the setup parameters.
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__form = filter_model.get('form', False)
        self.__wait_filling = filter_model.get('ignore', False)
        self.__cutoff = filter_model['min_overlap']
        self.__ma_length = filter_model['length']
        self.__default = filter_model['start']
        self.__fits = filter_model['fits']

        # Build Model
        self.__buffers = np.empty([len(self.__antennas), len(self.__polaris), self.__fits], np.object)
        for a in range(len(self.__antennas)):
            for p in range(len(self.__polaris)):
                for f in range(self.__fits):
                    self.__buffers[a, p, f] = MovingAverage(self.__default[f], self.__ma_length)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start notification

        Flush the buffers, and set the filling length
        """
        # Flush Buffers
        for f in range(self.__fits):
            for _buffer in self.__buffers[:, :, f].ravel():
                _buffer.flush(self.__default[f])

        # Refresh counters
        self.__filling = self.__ma_length if self.__wait_filling else 0

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameter List as described in Abstract Class
        """

        # Initialise Forward Message:
        forward[0] = np.zeros([len(self.__antennas), len(self.__polaris)], dtype=bool)

        # If Still Filling in...
        if self.__filling > 0:
            # Update Moving Average
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    for f in range(self.__fits):
                        if np.isnan(message.Default[a,p,2*f].ret_coeff()).any():
                            self.__buffers[a,p,f].add(self.__default[f])  # Add the default entry
                        else:
                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff())  # Just add without flagging anomaly...

        # Normal Operation ...
        else:
            # Temporary placeholder of anomalies
            anom_list = np.empty((len(self.__antennas), len(self.__polaris)), dtype=np.ndarray)
            for a in range(self.__buffers.shape[0]):
                for p in range(self.__buffers.shape[1]):
                    anom_fits = []
                    for f in range(self.__fits):
                        coeff = message.Default[a,p,2*f].ret_coeff()
                        chans = message.Default[a,p,2*f].ret_chans()
                        comparator = PolynomialCompare((chans[0],chans[-1]),self.__form)
                        overlap = comparator.compare(coeff, self.__buffers[a,p,f].Average)

                        if (overlap < self.__cutoff) or np.isnan(overlap):
                            anom_fits.append(overlap)
                        else:
                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff())  # Just add (without flagging anomaly...)

                    if len(anom_fits) != 0:
                        forward[0][a,p] = True
                        anom_list[a,p] = np.asarray(anom_fits)

            # Assign Anomalies List if archiving
            if self.ArchiveEnabled:
                forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
                for a in range(len(self.__antennas)):
                    for p in range(len(self.__polaris)):
                        if forward[0][a, p]:
                            forward[1][nxt] = FlaggedAnomaly()
                            forward[1][nxt].set(self.__antennas[a], self.__polaris[p],
                                                                   anom_list[a,p])
                            nxt += 1

        # Update Filling flag if need be...
        if self.__filling > 0: self.__filling -= 1  # Decrement for next iteration...

    def _clean_up(self, reason):
        """
        Empty

        Nothing to do... since '_will_start' will automatically flush memory...
        """
        pass

class OutlierThresholdDetector(AbstractProcessor):
    """
    Outlier-Based Anomaly Detector

    The class implements an Anomaly Detector based on a threshold of outlier-to-valid channel ratios. The outliers must
    have been detected by some other module. This is a simple module, and one which is quite robust, given that the
    outlier detection is reasonably so.

    This module should not need to concern itself with nan values, assuming that channel data is clean.

    Requires:
        * Boolean Matrix indicating the outlying channels in each antenna/polarisation (order A/P/C) [Default]
    Outputs:
        * Boolean indication of Anomalies (2D Numpy array, order A/P)
    Archives:
        * A list of FlaggedAnomaly objects indicating anomalous antennas/polarisations, with the anomaly field storing
            the number of outliers.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(OutlierThresholdDetector, self).__init__()
        self.__antennas = None
        self.__polaris = None
        self.__thresholds = None  # Threshold over which the number of outliers implies an anomaly.

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Python Dict containing 4 (Compulsory) parameters:
                * antennas - [Required]: The Indices of antennas in the tile.
                * polarisations - [Required]: The Indices of polarisations per antenna.
                * channels - [Required]: The Channels being utilised
                * threshold - [Required]: The ratio (fraction of 1.0) of outliers to tolerate before signaling an
                                    anomaly.
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified
        """
        # Assign Parameters
        self.__thresholds = float(filter_model['threshold']) * len(filter_model['channels'])
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        Nothing to do - the process is entirely stateless
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler
        Parameter List as described in Abstract Class
        """
        # Compute outlier thresholds (and hence forward Matrix)
        outliers = np.sum(message.Default, 2)       # Outlier Counts per Ant/Pol
        forward[0] = outliers > self.__thresholds   # Check if passes threshold...

        # Assign Anomalies List if archiving
        if self.ArchiveEnabled:
            forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    if forward[0][a,p]:
                        forward[1][nxt] = FlaggedAnomaly()
                        forward[1][nxt].set(self.__antennas[a], self.__polaris[p],
                                                               np.asarray(outliers[a,p]))
                        nxt += 1

    def _clean_up(self, reason):
        """
        Empty
        """
        pass

class OutlierWeightedOverlapDetector(AbstractProcessor):
    """
    Overlap-Based Historical Anomaly Detector with Outlier-weighted averaging

    This class implements an overlap-based Anomaly Detector, where the moving average computation in the case of an
    anomalous sample is weighted according to the number of outliers detected (typically by a smoothness-based outlier
    detector):
              { msg.default for f in no. of poly fits       if non-anomalous
        ma += { none                                        if msg.default is nan
              { ma * alpha + msg.default * (1 - alpha)      if anomalous

              where:
                += is the normal moving average update
                none indicates no update to the moving average
                alpha = 1 - min(1.0, msg.outliers/max_outliers), and
                max_outliers is the maximum allowed outliers to consider a sample at all (calculated from ratio to # of
                channels in the computation)

    This ensures that while anomalous samples do not corrupt the average, the history-based scheme is able to
    recover from a wrong start.

    The module deals with invalid models (containing NaN's) in the following ways:
        a) A model containing at least one FitStorage object set to check_nan=True is treated as an anomaly.
        b) While the buffer for some piecewise fit is filling up (if ignore is set), then if it encounters an nan-model,
           its contribution is replaced with the 'start' vector corresponding to the piecewise fit - the antenna/
           polarisation is not signalled at this stage however.
        c) During normal operation, the invalid models are signalled, but they do not contribute at all to the moving
           average.

    Note that the module assumes that the Outlier data does not contain np.NaN (since it is a boolean matrix)

    Requires:
      * FitStorage objects as 3D Numpy Array (order A/P/FitStorage), with Coeffs within FitStorage objects [Default]
      * The Outlier Channels per Antenna/Pol (3D Boolean Numpy array, order A/P/Chan)

    Outputs:
      * 2D Boolean Numpy Matrix indicating the presence of anomalies (order A/P)

    Archives:
      * The List of FlaggedAnomaly Objects, with the anomaly field indicating the overlap ratio.

    Limitations:
        a) The system may still be affected negatively by a long stream of high-magnitude anomalous antennas, which will
            shift the 'normal' state towards it. This can happen especially if the anomalous state does not exhibit
            considerable noise, but is just a magnitude shift. However, the possibilities of this should be remote.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(OutlierWeightedOverlapDetector, self).__init__()

        # Setup Parameters
        self.__antennas = None      # Indices of Antennas
        self.__polaris = None       # Indices of Polarisations
        self.__fits = None          # Number of fits considered
        self.__default = None       # The default (start) average value
        self.__cutoff = None        # Minimum cutoff (below which anomaly is flagged)
        self.__outlier = None       # Maximum number of outliers (above which the contribution of sample is ignored)
        self.__out_mdl = None       # The link to the outlier generator module
        self.__ma_length = None     # Moving Average Length
        self.__wait_filling = None  # Boolean flag indicating if will wait for buffer to fill before signaling anomalies

        # State Parameters
        self.__buffers = None       # Running Average Filters
        self.__filling = None       # Buffer is in filling status

    def _setup(self, filter_model):
        """
        Setup Method
        :param filter_model: Dictionary containing 11 (9 Compulsory, 2 Optional) parameters
                * antennas - [Required]: The Indices of Antennas to compute for
                * polarisations - [Required]: The Indices of Polarisations (per Antenna)
                * channels - [Required]: The (complete) list of channels fitted over (incl. interpolated regions) to
                                         evaluate over
                * fits - [Required]: The number of polynomial fits considered
                * start - [Required]: Numpy array of starting vectors (one for each fit). This will also dictate the
                                      shape of the model coefficients which will be received. It is also used as a
                                      stand-in during the initialisation phase, if an anomalous piecewise fit entry is
                                      detected due to nan's in which case this value is input instead to the moving
                                      average. Vectors must be supplied in the same order that the corresponding
                                      FitStorage objects are supplied
                * length - [Required]: The length of the Moving Average Filter
                * min_overlap - [Required]: Minimum Overlap ratio to consider for a valid antenna (used for all piecewise
                                            polynomial fits)
                * max_outlier - [Required]: The maximum number of outliers (ratio of number of channels) to allow for
                                            consideration of sample in Moving Average
                * outlier_gen - [Required]: The Outlier Generator Module (reference)
                * form - [Optional]: When True (defaults to False) indicates that the comparator should only consider
                                     the general shape (form) rather than the overall area. This is achieved effectively by
                                     zeroing out the constant term.
                * ignore - [Optional] : If True (default False), does not signal anomalies until after the moving average
                                        buffer for each piecewise fit has been filled up completely with real data. This
                                        option is typically used when the start vectors are non-informative (such as all
                                        zero vectors)
        :return: Status.SETUP if successful.
        """
        # Assign Parameters
        self.__antennas = tupler(filter_model['antennas'])
        self.__polaris = tupler(filter_model['polarisations'])
        self.__fits = filter_model['fits']
        self.__form = filter_model.get('form', False)
        self.__outlier = filter_model['max_outlier'] * len(filter_model['channels'])
        self.__wait_filling = filter_model.get('ignore', False)
        self.__out_mdl = filter_model['outlier_gen']
        self.__cutoff = filter_model['min_overlap']
        self.__ma_length = filter_model['length']
        self.__default = filter_model['start']

        # Build Model
        self.__buffers = np.empty([len(self.__antennas), len(self.__polaris), self.__fits], np.object)
        for a in range(len(self.__antennas)):
            for p in range(len(self.__polaris)):
                for f in range(self.__fits):
                    self.__buffers[a,p,f] = MovingAverage(self.__default[f], self.__ma_length)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Empty

        Clean up in preparation
        """
        # Flush Buffers
        for f in range(self.__fits):
            for _buffer in self.__buffers[:, :, f].ravel():
                _buffer.flush(self.__default[f])

        # Refresh counters
        self.__filling = self.__ma_length if self.__wait_filling else 0

    def _new_sample(self, message, forward):
        """
        Sample Handler
        Parameter List as described in Abstract Class
        """
        # Empty matrix of anomalies:
        forward[0] = np.zeros((len(self.__antennas), len(self.__polaris)), dtype=bool)

        # If Still Filling in...
        if self.__filling > 0:
            # Update Moving Average
            for a in range(len(self.__antennas)):
                for p in range(len(self.__polaris)):
                    for f in range(self.__fits):
                        if np.isnan(message.Default[a,p,2*f].ret_coeff()).any():
                            self.__buffers[a,p,f].add(self.__default[f])  # Add the default entry
                        else:
                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff())  # Just add without flagging anomaly...

        # Else if normal operation ...
        else:
            anom_list = np.empty((len(self.__antennas), len(self.__polaris)), dtype=np.ndarray)
            for a in range(self.__buffers.shape[0]):
                for p in range(self.__buffers.shape[1]):
                    anom_fits = []
                    for f in range(self.__fits):
                        coeff = message.Default[a,p,2*f].ret_coeff()
                        chans = message.Default[a,p,2*f].ret_chans()
                        comparator = PolynomialCompare((chans[0], chans[-1]), self.__form)
                        overlap = comparator.compare(coeff, self.__buffers[a,p,f].Average)

                        if np.isnan(overlap):  # If Nan Signal but do nothing - cannot add to buffer (will cause issues)
                            anom_fits.append(overlap)
                        elif overlap < self.__cutoff:  # If other anomaly, signal but also add weighted average
                            alpha = 1 - min(1.0, sum(message[self.__out_mdl][a, p]) / self.__outlier)

                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff() * alpha +
                                                 self.__buffers[a,p,f].Average * (1 - alpha))
                        else:  # No Anomaly... just add (without flagging anomaly...)
                            self.__buffers[a,p,f].add(message.Default[a,p,2*f].ret_coeff())

                        if len(anom_fits) != 0:
                            forward[0][a,p] = True
                            anom_list[a,p] = np.asarray(anom_fits)

            # Assign Anomalies List if archiving
                if self.ArchiveEnabled:
                    forward[1] = np.empty(np.sum(forward[0]), dtype=np.object); nxt = 0
                    for a in range(len(self.__antennas)):
                        for p in range(len(self.__polaris)):
                            if forward[0][a, p]:
                                forward[1][nxt] = FlaggedAnomaly()
                                forward[1][nxt].set(self.__antennas[a], self.__polaris[p],
                                                                       anom_list[a,p])
                                nxt += 1

        # Clean up Ignore flag
        if self.__filling > 0: self.__filling -= 1  # Decrement for next iteration...

    def _clean_up(self, reason):
        """
        Empty

        Nothing to do here - all taken care off in the will_start method
        """
        pass