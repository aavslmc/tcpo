import numpy as np

from pytcpo.Core.Archiving import ASerialiseable


class FlaggedAnomaly(ASerialiseable):
    """
    Generic Class for Encapsulating a Flagged Condition: i.e. an Antenna and corresponding Polarisation which is
    identified as being anomalous. The object is typically used only in archiving, as the normal pipeline flow passes
    a numpy matrix of boolean indications.

    The Class serves a dual purpose:
        a) Encode more information about the cause of the anomaly than just a matrix of booleans
        b) Encode (generally sparse) anomalies in a more compact manner

    Note also, that the Antenna/Polarisation (and in the case of outliers, the Channel) indices are absolute values.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        self._ant = None
        self._pol = None
        self._anomaly = None

    def set(self, ant, pol, anomaly):
        """
        Convenience Setter (for wrapping)

        :param ant:  The Antenna identifier.
        :param pol:  The polarisation index
        :param anomaly: The Anomaly Parameters - Must be an np array.
        :return: Reference to self (for chaining)
        """
        self._ant = ant
        self._pol = pol
        self._anomaly = np.asarray(anomaly)
        return self

    def serial(self):
        return {'_ant': int, '_pol': int, '_anomaly': np.ndarray}