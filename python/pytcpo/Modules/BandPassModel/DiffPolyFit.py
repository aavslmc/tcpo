import numpy as np
import numpy.polynomial.polynomial as nppol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage


class DiffPolyFit(AbstractProcessor):
    """
    The Class implements an alternative to the two-stage polynomial fitter, whereby the rejection is based on the
    presence of RFI spikes detected by a simple differential filter: i.e. in the polynomial fitting, samples which
    correspond to the presence of large derivatives are ignored.

    Invalid Channels (NaN's) do not factor in the Polynomial Generation: as a side-effect of the diff-scheme, the
    immediate right neighbours of NaN channels are also ignored.

    Requires:
        * 3D Numpy array containing the Channel Data (per Antenna/Polarisation) [Default]
    Outputs:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (lowest order first) where the first two dimensions
            are equal to the passed (channel) data, and the third dimension is of size (model order + 1)
    Archives:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (as above)

    Limitations:
        a) While correctly rejecting large spikes, it is also giving a false-positive on the return value of the spike
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(DiffPolyFit, self).__init__()

        # Setup Parameters
        self.__reject_mgn = None    # Number of standard deviations to reject
        self.__datashape = None     # Shape of the antenna/polarisation/order fit
        self.__channels = None      # Channels over which to compute
        self.__order = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 5 (Compulsary) arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polaris - [Required]: The Number of Polarisations we will be operating on
            * channels - [Required]: The Channels to fit upon
            * max_rate - [Required]: The Maximum Derivative value to tolerate
            * order - [Required]: The Model order to fit
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__datashape = [filter_model['antennas'], filter_model['polaris'], 1]
        self.__reject_mgn = float(filter_model['max_rate'])
        self.__channels = filter_model['channels']
        self.__order = int(filter_model['order'])

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """

        # Find NaN's in Channel Data
        _invalid_chans = np.isnan(message.Default)

        # Find Derivative (and pad to maintain size)
        _diff = np.pad(np.diff(message.Default, n=1, axis=2), ((0, 0), (0, 0), (1,0)), mode='edge')

        # Consequently, find inliers
        _inliers = np.logical_and(abs(_diff) < self.__reject_mgn, np.logical_not(_invalid_chans))

        # Perform the fit per antenna/polarisation. Due to the presence of outliers which may not be in the same
        #   position it is probably not possible to operate on the matrix as a whole (even if using masked arrays).
        forward[0] = np.empty(shape=self.__datashape, dtype=object)
        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):
                # Check that we have enough inliers to operate on and if not, indicate invalid model
                if sum(_inliers[a,p]) <= self.__order:
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=True)
                else:
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=False,
                                          coeff=nppol.polyfit(self.__channels[_inliers[a,p]],
                                                        message.Default[a,p,_inliers[a,p]], self.__order))

        # Update Archiving
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass