import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor

class PermanentRFIFiller(AbstractProcessor):

    def __init__(self):
        """
        Default Initialiser
        """
        super(PermanentRFIFiller, self).__init__()

        # Setup Parameters
        self.__antennas = None
        self.__polarisations = None
        self.__rfi_ranges = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 3 (Compulsary) arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polaris - [Required]: The Number of Polarisations we will be operating on
            * perm_rfi_chans - [Required]: List of Numpy arrays listing the absolute index of permanent RFI regions
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__antennas = filter_model['antennas']
        self.__polarisations = filter_model['polarisations']
        self.__rfi_ranges = filter_model['perm_rfi_chans']

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """

        sample = message.deep().Default

        # Replaces specified permanent RFI regions
        for rfi in self.__rfi_ranges:
            lower_edge = np.mean(message.Default[:,:,np.arange((rfi[0] - 3),(rfi[0] + 1))], axis=2)
            upper_edge = np.mean(message.Default[:,:,np.arange(rfi[-1],(rfi[-1] + 4))], axis=2)

            grad = np.divide(np.subtract(upper_edge, lower_edge), (rfi[-1] - rfi[0]))
            const = np.subtract(upper_edge, np.multiply(grad, rfi[-1]))

            for a, ant in enumerate(self.__antennas):
                for p, pol in enumerate(self.__polarisations):
                    for chan in rfi:
                        sample[ant,pol,chan] = np.polyval((grad[a,p], const[a,p]), chan)

        forward[0] = sample

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass