import numpy as np
import numpy.polynomial.polynomial as nppol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage
from pytcpo.Core.Common import tupler

class OutlierRejectionPolynomialFitter(AbstractProcessor):
    """
    Outlier-Based Rejection Polynomial Fitting

    The Class implements another alternative to the two-stage polynomial fitter, whereby the rejection is based on the
    presence of RFI spikes detected by an external outlier detector.

    NaN's in the channel data are automatically ignored. It is assumed that the outlier matrix does not have any invalid
    values (NaN's).

    The Output Models are compatible with the PolynEvaluator Module.

    Requires:
        * 3D Numpy array containing the Channel Data (order A/P/Chan) [Default]
        * 3D Numpy array containing the Channel Outliers (order A/P/Out)
    Outputs:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (lowest order first) where the first two dimensions
            are equal to the passed (channel) data, and the third dimension is of size (model order + 1)
    Archives:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (as above)

    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(OutlierRejectionPolynomialFitter, self).__init__()

        # Setup Parameters
        self.__datashape = None     # Output Data Shape
        self.__channels = None      # Channels over which to compute
        self.__outliers = None      # The Outliers module (reference)
        self.__order = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 5 (Compulsary) arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polaris - [Required]: The Number of Polarisations we will be operating on
            * channels - [Required]: The Channels to fit upon
            * outlier - [Required]: The reference to the outlier module
            * order - [Required]: The Model order to fit
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__datashape = [filter_model['antennas'], filter_model['polaris'], 1]
        self.__channels = tupler(filter_model['channels'])
        self.__outliers = filter_model['outlier']
        self.__order = int(filter_model['order'])

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Create the size of the output array
        forward[0] = np.empty(shape=self.__datashape, dtype=object)

        # Identify global inliers
        _inliers = np.logical_and(np.logical_not(message[self.__outliers]), np.logical_not(np.isnan(message.Default)))

        # Perform the fit per antenna/polarisation. Due to the variability of inliers between antenna/polarisations, it
        #   appears that it is not possible to operate on the matrix as a whole.
        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):
                    # Check if we still have enough samples to operate on:
                    if sum(_inliers[a,p]) <= self.__order:
                        forward[0][a,p,0] = FitStorage()
                        forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=True)
                    else:
                        forward[0][a,p,0] = FitStorage()
                        forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=False,
                                               coeff=nppol.polyfit(self.__channels[_inliers[a,p]],
                                                             message.Default[a,p,_inliers[a,p]], self.__order))

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass