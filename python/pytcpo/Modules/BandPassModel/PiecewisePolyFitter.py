import numpy as np
import numpy.polynomial.polynomial as nppol
from scipy.interpolate import lagrange as interpol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Archiving.Serialiseable import ASerialiseable


class PiecewisePolyFitter(AbstractProcessor):

    """
    The following processing module fits a number of polynomials, each with it's own respective channel range and order.
    Note that fitting ranges must overlap strictly by 10 channels. The piecewise polynomials are then joined together by
    means of interpolation within the 10 channel overlap regions, interpolating using points from overlapping fits
    (rather than using the raw data, which may include RFI in the interpolating region). Any fit bound between two
    interpolation regions must contain at least 20 channels (including overlapping channels).

    For a given channel range with n fits specified over this range, 2n - 1 FitStorage objects are returned: n polynomials
    corresponding to the specified fits, and n-1 polynomials corresponding to the overlapping regions.

    If a single fitting range contains an NaN value, the entire fit for that Antenna/Polarisation is set to NaNs, i.e.
    is effectively ignored.

    The module has internal absolute index addressing capabilities, and thus channel selection prior is not required
    (in fact this is discouraged, as the selection must be carried out on a per piece basis).

    Requires:
    * 3D Numpy Array of shape A/P/Chans
    * Absolute indices of channels forwarded to module
    Returns:
    * 3D Numpy Array of shape A/P/(2n + 1) for n fits, where axis 2 contains FitStorage objects.
    Archives:
    * As above.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PiecewisePolyFitter, self).__init__()

        # Setup Parameters
        self.__datashape = None
        self.__channels = None
        self.__fits = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 5 required arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polarisations - [Required]: The Number of Polarisations we will be operating on
            * channels - [Required]: The Channels forwarded to the module
            * fits - [Required]: 2-dimensional list of the form [Order, Fitting Channels], where the fitting channels
                                must be in a Numpy array (use of np.arange(min_fit_chan,max_fit_chan + 1) is suggested).
        """

        self.__datashape = [filter_model['antennas'], filter_model['polarisations'], (2 * len(filter_model['fits'])) - 1]
        self.__channels = filter_model['channels']
        self.__fits = filter_model['fits']

        for f in range(len(self.__fits)-1):
            if not np.array_equal(self.__fits[f][1][-10::], self.__fits[f+1][1][:10]):
                raise Exception('Incorrect Overlapping Channels Between Fits')

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler
        """

        # Create the size of the output array
        forward[0] = np.empty(shape=self.__datashape, dtype=object)

        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):

                if np.isnan(message.Default[a,p]).any():
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(np.arange(self.__fits[0][1][0], self.__fits[0][1][-10]),
                                            self.__fits[0][0], check_nan=True)

                    for f, fit in enumerate(self.__fits[1:-1]):
                        forward[0][a,p,(2*f)+2].FitStorage()
                        forward[0][a,p,(2*f)+2].setup(np.arange(fit[1][10],fit[1][-10]), fit[0], check_nan=True)

                    for f, fit in enumerate(self.__fits[:-1]):
                        forward[0][a,p,(2*f)+1] = FitStorage()
                        forward[0][a,p,(2*f)+1].setup(np.arange(fit[1][-10], fit[1][-1] + 1), 1, check_nan=True)
                        # Interpolated regions are given order 1 when NaN

                    forward[0][a,p,-1] = FitStorage()
                    forward[0][a,p,-1].setup(np.arange(self.__fits[-1][1][10], self.__fits[-1][1][-1] + 1),
                                             self.__fits[-1][0], check_nan=True)

                else:
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(np.arange(self.__fits[0][1][0], self.__fits[0][1][-10]), self.__fits[0][0],
                                            check_nan=False, coeff=nppol.polyfit(self.__fits[0][1], message.Default[a, p,
                                                            np.where(np.in1d(self.__channels, self.__fits[0][1]))[0]],
                                                            self.__fits[0][0]))

                    for f, fit in enumerate(self.__fits[1:-1]):
                        forward[0][a,p,(2*f)+2] = FitStorage()
                        forward[0][a,p,(2*f)+2].setup(np.arange(fit[1][10],fit[1][-10]), fit[0] , check_nan=False, coeff=
                                                      nppol.polyfit(fit[1], message.Default[a, p,
                                                                np.where(np.in1d(self.__channels, fit[1]))[0]], fit[0]))

                    forward[0][a,p,-1] = FitStorage()
                    forward[0][a,p,-1].setup(np.arange(self.__fits[-1][1][10], self.__fits[-1][1][-1] + 1),
                                             self.__fits[-1][0], check_nan=False, coeff= nppol.polyfit(self.__fits[-1][1],
                                                            message.Default[a, p, np.where(np.in1d(self.__channels,
                                                                        self.__fits[-1][1]))[0]], self.__fits[-1][0]))

                    for f, fit in enumerate(self.__fits[:-1]):
                        f_1_chans = np.arange(fit[1][-10], fit[1][-8])
                        f_2_chans = np.arange(fit[1][-1], fit[1][-1] + 2)

                        xp = np.concatenate((f_1_chans, f_2_chans))
                        yp = np.concatenate((nppol.polyval(f_1_chans, forward[0][a,p,2*f].ret_coeff()),
                                             nppol.polyval(f_2_chans, forward[0][a,p,(2*f)+2].ret_coeff())))

                        coeff_interp = interpol(xp,yp).c[::-1]

                        forward[0][a,p,(2*f)+1] = FitStorage()
                        forward[0][a,p,(2*f)+1].setup(np.arange(fit[1][-10], fit[1][-1] + 1), (len(coeff_interp)-1),
                                                      check_nan=False, coeff=coeff_interp)

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass

class FitStorage(ASerialiseable):

    """
    FitStorage is an ASerialisable object which for a given Ant/Pol, allows the storage of all coefficients and
    corresponding channel ranges that are required for a single or piecewise polynomial fit. Functionality is provided
    for registering a particular fit as being composed of NaN values.

    Note that use of the NaN capabilites of this object type are to be used with caution. By default the coefficient
    field is set to a NaN when the setup method is called, unless other coefficients are passed during setup. This
    ensures that the coefficient field is always set, avoiding NoneType related errors.
    """

    def __init__(self):
        self.coeff = None
        self.chans = None
        self.check_nan = None

    def setup(self, channels, order, check_nan, coeff=np.array([np.NaN])):
        self.check_nan = check_nan
        self.chans = np.asarray(channels, dtype=np.int64)

        if check_nan:
            self.coeff = np.empty((order+1,)) # Preserving coefficient shape
            self.coeff[:] = np.NaN
        else:
            self.coeff = np.asarray(coeff, dtype=np.float64)

    def ret_coeff(self):
        return self.coeff

    def ret_chans(self):
        return self.chans

    def ret_isnan(self):
        return self.check_nan

    def serial(self):
        return {'coeff': np.ndarray, 'chans': np.ndarray, 'check_nan': bool}

class PiecewisePolyEvaluator(AbstractProcessor):
    """
    The following processing module for a given Antenna/Polarisation evaluates the multiple FitStorage objects required
    for a fit for the given Ant/Pol, by evaluating each coefficient over the specified channel range. Thus this module
    allows the easy evaluation of piecewise polynomial fits (at least those as specified by the PiecewisePolyFitter).

    If a single FitStorage for a given Ant/Pol is set to NaN, the entire fit for that Ant/Pol is evaluated to NaNs.

    Requires:
    * 3D Numpy Array of shape A/P/(2n + 1) for n fits, where axis 2 contains FitStorage objects.
    Returns:
    * 3D Numpy Array of shape A/P/Fitted_Chans, where Fitted_Chans are the channels specified in the (2n + 1) FitStorage
      objects.
    Archives:
    * As above.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PiecewisePolyEvaluator, self).__init__()

        # Setup Parameters
        self.__datashape = None

    def _setup(self, filter_model):

        self.__datashape = [filter_model['antennas'],filter_model['polarisations'],len(filter_model['fit_channels'])]

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):

        # Create the size of the output array
        forward[0] = np.empty(shape=self.__datashape, dtype=np.float64)

        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):

                chan_idx = 0

                for fit in message.Default[a,p]:

                    chan_cnt = len(fit.ret_chans())
                    forward[0][a,p,chan_idx:(chan_idx + chan_cnt)] = nppol.polyval(fit.ret_chans(),fit.ret_coeff())
                    chan_idx += chan_cnt

        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass