import numpy as np
import numpy.polynomial.polynomial as nppol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage

class TwoPassPolyFit(AbstractProcessor):
    """
    The Class implements an improvement over the simple PolynFitter, whereby there is a two-stage fitting procedure. In
    the first stage, the polynomial is generated over all data points: following this, all samples beyond a specified
    amount of standard deviations are identified, and are subsequently ignored in the second-stage polynomial fitting.

    The Output Models are compatible with the PolynEvaluator Module.

    Requires:
        * 3D Numpy array containing the Channel Data (per Antenna/Polarisation) [Default]
    Outputs:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (highest order first) where the first two dimensions
            are equal to the passed (channel) data, and the third dimension is of size (model order + 1)
    Archives:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (as above)

    Limitations:
        a) Does not totally eliminate outliers.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(TwoPassPolyFit, self).__init__()

        # Setup Parameters
        self.__reject_dev = None    # Number of standard deviations to reject
        self.__datashape = None     # Shape of the antenna/polarisation/order fit
        self.__channels = None      # Channels over which to compute
        self.__order = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 5 (Compulsary) arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polaris - [Required]: The Number of Polarisations we will be operating on
            * channels - [Required]: The Channels to fit upon
            * reject_std - [Required]: The Number of Standard deviations beyond which to reject outliers
            * order - [Required]: The Model order to fit
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__datashape = [filter_model['antennas'], filter_model['polaris'], 1]
        self.__reject_dev = float(filter_model['reject_std'])
        self.__channels = filter_model['channels']
        self.__order = int(filter_model['order'])

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Create the size of the output array
        forward[0] = np.empty(shape=self.__datashape, dtype=object)

        # Perform the fit per antenna/polarisation. Due to the presence of nan's, it is not possible to operate on the
        #   matrix as a whole.
        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):
                # Check that input is correct, and if not handle
                if np.isnan(message.Default[a,p]).any():
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=True)

                # Input ok, continue...
                else:
                    # Compute First Pass
                    _model, _residuals = np.polyfit(self.__channels, message.Default[a,p,:], self.__order,
                                                    full=True)[0:2]

                    # Identify Inliers
                    _ideal = np.polyval(_model, self.__channels)
                    _stdev = np.sqrt(_residuals[0] / len(self.__channels))
                    _inliers = np.abs(message.Default[a,p,:] - _ideal) < self.__reject_dev * _stdev

                    # Check if we still have enough samples to operate on:
                    if sum(_inliers) <= self.__order:
                        forward[0][a,p,0] = FitStorage()
                        forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=True)

                    # Else compute 2nd iteration:
                    else:
                        forward[0][a,p,0] = FitStorage()
                        forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=False,
                                              coeff=nppol.polyfit(self.__channels[_inliers], message.Default[a,p,_inliers],
                                                          self.__order))

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass