# Implements the BandPass Model Fitting Modules
from PolynFitter import PolynFitter, PolynEvaluator
from DiffPolyFit import DiffPolyFit
from TwoPassPolyFit import TwoPassPolyFit
from MeanFit import PiecewiseMeanFitter, PiecewiseMeanEvaluator
from PiecewisePolyFitter import PiecewisePolyFitter, PiecewisePolyEvaluator, FitStorage
from PermanentRFIFiller import PermanentRFIFiller

from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(PolynFitter)
PipelineManager.registerModule(DiffPolyFit)
PipelineManager.registerModule(TwoPassPolyFit)
PipelineManager.registerModule(PiecewiseMeanFitter)
PipelineManager.registerModule(PiecewiseMeanEvaluator)
PipelineManager.registerModule(PolynEvaluator)
PipelineManager.registerModule(PiecewisePolyFitter)
PipelineManager.registerModule(PiecewisePolyEvaluator)
PipelineManager.registerModule(PermanentRFIFiller)

from pytcpo.Core.Archiving import ASerialiser

ASerialiser.registerSerialiseable(FitStorage)

__author__ = "Michael Camilleri"