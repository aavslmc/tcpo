import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor


class PiecewiseMeanFitter(AbstractProcessor):
    """
    Piecewise-Mean-Fitter with Outlier Rejection

    The Class implements a simple mean-fitting (which can be piecewise on contiguous subsets of the channels), with
    outlier rejection.

    NaN's in the channel data are automatically ignored. It is assumed that the outlier matrix does not have any invalid
    values (NaN's). If there are not enough channels to compute an average however (after ignoring NaN's) then the
    output is NaN.

    Requires:
        * 3D Numpy array containing the Channel Data (order A/P/Chan) [Default]
        * 3D Numpy array containing the Channel Outliers (order A/P/Out) [Optional]
    Outputs:
        * 3D Numpy array of Mean Values per Antenna/Polarisation (size is the number of pieces).
    Archives:
        * 3D Numpy array of Mean Coefficients.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PiecewiseMeanFitter, self).__init__()

        # Setup Parameters
        self.__piece_num = None     # Number of pieces
        self.__piece_size = None    # Size of the piece
        self.__outliers = None      # The Outliers module (reference)

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 3 (2 Compulsary, 1 Required) arguments:
            * pieces - [Required]: The Number of pieces to fit
            * sizes - [Required]: The size of each piece-wise channel range. 'pieces' * 'sizes' = # Channels
            * outlier - [Optional]: The reference to the outlier module. If not specified, or set to None, than this
                indicates that Outliers are considered in the mean computation and not ignored.
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__piece_num = int(filter_model['pieces'])
        self.__piece_size = int(filter_model['sizes'])
        self.__outliers = filter_model.get('outlier', None)

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Create the size of the output array
        forward[0] = np.empty(shape=[message.Default.shape[0], message.Default.shape[1], self.__piece_num],
                              dtype=message.Default.dtype)

        # Identify global outliers
        if self.__outliers is not None:
            _outliers = np.logical_or(message[self.__outliers], np.isnan(message.Default))
        else:
            _outliers = np.isnan(message.Default)

        # Update Message Matrix with nan's for outliers
        _bandpass = np.copy(message.Default)
        _bandpass[_outliers] = np.nan

        # Now Create Piece-wise Means
        for i in range(self.__piece_num):
            forward[0][:,:,i] = np.nanmean(_bandpass[:,:,i*self.__piece_size:(i+1)*self.__piece_size], axis=2)

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass


class PiecewiseMeanEvaluator(AbstractProcessor):
    """
    Piecewise-Mean-Evaluator

    The Class extends the mean-fitter output to match the same size as the original channel bandpass (repeating the mean
    values as necessary)

    NaN's in the computation are expanded as any other value.

    Requires:
        * 3D Numpy array containing the Piecewise means (order A/P/Means) [Default]
    Outputs:
        * 3D Numpy array of Mean-Valued Channel Powers
    Archives:
        * 3D Numpy array of Mean Coefficients.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PiecewiseMeanEvaluator, self).__init__()

        # Setup Parameters
        self.__piece_size = None  # Size of each mean-piece

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 1 (Compulsary) argument:
            * sizes - [Required]: The size of each piece
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__piece_size = int(filter_model['sizes'])

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Create the size of the output array
        forward[0] = np.repeat(message.Default, self.__piece_size, axis=2)

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass
