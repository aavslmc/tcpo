import numpy as np
import numpy.polynomial.polynomial as nppol

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Modules.BandPassModel.PiecewisePolyFitter import FitStorage

class PolynFitter(AbstractProcessor):
    """
    The Class implements a simple polynomial fitting over the bandpass using numpy's polyfit module.

    The PolynEvaluator Module may be used to evaluate the model on the available channels.

    Requires:
        * 3D Numpy array containing the Channel Data (order A/P/Chans) [Default]
    Outputs:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (lowest order first) where the first two dimensions
            are equal to the passed (channel) data, and the third dimension is of size (model order + 1)
    Archives:
        * 3D Numpy array of Coefficients per Antenna/Polarisation (as above)

    Limitations:
        a) Since this is a simple least-squares based polynomial fitting scheme, it suffers from bias towards large
            outliers (RFI Spikes).
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PolynFitter, self).__init__()

        # Setup Parameters
        self.__datashape = None
        self.__channels = None
        self.__order = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 4 (Compulsary) arguments:
            * antennas - [Required]: The Number of Antennas we will be operating on
            * polaris - [Required]: The Number of Polarisations we will be operating on
            * channels - [Required]: The Channels to fit upon
            * order - [Required]: The Model order to fit
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or any option is not specified.
        """
        self.__datashape = [filter_model['antennas'], filter_model['polaris'], 1]
        self.__channels = filter_model['channels']
        self.__order = int(filter_model['order'])

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Create the size of the output array
        forward[0] = np.empty(shape=self.__datashape, dtype=object)

        # Perform the fit per antenna/polarisation. Due to the presence of nan's, it is not possible to operate on the
        #   matrix as a whole.
        for a in range(self.__datashape[0]):
            for p in range(self.__datashape[1]):
                if np.isnan(message.Default[a,p]).any():
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=True)
                else:
                    forward[0][a,p,0] = FitStorage()
                    forward[0][a,p,0].setup(self.__channels, self.__order, check_nan=False, coeff=nppol.polyfit(self.__channels,
                                                         message.Default[a,p,self.__channels], self.__order))

        # Update Archiving setup
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass

class PolynEvaluator(AbstractProcessor):
    """
    The Class implements a simple polynomial evaluator to be used by other modules down the line. It serves mainly
    as a wrapper around Numpy's polyval function, specifically using the new Polynomial Package.

    NaN Models or Models which contain at least one invalid (NaN) coefficient, will output NaN for all channels. NaN
    channels will conversely evaluate to NaN

    Requires:
        * Polynomial Coefficients as 3D Numpy Array (order A/P/Coeff, Coeff's in increasing order) [Default]
    Outputs:
        * Evaluated Channel Data as 3D Numpy Array (order A/P/Chans)
    Archives:
        * None

    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PolynEvaluator, self).__init__()

        # Setup Parameters
        self.__channels = None

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: A Dictionary containing 1 (Compulsary) argument:
            * channels - [Required]: The Channels upon which the evaluation will be carried out.
        :return: Status.SETUP
        :raises TypeError, KeyError: If the filter_model is not a dictionary, or the option is not specified.
        """
        self.__channels = filter_model['channels']

    def _will_start(self):
        """
        Start Method

        (Empty since stateless)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        Parameters as described in parent class.
        """
        # Evaluate the polynomials: with some tricky reshaping since it expects the coefficients along first dimension.
        forward[0] = nppol.polyval(self.__channels, np.expand_dims(message.Default, axis=0).swapaxes(0, 3), tensor=False)

    def _clean_up(self, reason):
        """
        Cleanup Method

        (Empty)
        """
        pass