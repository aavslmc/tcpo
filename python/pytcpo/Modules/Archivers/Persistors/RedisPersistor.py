#   > Consider using UnixDomainSocketConnection protocol for communication, as opposed to TCP (which may be more
#       efficient)

import redis

from pytcpo.Core.Archiving import APersistor
from pytcpo.Core.Common import TCPOCode, TCPOException


class RedisPersistor(APersistor):
    """
    Implements a Redis-Centred Persistor (Archiver). Samples are stored as rows within lists, in string format, with
    the time-stamps written first, and separated by a whitespace. The keys identifying the component generating the
    data are part of the list name: the other part is the pipeline name itself. Samples are added to the right end of
    the list, although this is invisible to the user.

    The module requires a Redis Server to be already running: i.e. this is just a client. The persistence level of the
    archiver itself depends on the respective settings within the redis server configuration file (for details refer to
    https://redis.io/topics/quickstart). However, in general, opening the archive in write-mode without appending will
    first flush the current database.

    This persistor supports full random access when reading, and also simultaneous read-write capabilities. Writes
    always happen to the end of the list, while reads can be from any point in the list so far. Thread-safety (between
    reads/writes) are as guaranteed (and limited) by the underlying redis client/server.

    Due to the flexibility of redis, as well as its intended usage, this persistor also allows negative indexing. This
    means that accessing the last element in a key is as simple as calling the ReadSample method with index set to -1.
    """

    @property
    def SupportsReadWrite(self):
        return True

    @property
    def SupportsRandomAccess(self):
        return True

    def __init__(self):
        """
        Default Initialiser
        """
        super(RedisPersistor, self).__init__()

        # Own parameters
        self._client = None     # The Redis Client Instance
        self._types = None      # The Data Types associated with each key
        self._indcs = None      # The index for reading from next, when reading in

    def _initialise(self, mode, structure):
        """
        Initialisation Method
        :param mode:    The mode in which to open the archive - this can be one of the defined modes in APersistor
        :param structure:   A Dictionary, with six (3 required, 3 optional] key-value pairs:
                * host - [Required]: The IP of the Redis Server. This must already have been started, otherwise, it will
                                     fail.
                * port - [Required]: The port over which to communicate with the server.
                * keys - [Required]: Dictionary, mapping keys to sample types.
                * db - [Optional]: The database to work on - defaults to 0 (see https://redis.io/commands/select)
                * auth - [Optional]: If not None (default), this is the Authentication Password (string)
        :return:    Status.SETUP if successful.
        """
        # Prepare Connection
        self._client = redis.StrictRedis(host=structure['host'], port=structure['port'], password=structure.get('auth'),
                                         db=structure.get('db', 0), socket_connect_timeout=1.0)

        # Check Connection
        if not self._client.ping():
            raise TCPOException(TCPOCode.Error.SETUP, 'Pinging Failure')

        # Store rest of parameters
        self._types = structure['keys']
        self._indcs = { key:0 for key in self._types }

        # Now to Differentiate betwen Append/Write, if write and not append, we need to erase all keys already in the
        #   database. We can just flush the database.
        if not (mode & self.M_APPEND) and not (mode & self.M_READ): self._client.flushdb()

        # Return Success Code (optional)
        return TCPOCode.Status.SETUP

    def _writeSample(self, key, timestamp, sample):
        """
        Writes the sample to the end of the list named '<key>'
        :param key:         The list (sub)name
        :param timestamp:   The timestamp - written as a string, followed by a whitespace
        :param sample:      The serialised (string) sample
        :return: Status.OK
        :raises KeyError: If the key is inexistent (was not registered)
        """
        if not key in self._types: raise KeyError('%s Key not registered' % str(key))
        self._client.rpush(key, '%s %s' % (str(timestamp), sample))
        return TCPOCode.Status.OK

    def _readSample(self, key, idx = None):
        """
        Read the sample at the index specified from the list '<key>'
        :param key: Key to read from
        :param idx: Index to read at: if none, will read at the next available location and increment the index
        :return: 3-Tuple containing the time-stamp, sample-data-type and the serialised sample
        :raises KeyError: If the key is inexistent (was not registered)
        """
        if not key in self._types: raise KeyError('%s Key not registered' % str(key))   # Check that valid
        rd_idx = idx if idx is not None else self._indcs[key]                           # If need be, get the index
        [ts, sample] = self._client.lindex(key, rd_idx).split(' ', 1)
        self._indcs[key] += 1 if idx is None else 0                                     # If using next, update index
        return self.TS_TYPE(ts), self._types[key], sample                               # return

    def _seek(self, key, idx = -1):
        """
        Seeks to the specified index for the requested Key
        :param key:
        :param idx:
        :return: None
        """
        # Get the length to check that the entry is valid
        list_len = self._client.llen(key)
        if (idx >= list_len) or (idx < -list_len): # First check for Positive indices, then Negative ones
            raise TCPOException(TCPOCode.Error.INDEX, '%d Index is out of bounds for Key of length %d' % (idx, list_len))
        # Set the key index (negating from the length of the list if using negative indexing)
        self._indcs[key] = idx if idx >= 0 else list_len + idx

    def _skipSample(self, key, num = 1):
        """
        Skip num samples from key list - basically, increments the index for the associated key with num
        :param key: Key to reference
        :param num: Number of samples to skip
        :return: Status.OK
        :raises KeyError, TCPOException: If the key is inexistent (was not registered) or not enough values in list
        """
        if self._indcs[key] + num >= self._client.llen(key):
            raise TCPOException(TCPOCode.Error.VALUE, 'Cannot Skip %d samples. Not enough stored.' % num)
        self._indcs[key] += num
        return TCPOCode.Status.OK

    def _close(self):
        self._types = None
        self._client = None
        self._indcs = None
