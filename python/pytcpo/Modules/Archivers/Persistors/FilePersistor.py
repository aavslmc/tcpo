import os

from pytcpo.Core.Archiving import APersistor
from pytcpo.Core.Common import TCPOCode


class FilePersistor(APersistor):
    """
    Implements a Text-File archiver. Keys (Channels) are stored in different files within a root directory, which may
    be further subdivided into folders. Each key will thus be indexed as '<name>.tfa', where <name> can be a hieararchy
    of folders, terminating in a filename. Within each file, samples are stored line-wise, with the sample time being
    stored first and the serialised data next, separated by a space.

    The TFA does not currently support simultaneous read/write open-mode or random access. With regards to ensuring
    thread-safety, it is up to the user to ensure that simultaneous reads from or writes to the same key do not happen
    (this can easily be avoided if each source writes to a different key).
    """

    @property
    def SupportsReadWrite(self):
        return False

    @property
    def SupportsRandomAccess(self):
        return False

    def __init__(self):
        """
        Default Initialiser
        """
        super(FilePersistor, self).__init__()
        # Own parameters
        self._files = None  # Have to keep these around to close safely
        self._types = None  # The Data Types associated with each key

    def _initialise(self, mode, structure):
        """
        Initialisation Method
        :param mode:    The mode in which to open the archive - this can be one of the defined modes in APersistor
        :param structure:   A Dictionary, with two keys:
                    * source: The Root Directory where to store the various channels. This must be a folder (not a file)
                        and will automatically be created if it does not exist.
                    * keys: A dictionary mapping keys to value types. If any key is composed of a folder structure, the
                        hieararchy will automatically be created. A key name cannot contain invalid file-name characters
                        and must not end with a '/'.
        :return:    Status.SETUP if successful.
        """
        # Prepare Data Structures
        self._files = {}

        # Set Type Map from Structure
        self._types = structure['keys']

        # Iterate over keys
        for key in self._types:
            # Generate Complete Key
            file_name = structure['source'].rstrip('/') + '/' + key + '.tfa'
            # Check if any folder structure needs to be created...
            parent_folder = file_name.rsplit('/', 1)[0]
            if not os.path.isdir(parent_folder): os.makedirs(parent_folder)
            # Check Open Mode - due to checks, will never be opened in read/write mode since not supported.
            if mode & self.M_READ:
                self._files[key] = open(file_name, 'r')   # Create File and add to dictionary
            else:
                self._files[key] = open(file_name, 'a' if (mode & self.M_APPEND) else 'w')

        # Return Success Code (optional)
        return TCPOCode.Status.SETUP

    def _writeSample(self, key, timestamp, sample):
        """
        Writes a single sample to File
        :param key:         Specifies the file to write to
        :param timestamp:   The timestamp - written out as a string as the first part of the line, followed by a space
        :param sample:      The (serialised) sample data to write out as a string
        :return:            Status.OK
        :raises KeyError: If the key does not exist
        """
        self._files[key].write('%s %s\n' % (str(timestamp), sample))
        return TCPOCode.Status.OK

    def _readSample(self, key, idx = None):
        """
        Read the next sample - the index is ignored

        :param key: The Key (Channel) to read from
        :param idx: Ignored
        :return: Tuple containing the time-stamp, the data type of the sample, and the raw (serialised) sample data
        :raises KeyError if the key is inexistent
        """
        [ts, sample] = self._files[key].next().split(' ', 1)
        return (self.TS_TYPE(ts), self._types[key], sample)

    def _seek(self, key, idx=-1):
        """
        Empty Implementation (Not Supported)
        """
        pass

    def _skipSample(self, key, num = 1):
        """
        Skip num samples for channel key
        :param key: The channel to index
        :param num: Number of lines to skip
        :return: Status.OK if successful
        :raises KeyError if the key is inexistent
        """
        for _ in range(num): self._files[key].next()
        return TCPOCode.Status.OK

    def _close(self):
        """
        Close the files
        :return: None
        """
        self._types = None
        for _file in self._files.itervalues(): _file.close()
        self._files = None