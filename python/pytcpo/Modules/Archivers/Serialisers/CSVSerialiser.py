import StringIO
import csv
import numpy as np

from pytcpo.Core.Common import TCPOException, TCPOCode, namer
from pytcpo.Core.Archiving import ASerialiseable, ASerialiser


class CSVSerialiser(ASerialiser):
    """
    This class implements a White-Space-Separated serialiser using python's csv package. Strings are delimited by the
    pipe (|) character. Arrays are type, dimensionality and shape specified, with appropriate signaling of empty and
    scalar sizes.
    """
    def __init__(self):
        """
        Default Initialiser
        """
        super(CSVSerialiser, self).__init__()
        pass

    def _setup(self, mode, params):
        """
        No Setup is involved, since this is a stateless serialiser.
        :param mode:    Unused
        :param params:  Unused
        :return: None
        """
        pass

    def _serialise(self, obj):
        """
        Serialisation method.
        :param obj: The object to be serialised
        :return: string representation of the object
        """
        # Create String Buffer and Writer
        out_str = StringIO.StringIO()
        _csvwriter = csv.writer(out_str, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)

        # Now Serialise the Data
        row = []
        self.__write(obj, row)
        _csvwriter.writerow(row)

        # Return the string buffer (stripping excess newline at the end)
        return out_str.getvalue().rstrip()

    def _deserialise(self, sample, otype):
        """
        De-Serialisation method
        :param sample: The serialised (string) representation of the data
        :param otype: The type of data to extract
        :return: An instantiated otype appropriately initialised from the string representation.
        :raises TypeError, AttributeError: if the type is not supported by the serialiser (typically, the
                    aserialiseable is not registered)
        """
        _csvreader = csv.reader(StringIO.StringIO(sample), delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        row = _csvreader.next()
        return self._read(otype, row, [0])

    def _read(self, oType, row, lidx):
        """
        Protected recursive read method.
        Parses string to build the object of type oType
        :param oType: Type of sample to read
        :param row:   String (serialised) sample data
        :param lidx:  The index (in the row list) to read from next: for mutability, passed as the sole entry in a list
        :return:
        """
        # If Numpy array, store structure and recursively operate on elements.
        if oType is np.ndarray:
            dims = int(row[lidx[0]]); lidx[0] += 1         # Read the Dimensionality
            # Check if dimensionality is less than 1...
            if dims == 0:
                np_type = np.dtype(row[lidx[0]]); lidx[0] += 1 # This will always be a numpy type
                return np.empty(0, dtype=np_type)
            else:
                sizes = np.array((row[lidx[0]:lidx[0] + dims]), int); lidx[0] += dims  # Read the Shape
                nElem = sizes.prod()
                try:
                    np_type = np.dtype(row[lidx[0]]); lidx[0] += 1 # If a numpy type...
                    obj = np.array((row[lidx[0]:lidx[0] + nElem]), np_type).reshape(sizes)
                    lidx[0] += nElem
                except TypeError:
                    np_type = self.getSerialiseable(row[lidx[0]]); lidx[0] += 1  # This is an ASerialiseable Type (or will be none)
                    obj = np.empty(nElem, dtype=object)  # Allocate space
                    for i in range(nElem): obj[i] = self._read(np_type, row, lidx)  # Read in
                    obj = obj.reshape(sizes)  # Finally reshape accordingly
                return obj

        # Handle Boolean separate from other primitives, due to writing form
        elif oType is bool:
            obj = True if row[lidx[0]] == 'True' else False
            lidx[0] += 1
            return obj

        # If any of the other primitives, just convert
        elif issubclass(oType, ASerialiser.PRIMITIVES):
            obj = oType(row[lidx[0]])
            lidx[0] += 1
            return obj

        # If ASerialiseable...
        else:
            obj = oType()  #Instantiate
            for attr, aType in obj.serial().iteritems():  # Iterate through items and parse them
                obj.__dict__[attr] = self._read(aType, row, lidx)
            return obj

    def __write(self, obj, row):
        """
        Recursive Method for writing to file.
        :param obj: The data item to write. Must be one of the supported types.
        :param row: List where to append data
        :return:
        """
        # If ASerialisable, just handle each attribute
        if isinstance(obj, ASerialiseable):
            for attr in obj.serial():  # Iterate through items and append them... unless a failure happens...
                self.__write(obj.__dict__[attr], row)

        # If Numpy Array, store structure and type and iterate through items (recursively)
        elif isinstance(obj, np.ndarray):
            # If an Empty Array
            if obj.size == 0:
                row.append(0)  # Indicate that empty
                row.append(obj.dtype)  # Just for completeness, add dtype
            # If a Scalar or a single-valued array - do not use 0-dimensions!
            elif obj.size == 1:
                ndims = max(len(obj.shape), 1)              # Store the Number of dimensions, since could be size 1 but multiple dims...
                row.append(ndims)
                row.extend(np.ones(ndims, dtype=int))       # Shape
                row.append(type(obj.ravel()[0]).__name__)   # Handles both raw and complex types
                self.__write(obj.ravel()[0], row)
            # Otherwise...
            else:
                row.append(obj.ndim)  # Serialise Dimensionality
                row.extend(obj.shape)  # .. and the shape ...
                row.append(type(obj.ravel()[0]).__name__)  # Now do the type
                for elem in obj.ravel(): self.__write(elem, row)

        # If a primitive, just append
        elif isinstance(obj, ASerialiser.PRIMITIVES):
            row.append(obj)

        else:
            raise TCPOException(TCPOCode.Error.TYPE, 'Type %s not supported by TFA.' % namer(obj))