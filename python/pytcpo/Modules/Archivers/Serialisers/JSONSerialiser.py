import json
import numpy as np

from pytcpo.Core.Common import namer, TCPOCode, TCPOException
from pytcpo.Core.Archiving import ASerialiseable, ASerialiser


class JSONSerialiser(ASerialiser):
    """
    This class implements a JSON-based serialiser using the json package. Raw data types are handled automatically by
    the package, with complex numbers being stored explicitly as a two-entry list. ASerialiseable instances are
    handled as JSON objects, and numpy arrays are type-delimited (allowing for specification of empty arrays) as a JSON
    object.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(JSONSerialiser, self).__init__()
        pass

    def _setup(self, mode, params):
        """
        Setup Method

        Since this is a stateless serialiser, no parameters are required.
        :param mode:    Ignored
        :param params:  Ignored
        :return:        None
        """
        pass

    def _serialise(self, obj):
        """
        Serialisation Method

        :param obj: An object (of the supported types) to serialise
        :return:    String representation (in JSON formatting) of the object
        :raises Error.TYPE: if obj is of an unsupported type (through underlying call)
        """
        return json.dumps(obj, default=self.__encode)

    def _deserialise(self, sample, otype):
        """
        Deserialisation Function

        :param sample:  The (serialised) representation to read from
        :param otype:   The type of object to build
        :return:        The built object
        :raises Error.TYPE: if otype is not a recognised type (through underlying method)
        """
        return self.__decode(otype, json.loads(sample))

    @staticmethod
    def __encode(obj):
        """
        Encoding Method (private static function)

        :param obj: The object to encode as JSON
        :return: String representation (in JSON formatting) of the object
        :raises Error.TYPE: if obj is of an unsupported type
        """
        # First the Raw Data types (must be handled in this order due to some quirks)
        if isinstance(obj, complex):
            return [obj.real, obj.imag]
        elif isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, ASerialiser.PRIMITIVES):
            return obj

        # Now ASerialiseable Instances
        elif isinstance(obj, ASerialiseable):
            sub_dict = {}
            for attr in obj.serial():  # Iterate through items and append them... unless a failure happens...
                sub_dict[attr] = JSONSerialiser.__encode(obj.__dict__[attr])
            return sub_dict

        # Finally Arrays
        elif isinstance(obj, np.ndarray):
            if obj.size == 0:
                return {'type': namer(obj.dtype), 'np.arr': None}
            else:
                return {'type': type(obj.ravel()[0]).__name__, 'np.arr': obj.tolist()}

        # Anything else, raise error
        else:
            raise TCPOException(TCPOCode.Error.TYPE, 'Unsupported type (%s) encountered' % namer(type(obj)))

    @staticmethod
    def __decode(otype, json_object):
        """
        Decoding Function (private static function)

        :param otype: The type of object to build
        :param json_object : The JSON object (already deserialised) to build the type from.
        :return: The built object
        :raises Error.TYPE: if otype is not a recognised type
        """
        # Primitive Type
        if issubclass(otype, complex):
            return otype(*json_object)
        elif issubclass(otype, ASerialiser.PRIMITIVES):
            return otype(json_object)

        elif otype is np.ndarray:
            if json_object['np.arr'] is None:
                return np.empty(0, dtype=np.dtype(json_object['type']))
            else:
                try:  # Try First Numpy/Types
                    nptype = np.dtype(json_object['type'])
                    return np.asarray(json_object['np.arr'], dtype=nptype)
                except TypeError:  # This is an ASerialiseable-type array
                    nptype = ASerialiser.getSerialiseable(json_object['type'])
                    nparray = np.asarray(json_object['np.arr'], dtype=np.object)
                    np_ravl = nparray.ravel()
                    for idx in range(len(np_ravl)):
                        np_ravl[idx] = JSONSerialiser.__decode(nptype, np_ravl[idx])
                    return nparray

        elif issubclass(otype, ASerialiseable):
            obj = otype()  # Instantiate
            for attr, aType in obj.serial().iteritems():  # Iterate through items and parse them
                obj.__dict__[attr] = JSONSerialiser.__decode(aType, json_object[attr])
            return obj

        else:
            raise TCPOException(TCPOCode.Error.TYPE, 'Unsupported type (%s) encountered' % namer(otype))
