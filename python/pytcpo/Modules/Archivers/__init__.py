# This Package includes all the Archivers currently implemented.
__author__="Michael Camilleri"
from pytcpo.Core.Archiving import APersistor, PersistorFactory
from pytcpo.Modules.Archivers.Persistors.FilePersistor import FilePersistor
from pytcpo.Modules.Archivers.Persistors.RedisPersistor import RedisPersistor
from pytcpo.Modules.Archivers.Serialisers.CSVSerialiser import CSVSerialiser
from pytcpo.Modules.Archivers.Serialisers.JSONSerialiser import JSONSerialiser

APersistor.registerSerialiser(CSVSerialiser)
APersistor.registerSerialiser(JSONSerialiser)

PersistorFactory.registerPersistor(FilePersistor)
PersistorFactory.registerPersistor(RedisPersistor)
