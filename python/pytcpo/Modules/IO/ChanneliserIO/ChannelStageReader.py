import numpy as np
import time as tm
import logging

from pydaq.persisters import ChannelFormatFileManager
from pydaq.persisters import FileDAQModes

from pytcpo.Core.PipeLine import Message, AbstractGenerator
from pytcpo.Core.Common import TCPOCode, TCPOException, tupler


class ChannelStageReader(AbstractGenerator):
    """
    This Generator-type module implements the Channeliser-Output Stage DAQ Reader, based on Andrea deMarco's
    ChannelFileFormatManager. It is responsible for synchronising access to the underlying data and hence there should
    only ever be a single instance per station.

    In the interest of efficiency and modularity, tile-selection or any other antenna/polarisation/channel based subset
    grouping is performed using external modules.

    The Reader can be set to read only a specific range. This is typically used only when reading in static (offline)
    mode, but can be used also with negative indexing to start from the last entry for dynamic readout. A note is in
    order however in this case: specifically, the calculation of what is the last sample is performed based on the
    status of the DAQ file when it is first opened. Care must be taken in this regards to ensure that files across tiles
    have the same number of entries, otherwise, discrepancies in samples may occur.

    Reading can also be set to automatically end (and hence terminate the entire pipeline) when the file ending is
    reached. Note that if this option is set, then the generator loop will only terminate once at least 1 sample has
    been read from each of the tiles specified (i.e. if the file is inexistent, it will continue to loop indefinitely).

    Outputs:
        * The Channel BandPass Data
    Archives:
        * None

    Limitations:
        a) The current implementation operates on a tile-by tile basis, in that it reads all available data from a tile
            before moving on to the next one. This is usually not a problem when running in real-time, since it will
            automatically switch between tiles: however, for static readouts, it means that all data from one tile will
            have to be read and processed before moving on.
    """

    class TileState:
        """
        Utility Class for summarising the state of a single tile.
        """
        def __init__(self, id=0):
            self.id = id            # ID
            self.antennas = None    # The Antennas (typically a range)
            self.channels = None    # The Channels
            self.pols = None        # The Polarisations
            self.next_read = -1     # The sample to read next: starts at -1, indicating that file not yet opened.
            self.end_at = None      # When to end (if None, then keep reading)

        def reset(self):
            self.antennas = None
            self.channels = None
            self.pols = None
            self.next_read = -1
            self.end_at = None

    def __init__(self):
        """
        Default Initialiser
        """
        super(ChannelStageReader, self).__init__()

        # Setup Parameters
        self.__file_ts = None    # The File Timestamp which will be accessed each time
        self.__sleep_rt = None   # Default Sleep Rate
        self.__block_rd = None   # Default Block Read
        self.__stop_end = None   # Indicates if should stop on end of file
        self.__smp_b = None      # Sample Range (begin)
        self.__smp_e = None      # Sample Range (ending)

        # State Parameters
        self.__file_mgr = None  # File Manager Object
        self.__tiles = None     # Tile objects
        self.__start = None     # TimeStamp for start point

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python Dict, containing the following 7 (2 Required, 5 Optional) options:
                - path - [Required]: The Relative path to the root directory where the hdf5 files (will) reside
                - tiles - [Required]: An integer or tuple/list indicating which tiles to retrieve.
                - time - [Optional]: The file time-stamp to request. This will be shared across all tiles. If None
                            (default), will open the latest one in the specified directory.
                - sample_rate - [Optional]: A hint to the read loop indicating the rate at which to check the files for
                                new samples (defaults to 0.1s)
                - block_rate - [Optional]: A hint to the read loop indicating the blocks to read simultaneously in one
                                go (defaults to 1024)
                - stop_on_end - [Optional]: If True, readout will signal file-end when last sample read (default False)
                - samples - [Optional]: Can be one of 3 possibilities:
                            * None (Default) : Read all Samples, starting from the beginning
                            * int : Indicates the start sample (with unbounded end) to read from (inclusive). Using
                                    negative numbers counts backwards from the (current) end of the file.
                            * 2-tuple : Indicates a range (inclusive start, non-inclusive end) of samples to read. In
                                    this case, negative indexing is NOT allowed!
        :return: Status.SETUP if successful. Errors will be raised as Exceptions
        """
        # Setup the 'Immutable' Parameters first:
        self.__file_ts = filter_model.get('time', None)          # File Timestamp
        self.__sleep_rt = filter_model.get('sample_rate', 0.1)   # Rate Control (sleep rate)
        self.__block_rd = filter_model.get('block_rate', 1024)   # Rate Control (read block)
        self.__stop_end = filter_model.get('stop_on_end', False) # Mode

        # Check the Sample Range to read from
        samples = filter_model.get('samples')
        if samples is None:             # If None, then read entire range
            self.__smp_b = None
            self.__smp_e = None
        elif len(tupler(samples)) == 1: # If single entry, then interpret this as start
            if self.__stop_end:
                logging.warn('This Setup requests stopping on end of file. This means that the process may stop before '
                             'any samples have been read. Hence, correct behaviour cannot be guaranteed.')
            self.__smp_b = samples
            self.__smp_e = None
        else:
            if self.__stop_end:
                logging.warn('This Setup requests stopping on end of file. However, reading may stop before due to the '
                             'end of the range being specified: conversely, the process may stop before any samples '
                             'have been read. Hence, correct behaviour cannot be guaranteed.')
            if samples[0] < 0 or samples[-1] < 0:
                raise TCPOException(TCPOCode.Error.VALUE, 'When specifying range, negative indexing is not allowed')
            self.__smp_b = samples[0]
            self.__smp_e = samples[-1]

        # Initialise the file manager, which while a state parameter will actually not change between runs...
        self.__file_mgr = ChannelFormatFileManager(root_path=filter_model['path'], daq_mode=FileDAQModes.Integrated)

        # Create list of tile objects
        self.__tiles = [ChannelStageReader.TileState(id) for id in tupler(filter_model['tiles'])]

        # Indicate Success:
        return TCPOCode.Status.SETUP

    def _generator_loop(self):
        """
        Implementation of the Generator Loop

        :return: None - communication is through the Queue
        """
        # Start Initialisation (equivalent to will_start)
        for tile in self.__tiles: tile.reset()
        self.__start = tm.time()                # Register Start Time (this is more for debugging/logging)

        # Handle Exceptions
        try:
            # Flags (state)
            files_opened = np.zeros(len(self.__tiles), dtype=bool)
            samples_read = np.zeros(len(self.__tiles), dtype=bool)

            # Loop
            while not self._UserStop.wait(self.__sleep_rt):  # Start iterating until user requests stop
                # Loop Control is done first (this way, breaking from within tile iteration due to user-stop does not
                #   _inform)

                # First check if requested end after all read once...
                if self.__stop_end and files_opened.all(): # Files cannot have been opened until after first iteration
                    self._inform(TCPOCode.Request.STOP_FILE, 'Stopping on End of File')
                    break

                # Check across all tiles if end of samples has been reached
                if self.__smp_e and samples_read.all():
                    self._inform(TCPOCode.Request.STOP_SMPL, 'Stopping after all Samples read')
                    break

                # Else, continue with Tile Iteration
                for t_i, tile in enumerate(self.__tiles):
                    files_opened[t_i] = self.__read_samples(tile)        # Read All available Samples for this tile.
                    samples_read[t_i] = tile.next_read == tile.end_at   # Check if all required have been read
                    if self._UserStop.is_set(): break                   # Break from inner loop if user stop requested.

        except TCPOException as te:
            self._inform(te.Code, te.Msg)
        except BaseException as be:
            self._inform(TCPOCode.Error.MODULE, '%s Exception in CSR' % type(be).__name__)

        self.__log_time()

    def __read_samples(self, tile):
        """
        Individual Tile Reader

        Encapsulates reading samples from a single tile: the function will return when all available samples in the
        specified tile have been read, or immediately if the tile does not exist yet.

        :param tile: Tile Object to operate on
        :return: True if the file was opened, False otherwise (still inexistent). Errors are thrown as exceptions.
        """
        # Check Stop Flag
        if self._UserStop.is_set(): return tile.next_read > -1

        # Get Metadata, irrespective of state
        md = self.__file_mgr.get_metadata(self.__file_ts, tile.id)

        # If still unconfigured, attempt configuration
        if tile.next_read < 0:  # still not read file

            # Check if we have anything to configure from
            if md is None:
                logging.getLogger().warn('Tile ' + str(tile.id) + ' still has no data.')
                return False  # Still Waiting, not yet opened

            # Read in the available data
            tile.antennas = range(md['n_antennas'])
            tile.channels = range(md['n_chans'])
            tile.pols = range(md['n_pols'])

            # Now check the start samples depending on the value of the begin flag:
            if self.__smp_b is None:
                tile.next_read = 0
            elif self.__smp_b < 0:  # Instructed to read from end of file...
                tile.next_read = md['written_samples'] + self.__smp_b
            else:
                tile.next_read = self.__smp_b

            # Check also the end samples: just None if no end specified, else
            tile.end_at = self.__smp_e

        # Check Stop Flag again
        if self._UserStop.is_set(): return True # If made it this far, then file was read...

        # Check if the start point is available: if not, cannot read yet.
        if tile.next_read >= md['written_samples']: return True

        # Check the number of samples to read
        _max_read = min(tile.end_at, md['written_samples']) if tile.end_at is not None else md['written_samples']
        _unread = _max_read - tile.next_read  # Number of samples left to read

        # Deal with Blocks (and efficiency)
        _block_read = self.__block_rd

        # Loop:
        while (_unread > 0) and (not self._UserStop.is_set()):

            # Check how much I can read at one go...
            while _block_read > _unread:
                _block_read //= 2  # This will keep dividing the block-read rate until less than unread

            # Read Specified number & update left to read...
            self.__read_block(tile, _block_read)
            _unread = _max_read - tile.next_read

            # Output some debugging information
            self.__log_time(tile)

        # Return Opened
        return True

    def __read_block(self, tile, block_size):
        """
        Block Reading

        This Method encapsulates reading of a single block of data from a single tile and formatting it by sample.
        :param tile: The Tile to read from (reference to tile placeholder)
        :param block_size: The Number of samples to read at one go. In any case, samples are pushed one at a time.
        :return: None
        """
        sample, ts = self.__file_mgr.read_data(n_samples=block_size, sample_offset=tile.next_read, tile_id=tile.id,
                                               channels=tile.channels, antennas=tile.antennas, timestamp=self.__file_ts,
                                               polarizations=tile.pols)
        sample = sample.swapaxes(0, 2).swapaxes(0, 1)

        # Distribute on a sample by sample basis to the Consumers
        for s in range(block_size):
            # Inform consumers
            self._push(Message((Message.SelType.TILE, tile.id), tile.next_read + s, ts[s, 0], self.Name,
                               sample[:, :, :, s]),
                       None)
            # Stop if instructed
            if self._UserStop.is_set(): break

        # Update Tile to reflect number read for next iteration
        tile.next_read += block_size

    def __log_time(self, tile=None):

        if tile is None:
            logging.info('Time Elapsed = %s', tm.strftime("%H:%M:%S", tm.gmtime(tm.time() - self.__start)))

        else:
            if self.__smp_e:
                time_rem = (tile.end_at - tile.next_read) * \
                           ((tm.time() - self.__start) / float(tile.next_read - self.__smp_b))
                logging.getLogger().info('Tile %d : Next Read %d : Time Remaining : %s',
                                         tile.id, tile.next_read, tm.strftime("%H:%M:%S", tm.gmtime(time_rem)))
            else:
                logging.getLogger().info('Tile %d : Next Read %d ', tile.id, tile.next_read)

