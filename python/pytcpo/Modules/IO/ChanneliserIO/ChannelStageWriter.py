import time as tm

from pydaq.persisters import ChannelFormatFileManager as CFFM
from pydaq.persisters import FileDAQModes

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode


class ChannelStageWriter(AbstractProcessor):
    """
    Implements the Channel-Stage Writing of data, in the same format used by the Channel Stage Reader. The class is
    typically to be used by data emulation components.

    Requires:
        * Channel Bandpass Power Data per Antenna/Polarisation (3D Matrix, order A/P/Chan) [Default]
    Outputs:
        * None
    Archives:
        * None

    Limitations:
        a) The current architecture does not distinguish between different configurations per tile. This is however also
            a limitation of the underlying DAQ system, which means that in some way, the data must be padded to the
            largest possible size.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(ChannelStageWriter, self).__init__()
        # Setup Parameters
        self.__append = None    # Indicate if will be appending
        self.__file_ts = None   # The Time-Stamp to associate with the file

        # State Parameters
        self.__file_mgr = None      # The File Manager object
        self.__start_app = None     # Indicates if at the start, we should append (first sample: after that, will always
                                    # append)

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Python Dictionary containing 7 (4 Compulsary, 3 Optional) Parameters:
            * path - [Required]: The Path were to store the data. This should be a directory.
            * antennas - [Required]: The Number of Antennas to be stored (per Tile)
            * polaris - [Required]: The Number of polarisations to be stored (per Antenna)
            * channels - [Required]: The Number of Channels (per Antenna/Pol)
            * time - [Optional]: The TimeStamp to associate with the file. If None (default) uses current time.
            * type - [Optional]: String representation of the type of data to be stored. Defaults to 'uint16'
            * append - [Optional]: If True (Default), then appends data to any existing file with the same 'time'-
                        stamp. If not, then it is overwritten
        :return:
        """
        # First Setup Parameters
        self.__append = filter_model.get('append', True)
        self.__file_ts = filter_model['time'] if filter_model.get('time') else tm.time()

        # Now the File Manager. Although conceptually a state parameter, it is actually the same throughout successive
        # runs. Similarly holds for the metadata...
        self.__file_mgr = CFFM(root_path=filter_model['path'], daq_mode=FileDAQModes.Integrated,
                               data_type=filter_model.get('type', b'uint16'))
        self.__file_mgr.set_metadata(n_antennas=filter_model['antennas'], n_pols=filter_model['polaris'],
                                    n_chans=filter_model['channels'], n_samples=1, timestamp=self.__file_ts)

        # Return Success
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method
        Updates the Append Flag as necessary

        :return: None
        """
        self.__start_app = self.__append

    def _new_sample(self, message, forward):
        # Store the Data. This involves swapping the axes into the format required by the DAQ. Note that in our case,
        # the sampling_time is immaterial, since we store 1 sample at a time (and hence left to default)...
        self.__file_mgr.ingest_data(data_ptr=message.Default.swapaxes(0, 2).swapaxes(1, 2), timestamp=self.__file_ts,
                                    append=self.__start_app, buffer_timestamp=message.Time, tile_id=message.Selector)

        # Now update the append, since from now on will always be append...
        if not self.__start_app: self.__start_app = True

    def _clean_up(self, reason):
        """
        Cleaning Method

        (Empty, since will_start handles state)
        """
        pass

