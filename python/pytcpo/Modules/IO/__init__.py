## Contains the Implementation for the Channelizer-Output Stage Reader
__author__="Michael Camilleri"

from pytcpo.Core.PipeLine import PipelineManager
from pytcpo.Modules.IO.ChanneliserIO.ChannelStageWriter import ChannelStageWriter
from pytcpo.Modules.IO.ChanneliserIO.ChannelStageReader import ChannelStageReader
from pytcpo.Modules.IO.PointingIO.PointingRequester import PointingRequester

PipelineManager.registerModule(ChannelStageWriter)
PipelineManager.registerModule(ChannelStageReader)
PipelineManager.registerModule(PointingRequester)