from pytcpo.Core.PipeLine import AbstractGenerator, Message
from pytcpo.Core.Common import TCPOCode

class PointingRequester(AbstractGenerator):

    """
    The PointingRequester module is a simple generator class to handle pointing requests and present them as required
    by the Pointing module. Requests received by this module are to take the form of a 1D Numpy array that is pushed onto
    a queue.

    Required forwarded data :
    i) 1D Numpy Array of the form Beams/[Beam Index, Pointing Time, RA, Dec]

    Outputted data in message.Default :
    i) List of the form [RA, Dec]

    Notes:
    i) When forwarding to a module, Beam Index and Pointing Time are stored in message.Selector and message.Time respectively, with the SelectorMask
       set to BEAM.
    """

    def __init__(self):
        super(PointingRequester, self).__init__()

        self.request_q = None
        self.tm = None
        self.index = None

    def _setup(self, filter_model):

        """
        Setup Method

        :param filter_model: Dictionary with 1 required parameter:
                            * pointing_requests - Queue in which pointing requests of the form of 2D Numpy Array
                                                  are to be received.

        :return: None
        """

        self.request_q = filter_model['pointing_requests']
        self.tm = filter_model['TelescopeModel']

    def _generator_loop(self):
        """
        Generates one sample at a time and forwards generated data as the Default in a message to the next module down
        the line.
        """

        self.index = 0

        while not self._UserStop.is_set():
            while not self.request_q.empty():

                beam = self.request_q.get() # 1D numpy array of the form [Beam Index, Pointing Time, RA, Dec]

                try:
                    self.tm.beams[beam[0]]
                except KeyError:
                    print 'Skipping Beam: Beam ' + str(beam[0]) + ' from request ' + str(beam) + ' was not found in Telescope Model'
                    continue

                msg = Message((Message.SelType.BEAM, beam[0]), self.index, beam[1], self.Name)
                msg.Default = [beam[2], beam[3]] # list of the form [RA, Dec]
                self.index += 1
                self._push(msg,None)

                if self._UserStop.is_set() is True:
                    break

        self._inform(TCPOCode.Request.STOP_USER)