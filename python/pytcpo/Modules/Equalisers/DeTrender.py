import numpy as np

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode


class PolyDeTrender(AbstractProcessor):
    """
    Performs De-Trending of the Channel-Bandpass data based on the polynomial model for that time-stamp. This amounts
    to subtracting the polynomial evaluated at each channel from the actual channel data. De-Trending is otherwise
    sampling/antenna/polarisation independent.

    Requires:
        * Channel Bandpass Power Data per Antenna/Polarisation [Default]
        * Polynomial-evaluated Channel BandPassPower Data
    Outputs:
        * The De-Trended Channel BandPass Power Data
    Archives:
        * The De-Trended Channel BandPass Power Data
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(PolyDeTrender, self).__init__()
        self.__model = None # Reference to the Coefficients

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Dictionary containing 1 (required) parameter:
                * model - [Required]: The Reference to the data item containing the model coefficients.
        :return:
        """
        self.__model = filter_model['model']
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method

        (Empty - Stateless Module)
        """
        pass

    def _new_sample(self, message, forward):
        """
        Sample Handler

        (Standard Parameter/Return Types)
        """
        # Create Data Matrix
        forward[0] = message.Default - message[self.__model]

        # Archive Data
        forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Clean Function

        (Empty)
        """
        pass
