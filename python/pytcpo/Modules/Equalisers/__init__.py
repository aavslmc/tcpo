__author__ = 'Michael Camilleri'

from DeTrender import PolyDeTrender
from SimpleEqualisers import SampleEqualiser
from BitShiftEqualiser import BitShiftEqualiser

from pytcpo.Core.PipeLine import PipelineManager
PipelineManager.registerModule(PolyDeTrender)
PipelineManager.registerModule(SampleEqualiser)
PipelineManager.registerModule(BitShiftEqualiser)