import numpy as np

from pytcpo.Core.Archiving import ASerialiseable
from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode

# OBSOLETE - Not Functional


class SampleEqualiser(AbstractProcessor):
    """
    The Class implements a simple sample-independent equaliser based on the interplay between other antennas/
    polarisations. In effect, the mean is computed across antennas/polarisations as required (configurable) and this
    mean subtracted from the raw data (resulting in the signaling being centred around 0).

    Requires:
        * Channel Bandpass Power Data per Antenna/Polarisation [Default]
    Outputs:
        * The Equalised Channel BandPass Power Data
    Archives:
        * The Shift Value per Antenna/Polarisation encapsulated in a ShiftArchive object
    """
    # Dictionary which serves a two-fold purpose:
    #   - Identifying the allowable options
    #   - Mapping Options to indices over which to average
    ALLOWABLES = {'o': 2, 'a': (0, 2), 'p': (1, 2)}

    class ShiftArchive(ASerialiseable):
        """
        Shift Archive Data Type for encapsulating the shifting performed.
        """
        def __init__(self, axes = None, shift = None):
            self._axes = axes
            self._shift = shift

        def serial(self):
            return { '_axes' : str, '_shift' : np.ndarray }

    def __init__(self):
        """
        Default Initialiser
        """
        super(SampleEqualiser, self).__init__()
        # Setup Parameters
        self.__axes = None  # Axes over which to shift

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Dictionary with 1 (optional) parameter:
                * type - [Optional]: Indicates the axes over which to perform equalisation. Can be one of the following
                    (case-insensitive):
                    > 'o' : Equalise only over channels, treating each Antenna/Polarisation independently [default]
                    > 'a' : Equalise over Antennas, treating Polarisations independently
                    > 'p' : Equalise over Polarisations, treating Antennas independently
        :return: Status.SETUP
        :raises TypeError, KeyError: if filter_model is not a dictionary or the passed type is not supported.
        """
        self.__axes = self.ALLOWABLES[filter_model.get('type', 'o').lower()]

        # Return Status Setup
        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method Indication

        (Empty - Stateless)
        """
        pass

    def _new_sample(self, message, forward):
        # Need to copy the Matrix since we are modifying in place
        forward[0] = np.empty(message.Default.shape, dtype=message.Default.dtype)

        # Shift is defined as a local (across channels only) mean minus a global mean (governed by axes)
        # However, for the case of antenna-independence, have to work with transpose
        if self.__axes == 'p':
            shift = (np.mean(message.Default, 2).transpose() - np.mean(message.Default, (1,2))).transpose()  # Calculate shift
            shift_archive = self.ShiftArchive('p', np.squeeze(shift[:,0]))
        elif self.__axes == 'a':
            shift = np.mean(message.Default, 2) - np.mean(message.Default, (0,2))  # Calculate shift
            shift_archive = self.ShiftArchive('a', np.squeeze(shift[0, :].squeeze()))
        else:
            shift = np.mean(message.Default, 2) - np.mean(message.Default)  # Calculate shift
            shift_archive = self.ShiftArchive('ap', np.squeeze(shift))

        # Perform Shift and update
        for channel in range(np.shape(message.Default)[2]):
            forward[0][:, :, channel] = message.Default[:, :, channel] - shift

        # Archiving
        forward[1] = shift_archive

    def _clean_up(self, reason):
        pass
