import numpy as np
import threading as td
from collections import deque

from pytcpo.Core.PipeLine import AbstractProcessor
from pytcpo.Core.Common import TCPOCode


class BitShiftEqualiser(AbstractProcessor):
    """
    This Module returns the number of bits to shift each channel index by to achieve a normalised bandpass, taking into
    consideration the shifting and integration operations already performed on the data. Basically, this means that:

        output = -log_2(desired/message) + scaling + selection + -log_2(integration) + log_2(divisor)

    This module presents the need for feedback from the TANGO pipeline to update truncation scale, since this may also
    be modified externally (and hence cannot depend solely on an internal history). To this end, a callback (API)
    method is provided which can be invoked to update the current value. The module itself maintains a history which
    is updated when the output is set. Currently thread-safety is achieved through a mutex: however, if this starts
    causing dead-locks, then the implementation may be switched to queues.

    The Module handles invalid values in the following manner:
        > NaN Channels will be given an invalid-shift of -100 (error signal)
        > 0-Valued (or less than 1.0) channels will be given a shift of 0 (since in any case, shifting has no effect)

    Requires:
        * 3D Real-valued Numpy Array of the channel bandpass power (may be pre-filtered, order A/P/Chan) [Default]
    Outputs:
        * The De-Trending bit-shifts (3D Integer Numpy Array), typically in the range (0, 7) inclusive, positive
           indicating right-shifts, per Antenna/Polarisation/Channel
    Archives:
        * The De-Trending bit-shifts (as above)

    >=================================================================================================================<

    An Explanation is in order for the computation scheme:

     * The Channeliser spits out 18-bit data.

     * To this our equaliser applies a bit-shift of between 0 & 7, inclusive. This means that the least significant
        x-bits are truncated. At the same time, a corresponding amount of MSbits are removed (or added) so that the
        resulting data is 12 bits long (i.e. if our truncation is 0, then 6 bits have to be removed: but if our
        truncation is 7 bits, then one 0-bit has to be added).

     * Next the MSB only is taken and integrated over a number of samples.

     * A Scale factor is applied which serves to 'average' out the integration process (usually this is chosen such that
        the same dynamic range is retained)

     * The data is output to the pipeline as 16-bit unsigned integer.

    A numerical example will help to explain the process. We will adopt the convension that positive bit-shifts are to
    the right. Let there be a series of same-valued samples at 4096 (such that the integrator and scale-factor do not
    change anything): i.e.
        channeliser = 4096 = 0b000001000000000000 = 0x1000
    Let us also assume that the current truncation_scaler is at 2. This means that the 4096 is right shifted by 2 bits:
        trunc_scale = 1024 = 0b010000000000 = 0x400
    We will take the MSB: this amounts to further right shifting the trunc_scale output by 3 bits:
        msb_select = 128 = 0b01000000 = 0x80
    Now assume we integrate over 16 samples (scaling is log_2[16] = 4 to the left):
        integrate = 2048 = 0b...0000100000000000 = 0x000000000800
    Now for this example, assume we divide by 8 (log_2[8] = 3 to the right):
        scale_fact = 256 = 0b0000000100000000 = 0x0100

    Now Assume that we want to detrend our signal to a mean of 2048. This implies left-shifting our resultant data by 3.
    However, we now add the contribution of previous modules: -3 + 2 + 3 + -4 + 3 = 1: i.e. a right shift of 1, which
    is indeed the correct way to achieve 2048 from 4096.
    """

    INVALID_INDICATOR = -100

    def __init__(self):
        """
        Default Initialiser
        """
        super(BitShiftEqualiser, self).__init__()
        # Setup Parameters
        self.__desired = None       # The Desired value for the channels
        self.__select_offset = None # Selection Offset
        self.__integration = None   # Number of Samples in integration
        self.__scale_init = None    # Initial value for scaling
        self.__division = None      # Division after integration

        # State Parameters
        self.__scale = None         # Truncation Scale: this will be updated as necessary
        self.__scale_queue = None   # Scaling Queue
        self.__scale_lock = None    # Lock around scaler

    def _setup(self, filter_model):
        """
        Setup Method

        :param filter_model: Dictionary containing 5 (4 Compulsary, 1 Optional) parameters:
                * desired - [Required]: The preferred mean value to shift samples to.
                * select_offset - [Required]: The offset (bits) truncated when selecting the MSB (positive value)
                * integration - [Required]: The number of samples in integration
                * scaling - [Required]: The original scaling bit-shift to start off per Antenna/Pol/Channel. Note that
                            these are taken to be bit-shifts to the right although positive valued
                * division - [Optional]: The divisor (absolute) after integration. If not specified (or None) then
                        defaults to the same value as integration

        :return:
        """
        # SetUp Lock
        self.__scale_lock = td.Lock()

        # Setup
        self.__desired = float(filter_model['desired'])
        self.__select_offset = int(filter_model['select_offset'])
        self.__integration = -np.log2(filter_model['integration'])
        self.__scale_init = filter_model['scaling']

        divisor = filter_model.get('division')
        self.__division = np.log2(divisor) if divisor else -self.__integration

        return TCPOCode.Status.SETUP

    def _will_start(self):
        """
        Start Method
        """
        with self.__scale_lock:
            # Deque must be created in this manner, else will treat as individual elements
            self.__scale_queue = deque()
            self.__scale_queue.appendleft((-1.0, self.__scale_init))

    def _new_sample(self, message, forward):
        """
        Sample Handler

        (Standard Parameter/Return Types)
        """
        # Lock around the truncation scaler
        with self.__scale_lock:

            # First check if need to update self.__scale
            while len(self.__scale_queue) > 0 and (message.Time > self.__scale_queue[-1][0]):
                self.__scale = self.__scale_queue.pop()[1].astype(float)

            # Find those less than 1.0: these will be given a shift of 0 in the end
            less_than_1 = np.less(message.Default, 1.0)

            # Find those which were invalid previously - these will also be given a current shift of 0
            self.__scale[self.__scale == BitShiftEqualiser.INVALID_INDICATOR] = 0.0

            # Compute Mean Shift - NaN's will be preserved: note that we negate since we are using the opposite
            #   convention internally.
            self.__scale += -np.log2(np.divide(self.__desired, message.Default)) + self.__select_offset \
                            + self.__integration + self.__division

            # Update mean-shift to clean NaN's and ensure that those less than 1 are zeroed
            self.__scale[np.isnan(self.__scale)] = BitShiftEqualiser.INVALID_INDICATOR
            self.__scale[less_than_1] = 0.0

            # Set Forward & Archive data
            forward[0] = np.rint(self.__scale).astype(int)
            forward[1] = forward[0]

    def _clean_up(self, reason):
        """
        Clean Function

        (Empty)
        """
        pass

    def update_scale(self, bit_shifts, time_valid=-1.0):
        """
        External Callback

        This method may be used to indicate of a change in the currently applied scaling factor which comes about due to
        an external process. The time-stamp at which this change becomes valid is also passed.

        Note that the method may block.

        :param bit_shifts: The integer ndarray of bit-shifts per Antenna/Polarisation/Channel
        :param time_valid: The time at which this is valid: defaults to -1 (immediate)
        :return: None
        """
        with self.__scale_lock:
            self.__scale_queue.appendleft((time_valid, bit_shifts))

