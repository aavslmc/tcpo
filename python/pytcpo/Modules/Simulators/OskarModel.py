import numpy as np
import os
import datetime
import h5py
import logging
import subprocess
import shutil
import warnings
try:
    from casacore import tables
except ImportError:
    logging.warning('CasaCore not found.')

from pytcpo.Core.PipeLine import AbstractGenerator, Message
from pytcpo.Core.Common import TCPOCode


class OskarSetupBuilder(object):

    """
    The OSKARSetupBuilder class creates an OSKAR settings configuration file, necessary for running an 
    OSKAR-2 interferometer simulation, in .ini format.

    From the TM singleton, reads a dictionary named 'params', containing the required fields to complete a settings
    config file suitable for running an OSKAR interferometer simulation, as well as to obtain a beam
    pattern and sky image using OSKAR's beam pattern simulator and imager, if required.

    """

    class Sky:
        def __init__(self):
            self.Dir = None

    class Observation:
        def __init__(self):
            self.PCRA = None
            self.PCDec = None
            self.TSteps = None
            self.TStart = None
            self.TNext = None
            self.TLength = None
            self.StartFreq = None
            self.IncreFreq = None
            self.NumChan = None

    class Telescope:
        def __init__(self):
            self.Dir = None
            self.Long = None
            self.Lati = None
            self.PolM = None

    class Interferometer:
        def __init__(self):
            self.MS_Dir = None
            self.Bandwith = None
            self.TAverage = None
            self.VisF_Dir = None
            self.H5_Dir = None

    class BeamPattern:
        def __init__(self):
            self.Dir = None
            self.Img_FOV = None
            self.Img_Size = None
            self.CoordFrame = None

    class Image:
        def __init__(self):
            self.Dir = None
            self.Img_FOV = None
            self.Img_Size = None
            self.Img_Type = None

    def __init__(self):
        
        self.sky = OskarSetupBuilder.Sky()
        self.observation = OskarSetupBuilder.Observation()
        self.telescope = OskarSetupBuilder.Telescope()
        self.interferometer = OskarSetupBuilder.Interferometer()
        self.beampattern = OskarSetupBuilder.BeamPattern()
        self.image = OskarSetupBuilder.Image()
        self.ini_filename_1 = None
        self.ini_filename_2 = None
        self.ini_filename_3 = None
        self.UVW = None
        self.antenna1 = None
        self.antenna2 = None
        self.model_data = None
        self.basl_no = None
        self.model_data = None
        self.logger = logging.getLogger(__name__)

    def setup_config(self, params, i, counter, _dir, sky_model_out, zenith_ra, zenith_dec):

        if i == 0:

            ms_direct_path = _dir + '/MSets'
            vf_direct_path = _dir + '/VisF'
            h5_direct_path = _dir + '/H5Files'
            setups_direct_path = _dir + '/Setups'
            bp_direct_path = _dir + '/BPatterns'
            im_direct_path = _dir + '/Images'

            if os.path.exists(ms_direct_path):
                shutil.rmtree(ms_direct_path)
            subprocess.call(['mkdir', ms_direct_path])
            if os.path.exists(vf_direct_path):
                shutil.rmtree(vf_direct_path)
            subprocess.call(['mkdir', vf_direct_path])
            if os.path.exists(h5_direct_path):
                shutil.rmtree(h5_direct_path)
            subprocess.call(['mkdir', h5_direct_path])
            if os.path.exists(setups_direct_path):
                shutil.rmtree(setups_direct_path)
            subprocess.call(['mkdir', setups_direct_path])

            if os.path.exists(bp_direct_path):
                shutil.rmtree(bp_direct_path)
            if os.path.exists(im_direct_path):
                shutil.rmtree(im_direct_path)
            if params.testing_req is True:
                subprocess.call(['mkdir', bp_direct_path])
                subprocess.call(['mkdir', im_direct_path])

        self.sky.Dir = sky_model_out

        self.observation.TStart = datetime.datetime.strptime(params.StartTime, "%d-%m-%Y %H:%M:%S.%f") + \
            datetime.timedelta(0, int(i * params.TStepLength), 0, 0, 0, 0)
        self.observation.TNext = datetime.datetime.strptime(params.StartTime, "%d-%m-%Y %H:%M:%S.%f") + \
            datetime.timedelta(0, int(i+1 * params.TStepLength), 0, 0, 0, 0)
        self.observation.PCRA = zenith_ra
        self.observation.PCDec = zenith_dec
        self.observation.TSteps = 1
        self.observation.TLength = params.TStepLength

        frequency_increment = np.float(params.EndFreq - params.StartFreq) / np.float(params.NumChan)
        self.observation.StartFreq = (params.StartFreq + (frequency_increment * counter))

        self.observation.IncreFreq = 1
        self.observation.NumChan = 1

        self.telescope.Dir = _dir + '/TelModel/telescope.tm'
        self.telescope.Long = params.cen_lon
        self.telescope.Lati = params.cen_lat
        self.telescope.PolM = params.PolMode

        self.interferometer.MS_Dir = _dir + '/MSets/' + str(self.observation.TStart.date()) + \
                               str(self.observation.TStart.time()) + '.ms'
        self.interferometer.Bandwith = params.Bandwith
        self.interferometer.TAverage = params.TimeAverage
        self.interferometer.VisF_Dir = _dir + '/VisF/VisF.vis'
        self.interferometer.H5_Dir = _dir + '/H5Files/Model.h5'

        self.beampattern.Dir = _dir + '/BPatterns/' + str(self.observation.TStart.date()) + \
                               str(self.observation.TStart.time())
        self.beampattern.Img_FOV = params.BPattern_FOV
        self.beampattern.Img_Size = params.BPattern_Size
        self.beampattern.CoordFrame = params.BPattern_CoordFrame

        self.image.Dir = _dir + '/Images/' + str(self.observation.TStart.date()) + str(self.observation.TStart.time())
        self.image.Img_FOV = params.Img_FOV
        self.image.Img_Size = params.Img_Size
        self.image.Img_Type = params.Img_Type

    def setup_parser(self, _dir, counter, test):

        self.ini_filename_1 = _dir + "/Setups/oskar_sim_interferometer" + str(counter) + ".ini"

        setup_data_1 = (
            '[General]\n'
            'app=oskar_sim_interferometer\n'
            'version=2.7.0\n\n'
    
            '[simulator]\n'
            'double_precision=false\n\n'
    
            '[sky]\n'
            'oskar_sky_model/file=' + str(self.sky.Dir) + '\n\n'
    
            '[observation]\n'
            'num_channels=' + str(self.observation.NumChan) + '\n'
            'start_frequency_hz=' + str(self.observation.StartFreq) + '\n'
            'frequency_inc_hz=' + str(self.observation.IncreFreq) + '\n'
            'phase_centre_ra_deg=' + str(self.observation.PCRA) + '\n'
            'phase_centre_dec_deg=' + str(self.observation.PCDec) + '\n'
            'num_time_steps=' + str(self.observation.TSteps) + '\n'
            'start_time_utc=' + str(self.observation.TStart) + str('.000') + '\n'
            'length=' + str(self.observation.TLength) + '\n\n'
    
            '[telescope]\n'
            'pol_mode=' + str(self.telescope.PolM) + '\n'
            'station_type=Aperture array\n'
            'normalise_beams_at_phase_centre=true\n'
            'aperture_array/element_pattern/enable_numerical=false\n'
            'input_directory=' + str(self.telescope.Dir) + '\n\n'
    
            '[interferometer]\n'
            'oskar_vis_filename=' + str(self.interferometer.VisF_Dir) + '\n'
            'ms_filename=' + str(self.interferometer.MS_Dir) + '\n'
            'channel_bandwidth_hz=' + str(self.interferometer.Bandwith) + '\n'
            'correlation_type=Both' + '\n' 
            'time_average_sec=' + str(self.interferometer.TAverage) + '\n\n'
        )

        setup_file = open(self.ini_filename_1, 'w')
        setup_file.write(setup_data_1)
        setup_file.close()

        if test is True:
            self.ini_filename_2 = _dir + "/Setups/oskar_sim_beam_pattern" + str(counter) + ".ini"

            setup_data_2 = (
                '[General]\n'
                'app=oskar_sim_beam_pattern\n'
                'version=2.7.0\n\n'
    
                '[simulator]\n'
                'double_precision=false\n\n'
    
                '[observation]\n'
                'num_channels=' + str(self.observation.NumChan) + '\n'
                'start_frequency_hz=' + str(self.observation.StartFreq) + '\n'
                'frequency_inc_hz=' + str(self.observation.IncreFreq) + '\n'
                'phase_centre_ra_deg=' + str(self.observation.PCRA) + '\n'
                'phase_centre_dec_deg=' + str(self.observation.PCDec) + '\n'
                'num_time_steps=' + str( self.observation.TSteps) + '\n'
                'start_time_utc=' + str(self.observation.TStart) + str('.000') + '\n'
                'length=' + str(self.observation.TLength) + '\n\n'
    
                '[telescope]\n'
                'pol_mode=' + str(self.telescope.PolM) + '\n'
                'station_type=Aperture array\n'
                'normalise_beams_at_phase_centre=false\n'
                'aperture_array/element_pattern/enable_numerical=false\n'
                'aperture_array/array_pattern/normalise=false\n'
                'input_directory=' + str(self.telescope.Dir) + '\n\n'
    
                '[beam_pattern]\n'
                'coordinate_frame=' + str(self.beampattern.CoordFrame) + '\n'
                'root_path=' + str(self.beampattern.Dir) + '\n'
                'beam_image/size=' + str(self.beampattern.Img_Size) + '\n'
                'beam_image/fov_deg=' + str(self.beampattern.Img_FOV) + '\n'
                'station_outputs/fits_image/amp=true' + '\n'
                'station_outputs/fits_image/auto_power=false\n\n'
            )

            setup_file = open(self.ini_filename_2, 'w')
            setup_file.write(setup_data_2)
            setup_file.close()

            self.ini_filename_3 = _dir + "/Setups/oskar_imager" + str(counter) + ".ini"

            setup_data_3 = (
                '[General]\n'
                'app=oskar_imager\n'
                'version=2.7.0\n\n'
    
                '[image]\n'
                'fov_deg=' + str(self.image.Img_FOV) + '\n'
                'size=' + str(self.image.Img_Size) + '\n'
                'image_type=' + str(self.image.Img_Type) + '\n'
                'input_vis_data=' + str(self.interferometer.MS_Dir) + '\n'
                'root_path=' + str(self.image.Dir) + '\n'
                'direction=RA, Dec' + '\n'
                'direction/ra_deg=' + str(self.observation.PCRA) + '\n'
                'direction/dec_deg=' + str(self.observation.PCDec) + '\n\n'
            )

            setup_file = open(self.ini_filename_3, 'w')
            setup_file.write(setup_data_3)
            setup_file.close()

    def ms_to_h5(self):
        """
        MS to HDF5 converter. Converts OSKAR-generated visibilities from MS to H5 format if required.

        """

        np.set_printoptions(threshold='nan')
        tb = tables.table(self.interferometer.MS_Dir, ack=False)

        uvw = tb.getcol("UVW")
        antenna1 = np.array(tb.getcol("ANTENNA1"))
        antenna2 = np.array(tb.getcol("ANTENNA2"))
        model_data = np.array(tb.getcol("DATA"))
        basl_no = np.array(range(0, len(antenna1)))

        n_basl = len(model_data[:, 0])
        n_cross = len(model_data[0, :])

        model_data = np.transpose(model_data, (1, 0, 2))

        f = h5py.File(self.interferometer.H5_Dir, "w")
        name = "Vis_Data"
        d_set = f.create_dataset(name, (0, n_basl, 0, n_cross), dtype='c16')
        d_set[0, :, 0, :] = model_data[:, :]
        d_set2 = f.create_dataset("Baselines", (len(antenna2), 6))
        d_set2[:, :, :, :, :, :] = np.transpose([basl_no, antenna1, antenna2, uvw[0], uvw[1], uvw[2]])
        f.flush()
        f.close()

    def append_to_pathfile(self, text_file_name, i):
        """
        Append created model visibilities' timestamps to file, for reading and verification by Receiver Module.

        """

        ms_filename = str(self.observation.TStart)
        if i == 0:
            text_file = open(text_file_name, "w")
            text_file.write(ms_filename + "\n")
            text_file.close()
        if i != 0:
            text_file = open(text_file_name, "a")
            text_file.write(ms_filename + "\n")
            text_file.close()


class OskarTelescopeBuilder(object):

    """

    The OSKARModel class creates a directory hierarchy for an OSKAR-2 telescope model, necessary for 
    running an OSKAR-2 interferometer simulation. The created folder structure is saved in a known 
    location, with each antenna directed to its own station folder.

    Requirements: A numpy array of arrays, with each subset array containing each antenna's x,y,z location, 
    relative to some central location, as provided by the TelescopeModel class. 

    """

    def __init__(self):

        self.antennas = None

    def telescope_create(self, antennas, directory, telescope):

        self.antennas = antennas

        # Create new parent observation TM directory
        tel_model_path = str(directory) + '/TelModel'
        if os.path.exists(tel_model_path):
            shutil.rmtree(tel_model_path)
        subprocess.call(['mkdir', tel_model_path])
        tel_dir_name = tel_model_path + '/telescope.tm'
        subprocess.call(['mkdir', tel_dir_name])

        # Create top-level position.txt file
        position_file_name = tel_dir_name + '/position.txt'
        text_file = open(position_file_name, "w")
        text_file.write(str(telescope.cen_lon) + ' ' + str(telescope.cen_lat) + ' ' + str(telescope.cen_alt))
        text_file.close()

        # Initialize station number for antenna-station building as zero
        total_station_number = 0

        for h in range(len(antennas[:, 0])):

            # Numbering stations sequentially in obs_TM, as required by Oskar
            current_station = format(total_station_number, "03d")
            station_dir_name = tel_model_path + "/telescope.tm/station" + str(current_station)
            subprocess.call(['mkdir', station_dir_name])

            # Dumping a simple zeroed antenna position layout file per station
            stat_layout_file = station_dir_name + "/layout.txt"
            stat_layout_out = open(stat_layout_file, "a")
            stat_layout_out.write("0.0, 0.0")
            stat_layout_out.close()

            # Commit value for next station number for TM build
            total_station_number += 1

            # Create layout file
            text_file_name = str(tel_model_path) + "/telescope.tm/layout.txt"

            if h == 0:
                text_file = open(text_file_name, "w")
                text_file.write('%f' % antennas[h, 0] + ", " + '%f' % antennas[h, 1] + ", " + '%f' % antennas[h, 2] +
                                "\n")
                text_file.close()

            if h != 0:
                text_file = open(text_file_name, "a")
                text_file.write('%f' % antennas[h, 0] + ", " + '%f' % antennas[h, 1] + ", " + '%f' % antennas[h, 2] +
                                "\n")
                text_file.close()


class LocalSkyModelGen(object):

    def __init__(self):

        self.global_sky_model = None
        self.source_RAs = None
        self.source_Decs = None
        self.sky_model = {}
        self.latitude = None
        self.time_local = None
        self.date_now = None
        self.time_observation = None
        self.zenith_RA = None
        self.midnight_zenith_RA = None
        self.zenith_Dec = None
        self.position = None
        self.always_above_horizon = None
        self.always_below_horizon = None
        self.above_horizon = None
        self.below_horizon = None
        self.lsm_sources = None
        self.sun_RA = None
        self.sun_Dec = None
        self.sun_T_above = None
        self.sun_above_horizon = False

    def _gsm_reader(self, sky_model_name):

        self.global_sky_model = np.loadtxt(sky_model_name, dtype=np.float64)
        self.sky_model["source_RAs"] = self.global_sky_model[:, 0]
        self.sky_model["source_Decs"] = self.global_sky_model[:, 1]
        self.sky_model["source_T"] = np.zeros(len(self.sky_model["source_RAs"]), dtype=float)

    def _receive_observation_parameters(self, latitude, local_time, date, time_zone):

        self.latitude = latitude
        self.time_local = local_time
        self.date_now = date
        self.time_observation = datetime.datetime.combine(self.date_now, self.time_local)
        self.time_observation += datetime.timedelta(0, 0, 0, 0, 0, time_zone, 0)

    def _calculate_zenith_dec_ra(self):

        ra_daily_change_deg = 360. / 365.25
        days_seconds_from_sept_21 = self.time_observation - datetime.datetime(self.date_now.year, 9, 21, 0, 0, 0)
        days_from_sept_21 = days_seconds_from_sept_21.days
        hours_since_midnight = (((days_seconds_from_sept_21.seconds/60.)/60.)/24.)

        self.zenith_RA = (ra_daily_change_deg * days_from_sept_21) + (hours_since_midnight * 360.)
        if self.zenith_RA < 0.:
            self.zenith_RA += 360.
        if self.zenith_RA > 360.:
            self.zenith_RA -= 360.

        # Define Zenith Declination as Latitude
        self.zenith_Dec = self.latitude

        # Define RA for Zenith at Midnight on Day
        self.midnight_zenith_RA = ra_daily_change_deg * days_from_sept_21
        if self.midnight_zenith_RA < 0.:
            self.midnight_zenith_RA += 360.
        if self.midnight_zenith_RA > 360.:
            self.midnight_zenith_RA -= 360.

    def _add_sun(self):

        # Calculate Sun RA
        self.sun_RA = self.midnight_zenith_RA + 180.

        # Calculate Sun Dec
        dec_daily_change_deg = 93.76 / 365.25
        days_seconds_from_mar_21 = self.time_observation - datetime.datetime(self.date_now.year, 3, 21, 0, 0, 0)
        if days_seconds_from_mar_21.days < 0:
            self.sun_Dec = (dec_daily_change_deg * (days_seconds_from_mar_21.days -
                                                    (((days_seconds_from_mar_21.seconds/60.)/60.)/24.)))
        if days_seconds_from_mar_21.days >= 0:
            self.sun_Dec = (dec_daily_change_deg * (days_seconds_from_mar_21.days +
                                                    (((days_seconds_from_mar_21.seconds / 60.) / 60.) / 24.)))

        if self.sun_Dec < -23.44:
            self.sun_Dec = -23.44 + (-23.44 - self.sun_Dec)
        if self.sun_Dec > 23.44:
            self.sun_Dec = 23.44 - (self.sun_Dec - 23.44)
            if self.sun_Dec < -23.44:
                self.sun_Dec = -23.44 + (-23.44 - self.sun_Dec)

    def _calculate_dec_ranges(self):

        dec_zenith_range_above_horizon = [self.zenith_Dec - 90., self.zenith_Dec + 90.]

        if dec_zenith_range_above_horizon[0] < -90.:
            self.position = 'Southern_Hemisphere'
            self.always_above_horizon = -90. + (abs(dec_zenith_range_above_horizon[0]) - 90.)
            self.always_below_horizon = dec_zenith_range_above_horizon[1]
            dec_zenith_range_above_horizon[0] = -90.
        if dec_zenith_range_above_horizon[1] > 90.:
            self.position = 'Northern_Hemisphere'
            self.always_above_horizon = 90. - (abs(dec_zenith_range_above_horizon[1]) - 90.)
            self.always_below_horizon = dec_zenith_range_above_horizon[0]
            dec_zenith_range_above_horizon[1] = 90.
        if dec_zenith_range_above_horizon[0] == -90. and dec_zenith_range_above_horizon[1] == 90:
            self.position = 'Equator'
            self.always_above_horizon = 90.0
            self.always_below_horizon = -90.0

    def _determine_source_duration_above_horizon(self):

        for i in range(len(self.sky_model["source_RAs"])):

            if self.position == 'Northern_Hemisphere':
                if float(self.sky_model["source_Decs"][i]) >= self.always_below_horizon:
                    if float(self.sky_model["source_Decs"][i]) < self.always_above_horizon:
                        dec = np.deg2rad(self.sky_model["source_Decs"][i])
                        lat = np.deg2rad(self.latitude)
                        h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                        self.sky_model["source_T"][i] = np.degrees(h) / 7.42
                        continue
                if float(self.sky_model["source_Decs"][i]) < self.always_below_horizon:
                    self.sky_model["source_T"][i] = 0.
                if float(self.sky_model["source_Decs"][i]) >= self.always_above_horizon:
                    self.sky_model["source_T"][i] = 24.

            if self.position == 'Equator':
                dec = np.deg2rad(self.sky_model["source_Decs"][i])
                lat = np.deg2rad(self.latitude)
                h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                self.sky_model["source_T"][i] = np.degrees(h) / 7.42

            if self.position == 'Southern_Hemisphere':
                if float(self.sky_model["source_Decs"][i]) <= self.always_below_horizon:
                    if float(self.sky_model["source_Decs"][i]) > self.always_above_horizon:
                        dec = np.deg2rad(self.sky_model["source_Decs"][i])
                        lat = np.deg2rad(self.latitude)
                        h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
                        self.sky_model["source_T"][i] = np.degrees(h) / 7.42
                        continue
                if float(self.sky_model["source_Decs"][i]) > self.always_below_horizon:
                    self.sky_model["source_T"][i] = 0.
                if float(self.sky_model["source_Decs"][i]) <= self.always_above_horizon:
                    self.sky_model["source_T"][i] = 24.

        # Determine sun time above horizon
        dec = np.deg2rad(self.sun_Dec)
        lat = np.deg2rad(self.latitude)
        h = np.arccos((-np.sin(lat) * np.sin(dec)) / (np.cos(lat) * np.cos(dec)))
        self.sun_T_above = np.degrees(h) / 7.42

    def _define_sources_above_horizon(self):

        self.above_horizon = []
        self.below_horizon = []
        for i in range(len(self.sky_model["source_RAs"])):
            # If source is always below horizon
            if self.sky_model["source_T"][i] == 0.:
                self.below_horizon.append(i)
                continue
            # If source is always above horizon
            if self.sky_model["source_T"][i] == 24.:
                self.above_horizon.append(i)
                continue
            # If source IS NOT always above or below horizon, compute source time range above horizon
            ra_difference = self.midnight_zenith_RA - float(self.sky_model["source_RAs"][i])
            time_at_meridian = datetime.datetime(self.date_now.year, self.date_now.month, self.date_now.day, 0, 0, 0) \
                - datetime.timedelta(seconds=(ra_difference / 360. * (24. * 60. * 60.)))

            time_range_above_horizon = [
                (time_at_meridian - datetime.timedelta(hours=0.5 * float(self.sky_model["source_T"][i]))),
                (time_at_meridian + datetime.timedelta(hours=0.5 * float(self.sky_model["source_T"][i])))]

            # Verify correct date computation, or correct for it, for analysis of whether source is in sky
            if time_range_above_horizon[1] < self.time_observation:
                time_range_above_horizon[0] += datetime.timedelta(hours=24.)
                time_range_above_horizon[1] += datetime.timedelta(hours=24.)

            # Verify if source is above horizon during observation, or otherwise
            if self.time_observation >= time_range_above_horizon[0]:
                if self.time_observation <= time_range_above_horizon[1]:
                    self.above_horizon.append(i)
                    continue
            self.below_horizon.append(i)

        # Check if Sun above or below horizon
        ra_difference = self.midnight_zenith_RA - float(self.sun_RA)
        time_at_meridian = datetime.datetime(self.date_now.year, self.date_now.month, self.date_now.day, 0, 0, 0) \
                           - datetime.timedelta(seconds=(ra_difference / 360. * (24. * 60. * 60.)))

        time_range_above_horizon = [
            (time_at_meridian - datetime.timedelta(hours=0.5 * float(self.sun_T_above))),
            (time_at_meridian + datetime.timedelta(hours=0.5 * float(self.sun_T_above)))]

        # Verify correct date computation, or correct for it, for analysis of whether Sun is in sky
        if time_range_above_horizon[1] < self.time_observation:
            time_range_above_horizon[0] += datetime.timedelta(hours=24.)
            time_range_above_horizon[1] += datetime.timedelta(hours=24.)

        # Verify if Sun is above horizon during observation, or otherwise
        if self.time_observation >= time_range_above_horizon[0]:
            if self.time_observation <= time_range_above_horizon[1]:
                self.sun_above_horizon = True

    def _lsm_builder(self):

        self.lsm_sources = []
        for i in self.above_horizon:
            self.lsm_sources.append(self.global_sky_model[i, :])

        # Check Sun and add if above horizon
        if self.sun_above_horizon:
            sun_array = np.array([self.sun_RA, self.sun_Dec, 100., 0., 0., 0., 80.0e6, 1])
            self.lsm_sources.append(sun_array)

        self.lsm_sources = np.array(self.lsm_sources, dtype=float)

    def _lsm_writer(self, local_sky_model):

        text_file = open(local_sky_model, "w")
        for i in range(self.lsm_sources.shape[0]):
            for j in range(self.lsm_sources.shape[1]):
                if j < self.lsm_sources.shape[1]:
                    text_file.write(str(self.lsm_sources[i][j]) + ' ')
                if j == self.lsm_sources.shape[1]:
                    text_file.write(str(self.lsm_sources[i][j]))
            text_file.write('\n')
        text_file.close()

    def sky_build_manager(self, sky_model_in, latitude, time_local, date_now, local_sky_model):

        self._gsm_reader(sky_model_in)
        self._receive_observation_parameters(latitude, time_local, date_now, time_zone=8)
        self._calculate_zenith_dec_ra()
        self._add_sun()
        self._calculate_dec_ranges()
        self._determine_source_duration_above_horizon()
        self._define_sources_above_horizon()
        self._lsm_builder()
        self._lsm_writer(local_sky_model)


class OskarModel(AbstractGenerator):

    def __init__(self):

        super(OskarModel, self).__init__()
        self.setupbuild = OskarSetupBuilder()
        self.telbuild = OskarTelescopeBuilder()
        self.skybuild = LocalSkyModelGen()
        self.tm = None
        self._dir = None
        self._model_vis_file = None
        self.time_local = None
        self.date_now = None
        self.index = None

    def _setup(self, filter_model):

        self.tm = filter_model['tm']
        self._dir = filter_model['tmp_dir']
        self._model_vis_file = filter_model['model_vis_file']
        self.local_sky_model = filter_model['local_sky_model']

        self.time_local = (datetime.datetime.strptime(str(self.tm.StartTime), "%Y-%m-%dT%H:%M:%S.%f")).time()
        self.date_now = (datetime.datetime.strptime(str(self.tm.StartTime), "%Y-%m-%dT%H:%M:%S.%f")).date()

        root_dir = os.path.dirname(self._dir)
        if not os.path.exists(root_dir):
            os.makedirs(root_dir)

        self.telbuild.telescope_create(self.tm.antennas, self._dir, self.tm)
        self.index = 0

    def _generator_loop(self):

        counter2 = 0

        time_next_step = self.time_local
        date_next_step = self.date_now

        for i in range(self.tm.ObsLength):

            self.setupbuild.logger.info('Model Generation Run: {}'.format(i))
            if counter2 == self.tm.NumChan:
                counter2 = 0

            # Build local sky model from global sky model
            self.skybuild.sky_build_manager(self.tm.Skymodel_dir, self.tm.cen_lat, time_next_step,
                                            date_next_step, self.local_sky_model)

            # Build setup config file
            self.setupbuild.setup_config(self.tm, i, counter2, self._dir, self.local_sky_model,
                                         self.skybuild.zenith_RA, self.skybuild.zenith_Dec)
            counter2 += 1

            date_next_step = self.setupbuild.observation.TNext.date()
            time_next_step = self.setupbuild.observation.TNext.time()

            counter = format(i, "03d")
            self.setupbuild.setup_parser(self._dir, counter, self.tm.testing_req)

            interferometer_run = "oskar_sim_interferometer -q " + self.setupbuild.ini_filename_1

            null = open(os.devnull, 'w')
            subprocess.call(interferometer_run, stdout=null, shell=True)

            if self.tm.h5_req:
                self.setupbuild.ms_to_h5()

            if self.tm.testing_req:
                if i == 0:
                    beam_pattern_run = "oskar_sim_beam_pattern -q " + self.setupbuild.ini_filename_2
                    imager_run = "oskar_imager -q " + self.setupbuild.ini_filename_3
                    subprocess.Popen(beam_pattern_run, shell=True).wait()
                    subprocess.Popen(imager_run, shell=True).wait()

            self.setupbuild.append_to_pathfile(self._model_vis_file, i)

            msg = Message((Message.SelType.NONE, 0), self.index, self.tm.StartTime)
            msg.Default = self.setupbuild.interferometer.MS_Dir
            self.index += 1
            self._push(None, None)

        self._inform(TCPOCode.Request.STOP_SMPL)
