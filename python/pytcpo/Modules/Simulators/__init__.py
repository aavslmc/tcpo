# Package which contains the OM generator module
__author__ = "Josef Borg"

from OskarModel import OskarModel
from pytcpo.Core.PipeLine import PipelineManager

PipelineManager.registerModule(OskarModel)
