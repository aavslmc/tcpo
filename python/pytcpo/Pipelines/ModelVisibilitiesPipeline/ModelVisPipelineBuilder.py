from pytcpo.Core.PipeLine import AbstractPipelineBuilder, Message
from pytcpo.Modules.Simulators import OskarModel

from pytcpo.Core.Common import string, TCPOCode


class ModelVisPipelineBuilder(AbstractPipelineBuilder):

    def __init__(self):
        super(ModelVisPipelineBuilder, self).__init__()

    def _specification(self):
        """
        Defines the Specification
        :return: None
        """
        # First the Required Options
        self._add_option('TM_instance', ' Instance of the TM singleton', required=True)
        self._add_option('source', 'Directory containing TM data', required=True, allow_type=str)
        self._add_option('model_file', 'Path to the model visibilities file', required=True, allow_type=str)
        self._add_option('local_sky_model', ' Local sky model file path', required=True)

    def _structure(self, manager):
        """
        Defines the structure of this pipeline.

        Parameters as described in the virtual method.
        """

        manager.add_root('OskarModel', {'tm': self.TM_instance, 'tmp_dir': self.source,
                                        'model_vis_file': self.model_file, 'local_sky_model': self.local_sky_model})

        # Now Return Success
        return TCPOCode.Status.OK

    def archive(self):

        pass
