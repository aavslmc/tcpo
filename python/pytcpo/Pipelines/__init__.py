"""
This Package will contain a number of pipeline specifications to chose from.
"""
__author__ = 'Michael Camilleri'

from BandPassFitting.SimplePipeline import SimpleAnomalyPipelineBuilder, BitShiftInvocator
from PointingPipeline import PointingPipelineBuilder
from ModelVisibilitiesPipeline import ModelVisPipelineBuilder
from RealVisibilitiesPipeline import RealVisPipelineBuilder
