from pytcpo.Core.PipeLine import AbstractPipelineBuilder, Message

from pytcpo.Modules.Calibrators import CalibratorRun
from pytcpo.Modules.Calibrators import Consumer
from pytcpo.Modules.Calibrators import SelfCalRun
from pytcpo.Modules.Calibrators import StEFCalRun
from pytcpo.Modules.Calibrators import CoeffManagerRun
from pytcpo.Core.DataModels.TelescopeModel import TelescopeModel as TM

from pytcpo.Core.Common import string, TCPOCode
from pytcpo.Core.Common import directory
from pydaq.persisters import corr

import logging


class RealVisPipelineBuilder(AbstractPipelineBuilder):

    def __init__(self):
        super(RealVisPipelineBuilder, self).__init__()

    def _specification(self):
        """
        Defines the Specification
        :return: None
        """
        # First the Required Options
        self._add_option('actual_vis_directory', ' Real Visibilities Directory', required=True)
        self._add_option('model_vis_directory', 'Model Visibilities Directory', required=True)
        self._add_option('model_vis_list_path', 'List of Model Vis Generated', required=True)
        self._add_option('total_time_samples', 'Total no of time samples', required=True)
        self._add_option('selfcal', 'SelfCal Usage Bool', required=True)
        self._add_option('gaincal', 'GainCal Usage Bool', required=True)
        self._add_option('phasecal', 'PhaseCal Usage Bool', required=True)
        self._add_option('stefcal', 'Stefcal Usage Bool', required=True)
        self._add_option('fescal', 'FesCal Usage Bool', required=True)
        self._add_option('no_of_antennas', 'Number of antennas', required=True)
        self._add_option('coeffs_filepath', 'Filepath to found coefficients', required=True)
        self._add_option('calib_check_path', 'Filepath to calibration plot', required=True)
        self._add_option('transit_run', 'Observation transit bool', required=True)
        self._add_option('transit_file', 'Calibration transit file', required=True)
        self._add_option('obs_file', 'Observation transit file', required=True)
        self._add_option('coeff_test_run', 'Coeff Test run bool', required=True)
        self._add_option('baseline_no', 'Number of baselines', required=True)
        self._add_option('main_dir', 'Main directory', required=True)
        self._add_option('pointing_ra', 'Pointing RA', required=True)
        self._add_option('pointing_dec', 'Pointing Dec', required=True)
        self._add_option('calibration_dec', 'Calibration Dec', required=True)
        self._add_option('antennas', 'Antenna Positions', required=True)
        self._add_option('longitude', 'Array Central Longitude', required=True)
        self._add_option('latitude', 'Array Central Latitude', required=True)
        self._add_option('frequency', 'Frequency', required=True)
        self._add_option('bandwith', 'Bandwith', required=True)
        self._add_option('telescope', 'telescope', required=True)
        self._add_option('model_run', 'model_run', required=True)
        self._add_option('auto_corr', 'auto_corr', required=True)
        self._add_option('calib_coeffs_dir', 'calib_coeffs_dir', required=True)
        self._add_option('obs_time', 'obs_time', required=True)

    def _structure(self, manager):
        """
        Defines the structure of this pipeline.

        Parameters as described in the virtual method.
        """

        manager.add_root('ConsumerRun', {'actual_vis_directory': self.actual_vis_directory, 'telescope': self.telescope,
                                          'model_vis_directory': self.model_vis_directory,
                                          'model_vis_list_path': self.model_vis_list_path,
                                          'total_time_samples': self.total_time_samples,
                                          'transit_run': self.transit_run, 'transit_file': self.transit_file})

        manager.add_module('CalibratorRun', {'selfcal': self.selfcal, 'stefcal': self.stefcal, 'fescal': self.fescal,
                                             'baseline_no': self.baseline_no, 'transit_run': self.transit_run})

        if self.selfcal is True:
            manager.add_module('SelfCalRun', {'no_of_antennas': self.no_of_antennas, 'transit_run': self.transit_run,
                                              'stefcal': self.stefcal, 'pointing_ra': self.pointing_ra,
                                              'pointing_dec': self.pointing_dec, 'antennas': self.antennas,
                                              'longitude': self.longitude, 'latitude': self.latitude,
                                              'frequency': self.frequency, 'bandwith': self.bandwith,
                                              'calibration_dec': self.calibration_dec, 'baseline_no': self.baseline_no,
                                              'main_dir': self.actual_vis_directory, 'gaincal': self.gaincal,
                                              'phasecal': self.phasecal, 'calib_coeffs_dir': self.calib_coeffs_dir,
                                              'obs_time': self.obs_time})

        if self.stefcal is True:
            manager.add_module('StEFCalRun', {'selfcal': self.selfcal, 'no_of_antennas': self.no_of_antennas,
                                              'transit_run': self.transit_run})

        if self.fescal is True:
            manager.add_module('FesCalRun', {'no_of_antennas': self.no_of_antennas, 'auto_corr': self.auto_corr})

        manager.add_module('CoeffManagerRun', {'coeffs_filepath': self.coeffs_filepath, 'test_run': self.coeff_test_run,
                                               'baseline_no': self.baseline_no, 'main_dir': self.main_dir,
                                               'transit_run': self.transit_run, 'no_of_antennas': self.no_of_antennas,
                                               'transit_file': self.transit_file, 'model_generation': self.model_run,
                                               'calib_check_path': self.calib_check_path, 'obs_file': self.obs_file,
                                               'model_vis_directory': self.model_vis_directory})

        # Now Return Success
        return TCPOCode.Status.OK

    def archive(self):

        pass
