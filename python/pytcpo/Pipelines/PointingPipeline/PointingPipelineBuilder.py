import Queue as qu
import numpy as np
import copy

from pytcpo.Core.Common import TCPOCode, TCPOException
from pytcpo.Core.PipeLine import AbstractPipelineBuilder, Message

from pytcpo.Modules.Pointing import Pointing
from pytcpo.Modules.IO.PointingIO import PointingRequester
import pytcpo.Modules.Archivers

class PointingPipelineBuilder(AbstractPipelineBuilder):

    def __init__(self):
        super(PointingPipelineBuilder, self).__init__()

        # Specify the Archive Keys which will be used.
        self.archive_keys = {}

    def _specification(self):
        """
        Defines the Specification
        :return: None
        """
        # First the Required Options
        self._add_option('TM_instance', ' Instant of the TM singleton', required=True)
        self._add_option('request_queue', 'Queue to containing the pointing 1D Numpy Array', required=True)

        # Now the Default-able values
        self._add_option('serialiser', 'Tuple describing the type of serialiser and associated setup. Defaults to the JSON serialiser', required=False, allow_type=(tuple, list), default=('JSONSerialiser', {}))
        self._add_option('redis_server', 'Tuple/List containing the server address and port number: if None defaults to localhost:6379', required=False, default=None)
        self._add_option('redis_pwd', 'Authorisation Password for the Redis Server (if any): defaults to None', required=False, default=None)

    def _structure(self, manager):
        """
        Defines the structure of this pipeline.

        Parameters as described in the virtual method.
        """

        # Archive Keys
        self.archive_keys = {}

        self.archive_keys['pointing_delays'] = np.ndarray

        # Connection Settings
        server_addr = self.redis_server[0] if self.redis_server is not None else 'localhost'
        server_port = self.redis_server[1] if self.redis_server is not None else 6379

        # ================= Archiver Setup ================= #
        # Prepare Setup
        redis_setup = {'host': server_addr, 'port': server_port, 'keys': self.archive_keys, 'auth': self.redis_pwd}

        # Set it up
        manager.set_archiver(persistor={'type': 'RedisPersistor', 'setup': redis_setup, 'append': True},
                             serialiser={'type': self.serialiser[0], 'setup': self.serialiser[1]})

        # ================ Pointing Pipeline =============== #

        manager.add_root('PointingRequester', {'pointing_requests': self.request_queue, 'TelescopeModel': self.TM_instance})
        manager.add_module('Pointing', {'TelescopeModel': self.TM_instance}, archive_key='pointing_delays')

        # Now Return Success
        return TCPOCode.Status.OK

    @property
    def archive(self):
        return copy.deepcopy(self.archive_keys)