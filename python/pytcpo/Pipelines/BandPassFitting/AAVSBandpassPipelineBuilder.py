import numpy as np
import copy

from pytcpo.Core.Common import TCPOCode, TCPOException
from pytcpo.Core.PipeLine import AbstractPipelineBuilder, Message

from pytcpo.Modules import BandPassModel
from pytcpo.Modules import IO
from pytcpo.Modules import AnomalyDetectors
from pytcpo.Modules import RawFilters
from pytcpo.Modules import Equalisers


class BitShiftInvocator(object):
    """
    This is a helper class for wrapping the invocation of the BitShiftEqualiser update callback
    """

    def __init__(self, manager, tile_map):
        self.__manager = manager
        self.__tiles = tile_map

    def update_scale(self, tile, bit_shifts, time_valid=-1.0):
        """
        Update the Scaling factor for a particular tile
        :param tile: The Tile ID to update for
        :param bit_shifts: Matrix of bit-shifts per Antenna/Polarisation/Channel
        :param time_valid: the time at which this update will be valid
        :return:
        """
        self.__manager.invoke(self.__tiles[tile], 'update_scale', bit_shifts, time_valid)


class AAVSBandpassPipelineBuilder(AbstractPipelineBuilder):
    """
    This Builder represents the typical BandPass Characteriser and Anomaly detector pipeline. It is based off the
    following components:

        a) PiecewisePolyFit for polynomial characterisation (log-scale)
        b) DerivativeSignsWithDifferenceRejection & PolynomialDeviations for Outlier detection
        c) OutlierThreshold & OutlierWeightedOverlapDetector for Anomaly detection
        d) Mean-Shifting for coefficient generation

    With regards to archiving, the pipeline uses the RedisPersistor, but allows flexibility in the serialiser (which
    defaults to JSON). The pipeline exposes three data streams:
        a) outliers : FlaggedAnomaly array of outliers per Antenna/Polarisation
        b) anomalies : FlaggedAnomaly array of anomalous Antennas/Polarisations
        c) bitshifts : The Bit-shift coefficients to apply to equalise the model

    The actual naming follows the scheme: BandPass:station_<station#>:tile_<tile#>:<output>, with all data stored to
    database 0. It is up to the programmer to ensure that the database is clean, since in the interest of consistency
    with other pipelines running on the same server, the connection is opened in append mode.

    """

    def __init__(self):
        super(AAVSBandpassPipelineBuilder, self).__init__()

        # Specify the Archive Keys which will be used.
        self.archive_keys = {}

        # For the purpose of callbacks to bit-shifting, the reference to each tile's bitshifter is retained:
        self.equalisers = {}

    def _specification(self):
        """
        Defines the Specification
        :return: None
        """
        # First the Required Options
        self._add_option('source', 'Source Path for the DAQ system, from where to retrieve the data', required=True, allow_type=str)
        self._add_option('station', 'Station Name/Number. Must be a unique identifier', required=True, allow_type=(str, int))
        self._add_option('tile_config', 'Dictionary of which antennas are enabled in which tiles', required=True, allow_type=dict)
        self._add_option('poly_fits', '2D list of piecewise fits of the form [order, fitting_channels], with each piece overlapping by exactly 10 channels', required=True, allow_type=list)
        self._add_option('devs_multiple', 'Fraction/Multiple of Standard Deviation(s) to exceed for RFI outlier detection', required=True, allow_type=float)
        self._add_option('deriv_ignore', 'The (absolute) first order derivative magnitude to ignore (below) during derivative-based outlier rejection', required=True, allow_type=float)
        self._add_option('outlier_ratio', 'Maximum fraction of outlying channels to tolerate before signaling an anomaly. This also doubles as the maximum outlier ratio for the outlier-weighted overlap detector', required=True, allow_type=float)
        self._add_option('min_overlap', 'Minimum Overlap (0-1.0) between average and current polynomial before signaling an anomaly', required=True, allow_type=float)
        self._add_option('envelope_cutoff','Defines an envelope of width 2 times the set value about the resulting piecewise polynomial, in which outliers are not detected', required=True, allow_type=float)
        self._add_option('deviation_cutoff',
                         'Used in conjunction with the peak-deviation outlier detector: if the deviation is below this value, then even though it may be more than the specified standard deviation, it is ignored',
                         required=True, allow_type=float)
        self._add_option('avg_length', 'Length of the Moving Average Window', required=True, allow_type=int)
        self._add_option('mean_length', 'Length (# samples) of the piecewise-mean computation for the purpose of coefficient generation', required=True, allow_type=int)
        self._add_option('detrend_mean', 'The Desired mean value to detrend the bandpass signal to.', required=True, allow_type=float)

        # Now the Default-able values
        self._add_option('channel_range', 'Tuple specifying the resulting entire fitting range to be considered', required=False, default=(73,449), allow_type=tuple)
        self._add_option('permanent_rfi_replacing', 'Bool to specify if identified permanent RFI regions are to be replaced with a straight line model', required=False, default=True, allow_type=bool)
        self._add_option('permanent_rfi_channels', 'List of the Channel indices representing permanent RFI regions', required=False, default=[np.arange(305,348), np.arange(351,366), np.arange(436,448)], allow_type=list)
        self._add_option('polarisations', 'The Polarisations (indices) per Antenna: Defaults to (0,1)', required=False, default=(0,1), allow_type=(int, tuple, list))
        self._add_option('serialiser', 'Tuple describing the type of serialiser and associated setup. Defaults to the JSON serialiser', required=False, allow_type=(tuple, list), default=('JSONSerialiser', {}))
        self._add_option('redis_server', 'Tuple/List containing the server address and port number: if None defaults to localhost:6379', required=False, default=None)
        self._add_option('redis_pwd', 'Authorisation Password for the Redis Server (if any): defaults to None', required=False, default=None)
        self._add_option('daq_time', 'The Timestamp of the DAQ archive (defaults to None)', required=False, default=None)
        self._add_option('data_rate', 'The expected update rate (s) for Channel Integrated Data', required=False, default=0.1)
        self._add_option('range_samples', 'For Offline-Operation, dictates the range of samples (in terms of start, and stop, end non-inclusive) to evaluate', required=False)
        self._add_option('downsample', 'Rate at which to downsample the data coming out of the CSR (default 1, no downsampling)', required=False, allow_type=int, default=1)
        self._add_option('archive_coeffs', 'If True archives also the polynomial coefficients and the mean values, for debugging purposes (default False)', required=False, allow_type=bool, default=False)
        self._add_option('morph_window', 'Size of the window for the morpholigical extension filter for combining outlier detection', required=False, default=0, allow_type=int)
        self._add_option('default_model', 'Specifies the default (ideal) coefficients per piecewise fit for the Outlier-Weighted Anomaly Detector (defaults to 0-vector))', required=False, allow_type=np.ndarray, default=np.array([None]))
        self._add_option('ignore_outliers', 'If True (default) outliers are not considered in the mean computation', required=False, default=True, allow_type=bool)
        self._add_option('integration_samples', 'The number of samples in the integrated band-pass data: i.e. the number to divide by when de-trending.', required=False, default=1.0)
        self._add_option('MSB_offset', 'The Offset (# bits, default 0) to achieve the MSB in the TPM (to factor in bit-shifting).', required=False, allow_type=int, default=0)
        self._add_option('start_shift', 'The start (scalar) value to initialise all bit-shifts with: defaults to 0', required=False, default=0, allow_type=int)

    def _structure(self, manager):
        """
        Defines the structure of this pipeline.

        Parameters as described in the virtual method.
        """

        # ================= Calculations ================= #

        # Channel Expansion
        _channels_ = np.arange(self.channel_range[0], self.channel_range[-1])

        # Default Model
        poly_fit_orders = [self.poly_fits[i][0] for i in range(len(self.poly_fits))]
        if not np.array_equal(self.default_model,  np.array([None])):
            for o, order in enumerate(poly_fit_orders):
                if self.default_model[o].shape != (order+1):
                    raise TCPOException(TCPOCode.Error.VALUE, 'default model must be length polynomial order + 1')
        else:
            self.default_model = np.empty((len(poly_fit_orders)), dtype=np.object)
            for o, order in enumerate(poly_fit_orders):
                self.default_model[o] = np.zeros((order+1), dtype=float)

        # Archive Keys
        self.archive_keys = {}
        for tile in self.tile_config:
            self.archive_keys['BandPass:station_' + str(self.station) + ':tile_' + str(tile) + ':outliers'] = np.ndarray
            self.archive_keys['BandPass:station_' + str(self.station) + ':tile_' + str(tile) + ':anomalies'] = np.ndarray
            self.archive_keys['BandPass:station_' + str(self.station) + ':tile_' + str(tile) + ':bitshifts'] = np.ndarray
            if self.archive_coeffs:
                self.archive_keys['BandPass:station_' + str(self.station) + ':tile_' + str(tile) + ':polyfits'] = np.ndarray
                self.archive_keys['BandPass:station_' + str(self.station) + ':tile_' + str(tile) + ':meanfits'] = np.ndarray

        # Connection Settings
        server_addr = self.redis_server[0] if self.redis_server is not None else 'localhost'
        server_port = self.redis_server[1] if self.redis_server is not None else 6379

        # Finally the Equalisers, for their callbacks
        self.equalisers = {}


        # ================= Archiver Setup ================= #
        # Prepare Setup
        redis_setup = {'host': server_addr, 'port': server_port, 'keys': self.archive_keys, 'auth': self.redis_pwd}

        # Set it up
        manager.set_archiver(persistor={'type': 'RedisPersistor', 'setup': redis_setup, 'append': True},
                             serialiser={'type': self.serialiser[0], 'setup': self.serialiser[1]})


        # ================= Common Pipeline ================= #
        # The Channel Reader
        manager.add_root('ChannelStageReader', {'path':self.source, 'time':self.daq_time, 'sample_rate': self.data_rate,
                                                'stop_on_end': self._offline, 'tiles':[t for t in self.tile_config],
                                                'samples': self.range_samples})
        # The DownSampler (if enabled)
        if self.downsample > 1:
            manager.add_module('AliasingDownSampler', {'rate':self.downsample, 'start':self.range_samples[0]})
        down_sampler = manager.Last # The Tile-Fan-Out - this may be the original channeliser or the down-sampler

        # ================= Tiled Pipeline ================= #
        for tile, antennas in self.tile_config.iteritems():

            # Prepare Name
            basename = 'BandPass:station_' + str(self.station) + ':tile_' + str(tile)

            # First the Actual Tile Selector
            manager.add_module('BaseSelector', {'type':Message.SelType.TILE, 'select':tile},
                               parents=down_sampler)

            # The Permanent RFI Region Replacer (if enabled)
            if self.permanent_rfi_replacing:
                manager.add_module('PermanentRFIFiller', {'antennas':antennas, 'polarisations':self.polarisations,
                                                          'perm_rfi_chans':self.permanent_rfi_channels})

            # Now the Antenna Selector:
            manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas': antennas})

            # Now the Polarisation Selector:
            manager.add_module('PolarisationSelector', {'in_polarisations': np.arange(2), 'out_polarisations': self.polarisations})

            # Now the Channel Selector:
            manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels': _channels_})

            # Scale and power log if need be:
            raw_module = manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': float})
            log_module = manager.add_module('PowerLog', {'multiplier':10, 'log':'10'})

            # First the Band-Pass Model
            fit_module = manager.add_module('PiecewisePolyFitter',
                                            {'antennas':len(antennas), 'polarisations':len(self.polarisations),
                                             'channels':_channels_, 'fits': self.poly_fits},
                                            archive_key=(basename+':polyfits') if self.archive_coeffs else None)

            # And the Evaluator...
            eval_module = manager.add_module('PiecewisePolyEvaluator', {'antennas':len(antennas),
                                                                        'polarisations':len(self.polarisations),
                                                                        'fit_channels':_channels_})

            # Now the Polynomial-based Outlier Detection
            poly_out_module = manager.add_module('PolynomialDeviations',
                                                 {'channels': _channels_, 'outlier_std':self.devs_multiple,
                                                  'antennas': antennas, 'polarisations': self.polarisations,
                                                  'outlier_cut':self.deviation_cutoff, 'model':eval_module},
                                                 parents=(log_module, eval_module))

            # The Derivative-Based Outlier Detection
            der_out_module = manager.add_module('DerivativeSignsWithDifferenceRejection',
                                                {'antennas': antennas, 'polarisations': self.polarisations,
                                                 'channels': _channels_,  'min_cutoff': self.deriv_ignore,
                                                 'model':eval_module, 'envelope':self.envelope_cutoff},
                                                parents=(log_module, eval_module))
            # Now aggregate both
            agg_out_module = manager.add_module('OutlierAggregator',
                                                {'antennas': antennas, 'polarisations': self.polarisations,
                                                 'channels': _channels_, 'length': self.morph_window,
                                                 'others': (poly_out_module, der_out_module)},
                                                parents=(poly_out_module, der_out_module),
                                                archive_key=basename + ':outliers')

            # Now do the Anomaly Detectors, starting from the Outlier Threshold Detector
            out_thres_module = manager.add_module('OutlierThresholdDetector',
                                                  {'antennas': antennas, 'polarisations': self.polarisations,
                                                   'channels': _channels_, 'threshold': self.outlier_ratio},
                                                  parents=agg_out_module)

            # Next the Outlier-Weighted Overlap Detector
            overlap_module = manager.add_module('OutlierWeightedOverlapDetector',
                                                {'antennas': antennas, 'polarisations': self.polarisations,
                                                 'channels': _channels_, 'max_outlier': self.outlier_ratio,
                                                 'min_overlap': self.min_overlap, 'outlier_gen': agg_out_module,
                                                 'length': self.avg_length, 'start': self.default_model,
                                                 'ignore': True, 'fits':len(self.poly_fits)},
                                                parents=(fit_module, agg_out_module))

            # Now do the anomaly aggregator
            manager.add_module('AnomalyAggregator',
                               {'antennas': antennas, 'polarisations': self.polarisations},
                               parents=(out_thres_module, overlap_module), archive_key=basename + ':anomalies')

            # Now the Coefficient De-Trending - First the mean-computation with outlier rejection
            manager.add_module('PiecewiseMeanFitter',
                               {'pieces': len(_channels_)/self.mean_length, 'sizes': self.mean_length,
                                'outlier': agg_out_module if self.ignore_outliers else None},
                               parents=(raw_module, agg_out_module),
                               archive_key=(basename+':meanfits') if self.archive_coeffs else None)

            # Next re-expand to size:
            manager.add_module('PiecewiseMeanEvaluator', {'sizes': self.mean_length})

            # Finally the actual detrending
            original_scaler = np.full((len(antennas), len(self.polarisations), len(_channels_)), self.start_shift)
            self.equalisers[tile] = manager.add_module('BitShiftEqualiser',
                                                       {'desired': self.detrend_mean, 'select_offset': self.MSB_offset,
                                                        'integration': self.integration_samples,
                                                        'scaling': original_scaler},
                                                       archive_key=basename + ':bitshifts')

        # Now Return Success
        return TCPOCode.Status.OK

    @property
    def archive(self):
        return copy.deepcopy(self.archive_keys)

    def invocator(self, manager):
        """
        Builds a BitShift wrapper based on the equalisers specified in the equaliser list generated by the last 'build'
        invocation, and bound to the passed manager. Note that due to the deterministic nature of the building of the
        pipeline, the names will generally bind to any manager built with the same options.

        :return: BitShiftWrapper
        """
        return BitShiftInvocator(manager, self.equalisers)

