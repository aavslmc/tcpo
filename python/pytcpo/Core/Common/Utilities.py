import numpy as np
import os

__author__ = "Michael Camilleri"


class string(str):
    """
    Subclass of String for wrapping some added Functionality.
    """
    def trunc(self, max):
        """
        Returns a Truncated (or identity) string of itself.
        :param max: Maximum number of characters to allow
        :return: A View into the truncated string or the string itself if its length is less than max. Note that no
                 copying is carried out.
        """
        return self[0:max] if len(self) > max else self

    def acronym(self):
        """
        Acronym Generator for Strings
        :return: The string composed of the upper-case characters in the original string.
        """
        return ''.join([c for c in self if c.isupper()])


class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons, borrowed from Stack Overflow
    (https://stackoverflow.com/questions/31875/is-there-a-simple-elegant-way-to-define-singletons).

    The definition should be used as a decorator -- not a metaclass -- to the class that should be a singleton.

    The decorated class can define one `__init__` function that takes only the `self` argument. Also, the decorated
    class cannot be inherited from. Other than that, there are no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying to use `__call__` will result in a `TypeError`
    being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a new instance of the decorated class and calls
        its `__init__` method. On all subsequent calls, the already created instance is returned.

        :return: Singleton Instance of the decorated class
        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        """
        Enforces that Initialiser is not Called

        :raises TypeError: if attempted.
        """
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

def tupler(value):
    """
    Static (Module) Method for converting a scalar into a tuple if not already. Will also ignore None.

    :param value: The Value to convert
    :return: A Tuple containing value if a scalar, or value if already a list/tuple/numpy array/none
    """
    return value if (type(value) in (tuple, list, np.ndarray) or value is None) else (value,) # bug fix - original : (tuple, list, np.array)


def namer(_type):
    """
    Static (Module) Method for outputting the name of a data type. This amounts to calling the __name__ method on
    the passed _type if it exists, or calling string otherwise on it... (mainly for None Types)

    :param _type: A type object to stringify
    :return: string representation of '_type'
    """
    return _type.__name__ if hasattr(_type, '__name__') else str(_type)


def moduler(_module):
    """
    Function to retrieve the last name of a module. Basically splits by '.' and returns the last value

    :param _module: The full Module name (string)
    :return: String representing the module name's last element
    """
    return _module.split('.')[-1]


def directory(_path):
    """
    Static (Module) Method for ensuring that the given path exists, and if not, will attempt to create it.
    :param _path: The Full Directory to create
    :return: None
    :raises OSError: if unable to create the directory
    """
    try:
        os.makedirs(_path)
    except OSError:
        if not os.path.isdir(_path): raise