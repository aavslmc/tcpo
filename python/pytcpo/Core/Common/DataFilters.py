import logging
import numpy as np
import numpy.polynomial.polynomial as nppol

from Utilities import moduler

# Create Logger
logger = logging.getLogger(moduler(__name__))


class MovingAverage(object):
    """
    This Class implements an efficient Moving Average filter based on a circular buffer structure. The data type can be
    any sort as long as it implements the addition, subtraction and division operators - this includes numpy arrays.

    TODO - Consider vectorizing the Object (giving it the ability to work on Matrices of Moving Averages)
    """

    @property
    def Average(self):
        """
        Access to the Average
        :return: The current (running) computed Average.
        """
        return self.__average

    def __init__(self, start, length):
        """
        Initialiser. This sets the buffer such that the average is defined by the start value.
        :param start: The starting value. Trivially this can be 0, but allows arbitrary starting points.
        :param length: The length of the moving average
        """
        shape = [length]
        shape.extend(start.shape)
        self.__buffer = np.empty(shape, dtype=start.dtype)
        self.__length = length
        self.__average = None
        self.__index = None
        self.flush(start)

    def flush(self, start):
        """
        Returns the buffer to the original state (again defined by the start value specified).
        :param start: The start value
        :return: None
        """
        for i in range(self.__length):
            self.__buffer[i,...] = start / float(self.__length)
        self.__average = start
        self.__index = 0

    def add(self, sample):
        """
        Add a new sample to the Moving Average, discarding the oldest sample in its place and recomputing the average.
        The method achieves efficient computation by keeping the pre-scaled samples in the buffer, and at each addition,
        subtracting the oldest and adding the newest sample.

        :param sample: The new sample to consider. This must of the same shape and type as the other data points.
        :return: The recomputed average.
        """
        self.__average = np.subtract(self.__average, self.__buffer[self.__index,...])
        self.__buffer[self.__index,...] = np.divide(sample, float(self.__length))
        self.__average = np.add(self.__average, self.__buffer[self.__index,...])
        self.__index = (self.__index + 1) % self.__length
        return self.__average


class ConnectorFiller(object):
    """
    This is a 1D morphological filter for connecting together sparse binary vectors. Basically, False Values are set to
    True iff they are flanked (within a range) by True values. An efficient implementation ensures that the update is
    performed in one-pass over the data.
    """

    # State Enumerations
    S_FALSE = -1
    S_TRUE  = 0
    S_HYST  = 1 # Actually this will be a range of values, up to the length of the buffer

    def __init__(self, length):
        """
        Initialiser.
        Just sets the length threshold over which to join Truth values.
        :param length:
        """
        self.__length = length

    def compute(self, flagged):
        """
        Public Method for computing the connected boolean vector, which is returned as a different array (i.e. updates
        do not happen in place). The method recurses over all but the last dimension, treating each vector in each
        dimension independently.
        :param flagged: The Boolean Array to filter. This must be a Numpy Array of Boolean Values.
        :return: The resulting array after the morpholigical operation.
        """
        _output = np.copy(flagged) # Prepare computation
        self.__recursive_compute(flagged, _output) # Recurse over the dimensions
        return _output # Return Result

    def __recursive_compute(self, flagged, output):
        """
        Recursive Computation of the morphological operation. The recursion ensures that the algorithm handles any
        number of dimensions of the input, by recursing over each dimension apart from the last and looping over the
        entries in each. The last dimension is instead treated as the vector to be operated on.
        :param flagged: The numpy array to operate on: each recursion operation iterates over all elements in that
                        dimension and calls itself with the reduced dimensionality.
        :param output:  The in-place updateable output array.
        :return: None
        """
        if flagged.ndim > 1:
            for n_i in range(flagged.shape[0]): self.__recursive_compute(flagged[n_i], output[n_i])
        else:
            state = ConnectorFiller.S_FALSE
            for d_i in range(len(flagged)):
                if state == ConnectorFiller.S_FALSE:
                    if flagged[d_i]: # IF reached true case
                        state = ConnectorFiller.S_TRUE
                else:
                    state += 1  # Add 1
                    # If True, reset Truth and set all in window to true...
                    if flagged[d_i]:
                        output[d_i - state + 1:d_i] = True
                        state = ConnectorFiller.S_TRUE
                    # If False and window exceeded, then reset to False
                    elif state == self.__length - 1:
                        state = ConnectorFiller.S_FALSE

class MorphologicalThinner(object):
    """
    Based on Michael's ConnectorFiller implementation. This recursive morphological filter un-flags the end channels of
    a sequence of channels marked as outliers.
    """

    # State Enumerations
    S_FALSE = -1
    S_TRUE  = 0

    def __init__(self):
        """
        None
        """

    def compute(self, flagged):
        """
        Public Method for computing the 'thinned' boolean vector, which is returned as a different array (i.e. updates
        do not happen in place). The method recurses over all but the last dimension, treating each vector in each
        dimension independently.
        
        :param flagged: The Boolean Array to filter. This must be a Numpy Array of Boolean Values.
        :return: The resulting array after the morpholigical operation.
        """
        _output = np.copy(flagged) # Prepare computation
        self.__recursive_compute(flagged, _output) # Recurse over the dimensions
        return _output # Return Result

    def __recursive_compute(self, flagged, output):
        """
        Recursive Computation of the morphological operation. The recursion ensures that the algorithm handles any
        number of dimensions of the input, by recursing over each dimension apart from the last and looping over the
        entries in each. The last dimension is instead treated as the vector to be operated on.
        
        :param flagged: The numpy array to operate on: each recursion operation iterates over all elements in that
                        dimension and calls itself with the reduced dimensionality.
        :param output:  The in-place updateable output array.
        :return: None
        """
        if flagged.ndim > 1:
            for n_i in range(flagged.shape[0]): self.__recursive_compute(flagged[n_i], output[n_i])
        else:
            state = MorphologicalThinner.S_FALSE
            for d_i in range(len(flagged)):
                if state == MorphologicalThinner.S_FALSE:
                    if flagged[d_i]: # IF reached true case
                        state = MorphologicalThinner.S_TRUE
                else:
                    if flagged[d_i] and d_i == (len(flagged)-1): # For case when last channel is true
                        output[d_i - state - 1] = False
                        output[d_i] = False
                        state = MorphologicalThinner.S_FALSE
                    elif flagged[d_i]:                           # else for any other channel true
                        state += 1
                    elif state > 1 :                             # else for more sequence of contiguously flagged channels
                        output[d_i - state - 1] = False
                        output[d_i - 1] = False
                        state = MorphologicalThinner.S_FALSE
                    else :                                       # else for false channel after true channel
                        state = MorphologicalThinner.S_FALSE

class PolynomialCompare(object):
    """
    A Polynomial Comparison Method

    This object compares polynomials based on calculating the overlap between them in a specified range. The overlap is
    reported as a ratio of the largest area spanned by any of the two individual functions, i.e.:

      overlap_ratio = I(f(x)*g(x))/max(I(ff(x)), I(gg(x)))
        where I refers to the integration operation, and kk implies the squared k function.

    The result is a floating-value ratio in the range [-1,1], apart from the case when any polynomial contains nan
    values in which case the output is nan. The class utilises the newer Numpy.Polynomial package, where polynomials are
    expected in lowest-order first.
    """

    def __init__(self, integ_range, ignore_constant=False, numerical=True, steps=500):
        """
        Initialiser
        :param integ_range: 2-element tuple/list/array specifying the range over which the overlap is to be evaluated.
        :param ignore_constant: If True, the constant term in both polynomials is set to 0 (loosely, but not quite,
                                removing the mean). In this case, the measure is more sensitive to shape rather than
                                absolute area.
        :param numerical: Indicates if numerical (True) or analytical (False) integration is to be performed. Analytical
                          integration should be faster for polynomials, but could lead to numerical instabilities for
                          very high-order calculations (due to the squaring operation).
        :param steps: For numerical integration, specifies the number of steps to divide the range in.
        """
        self.IgnoreConstant = ignore_constant
        self.IntegralRange = np.linspace(integ_range[0], integ_range[-1], steps) if numerical \
            else [integ_range[0], integ_range[-1]]
        self.Integrator = self.__num_integrate if numerical else self.__poly_integrate

    def compare(self, left, right):
        """
        Compares two polynomials together based on overlap area. Note that the polynomials need not be the same order.

        :param left: The first polynomial. This can be an immutable type (it is not directly modified)
        :param right: The second polynomial. This can be an immutable type (it is not directly modified)
        :return: The overlap ratio, typically of the same data-type as the input arrays: if any polynomial contains nan
                    values, nan is returned.

        """
        # First Ensure that there are no nan values
        if np.any(np.isnan(left)) or np.any(np.isnan(right)):
            return np.nan

        # Now Optionally pad if unequal: this will also copy so any modification does not affect the original array.
        order = max(len(left), len(right))
        _left = np.pad(left, (order - len(left), 0), 'constant', constant_values=0)
        _right = np.pad(right, (order - len(right), 0), 'constant', constant_values=0)

        # Now if ignoring constant term:
        if self.IgnoreConstant:
            _left[0] = 0.0
            _right[0] = 0.0

        # Integrate
        divisor = np.maximum(self.Integrator(_left, _left), self.Integrator(_right, _right))
        return self.Integrator(_left, _right)/divisor if divisor != 0 else 1.0

    def compare_self(self, polies):
        """
        Efficient method for computing the mutual overlap between a set of polynomials. The method precomputes the
        self overlap (squared integral) apriori to increase efficiency, and then utilises a recursive function, allowing
        efficient scaling to large lists.
        :param polies: The 2D Numpy array of polynomials to be compared, with the polynomial coefficients stored along
                       the second dimension. Note that in this case, the polynomials must be of the same order (or
                       padded to be the same length, which is in any case required of a numpy array). The polynomials
                       themselves will not be modified.
        :return: A 2D Numpy Array representing a symmetric matrix of overlap ratios, where:
                    A[i,j] = A[j,i] = overlap ratio between f_i(x) and f_j(x), and
                    A[i,i] = A[j,i] = 1.0
        """
        # Check if ignoring constant term:
        if self.IgnoreConstant:
            polies = np.copy(polies)
            polies[:,0] *= 0.0      # This multiplication ensures that nan's remain nan's

        # Compute Squared Functions
        squared = np.empty((polies.shape[0]), polies.dtype)
        for idx, polyn in enumerate(polies): squared[idx] = self.Integrator(polyn, polyn)

        # Recursively compute
        result = np.zeros((polies.shape[0], polies.shape[0]))
        if self.__recur_comp(polies, squared, result): return result
        else: return None

    def __recur_comp(self, polynomials, self_area, result):
        """
        Recursive method for computing overlap ratios.
        The method computes the overlap between the first polynomial (at index 0) and all other functions, and then
        recurses with the remaining elements starting from index 1.
        :param polynomials: The current (sub)set of polynomials to compare together
        :param self_area: The precomputed list of squared self-overlap.
        :param result: The overlap ratio (as a 2D symmetric matrix) between the current 0th element and the other
                       functions.
        :return: True (always successful)
        """
        # The Root Item:
        result[0, 0] = np.nan if np.isnan(self_area[0]) else 1.0

        # Now Check for Base Case:
        if result.shape == (1, 1):
            return True

        # Otherwise need to compute
        if np.isnan(self_area[0]):
            result[0,:] = np.nan    # Fill the remainder with NAN's
            result[:,0] = np.nan

        else:
            for i in range(1, len(polynomials)):
                divisor = np.maximum(self_area[0], self_area[i])
                if divisor > 0:
                    result[0, i] = result[i, 0] = self.Integrator(polynomials[0], polynomials[i])/divisor
                elif np.isnan(divisor):
                    result[0, i] = result[i, 0] = np.nan
                else:
                    result[0, i] = result[i, 0] = 1.0 # This will only happen if both functions are constantly 0

        # Call Recursively
        return self.__recur_comp(polynomials[1:], self_area[1:], result[1:,1:])

    def __poly_integrate(self, func1, func2):
        """
        Analytical Variant of integrating the product of two polynomials. The product f*g(x) is first computed,
        integrated and then evaluated at both ends of the range, subtracting the difference.
        :param func1: One of the two polynomials
        :param func2: The other polynomial
        :return: The Integral over the specified range
        """
        integral = nppol.polyint(nppol.polymul(func1, func2))
        return nppol.polyval(self.IntegralRange[-1], integral) - nppol.polyval(self.IntegralRange[0], integral)

    def __num_integrate(self, func1, func2):
        """
        Numerical Variant for integrating the product of two polynomials, by evaluating both polynomials at equally-
        spaced points along the specified range, multiplying the value at each point and summing.
        :param func1: One of the two polynomials
        :param func2: The other polynomial
        :return: The Integral over the specified range
        """
        return np.sum(np.multiply(nppol.polyval(self.IntegralRange, func1), nppol.polyval(self.IntegralRange, func2)))
