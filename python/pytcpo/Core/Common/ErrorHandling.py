"""
This Module implements the Error Handling capabilities of the tcpo library. The convention is that error handling (i.e.
capturing the error) should be handled at the highest level, typically by the PipelineManager, unless the object itself
can handle it and deal with it. The only exception to this paradigm is when the pipeline modules are running (in their
own thread) in which case, signalling is through queue messages.
"""
import logging
import inspect


class TCPOCode:
    """
    This class defines a set of constants to be used by the TCPO library for error handling and status reporting. It
    defines four sets of Constants:
    a) Error codes - These occupy the negative Number Range (< -100) and are typically used through Exceptions
    b) Warning Codes - These occupy the negative Number Range (-99 to -1) and signify a non-critical error (warning)
    c) Status Codes - These are typically return status codes. They occupy the positive range (1 to 99)
    d) Request Codes - These are typically used as user-requests. They occupy the postive number range (> 100)
    For Posterity, each is organised within its own sub-class.
    """

    class Error:
        """
        Error Codes (range < -100)
        """
        GENERIC = -200      # Generic Error (otherwise unspecified)

        MODULE  = -150      # Module Error (something external to us)

        MESSAGE = -135      # Message (type) inconsistency

        ARCHIVE = -130      # Generic Archiver Error
        INDEX   = -124      # Index Error
        CLOSING = -123      # Error while closing (cleaning up) the Archiver
        READ    = -122      # Read Error
        WRITE   = -121      # Write Error

        SETUP   = -120      # Generic Setup Error
        DUPLICATE = -115    # Duplicate Value (or specification)
        STATE   = -114      # Operation attempted in wrong state
        VALUE   = -113      # Value not Allowed
        MISSING = -112      # Missing Value not specified
        TYPE    = -111      # Wrong Data Type

    class Warning:
        """
        Warning Codes (range -99 to -1)
        """
        DEFAULT = -99       # Default Value missing or not needed

        STATE = -89      # Function called when inactive. (When it does not warrant an error)
        DUPLICATE = -88     # Duplicate Message encountered. (When it does not warrant an error)
        VALUE     = -87     # Unrecognised value (When it does not warrant an error)

    class Status:
        """
        Status (Update) Codes (range 1 to 99)
        """
        OK        =  1
        SETUP     =  2
        ACTIVE    =  5

        FILE_END  = 10
        USER_STOP = 11
        SMPL_STOP = 12

    class Request:
        """
        Request Codes (range > 100)
        """
        STOP_USER = 101
        STOP_FILE = 102
        STOP_SMPL = 103

    # Private Dictionary mappings
    __code_str = {-200 : 'Generic Error',
                  -150 : 'Error in External Module',
                  -135 : 'Message Type is inconsistent',
                  -130 : 'Generic Archiver Error',
                  -124 : 'Indexing Error (out of range)',
                  -123 : 'Error in Closing (Archiver) - Data may have been lost',
                  -122 : 'Read Error (Archiver)',
                  -121 : 'Write Error (Archiver)',
                  -120 : 'Generic Error in Setup Procedure',
                  -115 : 'Duplicate Value encountered',
                  -114 : 'Operation attempted in incorrect State',
                  -113 : 'Incorrect Value for Operation',
                  -112 : 'Missing Parameter in Setup',
                  -111 : 'Type Mismatch in Parameter',

                  -99 : 'Default Value not specified or not needed',
                  -89 : 'Module is not active and cannot process request - ignoring',
                  -88 : 'Duplicate Message/Value encountered - dropping',
                  -87 : 'Unrecognised Value encountered - ignoring',

                   1 : 'OK - All Good',
                   2 : 'Setup Ready',
                   5 : 'Process is (still) Active',
                  10 : 'Data Generation Stopped',
                  11 : 'User Stopped the Process',
                  12 : 'All specified samples have been processed',

                  101 : 'User Requests to Stop',
                  102 : 'File Ended - Request Stop',
                  103 : 'Samples Ready - Request Stop'
                 }

    @classmethod
    def _print(cls, tc):
        """
        Helper Method for displaying the error code in a human readable format
        :param tc: Integer Code
        :return: String representation of the error code.
        """
        return cls.__code_str[tc]

    @classmethod
    def _type(cls, tc):
        """
        Helper method for indicating the type of code
        :param tc: The (integer) code
        :return: One of the classes defining the four types of codes.
        """
        if tc < -100:   return cls.Error
        elif tc < 0:    return cls.Warning
        elif tc < 100:  return cls.Status
        else:           return cls.Request


class TCPOUpdate(object):
    """
    The class encapsulates a Code/Identifier pair to support queue messages in the Pipeline. Additionally, an info field
    is provided for further identification of the error source (optional)
    """
    def __init__(self, code, identifier, info = None):
        """
        Initialiser
        This is mainly used when the TCPOCode is used to transfer information in the module processing.
        :param code:        The Error Code (choose from TCPOCode.Error)
        :param identifier:  The Module Identifier (name)
        :param info:        [Optional] Additional Specification of the error.
        """
        # Store Values
        self.Code = code
        self.Iden = identifier
        self.Info = info


class TCPOException(Exception):
    """
    This is the base-class for all TCPO-framework exceptions. The code helps identify the type of error without the need
    to subclass the exception.
    """

    def __init__(self, code, msg=None):
        """
        Initialiser
        Basically a wrapper around the super initialiser, and also handles the error logging (which in turn must be
        overridden)
        :param code: [Required] - The code to specify the error
        :param msg: [Optional] - An optional Message
        """
        super(TCPOException, self).__init__()

        # Store some data about caller
        _stack = inspect.stack()[1]
        self.Module = inspect.getmodule(_stack[0]).__name__
        self.Function = _stack[3]
        self.Line = _stack[2]
        self.Msg = msg
        self.Code = code

        # Log the Error
        logging.getLogger().error('%s @ %s : %d%s', TCPOCode._print(self.Code), self.Module.split('.')[-1], self.Line,
                                  self._error(msg))

    def __str__(self):
        return super(TCPOException, self).__str__() + '{:s} @ {:s} : {:d}{:s}'.format(TCPOCode._print(self.Code),
                                                                                      self.Module.split('.')[-1],
                                                                                      self.Line, self._error(self.Msg))

    def _error(self, msg):
        return '' if msg is None else ' ({:s})'.format(msg)



