import numpy as np
import six

from matplotlib.widgets import AxesWidget
from matplotlib.patches import Ellipse
from matplotlib.mlab import dist


class HorRadioButtons(AxesWidget):
    """
    A GUI neutral radio button.

    This implements the same functionality as the corresponding RadioButtons class in the widgets module, but in a
    horizontal orientation, and with an included Title Field. The class also attempts to be 'smarter' in scaling the
    selection indicators to remain circular irrespective of the aspect ratio of the radio-button axis.
    """

    radius = 0.025   # This is the radius

    def __init__(self, ax, labels, title='', active=0, activecolor='blue'):
        """
        Add radio buttons to :class:`matplotlib.axes.Axes` instance *ax*

        *labels*
            A len(buttons) list of labels as strings

        *title*
            A title

        *active*
            The index into labels for the button that is active

        *activecolor*
            The color of the button when clicked
        """
        AxesWidget.__init__(self, ax)
        self.activecolor = activecolor
        self.value_selected = None

        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_navigate(False)
        self.dx = 1.0 / (len(labels) + 1)
        xs = np.linspace(self.dx, 1 - self.dx, len(labels))
        cnt = 0
        axcolor = ax.get_facecolor()

        # Calculate Ratio of Axes
        bbox = ax.get_window_extent().transformed(ax.transAxes)
        ratio = self.dx*bbox.size[0]/bbox.size[1]

        self.labels = []
        self.circles = []
        for x, label in zip(xs, labels):
            t = ax.text(x, 0.5, label, ha='center', va='center') # transform=ax.transAxes,

            if cnt == active:
                self.value_selected = label
                facecolor = activecolor
            else:
                facecolor = axcolor

            p = Ellipse(xy=(x-self.dx/3.0, 0.5), width=HorRadioButtons.radius, height=HorRadioButtons.radius*ratio,
                        facecolor=facecolor)

            self.labels.append(t)
            self.circles.append(p)
            ax.add_patch(p)
            cnt += 1

        self.title = ax.text(-0.02, 0.5, title, transform=ax.transAxes, va='center', ha='right')

        self.connect_event('button_press_event', self._clicked)

        self.cnt = 0
        self.observers = {}

    def _clicked(self, event):
        """
        Clicked Event
        :param event:
        :return:
        """
        if self.ignore(event):
            return
        if event.button != 1:
            return
        if event.inaxes != self.ax:
            return
        xy = self.ax.transAxes.inverted().transform_point((event.x, event.y))
        pclicked = np.array([xy[0], xy[1]])

        def inside(p):
            pcirc = np.array([p.center[0], p.center[1]])
            return dist(pclicked, pcirc) < self.dx

        for i, (p, t) in enumerate(zip(self.circles, self.labels)):
            if t.get_window_extent().contains(event.x, event.y) or inside(p):
                self.set_active(i)
                break
        else:
            return

    def set_active(self, index):
        """
        Trigger which radio button to make active.

        *index* is an index into the original label list
            that this object was constructed with.
            Raise ValueError if the index is invalid.

        Callbacks will be triggered if :attr:`eventson` is True.

        """
        if 0 > index >= len(self.labels):
            raise ValueError("Invalid RadioButton index: %d" % index)

        self.value_selected = self.labels[index].get_text()

        for i, p in enumerate(self.circles):
            if i == index:
                color = self.activecolor
            else:
                color = self.ax.get_facecolor()
            p.set_facecolor(color)

        if self.drawon:
            self.ax.figure.canvas.draw()

        if not self.eventson:
            return
        for cid, func in six.iteritems(self.observers):
            func(self.labels[index].get_text())

    def on_clicked(self, func):
        """
        When the button is clicked, call *func* with button label

        A connection id is returned which can be used to disconnect
        """
        cid = self.cnt
        self.observers[cid] = func
        self.cnt += 1
        return cid

    def disconnect(self, cid):
        """remove the observer with connection id *cid*"""
        try:
            del self.observers[cid]
        except KeyError:
            pass