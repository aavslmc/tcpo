# This Module will serve as an alternative and extension to numpy - hence the name
import numpy as np
import math


def exp10(array):
    """
    Raise 10 to array, elementwise

    The function is a convenience similar to numpy's exp and exp2

    :param array: Arraylike or scalar to raise 10 to.
    :return: Numpy Array (or scalar) of same shape and type as array, with values 10 ^ array
    """
    return np.power(10.0, array)


def get_nearest_idx(array, value, sorted=False):
    """
    Get the Index in the numpy array which is closest to the passed value (typically floating point)
    :param array:   A 1-D Array
    :param value:   The value to compare to
    :param sorted:  Indicates if a sorted array - in this case, a more efficient method is employed. The default is
                    that it is unsorted.
    :return:        The index of the element which is nearest to the passed value
    """
    if sorted:
        idx = np.searchsorted(array, value, side="left")
        if idx > 0 and (idx == len(array) or math.fabs(value - array[idx - 1]) < math.fabs(value - array[idx])):
            return idx - 1
        else:
            return idx
    else:
        return (np.abs(array - value)).argmin()


def array_nan_equal(left, right):
    """
    Compares two numpy arrays to test for equality, but considers np.nan values to be equal to each other
    :param left: The Left array to consider
    :param right: The Right array to consider
    :return: True if left is the same shape as right, and all elements are equal (including nan's at same positions)
    """
    # Ensure that Arrays (potentially convert from list)
    try: left, right = np.asarray(left), np.asarray(right)
    except: return False

    # Check that Shape is correct
    if left.shape != right.shape: return False

    # Return
    return bool(np.logical_or(np.asarray(left == right), np.logical_and(np.isnan(left), np.isnan(right))).all())


def rms(x, axis=None, ignore_nan=False, keepdims=False):
    """
    Computes the Root-Mean-Square of an array.

    The method is simply a wrapper around calling np.sqrt(np.mean(np.square(x))).

    :param x:   The Matrix to compute for
    :param axis: Axis or Axes along which to operate
    :param ignore_nan: If True, will use nanmean rather than mean
    :param keepdims: Keepdims parameter passed to the mean method.
    :return: Matrix containing the RMS deviation.
    """
    if ignore_nan:
        return np.sqrt(np.nanmean(np.square(x), axis=axis, keepdims=keepdims))
    else:
        return np.sqrt(np.mean(np.square(x), axis=axis, keepdims=keepdims))
