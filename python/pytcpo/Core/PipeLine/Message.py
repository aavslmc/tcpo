# Defines Core Functionality to be used pipe-line wide
import copy
import logging


class Message(object):
    """
    Defines the Generic (but Concrete) Data Component Object

    The Message class encapsulates all data which is to flow through the pipeline. Apart from encapsulating the actual
    data, the class maintains the Selector, Index and TimeStamp associated with the message. These typically remain the
    same as the data flows through the pipeline. The Data item(s) themselves (which do change) are stored in a python
    dictionary for cross-referencing with generator. However, since most use cases assume a single data item, a default
    accessor is provided.

    A Note on the selection process is in order. In general, different pipelines may utilise different selection
    mechanisms (for example, separating messages based on tiles or based on beams). To help alleviate any confusion
    with what the Selector field is representing, a flag must be passed to the Message Constructor to specify this
    explicitly. Note that however, the Message itself does not perform any checks - it is up to the user to do this.
    """
    
    class SelType(object):
        """
        This class enumerates the type of selector(s) employed in the Message
        """
        NONE = 0  # No Selection is available
        TILE = 1  # Selection based on tile
        BEAM = 2  # Selection based on beam

    def __init__(self, selector, idx, ts, name='default', default=None):
        """
        Initialiser Method

        :param selector: A Sub-set Selection identifier. This must be a tuple, containing the selection-type flag, and
                    the actual selection value (respectively).
        :param idx: A numerically increasing index such that messages can be ordered. The generator must ensure that
                    indices are always increasing and that there are no duplicates.
        :param ts: Time-Stamp associated with this
        :param name: [Optional] Specifies the name (key) associated with the default data item. By default this is
                     'default'. Specifying None indicates that this message does not support a default data item (this
                     happens for example in Aggregator Modules).
        :param default: [Optional] Immediately sets the default data entry to the associated value.
        """
        # Store Meta Data
        self.SelectorMask = selector[0]
        self.Selector = selector[1]
        self.Index = idx
        self.Time = ts

        # Store Naming (Default field)
        if name is not None:
            self.__name = name              # Default Entry Name: in this case, include default entry if not None
            if default is not None:
                self.Data = {name: default}
            else:
                self.Data = {}              # Still Empty... but default name is set...
        else:
            logging.getLogger().debug('Message initialised without a default entry')
            self.Data = {}                  # This guy does not support default data

    def __getitem__(self, item):
        """
        Override for the [] operator to provide read access to the underlying dictionary items by name.
        :param item: String representing the entry key.
        :return: The Value associated with item.
        :raises KeyError: If the named item is not available.
        """
        return self.Data[item]

    def __setitem__(self, key, value):
        """
        Override of the [] operator to provide write access to the underlying dictionary items by name.
        :param key: String representation of the data item
        :param value: The value to associated with key
        :return: None
        """
        self.Data[key] = value

    def __len__(self):
        """
        Override of the length method to provide access to the number of items in the dictionary.
        :return: The number of data items currently defined in the message
        """
        return len(self.Data)

    @property
    def Default(self):
        """
        Property getter of the Default Data Item. This is equivalent to default = Message[Message.__name]. The method
        will raise an *AttributeError* exception if the Message does not support a default data item.
        :return: The component associated with the default entry
        """
        return self.Data[self.__name]

    @Default.setter
    def Default(self, component):
        """
        Property setter for the Default Data Item. This is equivalent to Message[Message.__name] = default. The method
        will raise an *AttributeError* if the default data item functionality is not supported.
        :param component: The component to set the default data item with.
        :return: None
        """
        self.Data[self.__name] = component

    @property
    def All(self):
        """
        Property accessor to all data items
        :return: A View into all data items (in no particular order)
        """
        return self.Data.viewvalues()

    def add(self, name, component):
        """
        Data Item Addition
        This (rather than the []-setter) is the preferred way of adding data items to the message.
        :param name: The name of the component to add
        :param component: The actual value of the data component
        :return: None
        """
        self.Data[name] = component

    def part(self, name):
        """
        Part Copy of the Message

        This creates a *NEW* message, with the same Selector (and mask), Index and Time, but a specified default entry
        name. This is typically used when processing messages in the pipeline, as a convenient way of forwarding the
        results of an operation to the next processor in turn.

        :param name: The name to associate with the default data item of the new message. The same rules as for the
                     Initialiser apply.
        :return: The newly created message.
        """
        return Message((self.SelectorMask, self.Selector), self.Index, self.Time, name)

    def shallow(self, name=None):
        """
        Performs a shallow copy of the Message.
        Within the TCPO Pipeline framework, a shallow copy implies that the new message has:
         * Copies of the Selector, Index and TimeStamp (which are raw data types passed by value)
         * A newly-specified default name.
         * Copies of the dictionary keys (i.e. a new dictionary)
         * References to the current data items (i.e. this is where the Shallow Copy appears)
        :param name: The new default name for the new message
        :return: The newly created message.
        """
        msg = self.part(name if name is not None else self.__name)
        msg.Data = {}
        for k, v in self.Data.iteritems():
            if k == self.__name:   # If our default data item:
                msg.Default = v # Use the new Default Name
            else:
                msg.Data[k] = v
        return msg

    def deep(self, name=None):
        """
        Performs a deep copy of the Message.
        The deep copy copies all components of the message, enforcing also a deep copy of the data items themselves. The
        only exception is again the default (name) behaviour which can be specified.
        :param name: The new default name for the new message
        :return: The newly created message
        """
        msg = self.part(name if name is not None else self.__name)
        msg.Data = {}
        for k, v in self.Data.iteritems():
            if k == self.__name:   # If our default data item:
                msg.Default = copy.deepcopy(v) # Use the new Default Name
            else:
                msg.Data[k] = copy.deepcopy(v)
        return msg