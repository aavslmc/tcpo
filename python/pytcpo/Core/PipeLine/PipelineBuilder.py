import abc
import sys
import copy
import logging

from PipelineManager import PipelineManager
from pytcpo.Core.Common import tupler, namer
from pytcpo.Core.Common import TCPOCode, TCPOException


class AbstractPipelineBuilder(object):
    """
    This Abstract Base Class defines the interface to be exposed by Concrete Pipeline Builders which serve a specific
    purpose (for example, a Calibration Pipeline/Band-Pass Fitting Pipeline/Beam-Forming Pipeline).

    The methodology to be followed is that the Builder acts like a quasi-static definition of the pipeline, with the
    user able to tune some limited parameters.  In this way, the builder can act as a replicator of the pipeline and
    for communicating the structure around. Each call to the build method creates a new PipelineManager, sets it up,
    builds it and returns it.

    The Parameter Specification is designed along the paradigm used by the optparse library in python: i.e. a method is
    provided to add arguments, which method must be used within the specification abstract method to define the
    parameters to use.

    Since this is a setup-stage object, all errors are signalled through exceptions.
    """

    __metaclass__ = abc.ABCMeta

    class Parameter(object):
        """
        Encapsulated class for specifying the options associated with a parameter
        """
        def __init__(self, name, description, required, allow_type, default):
            self.Name = name
            self.Desc = description
            self.Reqd = required
            self.DefV = default
            self.Type = tupler(allow_type)

        def represent(self):
            required = " [Required] " if self.Reqd else " [Optional] "
            default = (" (Default: " + str(self.DefV) + ")" ) if self.DefV is not None else ""
            type_val = (" {Type: (" + "".join((namer(t) + ",") for t in self.Type) + ")}") if self.Type is not None else ""
            return [self.Name, required, self.Desc, default, type_val]

    @abc.abstractmethod
    def _specification(self):
        """
        Option Specification Method (protected: should not be called explicitly)

        Abstract Method which must be implemented to specify the parameters associated with the pipeline. The
        implementation should make use of the _add_option(...) method to specify the list of options which will then
        appear as object attributes. The method will be called within the initialiser.

        :return: None (Ignored). Errors must be raised as exceptions.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _structure(self, manager):
        """
        Pipeline Builder Method (protected: should not be called explicitly)

        Abstract Method which must be implemented to build the structure for the provided Pipeline Manager instance. The
        AbstractPipelineBuilder guarantees that when this is called, the system has already been setup (options have all
        been specified)

        :param manager: The 'clean' Pipeline Manager instance to build upon
        :return: CommandCode indicating success: Errors should be raised as Exceptions
        """
        raise NotImplementedError()

    @abc.abstractproperty
    def archive(self):
        """
        Archive Output Specification (abstract property)

        Must be implemented to indicate (programatically) the list/type of output the pipeline produces through its
        archiver (if any). This is indicated through a dictionary. If maintained as a member variable, this returned
        dictionary must be deep copied to prevent accidental modification.

        :return: Python dict mapping archive keys (fully qualified names) to ASerialiseable/core types stored at that
                 key. If the pipeline does not archive anything, it should return an empty dictionary.
        """

    # ====================== Implementation ====================== #

    def __init__(self):
        """
        Default Initialiser.

        ***IMPORTANT*** This Initialiser must be called by any derived class, since otherwise, the option processing
        will not work.
        """
        self._Params = {}
        self._specification()
        self._setup = False
        self._offline = None

    def setup(self, params, offline=False):
        """
        Setup Method

        Should be called to setup the pipeline with options. The Method will ensure that all required parameters are
        set, and that the correct types are specified if provided.

        Note that since the module is in general stateless (apart from needing to be setup to build), multiple setup's
        will overwrite each other

        :param params: A Dictionary of parameter/value pairs. All required parameters must be provided, otherwise will
                       fail.
        :param offline: This is the only abstract-class-wide option. It governs whether the pipeline will run in an
                        offline or online manner. The interface itself does not make any use of it, but it is available
                        for all setup to access.
        :return: CommandCode Indication of success - Errors are raised as Exceptions
        """
        # Warn if previously setup
        if self._setup:
            logging.warn('%s was already set-up: new call will overwrite old parameters.' % type(self).__name__)

        # Reset Setup, so if an error happens, then will not be garbage values.
        self._setup = False

        # Ensure that python dict
        if type(params) is not dict:
            raise TCPOException(TCPOCode.Error.TYPE, "Argument 'params' must be a Python dict.")

        # Iterate through all options
        for key, option in self._Params.iteritems():
            # Check if available in dictionary, and if not but required, fail
            if not (key in params) and option.Reqd:
                raise TCPOException(TCPOCode.Error.MISSING, "Option " + key + " is required but was not provided.")

            # Now get the value, with optional default and check type
            value = params.get(key, option.DefV)
            if (option.Type is not None) and not (type(value) in option.Type):
                raise TCPOException(TCPOCode.Error.TYPE, "Option " + key + " must be of type " + str(option.Type))

            # Assign Value
            self.__dict__[option.Name] = value

        # If made it this far, return success after updating state
        self._setup = True
        self._offline = offline
        return TCPOCode.Status.SETUP

    def build(self):
        """
        Pipeline Building (publicly accessible)

        Called to build (generate) a Pipeline Manager Instance which is then returned.

        :return: The Pipeline Manager instance if successful (error code are raised as exceptions)
        """
        # Ensure we have been setup
        if not self._setup:
            raise TCPOException(TCPOCode.Error.STATE, "Builder has not been setup yet.")

        # Create, setup and build Manager
        manager = PipelineManager()
        self._structure(manager)
        manager.build()

        # Return Result
        return manager

    @property
    def options(self):
        """
        Property accessor for the List of Specified Options.

        :return: A Deep Copy of the options dictionary (prevents accidental modification by user)
        """
        return copy.deepcopy(self._Params)

    def help(self, output=sys.stdout):
        """
        Helper function for printing the list of options

        :return: None - Output is printed to screen
        """
        output.write("Options for " + namer(type(self)) + " Pipeline\n\n")

        if len(self._Params) < 1:
            output.write('No Options Registered\n')

        else:
            width = max(len(opt) for opt in self._Params) + 2

            for opt in self._Params.itervalues():
                l = opt.represent()
                output.write("%-*s %s %s %s %s\n" % (width, l[0], l[1], l[2], l[3], l[4]))

    def _add_option(self, name, description, required = True, allow_type = None, default = None):
        """
        Option Definition (protected: should be used only within the _specification method)

        Method used for defining the options to the pipeline object.

        There is an ISSUE in defining options which can accept a None Value amongst others. In this case, the only
        possibility is to not specify the data type at all. Similarly, when defining the default value, if this is None,
        there is no difference from not specifying a default value.

        :param name:        [Required] The option name (as a string)
        :param description: [Required] The description of the feature
        :param required:    [Optional] Indicates whether the option is required (defaults to True)
        :param allow_type:  [Optional] A type or tuple of types indicating what the possible types are for the option.
                                If left none, then no type-checking is performed (all types accepted)
        :param default:     [Optional] A default value (if any - defaults to None)
        :return: Indication of success (CommandCode.OK) or failure through Exception.
        """
        # First check that option is not already in existence, and if so fail
        if name in self._Params:
            raise TCPOException(TCPOCode.Error.DUPLICATE, "Option " + name + " has already been registered.")

        # Now Failure, if Optional, Default Value provided not of same type as allow_type
        if (not required) and (allow_type is not None) and (type(default) not in tupler(allow_type)):
            raise TCPOException(TCPOCode.Error.TYPE, "Option %s requires type(s) %s but default is of type %s." %
                                                    (name, str(allow_type), type(default)))

        # Now do a warning that if required but default specified, it is useless
        if required and default is not None:
            logging.getLogger().warn("Option " + name + " is required - ignoring default value")
            default = None

        # Another warning if default is none but optional
        if not required and default is None:
            logging.getLogger().warn("Option " + name + " is optional but has no explicit default value apart from None")

        # Finally fill in and set attribute
        self._Params[name] = self.Parameter(name, description, required, allow_type, default)
        setattr(self, name, default)

        # Return OK
        return TCPOCode.Status.OK
