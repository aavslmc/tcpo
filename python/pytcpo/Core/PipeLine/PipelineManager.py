import logging
import Queue as qu
import threading as td

from PipelineModule import AggregatorModule
from pytcpo.Core.Common import tupler
from pytcpo.Core.Common.ErrorHandling import TCPOCode, TCPOUpdate, TCPOException
from pytcpo.Core.Archiving import PersistorFactory, APersistor


class PipelineManagerState(object):
    """
    This is a convenience class for encapsulating (enumerating) the Pipeline State, in the absence of enum constructs.
    """
    # Class Constants
    ERROR = -1      # Error State
    INIT = 0        # Initialised
    BUILT = 1       # Built
    ACTIVE = 2      # Actively executing
    STOPPED = 3     # Stopped

    # Private Dictionary
    __code_str = {-1:'Generic Error - Undefined State',
                   0:'Initialisation Process',
                   1:'Pipeline is Built',
                   2:'Active Process'}

    @classmethod
    def _print(cls, tc):
        """
        Helper Method (class method) for outputting the state code in a human-readable format.
        :param tc: The State Code value
        :return: String representation of the state code
        """
        return PipelineManagerState.__code_str[tc]

    def __init__(self):
        """
        Empty Initialiser
        """
        pass


class PipelineManager(object):
    """
    This class is designed to manage a pipeline's life-cycle, all the way from construction through tear-down. It also
    handles errors and guards against illegal operations. The convention is that setup errors are raised as exceptions
    nonetheless (since code cannot progress) up to the point of starting, but run-time erros are caught and handled,
    and may terminate the pipeline but will do so cleanly.

    Nodes within the pipeline are identified by a unique string, which is automatically generated and used to refer to
    the specific module.

    The current limitation in the pipeline manager concerns the stopping scheme, specifically when using a callback. In
    this case, the programmer still is responsible for calling stop and must do so outside the callback (otherwise will
    block). May need to be sorted out more cleanly in the future.
    """

    # Factory Pattern - Placeholder for the modules to utilise
    __Modules = {}

  #==================================================================================================================#

    @classmethod
    def registerModule(cls, pipeline_module):
        """
        Factory Pattern Registration
        All Module types must be registered with the Manager to allow dynamic instantiation. The registration must
        happen at the outermost level from which the object is imported (typically the __init__ file for the package)
        :param pipeline_module: The Module type. This must be a concrete implementation of either AbstractProcessor or
               AbstractGenerator.
        :return: None
        """
        cls.__Modules[pipeline_module.__name__] = pipeline_module

    @classmethod
    def getModule(cls, name):
        """
        Provides Access to the registered module types by way of their string name.
        :param name: The string representation of the module type.
        :return: The Type object corresponding to the class associated with name. Will raise an exception if it was
                 not registered.
        """
        return cls.__Modules[name]

  #==================================================================================================================#

    @property
    def Last(self):
        """
        Provides access to the last-added leaf node in the DAG which forms the pipeline.
        :return: The String identifier of the last-added leaf module or None if the structure is empty.
        """
        return self.__pipeline_l[-1].Name if len(self.__pipeline_l) > 0 else None

    def __init__(self):
        """
        Initialiser
        """
        self.__lock = td.Lock()                     # thread-safety locking
        self.__pipeline_d = {}                      # Will be a dict of pipeline modules
        self.__pipeline_l = []                      # Pipeline List
        self.__archiver = None                      # The Archiver instance
        self.__arch_opts = None                     # Archiver options
        self.__state = PipelineManagerState.INIT    # State of the pipeline-manager - For Error Handling
        self.__manage_thrd = None                   # The management Thread (which will be used to regularly check for failures etc..)
        self.__manage_queu = qu.Queue()             # Management Queue: will receive CommandCode types
        self.__exit_code = None                     # Exit Code
        self.__end_callback = None                  # An End Callback (registered during start)

    def set_archiver(self, persistor, serialiser):
        """
        Method for Setting up the Archiving System to use with this manager.
        :param persistor: A Python dict containing the following items relating to the Persistor
                    * type - [Required]: The Persistor type (name)
                    * append - [Optional]: Boolean flag indicating whether in append mode (default True)
                    * setup - [Optional] : Additional parameters required for the Persistor (default None)
        :param serialiser: A Python dict containing the following items relating to the Serialiser
                    * type - [Required]: The Serialiser type (name)
                    * setup - [Optional]: Any additional specifications for the serialiser
        :return: Status.OK if successful
        :raise TypeError: If persistor/serialiser are not dictionaries
        :raise KeyError: If any required parameter is not specified
        :raise TCPOException:
                    * Error.STATE - Manager is already built
                    * Error.DUPLICATE - An archiver is already specified
        """
        with self.__lock:
            # Error Checking
            if self.__state != PipelineManagerState.INIT:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot setup Archiver: Manager has been built or is active')
            if self.__archiver is not None:
                raise TCPOException(TCPOCode.Error.DUPLICATE, 'Cannot setup Archiver: One is already specified')

            # Store Options so it can be setup on start
            self.__arch_opts = [APersistor.M_WRITE | APersistor.M_APPEND if persistor.get('append', True) else APersistor.M_WRITE,
                                persistor.get('setup', None),
                                serialiser['type'],
                                serialiser.get('setup', None)]

            # Create one of the correct type: and return if no error
            self.__archiver = PersistorFactory.getPersistor(persistor['type'])()
            return TCPOCode.Status.OK

    def add_root(self, type_name, setup, archive_key = None):
        """
        Method for specifying the Root Node.
        This method must be called only once and before adding any additional modules. The Method is thread-safe.
        :param type_name:   The type of module to add (string name). This is typically a generator module.
        :param setup:       The setup data associated with the root node (module of type type_name)
        :param archive_key: The Key (if any) to associate with this module
        :return: The String identifier which can be used to refer to this module. This is typically '<moduletype>_000'.
        :raise TCPOException: on error
        """
        with self.__lock:
            # Error Checking
            if self.__state != PipelineManagerState.INIT:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot add modules: Manager has been built or is active')
            if len(self.__pipeline_l) > 0:
                raise TCPOException(TCPOCode.Error.DUPLICATE, 'Cannot add another root: one already exists')
            if archive_key is not None and self.__archiver is None:
                raise TCPOException(TCPOCode.Error.ARCHIVE, 'Cannot add archiveable module: Archiver has not been set up')

            # Attempt to create and setup
            module_type = self.getModule(type_name)
            module_object = module_type()
            module_name = type_name + '_000'
            archive_key = None if archive_key is None else (self.__archiver, archive_key)
            module_object.setup(module_name, self.__manage_queu, setup, archive_key)

            # Update Pipeline and return
            self.__pipeline_d[module_name] = module_object
            self.__pipeline_l.append(module_object)

            # Log & return name
            logging.debug('Adding Root Module: %s', module_name)
            return module_name

    def add_module(self, type_name, setup, parents = None, archive_key = None):
        """
        More generic module addition, whereby structure is specified by referencing the parent(s). Parents are specified
        through their string identifier. If the number of parents is larger than 1, then:
           a) An Aggregator Module is automatically inserted
           b) The first parent specified is set as the default data item
        :param type_name: The string-based type of the module.
        :param setup: The setup data required by the module
        :param parents: [Optional] A parent name or tuple containing the parents of this node. If None, then the module
                        is automatically attached to the last-added node.
        :param archive_key: [Optional] Key (if any) to associate this module with when storing
        :return: The (string) identifier of the module if successful
        :raise TCPOException: on (state) Error
        """
        with self.__lock:
            # State Error Checking
            if self.__state != PipelineManagerState.INIT:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot add modules: Manager has been built or is active')
            if len(self.__pipeline_l) < 1 :
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot add module: No Root has been specified')
            if archive_key is not None and self.__archiver is None:
                raise TCPOException(TCPOCode.Error.ARCHIVE, 'Cannot add archiveable module: Archiver has not been set up')

            # Tuple-format the Parents and then check that all exist
            parents = tupler(parents if parents is not None else self.__pipeline_l[-1].Name)
            for parent in parents:
                if not parent in self.__pipeline_d:
                    raise TCPOException(TCPOCode.Error.VALUE, 'Parent %s does not exist' % parent)

            # Attempt to create and setup
            module_type = self.getModule(type_name)
            module_object = module_type()
            module_name = type_name + '_' + str(len(self.__pipeline_l)).zfill(3)
            archive_key = None if archive_key is None else (self.__archiver, archive_key)
            module_object.setup(module_name, self.__manage_queu, setup, archive_key)

            # Branch on length of pipeline
            if len(parents) > 1:
                # Create Aggregator
                logging.debug('%s has multiple parents: adding Aggregator Module', module_name)
                aggregator = AggregatorModule()
                aggregator_name = 'aggregator_' + str(len(self.__pipeline_l) + 1).zfill(3)
                aggregator.setup(aggregator_name, self.__manage_queu, {'default':parents[0], 'names':parents})

                # Register connections
                for parent in parents: self.__pipeline_d[parent].registerConsumer(aggregator)
                aggregator.registerConsumer(module_object)

                # Update pipeline
                self.__pipeline_d[aggregator_name] = aggregator
                self.__pipeline_l.append(aggregator)
                self.__pipeline_d[module_name] = module_object
                self.__pipeline_l.append(module_object)
            else:
                # Register connections
                for parent in parents: self.__pipeline_d[parent].registerConsumer(module_object)

                # Update pipeline
                self.__pipeline_d[module_name] = module_object
                self.__pipeline_l.append(module_object)

            # Log & return the Name
            logging.debug('Adding Module: %s', module_name)
            return module_name

    def invoke(self, module, method, *args):
        """
        Access a module's method by name

        This method should be used with caution, and generally only through a pipeline builder interface. It allows
        invoking a specific method on a module. The entire process is wrapped within the global lock, so is somewhat
        threadsafe.

        :param module: The Module reference to identify the processor/generator module...
        :param method: The Method to invoke
        :param args: Any arguments which
        :return: Any return value returned by the module method
        :raises TCPOException: if pipeline has not been built.
        :raises KeyError: if module does not exist
        :raises AttributeError: if method is not implemented for module.
        """
        with self.__lock:
            # Ensure that can invoke
            if self.__state < PipelineManagerState.BUILT:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot invoke methods on non-built pipeline')
            # Invoke method
            return getattr(self.__pipeline_d[module], method)(*args)

    def build(self):
        """
        This method was implemented to support the future possibility of separating the pipeline specification from the
        setup stage (this would also allow replication of pipelines). Currently, it simply advances the state (unless
        called in an incorrect state to start with).
        :return: CommmandCode.OK if successful: Errors will be signaled through Exceptions
        """
        with self.__lock:
            # Error Handling
            if self.__state != PipelineManagerState.INIT:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot Build - Pipeline is not initialised or is active.')
            if len(self.__pipeline_l) < 1:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot Build - No Modules to build.')

            # Perform Actual Building
            self.__state = PipelineManagerState.BUILT

            # Return OK
            return TCPOCode.Status.OK

    def reset(self):
        """
        Resets the Pipeline Structure to an empty Architecture (clean slate). Will fail if the pipeline is active.
        :return: None
        :raises TCPOException: Will signal STATE error if in active state.
        """
        with self.__lock:
            if self.__state == PipelineManagerState.ACTIVE:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot Reset - Pipeline in operation')
            else:
                self.__pipeline_d = {}
                self.__pipeline_l = []
                self.__archiver = None
                self.__state = PipelineManagerState.INIT  # State of the pipeline-manager - For Error Handling
                self.__manage_thrd = None  # The management Thread (which will be used to regularly check for failures etc..)
                self.__manage_queu = qu.Queue()  # Management Queue: will receive CommandCode types
                self.__exit_code = None

    def start(self, callback = None):
        """
        Start the Pipeline Run, and consequently the management loop on a separate thread and return.
        The pipeline must have been defined and built. The function takes care of initialising communication channels,
        before starting each module, starting from the leaf nodes to the root. The method also handles exceptions itself
        and ensures a consistent state - i.e. it does not raise exceptions.

        Note that no guarantee is given on the start order, except that children are always initialised before their
        parents.

        For Communication with the main loop, the manager provides the option of passing in a callback, taking in one
        parameter (the Exit Code). This will be called when the pipeline terminates, iff start succeeds: i.e. if start
        fails, it will NOT be called. Note that it will be called from the management loop (and hence on a separate
        thread) - Care should be taken to ensure that this does not affect thread-safety.

        :param callback: A function to be called when the pipeline terminates. The method should accept one argument.
        :return: CommandCode.STILL_ACTIVE if successful, appropriate error otherwise.
        """
        with self.__lock:
            # Ensure correct state
            if self.__state != PipelineManagerState.BUILT and self.__state != PipelineManagerState.STOPPED:
                logging.getLogger().error('Cannot Start - Pipeline has not been built or is already active.')
                return TCPOCode.Error.STATE

            # Start the Archiver if available:
            #   Since the Archiving framework does not have a start method, the initialisation all happens in the setup
            #   phase. As such, it is expected to be called each time the pipeline is started to really restart.
            if self.__archiver is not None:
                try:
                    self.__archiver.setup(*self.__arch_opts)
                except TCPOException as te:
                    logging.info('Caught TCPO Exception %s from Archiver', TCPOCode._print(te.Code))
                    return te.Code
                except BaseException as be:
                    logging.error('Start Failed due to (%s:%s) Exception in Archiver', type(be).__name__, be.message)
                    return TCPOCode.Error.MODULE

            # Initialise Core Threading/Management Stuff - but do not start it...
            self.__manage_thrd = td.Thread(target=self.__management_loop)
            self.__manage_thrd.daemon = True

            # Now start from the end and inform starting process:
            try:
                for i in reversed(range(len(self.__pipeline_l))):
                    self.__pipeline_l[i].start()
            except TCPOException as te:
                logging.getLogger().info('Caught TCPO Exception %s from module %s', TCPOCode._print(te.Code), self.__pipeline_l[i].Name)
                for j in range(i+1, len(self.__pipeline_l)):
                    self.__pipeline_l[j].stop(te.Code)
                return te.Code
            except BaseException as e:
                logging.getLogger().error('Start Failed due to %s Exception in module %s', e, self.__pipeline_l[i].Name)
                for j in range(i + 1, len(self.__pipeline_l)):
                    self.__pipeline_l[j].stop(TCPOCode.Error.MODULE)
                return TCPOCode.Error.MODULE

            # Finally, if made it this far register the callback (if any) and start the management loop
            self.__end_callback = callback
            self.__manage_thrd.start()
            self.__state = PipelineManagerState.ACTIVE

            # Return Success if made it this far...
            return TCPOCode.Status.ACTIVE

    def wait_until_ready(self, timeout = None):
        """
        Allows the caller to block until the pipeline run has terminated. Note that this method will not send a stop
        signal, meaning if the generators in the pipeline do not terminate of their own accord the method will block
        indefinitely (or until an error happens), unless a timeout is specified.

        If a callback is registered, it is the responsibility of the programmer that dead-locks are prevented (since the
        callback will be called on a separate thread).

        :param timeout: [Optional] The time in seconds to block for: if None will block indefinitely
        :return: The exit code if the pipeline stopped within the timeout, CommandCode.STILL_ACTIVE if the waiting
                 timed out and the appropriate error code if the request is unsuccessful.
        """
        with self.__lock:
            # Ensure correct state
            if self.__state < PipelineManagerState.ACTIVE:
                logging.getLogger().warn('Pipeline is Not Active')
                return TCPOCode.Warning.STATE

            # If already stopped
            if self.__state == PipelineManagerState.STOPPED:
                logging.debug('Pipeline had already been stopped.')
                self.__manage_thrd = None
                self.__manage_queu = None
                if self.__archiver is not None: self.__archiver.Close()
                return self.__exit_code

            # Else, join
            logging.debug('Manager - Waiting...')
            self.__manage_thrd.join(timeout)
            if self.__manage_thrd.is_alive():
                logging.getLogger().warn('Pipeline did not finish within timeout specified')
                return TCPOCode.Status.ACTIVE

            # Clean up otherwise
            self.__manage_thrd = None
            self.__manage_queu = None
            if self.__archiver is not None: self.__archiver.Close()
            self.__state = PipelineManagerState.STOPPED

            # Return Exit Code
            return self.__exit_code

    def stop(self):
        """
        Request the Management Loop to stop and wait for it to actually terminate. The method also performs the
        necessary cleanup. It must be called at the end of the run to clean up everything, (unless using
        wait_until_ready): this includes when using a callback. In the latter case, it MUST not be called from within
        the callback itself since it will cause deadlocks. Rather the programmer is responsible for using event-driven
        processing to ensure consistency.

        All exceptions are handled, ensuring that the method does not raise any.

        :return: The exit code, typically CommandCode.USER_STOPPED, unless an error happens.
        """
        with self.__lock:
            # Ensure correct state
            if self.__state < PipelineManagerState.ACTIVE:
                logging.getLogger().warn('Pipeline is not active - nothing to stop')
                return TCPOCode.Warning.STATE

            # If already stopped
            if self.__state == PipelineManagerState.STOPPED:
                logging.getLogger().debug('Pipeline has already been stopped.')
                self.__manage_thrd = None
                self.__manage_queu = None
                if self.__archiver is not None: self.__archiver.Close()
                return self.__exit_code

            # Else, join
            self.__manage_queu.put(TCPOUpdate(TCPOCode.Request.STOP_USER, 'manager'))
            self.__manage_thrd.join()

            # Clean up otherwise
            self.__manage_thrd = None
            self.__manage_queu = None
            self.__state = PipelineManagerState.STOPPED

            # Close the Archiver, with exception handling
            try:
                if self.__archiver is not None: self.__archiver.Close()
            except TCPOException as te:
                logging.info('Caught TCPO Exception %s when closing Archiver', TCPOCode._print(te.Code))
                return te.Code
            except BaseException as be:
                logging.error('Clean Close failed due to (%s:%s) Exception in Archiver', type(be).__name__, be.message)
                return TCPOCode.Error.CLOSING

            # Return Exit Code - which may not be a User_Stopped due to errors...
            return self.__exit_code

    def __management_loop(self):
        """
        This is a private method which implements the management thread which listens to and handles the requests on
        the management queue (which may come from either the modules or the user requesting to stop). It also ensures
        clean termination, by cleanly stopping the modules, starting from the roots and working towards the leaves (i.e.
        in the opposite order of starting)

        :return: None
        """
        # Set up Exit Code, which will be also the flag for the loop
        self.__exit_code = TCPOCode.Status.ACTIVE

        # Loop ad infinitum...
        while self.__exit_code == TCPOCode.Status.ACTIVE:

            # Wait (block) until receive a message
            request = self.__manage_queu.get(block=True)

            # Switch on the requests
            if request.Code ==  TCPOCode.Request.STOP_USER:
                logging.getLogger().info('User Stop Requested')
                self.__exit_code = TCPOCode.Status.USER_STOP

            elif request.Code == TCPOCode.Request.STOP_FILE:
                logging.getLogger().info('File Ended - Requesting Stop')
                self.__exit_code = TCPOCode.Status.FILE_END

            elif request.Code == TCPOCode.Request.STOP_SMPL:
                logging.getLogger().info('Desired Sample Range Read - Requesting Stop')
                self.__exit_code = TCPOCode.Status.SMPL_STOP

            elif TCPOCode._type(request.Code) is TCPOCode.Error:
                logging.getLogger().info('Received %s Error - Stopping', TCPOCode._print(request.Code))
                self.__exit_code = request.Code

            elif TCPOCode._type(request.Code) is TCPOCode.Warning:
                logging.getLogger().info('Received %s Warning - Continuing', TCPOCode._print(request.Code))

            elif TCPOCode._type(request.Code) is TCPOCode.Status:
                logging.getLogger().debug('Received %s Status - Ignoring', TCPOCode._print(request.Code))

            else:
                logging.getLogger().warn('Received Strange Request %d - Ignoring', request.Code)

            # Task Done
            self.__manage_queu.task_done()

        # If exited loop, then stop each module cleanly
        for pi_mod in self.__pipeline_l: pi_mod.stop(self.__exit_code)

        # Finally invoke callback if any...
        if self.__end_callback is not None: self.__end_callback(self.__exit_code)

    @property
    def get_state(self):
        return self.__state
