# System Modules
import abc  # For Abstract Class
import inspect
import logging
import threading as td

from pytcpo.Core.Common import TCPOException, TCPOCode, TCPOUpdate


class AbstractPipelineModule(object):
    """
    This is the Abstract Base Class which represents a pipeline component. It is not deisgned to be inherited from
    directly - instead, users should inherit from the similarly abstract, but more complete, generator and processor
    modules defined below.

    The class is responsible for the base functionality, including naming of the module, consumer registration and
    handling, archive handling and finally error handling (through the Queue reference held) as well as thread-safety
    (a responsibility which is shared with the generator and processor derivations).
    """
    __metaclass__ = abc.ABCMeta # Enforce Abstraction

    @abc.abstractmethod
    def start(self):
        """
        Abstract Start Method (publicly accessible)

        This is implemented by the generator/processor classes to perform their own startup.

        :return: [Optional] Status Code (typically ACTIVE). Errors during startup should be signalled through Exceptions.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def stop(self, reason):
        """
        Abstract Stop Method (publicly accessible)

        Again, this is implemented by the generator/processor classes to handle cleanup at the end of an observation
        run.

        :param reason: The reason why the pipeline is being stopped (which may be used by the module)
        :return: None
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _setup(self, filter_model):
        """
        Abstract Setup Method (protected: should not be explicitly called)

        This is guaranteed to be the first method called after initialisation, and never called more than once if the
        module is used through the PipelineManager (which guards against out-of-order calls). The derived class may use
        this to setup resources required for operation or perform any performance-intensive operations apriori. It
        should be noted, that according to the pipeline life cycle, it is not necessary that the method is called before
        every call to start, as long as setup has completed successfully at least once.

        :param filter_model: The data to pass on to the concrete implementation, typically in the form of a dictionary.
        :return: [Optional] Status Code (typically Setup). Errors should be signalled through Exceptions.
        """
        raise NotImplementedError()

    @property
    def ArchiveEnabled(self):
        """
        Property which indicates whether this module is archiving data (typically used by the concrete implementation to
        avoid processing time associated with archive data if the archiver has not been set).

        :return: True if enabled, False otherwise.
        """
        return self.__archiver is not None

    def __init__(self):
        """
        Default Initialiser
        """
        # Thread-safety
        self._lock = td.Lock()  # Locking around outer parameters: protected but not private, so direct descendants
                                #  can access it...

        # Data Management
        self.Name = None        # The Module Identifier - and also the name which will be used in data output
        self._consumers = None  # Consumer List
        self.__archiver = None  # Archiver Reference
        self.__arch_key = None  # The Archiver Key to associate with this module
        self.__queue = None     # Queue for informing the manager with status updates (typically error handling)

    def setup(self, name, queue, module_opt = None, archiver = None):
        """
        Module Setup Procedure (publicly accessible)

        This must be the first method called. This is ensured if the module is used through the manager.

        :param name: The name to associate with this module (which is also used to set the default message item)
        :param queue: The Queue object to publish status updates on.
        :param module_opt: Any setup parameters to pass on to the concrete implementation.
        :param archiver: A Tuple consisting of the Archiver and the associated Key to use, when desired. Otherwise,
                         it can be left None.
        :return: Whatever the derived method '_setup' returns, typically Status.SETUP. Errors are raised as exceptions.
        """
        with self._lock:
            # Set up Name, Archiver and consumers
            self.Name = name
            self._consumers = []  # Empty List
            self.__archiver = archiver[0] if archiver is not None else None
            self.__arch_key = archiver[1] if archiver is not None else None
            self.__queue = queue

            # Perform Derived Setup
            return self._setup(module_opt)

    def registerConsumer(self, consumer):
        """
        Used to register a new consumer to forward the results of this module to.

        :param consumer: Reference to the consumer to push data to.
        :return: [Optional] Status Code (OK) if successful. Errors will be raised as exceptions, such as if the method
                 is used on itself, if setup has not completed successfully or if the consumer already is registered.
        """
        with self._lock:
            # Some Minor Error Checking
            if consumer is self: raise TCPOException(TCPOCode.Error.VALUE, 'Attempt to register self')
            if self._consumers is None: raise TCPOException(TCPOCode.Error.STATE, 'Setup has not been completed')
            if consumer in self._consumers: raise TCPOException(TCPOCode.Error.DUPLICATE, 'Consumer alredy registered')

            # Add consumer and return
            self._consumers.append(consumer)
            return TCPOCode.Status.OK

    def _removeConsumer(self, consumer):
        """
        Used to remove a previously reigstered consumer. The method should be used sparingly since lists are not
        designed for efficient deletions.

        :param consumer: The consumer to remove
        :return: None (Errors are signalled through exceptions)
        """
        with self._lock:
            self._consumers.remove(consumer)

    def _inform(self, reason, info = None):
        """
        Convenience method for abstracting the handling of the Error-Notification Queue.

        Note that since it is designed to be used within other functions, it is not Locked

        :param reason: The Error Code to report on the queue.
        :param info: Additional information to log (typically a string explaining the error in more detail)
        :return: None.
        """
        # First Log
        msg = '{:s} reports (from ln {:s}) {:s}{:s}'.format(self.Name, str(inspect.stack()[1][2]), TCPOCode._print(reason), (' ({:s})'.format(info) if info is not None else ''))
        if TCPOCode._type(reason) is TCPOCode.Error:
            logging.getLogger().error(msg)
        elif TCPOCode._type(reason) is TCPOCode.Warning:
            logging.getLogger().warn(msg)
        else:
            logging.getLogger().debug(msg)

        # Now send out
        if self.__queue is not None:
            self.__queue.put(TCPOUpdate(reason, self.Name, info))
        else:
            logging.critical('Queue does not exist for %s - cannot inform Manager of msg "%s"', self.Name, msg)

    def _archive(self, timestamp, data):
        """
        Archive Wrapper (protected: should not be used externally)

        Convenience Method for wrapping the archiving functionality. Should be called only from the directly-derived
        classes (generator and processor modules).  For this same reason, it is not thread-locked.

        :param timestamp: The Time-Stamp associated with the sample
        :param data: The sample data
        :return: TCPOCode.Status.OK if successful. Any failures will be thrown as exceptions
        """
        if self.ArchiveEnabled: self.__archiver.WriteSample(self.__arch_key, timestamp, data)
        return TCPOCode.Status.OK


class AbstractGenerator(AbstractPipelineModule):
    """
    The Abstract Generator Module encompasses modules which are placed at the start of the pipeline and serve to
    generate data. These are characterised by having no parents, and typically running their own loop, which for
    convenience is implemented on a separate thread. This class is hence responsible for thread-safety within the
    loop and (limited) lifetime management (which responsibility is in turn shared with the Pipeline Manager).
    """

    @abc.abstractmethod
    def _generator_loop(self):
        """
        Abstract Method for implementing the thread loop, used for the data generation.

        The method will be called when the generator module is started, running on a separate thread. It should take
        care of initialisation and cleanup within itself, and never return in an undefined state. All exceptions should
        be caught and the manager informed through the queue: in this case, it should cleanly terminate.

        :return: None
        """
        raise NotImplementedError()

    @property
    def Active(self):
        """
        Property-based indicator of Activity

        :return: True if the generation loop is active, false otherwise.
        """
        with self._lock: return self.__thread is not None

    def __init__(self):
        """
        Initialiser
        """
        super(AbstractGenerator, self).__init__()
        self._UserStop = td.Event() # An event used to signal the generator that an outside entity is requesting stop
        self.__thread = None # No Thread so far...

    def start(self):
        """
        Start override

        The method is thread-safe.

        :return: Status.ACTIVE if successful
        :raises TCPOException: If in incorrect state (not initialised, or already active)
        """
        with self._lock:
            # Error Handling
            if self.Name is None:
                raise TCPOException(TCPOCode.Error.STATE, '%s is not Initialised' % type(self).__name__)
            if self.__thread is not None:
                raise TCPOException(TCPOCode.Error.STATE, '%s is already Active' % type(self).__name__)

            # Start Generator Loop
            self.__thread = td.Thread(target=self._generator_loop)
            self.__thread.daemon = True
            self.__thread.start()

            # Indicate ok
            return TCPOCode.Status.ACTIVE

    def start_static(self):
        """
        Start in an 'offline' manner

        This is a convenience method for a static (blocking) start of the generation loop, when used outside the
        PipelineManager. The method will use the queue passed in as parameter to the setup method to communicate.
        The Method should be used with caution, as if the generator does not stop, it can block indefinitely, but
        conversely, it will stop on any queue message from the generator loop.

        :return: The result code published on the queue upon completion.
        :raise TCPOException: If cannot start: see start method above.
        :raise TypeError: If the queue was not correctly set - Note that in this case, the system may be in an undefined
                        state.
        """
        self.start()
        result = self._AbstractPipelineModule__queue.get(block=True) # Block until a message put on queue...
        self.stop(result.Code)
        return result.Code

    def _push(self, new_message, to_store):
        """
        Push Data (protected: Must be used solely by direct descendants)

        Convenience Method (to be used by concrete derivations) to forward the data to the consumers down the line. Note
        that the method may raise exceptions (typically through the archiving) which must be handled. The method is not
        thread-locked.

        :param new_message: The data (in Message format) to pass on
        :param to_store: Any Data to be Archived
        :return: [Optional] Status Code (OK) if successful. Errors (due to archiving) will be signaled through the
                 appropriate exception. The caller must capture the exception and handle it (possibly by informing the
                 manager).
        """
        # Potentially, archive
        if to_store is not None:
            self._archive(new_message.Time, to_store)
        if new_message is not None:
            for cons in self._consumers: cons.onSample(new_message)

        # Otherwise ok
        return True

    def stop(self, reason):
        """
        Stop Override (publicly accessible)

        Implementation of the stop (clean-up) method. This involves informing the generator loop through an event-based
        messaging system. The method returns once the generator loop has in fact terminated and the thread been joined.

        :param reason: Reason for terminating - not actually used
        :return: None
        """
        with self._lock:
            logging.debug('Stopping %s', self.Name)

            # Check that thread is actually active
            if self.__thread is None:
                logging.warn('Thread is already inactive - Nothing to stop')
                self._UserStop.clear() # Just in case...
                return

            # Stop Cleanly
            self._UserStop.set()
            self.__thread.join()

            # Clean up in preparation for a new run if need be...
            self.__thread = None
            self._UserStop.clear()


class AbstractProcessor(AbstractPipelineModule):
    """
    This Abstract Base Class defines the responsibilities for a processor-type module, which typically has a parent
    and one or more children: it receives data from that parent, processes it and forwards the result to each of its
    children.
    """

    @abc.abstractmethod
    def _new_sample(self, message, forward):
        """
        Sample Processing (protected: should not be called explicitly)

        To be implemented to handle reception and processing of a new Sample (Message). Will be called automatically for
        each sample that needs to be processed by this module. It is guaranteed that samples will arrive in order, and
        in a multi-processing environment, samples will be operated on in sequence.

        :param message: The message to process.
        :param forward: A list serving as a placeholder for two items, which must be filled by the implementation:
                    * forward[0] must be the message to push to the modules downstream. Can be None.
                    * forward[1] must be any data to be archived. Again, can be None
        :return: None (Will be ignored). Errors must be signalled through Exceptions (which will be caught).
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _will_start(self):
        """
        Indication of start (protected: should not be called explicitly)

        Will be called when the data driving process is starting. May be used to initialise any state-variables as
        necessary

        :return: None. Exceptions must be thrown on error.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _clean_up(self, reason):
        """
        Clean up Method (protected: should not be called explicitly)

        Called when the pipeline is stopping and the module needs to clean up.  Guaranteed to be called for each
        matching call to will_start, even if will_start fails! Note that the method MUST NOT raise exceptions.

        :param reason: Reason for stopping
        :return: None (ignored)
        """
        raise NotImplementedError()

    def __init__(self):
        """
        Initialiser
        """
        super(AbstractProcessor, self).__init__()
        self.__active = False

    def start(self):
        """
        Implementation of the Start Method

        :return: [Optional] Status Code if successful. Raises Exceptions on error
        """
        with self._lock:
            # Some Minor Error Checking
            if self.Name is None: raise TCPOException(TCPOCode.Error.STATE, 'Cannot Start if not Setup')
            if self.__active : raise TCPOException(TCPOCode.Error.STATE, 'Cannot Start already-started Module')

            # Wrap around exception catching to allow cleanup to happen correctly.
            try:
                self._will_start()
                self.__active = True
            except TCPOException as te:
                self._clean_up(te.Code)
                raise
            except BaseException:
                self._clean_up(TCPOCode.Error.MODULE)
                raise

            # Optional Return
            return TCPOCode.Status.ACTIVE

    def onSample(self, message):
        """
        Sample Handler

        Will be called by the parent module to handle a new message. For this reason, it is not designed to be
        explicitly called. The method is designed to be self-contained, capturing all exceptions and forwarding them
        over the queue to the manager. The method catches all exceptions and ensures that the pipeline manager is duly
        informed.

        :param message: The Message to process
        :return: None (ignored)
        """
        with self._lock:

            logging.debug('Sample %d for %s', message.Index, self.Name)
            # Ensure that active and if not raise warning (a warning, since this may happen while the master is shutting
            #   down the items...
            if not self.__active:
                self._inform(TCPOCode.Warning.STATE, 'onSample called when Inactive')
                return

            # Wrap in Try/Catch Block
            try:
                # First Process the Sample
                forward = [None, None]
                self._new_sample(message, forward)

                # Archive if need be
                if forward[1] is not None:
                    self._archive(message.Time, forward[1])

                # Forward Message(s)
                if forward[0] is not None and len(self._consumers) > 0:
                    new_message = message.part(self.Name)
                    new_message.Default = forward[0]
                    for cons in self._consumers: cons.onSample(new_message)

            except TCPOException as te:
                self._clean_up(te.Code)
                self._inform(te.Code, 'in onSample')
                self.__active = False

            except BaseException as be:
                self._clean_up(TCPOCode.Error.MODULE)
                self._inform(TCPOCode.Error.MODULE, 'onSample Failed due to {%s: %s}' % (type(be).__name__, str(be)))
                self.__active = False

    def stop(self, reason):
        """
        Implementation of the stop (cleanup) method. Same arguments/return values apply.
        """
        with self._lock:
            logging.getLogger().debug('Stopping %s', self.Name)
            if self.__active: self._clean_up(reason)
            self.__active = False


class AggregatorModule(AbstractPipelineModule):
    """
    Aggregator and Data Buffer Class

    This class is used as the front-end to fan-in architectures in the pipeline. It serves a dual purpose, that of
    consolidating messages arriving from different sources into one message with multiple data items, and in
    synchronising the same multi-source messages based on the index value through appropriate buffering.

    The class inherits directly from the AbstractPipelineModule since it does not need to implement any processing nor
    any generation functionality. Note that the implementation is able to also handle messages which already contain
    multiple data items.
    """

    def __init__(self):
        """
        Default Initialiser
        """
        super(AggregatorModule, self).__init__()

        # Setup Parameters
        self.__allowables = None    # Which are the allowable names
        self.__default_name = None  # Set something for the default

        # State Parameters
        self.__index_ptr = None     # Stores the samples compounded for a particular index: this is an integer
                                    # dictionary (indexed by Index) of string dictionaries (indexed by name). If none
                                    # indicates not active

    def _setup(self, filter_model):
        """
        Setup the Aggregator Module.

        :param filter_model: A Dictionary with two key-value pairs:
                'names' : The List of names of data items which will be received. The aggregator will use this to
                          identify when all data items at a particular time-index have been received and can be
                          forwarded as a single message.
                'default' : [Optional] An optional key to assign as the default entry in the compound message. If not
                            specified (none), the aggregated message will have no default data item. The name itself
                            may or may not have been already specified in the 'names' list.
        :return: Status Code SETUP on success. Errors are indicated via Exceptions.
        """
        # Ensure that not already active...
        if self.__index_ptr is not None:
            raise TCPOException(TCPOCode.Error.STATE, 'Cannot Setup - Module %s is already active' % self.Name)

        # Add Values
        self.__allowables = filter_model['names']
        self.__default_name = filter_model.get('default', None)
        if self.__default_name is not None and not self.__default_name in self.__allowables:
            self.__allowables.append(self.__default_name) # Append to allowables if not already in it...

        # Return OK
        return TCPOCode.Status.SETUP

    def start(self):
        """
        Implementation of the virtual start method.

        :return: Status Code (Errors through Exceptions)
        """
        with self._lock:
            # Check not active
            if self.__index_ptr is not None:
                raise TCPOException(TCPOCode.Error.STATE, 'Cannot ReStart - Module %s is already active' % self.Name)

            # Prepare
            self.__index_ptr = {}

            # Return
            return TCPOCode.Status.ACTIVE

    def onSample(self, message):
        """
        Message Handler

        When a message is received, it is either added to the waiting list of messages at the same index, or if, it is
        the last message to be received for that index, encapsulated in a new message and forwarded with the rest to the
        next processor in the pipeline.

        :param message: The new message to deal with.
        :return: None (errors/warnings are indicated through queue messages).
        """
        with self._lock:
            try:
                # Check that active
                if self.__index_ptr is None:
                    self._inform(TCPOCode.Warning.STATE, 'Cannot Handle Message when Inactive')
                    return

                # If we have not yet received any samples on this index, then need to create new message...
                if not message.Index in self.__index_ptr:
                    self.__index_ptr[message.Index] = message.part(self.__default_name) # May have a default entry

                # Keep Track of the message at this index...
                msg = self.__index_ptr[message.Index]
                # Iterate through each data item in this message:
                for key in message.Data:
                    if not key in self.__allowables:
                        self._inform(TCPOCode.Warning.VALUE, '%s Name was not registered with %s - Dropping' %
                                     (key, self.Name))
                        continue # Skip this one
                    if key in msg.Data:
                        self._inform(TCPOCode.Warning.DUPLICATE,
                                     '%s Data item with index %d has already been received at %s - Ignoring' %
                                     (key, msg.Index, self.Name))
                        continue # Skip this one
                    msg.add(key, message[key]) # Will be a shallow copy of the value but not the key

                # Finally, check if full and pass on...
                if len(msg) == len(self.__allowables):    # If all retrieved...
                    del(self.__index_ptr[message.Index])  # Remove the dictionary entry
                    for cons in self._consumers: cons.onSample(msg)

            except BaseException as be:
                self.__index_ptr = None
                self._inform(TCPOCode.Error.MODULE, 'onSample Failed due to {%s: %s}' % (type(be).__name__, be.message))

    def stop(self, reason):
        """
        Override of the stop (cleanup) method.

        :param reason: [Ignored] The reason for stopping.
        :return: None
        """
        with self._lock:
            self.__index_ptr = None # Just clean up
