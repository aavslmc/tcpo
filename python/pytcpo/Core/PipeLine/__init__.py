# This Package contains a single module for defining an Abstract Pipeline
__author__ = 'Michael Camilleri'

from Message import Message
from PipelineModule import AbstractProcessor, AbstractGenerator, AggregatorModule
from PipelineManager import PipelineManager, PipelineManagerState
from PipelineBuilder import AbstractPipelineBuilder
