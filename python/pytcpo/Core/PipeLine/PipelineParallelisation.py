from multiprocessing import Process
from pytcpo.Core.Common import TCPOCode
from pytcpo.Core.PipeLine import PipelineManager, PipelineManagerState


class PipelineParallelisation(Process):

    def __init__(self, manager):
        super(PipelineParallelisation, self).__init__()
        self.name = 'CalibrationPipeline'
        if not isinstance(manager, PipelineManager):
            print 'Error: Object not a Pipeline Manager Instance'
            exit(-1)
        elif manager.get_state is not PipelineManagerState.BUILT:
            print 'Error: Pipeline Manager Instance not in Built State'
            exit(-1)
        else:
            self.manager = manager

    def run(self):

        result = self.manager.start()

        if result is not TCPOCode.Status.ACTIVE:
            print 'Error in Starting Pipeline - ' + TCPOCode._print(result)
            exit(-1)

            # Wait for completion
        assert (self.manager.wait_until_ready())
