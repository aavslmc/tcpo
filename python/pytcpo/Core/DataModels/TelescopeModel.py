import json
import numpy as np

from astropy.coordinates import EarthLocation
from pytcpo.Core.Common import TCPOException, TCPOCode, Singleton


@Singleton
class TelescopeModel:

    """
    Telescope & Observation Constants

    This object serves as the central repository for the observation run constants. This includes antenna positions
    (Latitude/Longitude coordinates as well as Eastings/Northings are supported), Antenna grouping by tiles/beams,
    frequency channels of operation and observation timings. Note that the model uses the GRS80 ellipsoid for all
    locations. It is also heavily dependent on the astropy library.

    The model is implemented as a singleton, to enforce the fact that there should ever be one instance. This is
    globally initialised, typically from a JSON object and made available throughout the process.

    **Initialisation Parameters**
        'longitude_centre' - [Required]: Central (absolute) Longitude of the reference position
        'latitude_centre' - [Required]: Central (absolute) Latitude of the reference position
        'altitude_centre' - [Optional]: Central (absolute) Altitude of the reference position

        'relative_coords' - [Optional]: If true indicates that the passed coordinates are relative x-y-z values:
                            otherwise the coordinates are interpreted as absolute latitude/longitude/altitude values.
                            Defaults to true.


        'num_channels' - [Required]: Number of channels in our bandpass
        'starting_channel' - [Required]: Frequency (Hz) of the starting channel

    """

    R = 6371.0 * 1000  # Radius of Earth in m

    @property
    def reference(self):
        """
        Reference Position (property)
        :return: EarthLocation object containing the reference Latitude/Longitude
        """
        return self.centre

    def __init__(self):
        """
        Default Initialiser

        Note that being a singleton, the Class should not be explicitly initialised!
        """
        self.centre = None      # Reference Position;

        self.antennas = None    # The list of all antennas

        self.Start_Freq = None
        self.Incre_Freq = None

        self.end_freq = None
        self.NumChan = None
        self.tile_list = None
        self.antenna_tile_idx = None
        self.beam_list = None
        self.beams = {}
        self.tiles = {}
        self.Img_Size = None
        self.Img_Type = None
        self.Img_FOV = None
        self.BPattern_FOV = None
        self.BPattern_Size = None
        self.BPattern_CoordFrame = None
        self.Bandwith = None
        self.TimeAverage = None
        self.PolMode = None
        self.Latitude = None
        self.Longitude = None
        self.Altitude = None
        self.TStepLength = None
        self.PCDec = None
        self.PCRA = None
        self.StartTime = None
        self.Skymodel_dir = None
        self.ObsLength = None
        self.cen_lat = None
        self.cen_lon = None
        self.cen_alt = None

        self.h5required = None
        self.testing = None

    def from_file(self, tm_file):
        """
        Parse the Telescope Parameters from a file

        N.B. Will issue a warning if the method is called if the object has already been loaded once.

        :param tm_file: (Absolute) path to the file to read from. Contents must be JSON compatible
        :return: reference to self, for chaining.
        """
        # Load from file and parse
        with open(tm_file) as data_file:
            self.__parse(json.load(data_file))

        return self

    def from_string(self, tm_string):
        """
        Parse the Telescope Parameters from a string

        N.B. Will issue a warning if the method is called if the object has already been loaded once.

        :param tm_string: String containing the (JSON-Encoded) telescope parameters
        :return: reference to self, for chaining.
        """
        # Load from string and parse
        self.__parse(json.loads(tm_string))

        return self

    def from_dict(self, tm_dict):
        """
        Parse the Telescope Parameters from a dict

        N.B. Will issue a warning if the method is called if the object has already been loaded once.

        :param tm_dict: Dict containing the telescope parameters
        :return: reference to self, for chaining.
        """
        # Load dict and parse
        self.__parse(tm_dict)

        return self

    def __parse(self, tm_dict):
        """
        Parses the python dict to build the telescope parameters

        :param tm_dict: Python dict, typically initialised from json
        :return: None
        """
        # Build Antenna Array Positions
        self.__coordinate_converter(tm_dict)

        # Convert the Antenna Indices into Tiles
        self.__tile_converter(tm_dict)

        # Setup the Beam Array
        self.__beam_converter(tm_dict)

        # Store the Channel Configuration
        self.StartFreq = tm_dict.get("starting_channel", self.Start_Freq)
        self.EndFreq = tm_dict.get("end_channel", self.Start_Freq)
        self.NumChan = tm_dict.get("num_channels", 1)
        self.BPattern_CoordFrame = tm_dict.get("bp_coord", "Equatorial")
        self.BPattern_FOV = tm_dict.get("bp_fov", 180)
        self.BPattern_Size = tm_dict.get("bp_size", 256)
        self.Img_FOV = tm_dict.get("img_fov", 180)
        self.Img_Size = tm_dict.get("img_size", 256)
        self.Img_Type = tm_dict.get("img_type", "I")
        self.Latitude = tm_dict.get("latitudes", None)
        self.Longitude = tm_dict.get("longitudes", None)
        self.Altitude = tm_dict.get("altitudes", None)
        self.TimeAverage = tm_dict.get("t_average", 1.0)
        self.TStepLength = tm_dict.get("tstep_length", 1.0)
        self.ObsLength = tm_dict.get("obs_length", 1)
        self.StartTime = tm_dict["start_time"]
        self.cen_lat = tm_dict["latitude_centre"]
        self.cen_lon = tm_dict["longitude_centre"]
        self.cen_alt = tm_dict.get("altitude_centre", 0.0)
        self.Bandwith = tm_dict.get("bandwith", 1.0)
        self.PCRA = tm_dict.get("PCRA", 0.0)
        self.PCDec = tm_dict.get("PCDec", self.cen_lat)
        self.PolMode = tm_dict.get("Pol_Mode", "Scalar")

        self.h5_req = tm_dict.get("h5_req", False)
        self.testing_req = tm_dict.get("testing_req", False)

        self.Skymodel_dir = tm_dict.get("skymodel_dir", None)

    def __coordinate_converter(self, tm_dict):
        """
        Aim of coordinate converter is to produce an antenna array of arrays, with x,y,z values per antenna

        :param tm_dict: python dictionary containing the specific parameters
        :return: None
        """
        coords_type = tm_dict.get('relative_coords', True)

        # If Relative, just copy
        if coords_type:

            self.antennas = np.zeros((len(tm_dict["antennas"]), 3))
            for i in range(len(tm_dict["antennas"])):
                self.antennas[i, :] = np.asarray(tm_dict["antennas"][str(i)][2])

            # Store Reference Antenna Position
            self.centre = EarthLocation.from_geodetic(lon=tm_dict["longitude_centre"], lat=tm_dict["latitude_centre"],
                                                      height=tm_dict.get("altitude_centre", 0.0), ellipsoid='GRS80')

        # else, (absolute)
        else:
            lats = np.asarray(tm_dict["latitudes"])
            longs = np.asarray(tm_dict["longitudes"])
            alts = np.asarray(tm_dict["altitudes"])

            # Store Reference Antenna Position from centre of absolute positions
            self.centre = EarthLocation.from_geodetic(lon=np.mean(longs), lat=np.mean(lats), height=np.mean(alts),
                                                      ellipsoid='GRS80')

            # Calculate ECEF of Antennas
            x = np.zeros(len(longs))
            y = np.zeros(len(longs))
            z = np.zeros(len(longs))
            for i in range(len(longs)):
                xyz = EarthLocation.from_geodetic(lon=longs[i], lat=lats[i], height=alts[i], ellipsoid='GRS80')
                x[i] = xyz.x
                y[i] = xyz.y
                z[i] = xyz.z

            # Calculate ECEF of Reference (Centre)
            reference_x = self.centre.x
            reference_y = self.centre.y
            reference_z = self.centre.z

            # Calculate relative x,y,z to reference
            for i in range(len(longs)):
                x[i] -= reference_x
                y[i] -= reference_y
                z[i] -= reference_z

            # Define relative telescope model
            self.antennas = np.zeros((len(x), 3))
            for i in range(len(x)):
                self.antennas[i, 0] = x[i]
                self.antennas[i, 1] = y[i]
                self.antennas[i, 2] = z[i]

    def __tile_converter(self, tm_dict):

        """
        Function Descriptor: Convert to standardized tile-antenna format
        Requirements: From reader
        """
        self.tile_list = tm_dict.get("tiles", None)
        if self.tile_list is not None:
            self.tiles = {}
            for i in range(len(self.tile_list)):
                dict_entry = str(i)
                antenna_idxs = self.tile_list[dict_entry]
                self.tiles[i] = list()
                for j in antenna_idxs:
                    self.tiles[i].append([tm_dict["antennas"][str(j)][1], self.antennas[j, :]])

    def __beam_converter(self, tm_dict):

        """
        Function Descriptor: Convert to standardized beam-antenna format
        Requirements: From reader
        """
        self.beam_list = tm_dict.get("beams", None)
        if self.beam_list is not None:
            self.beams = {}
            for i in range(len(self.beam_list)):
                dict_entry = str(i)
                antenna_idxs = self.beam_list[dict_entry]
                beam_i_array = np.empty((len(antenna_idxs), 5))
                for a, ant in enumerate(antenna_idxs):
                    beam_i_array[a, :] = np.asarray(
                        [tm_dict["antennas"][str(ant)][0], tm_dict["antennas"][str(ant)][1]] +
                        self.antennas[ant, :].tolist())
                self.beams[i] = beam_i_array
