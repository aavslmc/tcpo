# This Package implements the Data Archiving Scheme for the LFAA TPM's. It does so through two abstract classes, which
#   separate the responsibility of defining what needs to be stored, with how it is actually stored.
__author__ = 'Michael Camilleri'

from Serialiseable import ASerialiseable
from Serialiser import ASerialiser
from Persistor import APersistor, PersistorFactory