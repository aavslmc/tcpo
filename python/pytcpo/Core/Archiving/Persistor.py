# Defines the Abstract Interface for Serialisation as well as the Serialiser itself.
import abc
import rwlock
import logging

from pytcpo.Core.Common import TCPOCode, TCPOException


class PersistorFactory(object):
    """
    Singleton-style instance for maintaining references to Persistors
    """
    __Persistors = {}

    @classmethod
    def registerPersistor(cls, persistor, test_setup = None):
        """
        Class Method which must be used to register Persistor-type classes allowing them to be dynamically instantiated
        at run-time. The registration must happen at the outermost level from which the object is imported.
        :param persistor: An AbstractPersistor-derived instance
        :return: None
        """
        cls.__Persistors[persistor.__name__] = persistor

    @classmethod
    def getPersistor(cls, name):
        """
        Maps a string class name to the associated data type.
        :param name: String name of the Persistor
        :return: The Type object for the associated class
        :raises KeyError: If the persistor is not registered
        """
        return cls.__Persistors[name]


class APersistor(object):

    """
    The APersistor ABC class (which also inherits from object) enforces the responsibility for specifying how
    serialised data is to be persisted. At this level, other than imposing a call order on the available methods (thus
    preventing state mismatches), it does not restrict the implementation, apart from enforcing that a time-stamp is
    part of the stored data. The class also acts as a Factory object for instantiating Serialiser-derived instances.

    The responsibility to support both random and serial reads rests with the concrete implementation. An archiver must
    support serial reads: it may also support random reads (which is indicated through the appropriate flag)

    With regards to Thread-Safety, the paradigm employed attempts to balance the capability for simultaneous access
    from different threads (especially if writing/reading from different channels) with the need to ensure that the main
    setup/teardown methods do not interfere if called from separate threads. As such, the following two guarantees are
    provided by the Abstract Implementation:
      a) Archive Setup, Closing and Data Access (read/write) WILL NOT happen concurrently and are exclusively locked.
         These exclude the property accessors and the Factory Methods (which in any case are usually called only on
         module inclusion at the start when only one thread is available).
      b) Read/Write activities themselves may happen in parallel, as long as there is no Setup/Closing in progress.
    Anything above and beyond these must be implemented by the concrete class.
    """

    # Flags for Reading/Writing
    M_CLOSED = 0                   # Database is not initialised
    M_READ   = 1                   # Database is opened for Reading only - if file does not exist, it typically fails.
    M_WRITE  = 2                   # Database is opened for Writing only - if file exists, it is overwritten
    M_RDWRT  = M_READ | M_WRITE    # Database is in Read-Write Mode
    M_APPEND = 16                  # Valid for Write-Operations. Indicates that the file, if it exists, must not be
                                   #   overwritten

    # Time-Stamp Data Type
    TS_TYPE  = float

    # Enforce that Abstract
    __metaclass__ = abc.ABCMeta

    # Serialiser list
    __Serialisers = {}
    __Test_Setup = {}

    @abc.abstractproperty
    def SupportsReadWrite(self):
        """
        Abstract Read-Only Property - Indicates if simultaneous read-write operations are supported (i.e. append mode)
        """
        raise NotImplementedError()

    @abc.abstractproperty
    def SupportsRandomAccess(self):
        """
        Abstract Read-Only Property - Indicates if the class supports random access to stored data (reads)
        :return:
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _initialise(self, mode, metadata):
        """
        Setup Method (Protected Abstract Method)

        This is guaranteed to be the first method called after initialisation (constructor). In turn, it
        must guarantee, that on failure, the callee is able to invoke the method again with new parameters to attempt
        a successful initialisation - i.e. exceptions should not cause an undefined state.

        :param mode:   The mode in which to open the resource, see the modes up front.
        :param metadata: Any additional data, such as a database schema or type of object stored.
        :return: [Optional] OK Status code if successful. An Exception should be raised on error
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _writeSample(self, key, timestamp, sample):
        """
        Abstract Method which is called to write out a single sample to the specified key (location). It is up to the
        implementation to define how (and where) the data is stored, as long as:
         a) The Data source can be identified by a unique key
         b) The Time-stamp is stored as well.
        :param key: The Key to add this sample to. In effect, this allows multiple modules writing to the same archive.
        :param timestamp: The Timestamp associated with this sample. This will be a float data type
        :param sample:  The sample data (this will have been already serialised) as a string
        :return: [Optional] OK Status Code if successful. Errors should be signalled through exceptions
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _readSample(self, key, idx = None):
        """
        Abstract Method which is called to read a sample from the Database. Note that random indexing is not necessarily
        supported: in this case, the passed index will indeed be None, and can be ignored.
        :param key: The key to locate the data from.
        :param idx: The Idx to read from. For non-random archivers, this will be None
        :return: A tuple consisting of
                    a) The time-stamp
                    b) The data type stored
                    c) The Sample Data (still serialised).
                An Exception should be raised if there is an error, including if the key does not exist, or the sample
                cannot be read.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _skipSample(self, key, num = 1):
        """
        This method is implemented mainly for the sake of non-random read-out archivers. It is intended if possible to
        speed up readout of invalid/irrelevant samples (for example by not parsing into structure). It must still be
        supported by random-access archivers, by updating any internal pointers as necessary.
        :param key: The key to locate the data from.
        :param num: The number of samples to skip. Should support any integer from 0 (no skipping) upwards.
        :return: [Optional] OK Status Code. Other Errors will be signaled by exceptions.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _seek(self, key, idx = -1):
        """
        Protected Abstract Method (should not be explicitly called):

        Implemented by persistors which support random access to set the next index to read from. Persistors which do
        not support random access may provide an empty implementation, since it is guaranteed it will not be called.

        :param key: Key for which to seek.
        :param idx: Index to seek to. Must support negative indexing. Defaults to -1 (read last element)
        :return: None
        :raises TCPOException with INDEX error if the index is out of bounds.
        """

    @abc.abstractmethod
    def _close(self):
        """
        Will be called to indicate that the archiver can release any resources which were obtained during initialisation.
        :return: None. Exceptions may be thrown
        """
        raise NotImplementedError()

    #==================================================================================================================#

    @classmethod
    def registerSerialiser(cls, serialiser, test_setup = None):
        """
        Class Method which must be used to register ASerialiseable-derived classes allowing them to be dynamically
        instantiated during reading. The registration must happen at the outermost level from which the object is
        imported.
        :param serialiseable: An ASerialiseable derived instance
        :param test_setup [Optional]: Allows for dynamic testing, with a default setup parameter
        :return: None
        """
        cls.__Serialisers[serialiser.__name__] = serialiser
        cls.__Test_Setup[serialiser.__name__] = test_setup

    @classmethod
    def getSerialiser(cls, name):
        """
        Maps a string class name to the associated data type.
        :param name: String name of the Serialiser
        :return: The Type object for the associated class, or None if it does not exist.
        """
        return cls.__Serialisers[name]

    @classmethod
    def getTestSetup(cls, name):
        """
        Retrieves the default setup to use for the associated type
        :param name:
        :return:
        """
        return cls.__Test_Setup[name]

    def __init__(self):
        """
        Default Initialiser
        """
        self._mode = 0  # Mode in which the archiver was initialised
        self._lock = rwlock.RWLock()
        self._serialiser = None

    def setup(self, mode, p_setup, serialiser, s_setup):
        """
        Setup Method - This must be the first method called. At the very least, it specifies the source where to read
        from / write to and the mode. Optionally, any structure information may also be specified. This is the public
        counterpart to the _initialise method, with the arguments serving the same functionality: this wrapping however
        ensures a correct ordering of method calls.
        :param mode:        The opening mode (typically in read/write etc...)
        :param p_setup:     Additional Setup information to pass on to the Persistor (the concrete implementation)
        :param serialiser:  The (string) name of the serialiser object type
        :param s_setup:     Additional Setup Information to pass on to the Serialiser
        :return: Setup Status Code if successful. Any issue will be thrown as an exception.
        """
        # Obtain a Writer-Lock
        with self._lock.writer_lock:
            # First close...
            if self._mode > 0:
                logging.warning('Setup %s: Closing previously-open connection', type(self).__name__)
                self.Close()

            # Ensure that OpenMode is supported
            if ((mode & self.M_RDWRT) == self.M_RDWRT) and not self.SupportsReadWrite:
                raise TCPOException(TCPOCode.Error.SETUP, 'Read-Write Mode is not supported by %s' % type(self).__name__)

            # Attempt to initialise the Serialiser and set it up
            self._serialiser = self.getSerialiser(serialiser)()
            self._serialiser.setup((mode & self.M_RDWRT), s_setup)

            # Otherwise, just attempt open... and if no exception is thrown, set mode.
            self._initialise(mode, p_setup)
            self._mode = mode

            return TCPOCode.Status.SETUP

    def WriteSample(self, key, timestamp, sample):
        """
        Public counterpart to the sample-writing method. The method wraps the private method, ensuring mode mismatch
        errors are caught. Refer to _writeSample for the argument/return specification.
        """
        # Obtain a 'reader' lock since it does not modify the global file pointers etc...
        with self._lock.reader_lock:
            # Ensure we can write
            if (self._mode & self.M_WRITE) == 0:
                raise TCPOException(code=TCPOCode.Error.WRITE, msg='Cannot Write - Archive was opened in Read-Only Mode')

            # Else, just write (after serialisation)
            return self._writeSample(key, timestamp, self._serialiser.serialise(sample))

    def ReadSample(self, key, idx):
        """
        Public counterpart to the sample-reading method. Again, the wrapping ensures that readout is not requested when
        in write mode or when random reads are not supported.

        :param key: The Key to read from
        :param idx: The index to read at. Negative indexing is supported
        :return: A Tuple containing the time-stamp and the sample object at the specified index
        """
        # Obtain a 'reader' lock since it does not modify the global file pointers etc...
        with self._lock.reader_lock:
            # Ensure we can read
            if (self._mode & self.M_READ) == 0:
                raise TCPOException(TCPOCode.Error.READ, '%s was opened in Write-Only Mode' % type(self).__name__)
            if not self.SupportsRandomAccess:
                raise TCPOException(TCPOCode.Error.READ, '%s does not support Random Reads' % type(self).__name__)

            # Else, attempt read
            ts, o_type, ser_samp = self._readSample(key, idx)
            return ts, self._serialiser.deserialise(ser_samp, o_type)

    def ReadNext(self, key):
        """
        Alternative to ReadSample which ensures sequential access to samples, even for random-read-supporting archivers.
        Responsibility of maintaining a running index rests with the concrete implementation

        :param key: The data channel to read from
        :return: A Tuple containing the time-stamp and the sample object at the specified index (Same as ReadSample).
        """
        # Obtain a 'reader' lock since it does not modify the global file pointers etc...
        with self._lock.reader_lock:
            # Ensure we can read:
            if self._mode & self.M_READ == 0:
                raise TCPOException(TCPOCode.Error.READ, 'Cannot Read - Archive was opened in Write-Only Mode')

            # Else, attempt read
            ts, o_type, ser_samp = self._readSample(key, None)
            return ts, self._serialiser.deserialise(ser_samp, o_type)

    def Seek(self, key, idx):
        """
        Public Wrapper (with mode testing) around the _seek method.

        :param key: Key to seek from
        :param idx: Index to seek to: supports negative indexing
        :return: None
        :raises TCPOException: with READ or INDEX error
        """
        # Obtain a 'reader' lock since it does not modify the global file pointers etc...
        with self._lock.reader_lock:
            # Error Checking
            if self._mode & self.M_READ == 0:
                raise TCPOException(TCPOCode.Error.READ,
                                    '%s was opened in Write-Only Mode (Seek)' % type(self).__name__)
            if not self.SupportsRandomAccess:
                raise TCPOException(TCPOCode.Error.ARCHIVE,
                                    '%s Does not support Random Access (Seek)' % type(self).__name__)

            # Else Seek
            self._seek(key, idx)

    def SkipNext(self, key, num):
        """
        Public Wrapper (with mode testing) around the _skipSamples method. Refer to _skipSample for the argument/return
        specification.
        """
        # Obtain a 'reader' lock since it does not modify the global file pointers etc...
        with self._lock.reader_lock:
            # Error Checking
            if self._mode & self.M_READ == 0:
                raise TCPOException(TCPOCode.Error.READ,
                                    '%s was opened in Write-Only Mode (Skip)' % type(self).__name__)

            # Skip and return
            return self._skipSample(key, num)

    def Close(self):
        """
        Public Wrapper around the cleanup method, which also handles APersistor-level resources.
        :return: None
        """
        # Obtain a 'writer' lock since we are modifying core members
        with self._lock.writer_lock:
            self._close()
            self._serialiser = None
            self._mode = self.M_CLOSED

