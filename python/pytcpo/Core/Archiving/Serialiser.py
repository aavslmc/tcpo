import abc
import numpy as np

from pytcpo.Core.Common import TCPOCode, TCPOException


class ASerialiser(object):
    """
    This Class implements the Serialisation of ASerialiseable objects. Typically, this includes converting the object
    to a string format (or binary) which can then be acted upon by the archiver class.

    The current architecture is expected (in the concrete implementations) to support:
        a) All primitive data types (int, float, complex, bool, str):
        b) All numpy numeric data types (as subclassed by np.number):
        c) ASerialiseable instances (must be registered with the ASerialiser factory):
        d) Numpy Arrays of all the above, including empty Numpy arrays.

    The class also implements the Factory Pattern for ASerialiseable-derived instances.
    """

    __metaclass__ = abc.ABCMeta  # Enforces that Abstract

    # Flags
    M_UNINIT= 0  # Not yet initialised
    M_READ  = 1  # Will be used to write
    M_WRITE = 2  # Will be used to read

    # The Supported Primitives...
    PRIMITIVES = (int, float, complex, bool, str, np.number)

    # ... and Factory Pattern for the serialiseable data types
    __Serialiseables = {'ndarray': np.ndarray}  # Start off with the entry for an ndarray

    # ================================================================================================================ #

    @abc.abstractmethod
    def _setup(self, mode, params):
        """
        Setup Method
        :param mode: Indicates if Reading or Writing
        :param params: This is typically a python dict of key-value pairs.
        :return: [Optional] Status.SETUP. Errors must be signalled through exceptions.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _serialise(self, obj):
        """
        Serialisation Method
        :param obj: The Object to serialise. Must be one of the supported types.
        :return:
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def _deserialise(self, sample, otype):
        """
        DeSerialisation Method
        :param sample: The String to de-serialise into the object type
        :param otype: The object type to deserialise
        :return: the deserialised object
        """
        raise NotImplementedError()

    # ================================================================================================================ #

    @classmethod
    def registerSerialiseable(cls, serialiseable):
        """
        Class Method which must be used to register ASerialiseable-derived classes allowing them to be dynamically
        instantiated during reading. The registration must happen at the outermost level from which the object is
        imported.
        :param serialiseable: An ASerialiseable derived instance
        :return: None
        """
        cls.__Serialiseables[serialiseable.__name__] = serialiseable

    @classmethod
    def getSerialiseable(cls, name):
        """
        Maps a string class name to the associated data type.
        :param name: String name of the ASerialiseable-derived type
        :return: The Type object for the associated class
        """
        return cls.__Serialiseables[name]

    # ================================================================================================================ #

    def __init__(self):
        """
        Default Initialiser
        """
        self._init = self.M_UNINIT

    def setup(self, mode, params):
        """
        Setup Method. Must be called before starting reading/writing
        Note that the Serialiser may utilise some kind of state: hence, after a pipeline run, it is expected that
        the setup will be recalled.
        :param mode:    The mode in which we will be operating (serialisation vs deserialisation)
        :param params:  Any parameters specific to the Serialiser
        :return:        TCPOCode.Status.SETUP
        """
        # Check that not already setup
        if self._init > self.M_UNINIT:
            raise TCPOException(TCPOCode.Error.STATE, '%s Serialiser is already setup' % type(self).__name__)

        # Perform Actual Setup
        self._setup(mode, params)
        self._init = mode

        return TCPOCode.Status.SETUP

    def serialise(self, obj):
        """
        Serialisation Method.
        :param obj: The Object to serialise. Must be one of the supported types
        :return:    String representation of the object
        """
        if not (self._init & self.M_WRITE):
            raise TCPOException(TCPOCode.Error.STATE, '%s Serialiser is not setup for Write' % type(self).__name__)

        return self._serialise(obj)

    def deserialise(self, data, otype):
        """
        DeSerialisation Method
        :param data:   The serialised data to retrieve the object from.
        :param otype:  The Type of object to retrieve
        :return:       An instantiated and initialised instance of otype, appropriately deserialised from data.
        """
        if not (self._init & self.M_READ):
            raise TCPOException(TCPOCode.Error.STATE, '%s Serialiser is not setup for Read' % type(self).__name__)

        return self._deserialise(data, otype)