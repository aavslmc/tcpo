import abc
import numbers
import numpy as np


class ASerialiseable(object):

    """
    This Class which inherits from Object (new-style classes) defines the interface for serialisation. Any data item
    which may need to be archived should inherit from this class and implement the 'serial()' method which specifies
    which member attributes need to be stored. For the sake of testing, the class also defines the equality-operator
    based on the same attributes defined by serial.

    With regards to implementation, the class is in reality an Abstract Base Class. Additionally, the inheritance
    from Object ensures support for polymorphism and correct instantiation. It should also be noted that concrete
    implementations must
      a) Support a Default Initialiser
      b) Register themselves with the APersistor (below), in the appropriate __init__ file.
    """

    __metaclass__ = abc.ABCMeta # Enforces that Abstract

    @abc.abstractmethod
    def serial(self):
        """
        Abstract Method (Must be implemented)

        This is the method which defines what needs to be serialised/deserialised

        :return: A python dictionary mapping the (string) names of the particular member attributes to their type.
                 Attributes can be straight up member variables or even properties.
        """
        raise NotImplementedError()

    @classmethod
    def base_equatable(cls, left, right):
        """
        Recursive method for comparing ASerialiseable instances based ONLY on the attributes defined in the serial
        method. Note that this is actually a class method. It is not designed to be explicitly called: rather, the
        user is expected to make use of the respective equality(==)/inequality(!=) operators. Equality is defined
        based on the value and not identity of referencing.

        :param left: The left item to compare
        :param right: The right item to compare
        :return: True if the type of left and right is the same and their values are the same.
        """
        # Check Type
        if type(left) != type(right): return False

        # Handle Numpy Arrays
        if type(left) == np.ndarray:
            if np.shape(left) != np.shape(right): return False
            a_r = left.ravel()
            b_r = right.ravel()

            for i in range(len(a_r)):
                if not ASerialiseable.base_equatable(a_r[i], b_r[i]): return False

            return True

        # Handle A Serialiseable Instances
        elif isinstance(left, ASerialiseable):
            for attr, aType in left.serial().iteritems():
                if not ASerialiseable.base_equatable(left.__dict__[attr], right.__dict__[attr]): return False
            return True

        # Handle String/Characters
        elif isinstance(left, (str, np.character)):
            return left == right

        # Handle integers and booleans
        elif isinstance(left, numbers.Integral):
            return left == right

        # Handle Floats/Complex
        else:
            return np.isclose(left, right)

    def __eq__(self, other):
        """
        Overload of Equality Operator
        :param other: Other instance to compare this to.
        :return: True if all serialiseable elements are value-wise equal, False otherwise.
        """
        return ASerialiseable.base_equatable(self, other)

    def __ne__(self, other):
        """
        Overload of Inequality Operator
        :param other: Other instance to compare this to.
        :return: True if any serialiseable element is not value-wise equal, False otherwise
        """
        return not ASerialiseable.base_equatable(self, other)

