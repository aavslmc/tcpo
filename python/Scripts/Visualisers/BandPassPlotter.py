import logging

import numpy as np
import numpy.polynomial.polynomial as nppol
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons, CheckButtons

from pytcpo.Core.Common import TCPOCode, HorRadioButtons
from pytcpo.Core.PipeLine import PipelineManager, AbstractProcessor
from pytcpo.Modules.Archivers import RedisPersistor

import pytcpo.Modules.IO
import pytcpo.Modules.Archivers
import pytcpo.Modules.RawFilters
import pytcpo.Modules.BandPassModel
import pytcpo.Modules.AnomalyDetectors

# TODO Support different antennas/tile

# Globals
Freq_Offs = 0
Freq_Scale = 0.78125

# Original 'Storage-Specific' Definitions
O_Station = 0
O_Samples = (0, 2047)                    # Sample Range in original file
O_TileConfig = {0: (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)} # Available Tile Indices & associated Antennas
O_Channels = np.arange(0,512)           # Channel Range to consider in fit: must lie between 0 and 512
O_Polarisations = (0,1)                 # Number of Polarisations
O_DownSample = 1                        # Down-Sample Factor (skipping values)
O_Order = 9                             # The Polynomial order in the test
O_Mean = 8                              # Mean computation window.

# Plotting Specifications
P_DownSample = 1    # DownSample Rate to plot at (relative to original)
P_Frequencies = O_Channels*Freq_Scale + Freq_Offs

# Computations
Num_Samples = (O_Samples[1] - O_Samples[0])//(O_DownSample * P_DownSample)

# Source Location
# raw_location = '/media/michael/Elements/MichaelC/Data/SKA-Run (March 2017)/raw'
raw_location = '/home/michael/Downloads'
# raw_location = '/home/xandru/Documents/Data/TestDataFromPolyGen'

# Serialiser Data
serialiser = 'JSONSerialiser'


class SampleConsumer(AbstractProcessor):

    def __init__(self):
        super(SampleConsumer, self).__init__()
        self.store = None
        self.index = None

    def _setup(self, filter_model):
        self.store = filter_model
        return True

    def _will_start(self):
        self.index = 0
        return True

    def _new_sample(self, message, forward):
        self.store[:,:,self.index,:] = message.Default
        self.index += 1
        return True

    def _clean_up(self, reason):
        pass

PipelineManager.registerModule(SampleConsumer)


if __name__=='__main__':

    logging.basicConfig(level=logging.INFO)

    # Pre-Allocate
    orig_data = {t: np.empty((2, len(O_TileConfig[t]), len(O_Polarisations), Num_Samples, len(O_Channels))) for t in O_TileConfig}  # Original Data (linear and log scale)
    coef_data = {t: np.empty((2, len(O_TileConfig[t]), len(O_Polarisations), Num_Samples, len(O_Channels))) for t in O_TileConfig}  # Coeff evaluated data (polynomial and means)
    outliers = {t: np.empty((len(O_TileConfig[t]), len(O_Polarisations), Num_Samples), dtype=np.ndarray) for t in O_TileConfig}     # Outlier list (aggregated)
    anomalies = {t: np.zeros((len(O_TileConfig[t]), len(O_Polarisations), Num_Samples), dtype=np.bool) for t in O_TileConfig}       # Anomaly flag (aggregated)

    print ' Reading Raw Samples... '
    manager = PipelineManager()

    # Reader
    manager.add_root('ChannelStageReader',
                     {'path': raw_location, 'time': None, 'tiles': 0, 'sample_rate': 0, 'block_rate': 2048,
                      'samples': O_Samples, 'stop_on_end': True})
    # Down-Sampler
    down_sample_module = manager.add_module('AliasingDownSampler',
                                            {'rate': (O_DownSample * P_DownSample), 'start': O_Samples[0]})
    # De-Tiling
    for tile in O_TileConfig:
        # Antenna Selector
        manager.add_module('AntennaSelector', {'antennas': O_TileConfig[tile]}, parents=down_sample_module)
        # ChannelSelector
        manager.add_module('ChannelSelector', {'channels': O_Channels})
        # CScaler
        scaler = manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': float})
        # Log-Scale
        logger = manager.add_module('PowerLog', {'multiplier': 10, 'log': '10'})
        # Samplers
        manager.add_module('SampleConsumer', orig_data[tile][0], parents=scaler)   # Linear values
        manager.add_module('SampleConsumer', orig_data[tile][1], parents=logger)   # Power Log values

    # Build
    manager.build()

    # Run Pipeline
    assert (manager.start() == TCPOCode.Status.ACTIVE)
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)

    # Connect to archiver
    persistor = RedisPersistor()

    archive_keys = {}
    for tile in O_TileConfig:
        archive_keys['station_' + str(O_Station) + ':tile_' + str(tile) + ':outliers'] = np.ndarray
        archive_keys['station_' + str(O_Station) + ':tile_' + str(tile) + ':anomalies'] = np.ndarray
        archive_keys['station_' + str(O_Station) + ':tile_' + str(tile) + ':bitshifts'] = np.ndarray
        archive_keys['station_' + str(O_Station) + ':tile_' + str(tile) + ':polyfits'] = np.ndarray
        archive_keys['station_' + str(O_Station) + ':tile_' + str(tile) + ':meanfits'] = np.ndarray

    persistor.setup(RedisPersistor.M_READ,
                    p_setup={'host': 'localhost', 'port': 1500, 'keys': archive_keys, 'name': 'BandPass'},
                    serialiser=serialiser, s_setup=None)

    print ' Reading Polynomial Fits... '
    for t in O_TileConfig:
        key_name = 'station_' + str(O_Station) + ':tile_' + str(t) + ':polyfits'
        s_idx = 0
        for s in range(O_Samples[0], O_Samples[-1], O_DownSample*P_DownSample):
            # Now Read in the sample @ idx and evaluate
            ts, sample = persistor.ReadSample(key_name, s)
            coef_data[t][0,:,:,s_idx,:] = nppol.polyval(O_Channels, np.expand_dims(sample, axis=0).swapaxes(0, 3), tensor=False)
            s_idx += 1

    print ' Reading Mean Fits... '
    for t in O_TileConfig:
        key_name = 'station_' + str(O_Station) + ':tile_' + str(t) + ':meanfits'
        s_idx = 0
        for s in range(O_Samples[0], O_Samples[-1], O_DownSample * P_DownSample):
            # Now Read in the sample @ idx and evaluate
            ts, sample = persistor.ReadSample(key_name, s)
            coef_data[t][1,:,:,s_idx,:] = np.repeat(sample, O_Mean, axis=2)
            s_idx += 1

    print ' Reading Outliers'
    for t in O_TileConfig:
        key_name = 'station_' + str(O_Station) + ':tile_' + str(t) + ':outliers'
        s_idx = 0
        for s in range(O_Samples[0], O_Samples[-1], O_DownSample * P_DownSample):
            # Now Read in the sample @ idx and evaluate
            ts, sample = persistor.ReadSample(key_name, s)
            for anom in sample:
                outliers[t][O_TileConfig[t].index(anom._ant), O_Polarisations.index(anom._pol),s_idx] = anom._anomaly
            s_idx += 1

    print ' Reading Anomalies'
    for t in O_TileConfig:
        key_name = 'station_' + str(O_Station) + ':tile_' + str(t) + ':anomalies'
        s_idx = 0
        for s in range(O_Samples[0], O_Samples[-1], O_DownSample * P_DownSample):
            # Now Read in the sample @ idx and evaluate
            ts, sample = persistor.ReadSample(key_name, s)
            for anom in sample:
                anomalies[t][O_TileConfig[t].index(anom._ant), O_Polarisations.index(anom._pol),s_idx] = True
            s_idx += 1

    print ' Plotting ... '

    # Plot Objects
    _figure = plt.figure()
    _axes   = np.empty(2, dtype=np.object)  # Linear & Log Scale
    _linear = np.empty(2, dtype=np.object)  # Linear plot contains 2 lines (linear data and mean)
    _logar  = np.empty(3, dtype=np.object)  # Logarithm plot contains 3 lines (log data, polynomial fit and outliers)

    # Options (Selectors)
    _sel_til = O_TileConfig.iterkeys().next()    # Tile Selection - the default one
    _sel_ant = 0    # Antenna Selection
    _sel_pol = 0    # Polarisation Selection
    _sel_smp = 0    # Sample Selection
    _scale_ax = True # Scale Axes

    # Define Some Helper Functions
    def set_axes_color(axis, colour, width):
        for name in ['top', 'bottom', 'left', 'right']:
            axis.spines[name].set_color(colour)
            axis.spines[name].set_linewidth(width)

    def update_plot():
        plt.suptitle('Outliers and Anomaly Detection for Tile %d Antenna %d and Polarisation %d' %
                     (_sel_til, O_TileConfig[_sel_til][_sel_ant], O_Polarisations[_sel_pol]))

        # Update the polynomial fit
        _logar[0].set_ydata(orig_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp])
        _logar[1].set_ydata(coef_data[_sel_til][0, _sel_ant, _sel_pol, _sel_smp])
        __outliers = outliers[_sel_til][_sel_ant, _sel_pol, _sel_smp]
        if __outliers is not None:
            _logar[2].set_data(P_Frequencies[__outliers], orig_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp, __outliers])
        else:
            _logar[2].set_data(P_Frequencies, np.full(P_Frequencies.shape, np.nan))

        # Update the Linear Mean Fit
        _linear[0].set_ydata(orig_data[_sel_til][0, _sel_ant, _sel_pol, _sel_smp])
        _linear[1].set_ydata(coef_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp])

        # Update Anomaly Flagging
        for _ax in _axes:
            set_axes_color(_ax, 'r', 2.5) if anomalies[_sel_til][_sel_ant, _sel_pol, _sel_smp] else set_axes_color(_ax, 'k', 1.0)

        # Finally Relimit
        if _scale_ax:
            for ax in _axes:
                ax.relim()
                ax.autoscale()

        # Update actual plot
        plt.draw()

    def on_Tile(tile):
        global _sel_til
        _sel_til = int(tile)
        update_plot()

    def on_Antenna(antenna):
        global _sel_ant
        _sel_ant = O_TileConfig[_sel_til].index(int(antenna))
        update_plot()

    def on_Polarisation(polaris):
        global _sel_pol
        _sel_pol = O_Polarisations.index(int(polaris))
        update_plot()

    def on_Sample(sample):
        global _sel_smp
        _sel_smp = int(sample)
        update_plot()

    def on_Scale(label):
        global _scale_ax
        if label == 'Scale':
            _scale_ax = not(_scale_ax)
            if _scale_ax:
                for ax in _axes:
                    ax.relim()
                    ax.autoscale()
                plt.draw()

    def on_Sample_Sub(event):
        global _sel_smp, _smp_sld
        _sel_smp = max(_sel_smp - 1, 0)
        _smp_sld.set_val(_sel_smp)

    def on_Sample_Add(event):
        global _sel_smp, _smp_sld
        _sel_smp = min(_sel_smp + 1, Num_Samples-1)
        _smp_sld.set_val(_sel_smp)

    # Create Plot(s)
    plt.suptitle('Outliers and Anomaly Detection for Tile %d Antenna %d and Polarisation %d' %
                 (_sel_til, O_TileConfig[_sel_til][_sel_ant], O_Polarisations[_sel_pol]))

    # First do the log scale (with polynomial fit and outliers)
    _axes[0] = _figure.add_subplot(121)
    _axes[0].set_title('Log Scale (with fit & outliers)')
    # Plot Data and fit
    _logar[0], = plt.plot(P_Frequencies, orig_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp]) # Plot Log Data
    _logar[1], = plt.plot(P_Frequencies, coef_data[_sel_til][0, _sel_ant, _sel_pol, _sel_smp]) # Plot Polynomial Fit
    # Plot Outliers
    _outliers = outliers[_sel_til][_sel_ant, _sel_pol, _sel_smp]
    if _outliers is not None:
        _logar[2], = plt.plot(P_Frequencies[_outliers], orig_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp, _outliers],
                              'x')
    else:
        _logar[2], = plt.plot(P_Frequencies, np.full(P_Frequencies.shape, np.nan), 'x')
    # Labels
    plt.ylabel('Magnitude')
    plt.xlabel('Frequency')
    plt.legend(_logar, ['Raw (Log) Data', 'ModelFit', 'Outliers'])

    # Now do the Linear scale (with mean fit)
    _axes[1] = _figure.add_subplot(122)
    _axes[1].set_title('Linear Scale (with means)')
    # Plot Data and fit
    _linear[0], = plt.plot(P_Frequencies, orig_data[_sel_til][0, _sel_ant, _sel_pol, _sel_smp])  # Plot Linear Data
    _linear[1], = plt.plot(P_Frequencies, coef_data[_sel_til][1, _sel_ant, _sel_pol, _sel_smp])  # Plot Mean Fit
    # Labels
    plt.xlabel('Frequency')
    plt.legend(_linear, ['Raw (Lin) Data', 'Mean Fit'])

    # Flag Anomaly
    for _ax in _axes:
        set_axes_color(_ax, 'r', 2.5) if anomalies[_sel_til][_sel_ant, _sel_pol, _sel_smp] else set_axes_color(_ax, 'k', 1.0)

    # Arrange Space and add Controls
    _figure.subplots_adjust(left=0.15, wspace=0.1, right=0.95, bottom=0.15, top=0.925)

    # Tile Selection
    _tile_rb = RadioButtons(_figure.add_axes([0.02, 0.55, 0.025, 0.4], facecolor='lightgoldenrodyellow'),
                            O_TileConfig.keys(), active=0, activecolor='red')
    plt.title('TIL')
    _tile_rb.on_clicked(on_Tile)

    # Antenna Selection
    _ant_rb = RadioButtons(_figure.add_axes([0.06, 0.55, 0.025, 0.4], facecolor='lightgoldenrodyellow'),
                            O_TileConfig[_sel_til], active=0, activecolor='red')
    plt.title('ANT')
    _ant_rb.on_clicked(on_Antenna)

    # Polaristion Selection
    _pol_rb = HorRadioButtons(_figure.add_axes([0.02, 0.4, 0.065, 0.04], facecolor='lightgoldenrodyellow'),
                              O_Polarisations, active=0)
    plt.title('POL')
    _pol_rb.on_clicked(on_Polarisation)

    # Axes Scaling Selection
    _axis_scaler = CheckButtons(_figure.add_axes([0.02, 0.3, 0.065, 0.04], facecolor='lightgoldenrodyellow'),
                                ('Scale',), (True,))
    _axis_scaler.on_clicked(on_Scale)

    # Sample Selection (slider)
    _smp_sld = Slider(_figure.add_axes([0.05, 0.06, 0.9, 0.03], facecolor='lightgoldenrodyellow'), label='Sample',
                      valmin=0, valmax=Num_Samples-1, valinit=_sel_smp, valfmt='%0.0fx'+str(P_DownSample*O_DownSample))
    _smp_sld.on_changed(on_Sample)

    # Sample Selection (Buttons)
    _smp_add = Button(_figure.add_axes([0.15, 0.02, 0.1, 0.03]), 'Sample (-)', color='lightgoldenrodyellow',
                      hovercolor='0.975')
    _smp_add.on_clicked(on_Sample_Sub)
    _smp_sub = Button(_figure.add_axes([0.70, 0.02, 0.1, 0.03]), 'Sample (+)', color='lightgoldenrodyellow',
                      hovercolor='0.975')
    _smp_sub.on_clicked(on_Sample_Add)

    plt.show()


