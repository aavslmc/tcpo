import matplotlib as mplib
import numpy as np
import scipy.io as sio

mplib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gspec

from matplotlib import cm
from mpl_toolkits import mplot3d
from matplotlib import colors as clrs
from matplotlib import colorbar as clrbr
from matplotlib.widgets import Slider, Button, RadioButtons

from pytcpo.Core.Common import HorRadioButtons

# Setup
model_file = '/media/michael/Elements/MichaelC/Data/EM Model/EEP_199.68MHz.mat'  # EM Models
# model_file = '/media/michael/Elements/MichaelC/Data/EM Model/EEP_dummy.mat'  # EM Models
# model_file = '/home/michael/Downloads/EM.mat'  # EM Models
# model_file = '/home/michael/Downloads/EPP_Dummy.mat'  # EM Models

ports = [1, 2]
theta_step = 1
phi_step = 2
antennas = list(range(256))             # The Antennas (0 through 256)
components = ['pvv', 'phh', 'I', 'I*']  # The last two are the magnitude overall, computed according to different schemes
represent = ['Gain', 'Polar']           # Represent the 3D gain and the polar plot
surf_type = ['Rect.', 'L. Rect', 'Spher.', 'L. Spher.']    # The type of plot to draw
polar_type = ['Lin.', 'Log.']   # The type of display for polar plot
clip = 60

if __name__ == '__main__':

    # Load the Mat File & Prepare
    data = sio.loadmat(model_file)
    theta = np.arange(0, 91, theta_step) * np.pi / 180.0  # Theta Values [0 through 90 in degrees]
    phi = np.arange(0, 361, phi_step) * np.pi / 180.0  # The Phi Values [0 through 360, stepping at 2]
    t_dim = len(theta)
    p_dim = len(phi)
    T = np.arange(-90, 91, theta_step) * np.pi / 180.0

    # Mesh the Dimensions
    u, v = np.meshgrid(theta, phi)

    # This has the same effect as the mesh-grid in Matlab, since it is an outer product!
    x = np.transpose(np.multiply(np.sin(u), np.cos(v)))
    y = np.transpose(np.multiply(np.sin(u), np.sin(v)))
    z = np.transpose(np.cos(u))

    # Electric Field Strength - This is just the beam-pattern
    #                 1/2        vv/hh/I/I*        0..255     theta   phi
    E = np.empty((len(ports), len(components), len(antennas), t_dim, p_dim), dtype=np.float)
    E_VV = np.zeros((len(ports), t_dim, p_dim), dtype=np.complex)
    E_HH = np.zeros((len(ports), t_dim, p_dim), dtype=np.complex)
    for idx, polaris in enumerate(ports):
        for ant in range(len(antennas)):
            # Pvv Component (0)
            e_vv = np.reshape(data[components[0] + str(polaris)][:, ant], (t_dim, p_dim), 'F')
            E[idx, 0, ant] = np.absolute(e_vv)  # gain
            # Phh Component (1)
            e_hh = np.reshape(data[components[1] + str(polaris)][:, ant], (t_dim, p_dim), 'F')
            E[idx, 1, ant] = np.absolute(e_hh)  # gain
            # Intensity (take 1)
            E[idx, 2, ant] = np.sqrt(np.square(E[idx, 0, ant, :, :]) + np.square(E[idx, 1, ant, :, :]))
            # Intensity (take 2)
            E[idx, 3, ant] = np.absolute(e_vv + e_hh)
            # Summation for Beam Synthesis:
            E_VV[idx] += e_vv
            E_HH[idx] += e_hh
    # Log (and normalised log) of the E-Matrix
    K = E/np.amax(E, axis=(3, 4)).reshape((len(ports), len(components), len(antennas), 1, 1))
    K[K == 0] = np.nan
    L = np.nan_to_num(20 * np.log10(K))
    L_n = L - np.amax(L, axis=(3, 4)).reshape((len(ports), len(components), len(antennas), 1, 1)) + clip
    L_n[L_n < 0] = 0

    # Compute FaceColors:
    fc_E = cm.jet(np.nan_to_num(E / np.amax(E, axis=(3, 4)).reshape((len(ports), len(components), len(antennas), 1, 1))))
    fc_L = cm.jet(np.nan_to_num(L_n / np.amax(np.abs(L_n), axis=(3, 4)).reshape((len(ports), len(components), len(antennas), 1, 1))))

    # Compute Limits
    lim_E = np.empty((len(ports), len(components), len(antennas), 2), dtype=np.float)
    lim_E[:, :, :, 0] = np.amin(E, axis=(3, 4))
    lim_E[:, :, :, 1] = np.amax(E, axis=(3, 4))
    lim_L = np.empty((len(ports), len(components), len(antennas), 2), dtype=np.float)
    lim_L[:, :, :, 0] = np.amin(L, axis=(3, 4))
    lim_L[:, :, :, 1] = np.amax(L, axis=(3, 4))
    lim_L_n = np.empty((len(ports), len(components), len(antennas), 2), dtype=np.float)
    lim_L_n[:, :, :, 0] = np.amin(L_n, axis=(3, 4)) - clip
    lim_L_n[:, :, :, 1] = np.amax(L_n, axis=(3, 4)) - clip

    # ===== Multi-Antenna Views ===== #
    # Prepare the Figure/Axes
    _figure = plt.figure()
    _axes_lst = np.empty((len(represent),), dtype=np.object) # Gain Plot * Polar Plot
    _clr_axis = None    # Color Bar Axis
    _surf_plt = None    # The Surface Plot
    _clr_bar = None     # The ColorBar
    _plr_plt = None     # The Polar Plot
    _sel_ant = 0  # Antenna Selection
    _sel_prt = 0  # Polarisation Selection
    _sel_com = 0  # Component Selection
    _sel_typ = 0  # Type Selection
    _sel_phi = 0  # Phi Angle Selection for Polar plot
    _sel_pol = 0  # Polar selection (linear vs log)

    # Prepare the Axis Computations for Spherical Coordinates
    X_E = np.multiply(E, x)
    Y_E = np.multiply(E, y)
    Z_E = np.multiply(E, z)

    X_L = np.multiply(L_n, x)
    Y_L = np.multiply(L_n, y)
    Z_L = np.multiply(L_n, z)

    def update_plot_sep(surf_plot = True, polar_plot = True):
        global _surf_plt, _clr_bar, _plr_plt
        # Selectively, update the main plot
        if surf_plot:
            # Update the Title:
            _figure.suptitle('EM Model for Antenna ' + str(antennas[_sel_ant]) + ' in polarisation ' + str(ports[_sel_prt])
                             + ' and component ' + components[_sel_com])
            # Remove the previous Surface
            if _surf_plt is not None: _surf_plt.remove(); _surf_sep = None
            # Do the Surface Plot First
            if _sel_typ == 0:
                _surf_plt = _axes_lst[0].plot_surface(x, y, E[_sel_prt, _sel_com, _sel_ant], linewidth=0, cmap=cm.jet)
                limits = lim_E[_sel_prt, _sel_com, _sel_ant]
                _axes_lst[0].axis('on')
                _axes_lst[0].set_xlabel('u')
                _axes_lst[0].set_ylabel('v')
                _axes_lst[0].set_zlabel('|E|')
            elif _sel_typ == 1:
                _surf_plt = _axes_lst[0].plot_surface(x, y, L[_sel_prt, _sel_com, _sel_ant], linewidth=0, cmap=cm.jet)
                limits = lim_L[_sel_prt, _sel_com, _sel_ant]
                _axes_lst[0].axis('on')
                _axes_lst[0].set_xlabel('u')
                _axes_lst[0].set_ylabel('v')
                _axes_lst[0].set_zlabel('log|E|')
            elif _sel_typ == 2:
                _surf_plt = _axes_lst[0].plot_surface(X_E[_sel_prt, _sel_com, _sel_ant],
                                                      Y_E[_sel_prt, _sel_com, _sel_ant],
                                                      Z_E[_sel_prt, _sel_com, _sel_ant], linewidth=0,
                                                      facecolors=fc_E[_sel_prt, _sel_com, _sel_ant])
                limits = lim_E[_sel_prt, _sel_com, _sel_ant]
                _axes_lst[0].axis('off')
            else:
                _surf_plt = _axes_lst[0].plot_surface(X_L[_sel_prt, _sel_com, _sel_ant],
                                                      Y_L[_sel_prt, _sel_com, _sel_ant],
                                                      Z_L[_sel_prt, _sel_com, _sel_ant], linewidth=0,
                                                      facecolors=fc_L[_sel_prt, _sel_com, _sel_ant])
                limits = lim_L_n[_sel_prt, _sel_com, _sel_ant]
                _axes_lst[0].axis('off')
            # Now the Color Bar
            _clr_bar = clrbr.ColorbarBase(_clr_axis, cmap=cm.jet, norm=clrs.Normalize(*limits))
        # Selectively update the Polar Plot
        if polar_plot:
            phi_indx_l = int(_sel_phi / phi_step)
            phi_indx_r = phi_indx_l+(180/phi_step)
            if _sel_pol == 0:
                data = np.concatenate((E[_sel_prt, _sel_com, _sel_ant, ::-1, phi_indx_r],
                                       E[_sel_prt, _sel_com, _sel_ant, 1::, phi_indx_l]))
            else:
                data = np.concatenate((L_n[_sel_prt, _sel_com, _sel_ant, ::-1, phi_indx_r],
                                       L_n[_sel_prt, _sel_com, _sel_ant, 1::, phi_indx_l]))
            if _plr_plt is None:
                _plr_plt, = _axes_lst[1].plot(T, data)
                _axes_lst[1].set_theta_zero_location("N")
            else:
                _plr_plt.set_ydata(data)
                _axes_lst[1].relim()
                _axes_lst[1].autoscale()
        # Finally Draw
        plt.draw()

    # Generate Plot Axes
    gs = gspec.GridSpec(1, 2, width_ratios=[2, 1])
    _axes_lst[0] = plt.subplot(gs[0], projection='3d')
    _axes_lst[1] = plt.subplot(gs[1], projection='polar')
    _figure.subplots_adjust(left=0.04, right=0.95, wspace=0.25, bottom=0.125, top=0.900)
    _clr_axis = _figure.add_axes([0.575, 0.2, 0.02, 0.6])
    update_plot_sep()

    def on_Antenna(antenna):
        global _sel_ant
        _sel_ant = int(antenna)
        update_plot_sep()

    def on_Antenna_Sub(event):
        global _sel_ant, _ant_sld_sep
        _sel_ant = max(_sel_ant - 1, 0)
        _ant_sld_sep.set_val(_sel_ant)

    def on_Antenna_Add(event):
        global _sel_ant, _ant_sld_sep
        _sel_ant = min(_sel_ant + 1, len(antennas) - 1)
        _ant_sld_sep.set_val(_sel_ant)

    def on_Port_sep(polaris):
        global _sel_prt
        _sel_prt = ports.index(int(polaris))
        update_plot_sep() # No need to update raw-data

    def on_Comp_sep(comp):
        global _sel_com
        _sel_com = components.index(comp)
        update_plot_sep() # No need to update raw-data

    def on_Type_sep(label):
        global _sel_typ
        _sel_typ = surf_type.index(label)
        update_plot_sep(polar_plot=False)

    def on_Phi(phi):
        global _sel_phi
        _sel_phi = float(phi)
        update_plot_sep(surf_plot=False)

    def on_Pol_scale(scale):
        global _sel_pol
        _sel_pol = polar_type.index(scale)
        update_plot_sep(surf_plot=False)

    # Polarisation Select
    _pol_rb_sep = RadioButtons(_figure.add_axes([0.02, 0.6, 0.03, 0.075], facecolor='lightgoldenrodyellow'),
                               ports, active=0, activecolor='red')
    plt.title('PORT')
    _pol_rb_sep.on_clicked(on_Port_sep)

    # Component Selection
    _com_rb_sep = RadioButtons(_figure.add_axes([0.02, 0.4, 0.03, 0.125], facecolor='lightgoldenrodyellow'),
                               components, active=0, activecolor='red')
    plt.title('COM')
    _com_rb_sep.on_clicked(on_Comp_sep)

    # Style Selection
    _typ_select_sep = HorRadioButtons(_figure.add_axes([0.150, 0.90, 0.300, 0.030], fc='lightgoldenrodyellow'),
                                      surf_type, 'Style', active=0, activecolor='red')
    _typ_select_sep.on_clicked(on_Type_sep)

    # Antenna Selection (slider)
    _ant_sld_sep = Slider(_figure.add_axes([0.05, 0.06, 0.9, 0.03], facecolor='lightgoldenrodyellow'), label='Sample',
                          valmin=0, valmax=len(antennas) - 1, valinit=_sel_ant, valfmt='%d')
    _ant_sld_sep.on_changed(on_Antenna)
    # Antenna Selection (buttons)
    _ant_add_sep = Button(_figure.add_axes([0.15, 0.02, 0.1, 0.03]), 'Antenna (-)', color='lightgoldenrodyellow',
                          hovercolor='0.975')
    _ant_add_sep.on_clicked(on_Antenna_Sub)
    _ant_sub_sep = Button(_figure.add_axes([0.70, 0.02, 0.1, 0.03]), 'Antenna (+)', color='lightgoldenrodyellow',
                          hovercolor='0.975')
    _ant_sub_sep.on_clicked(on_Antenna_Add)

    # Phi Selection (slider)
    _phi_sld = Slider(_figure.add_axes([0.65, 0.2, 0.3, 0.03], facecolor='lightgoldenrodyellow'), label='Phi',
                      valmin = 0.0, valmax=179.0, valinit=_sel_phi, valfmt='%d')
    _phi_sld.on_changed(on_Phi)

    # Polar Type Selection
    _pol_select_sep = HorRadioButtons(_figure.add_axes([0.700, 0.90, 0.200, 0.030], fc='lightgoldenrodyellow'),
                                      polar_type, 'Scale', active=0, activecolor='red')
    _pol_select_sep.on_clicked(on_Pol_scale)


    plt.show()
