import matplotlib as mplib
import numpy as np
import scipy.io as sio

mplib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gspec

from matplotlib import cm
from mpl_toolkits import mplot3d
from matplotlib import colors as clrs
from matplotlib import colorbar as clrbr
from matplotlib.widgets import Slider, RadioButtons

from pytcpo.Core.Common import HorRadioButtons

# Setup
model_file = '/media/michael/Elements/MichaelC/Data/EM Model/EEP_199.68MHz.mat'  # EM Models
# model_file = '/media/michael/Elements/MichaelC/Data/EM Model/EEP_dummy.mat'  # EM Models
# model_file = '/home/michael/Downloads/EM.mat'  # EM Models
# model_file = '/home/michael/Downloads/EPP_Dummy.mat'  # EM Models

ports = [1, 2]
theta_step = 1
phi_step = 2
antennas = list(range(256))             # The Antennas (0 through 256)
components = ['pvv', 'phh', 'I', 'I*']  # The last two are the magnitude overall, computed according to different schemes
represent = ['Gain', 'Polar']           # Represent the 3D gain and the polar plot
surf_type = ['Rect.', 'L. Rect', 'Spher.', 'L. Spher.']    # The type of plot to draw
polar_type = ['Lin.', 'Log.']           # The type of display for polar plot
clip = 60

if __name__=='__main__':

    # Load the Mat File & Prepare
    data = sio.loadmat(model_file)
    theta = np.arange(0, 91, theta_step) * np.pi / 180.0  # Theta Values [0 through 90 in degrees]
    phi = np.arange(0, 361, phi_step) * np.pi / 180.0  # The Phi Values [0 through 360, stepping at 2]
    t_dim = len(theta)
    p_dim = len(phi)
    T = np.arange(-90, 91, theta_step) * np.pi / 180.0

    # Mesh the Dimensions
    u, v = np.meshgrid(theta, phi)

    # This has the same effect as the mesh-grid in Matlab, since it is an outer product!
    x = np.transpose(np.multiply(np.sin(u), np.cos(v)))
    y = np.transpose(np.multiply(np.sin(u), np.sin(v)))
    z = np.transpose(np.cos(u))

    # Electric Field Strength - This is just the beam-pattern
    #                 1/2        vv/hh/I/I*    theta   phi
    P = np.empty((len(ports), len(components), t_dim, p_dim), dtype=np.float)
    E_VV = np.zeros((len(ports), t_dim, p_dim), dtype=np.complex)
    E_HH = np.zeros((len(ports), t_dim, p_dim), dtype=np.complex)
    for idx, polaris in enumerate(ports):
        for ant in range(len(antennas)):
            # Pvv Component (0)
            E_VV[idx] += np.reshape(data[components[0] + str(polaris)][:, ant], (t_dim, p_dim), 'F')
            # Phh Component (1)
            E_HH[idx] += np.reshape(data[components[1] + str(polaris)][:, ant], (t_dim, p_dim), 'F')
    # Now Compute
    P[:,0] = np.absolute(E_VV)
    P[:,1] = np.absolute(E_HH)
    P[:,2] = np.sqrt(np.square(P[:,0]) + np.square(P[:,1]))
    P[:,3] = np.absolute(E_VV + E_HH)


    # Log (and normalised log) of the P-Matrix
    K = P/np.amax(P, axis=(2, 3)).reshape((len(ports), len(components), 1, 1))
    K[K == 0] = np.nan
    L = np.nan_to_num(20 * np.log10(K))
    L_n = L - np.amax(L, axis=(2, 3)).reshape((len(ports), len(components), 1, 1)) + clip
    L_n[L_n < 0] = 0

    # Compute FaceColors:
    fc_P = cm.jet(np.nan_to_num(P / np.amax(P, axis=(2, 3)).reshape((len(ports), len(components), 1, 1))))
    fc_P = cm.jet(np.nan_to_num(L_n / np.amax(np.abs(L_n), axis=(2, 3)).reshape((len(ports), len(components), 1, 1))))

    # Compute Limits
    lim_P = np.empty((len(ports), len(components), 2), dtype=np.float)
    lim_P[:, :, 0] = np.amin(P, axis=(2, 3))
    lim_P[:, :, 1] = np.amax(P, axis=(2, 3))
    lim_L = np.empty((len(ports), len(components), 2), dtype=np.float)
    lim_L[:, :, 0] = np.amin(L, axis=(2, 3))
    lim_L[:, :, 1] = np.amax(L, axis=(2, 3))
    lim_L_n = np.empty((len(ports), len(components), 2), dtype=np.float)
    lim_L_n[:, :, 0] = np.amin(L_n, axis=(2, 3)) - clip
    lim_L_n[:, :, 1] = np.amax(L_n, axis=(2, 3)) - clip

    # ===== Multi-Antenna Views ===== #
    # Prepare the Figure/Axes
    _figure = plt.figure()
    _axes_lst = np.empty((len(represent),), dtype=np.object)  # Gain Plot * Polar Plot
    _clr_axis = None  # Color Bar Axis
    _surf_plt = None  # The Surface Plot
    _clr_bar = None  # The ColorBar
    _plr_plt = None  # The Polar Plot
    _sel_prt = 0  # Polarisation Selection
    _sel_com = 0  # Component Selection
    _sel_typ = 0  # Type Selection
    _sel_phi = 0  # Phi Angle Selection for Polar plot
    _sel_pol = 0  # Polar selection (linear vs log)

    # Prepare the Axis Computations for Spherical Coordinates
    X_P = np.multiply(P, x)
    Y_P = np.multiply(P, y)
    Z_P = np.multiply(P, z)

    X_L = np.multiply(L_n, x)
    Y_L = np.multiply(L_n, y)
    Z_L = np.multiply(L_n, z)


    def update_plot_sep(surf_plot=True, polar_plot=True):
        global _surf_plt, _clr_bar, _plr_plt
        # Selectively, update the main plot
        if surf_plot:
        # Update the Title:
            _figure.suptitle('EM Model for port ' + str(ports[_sel_prt]) + ' and component ' + components[_sel_com])
            # Remove the previous Surface
            if _surf_plt is not None: _surf_plt.remove(); _surf_plt = None
            # Do the Surface Plot First
            if _sel_typ == 0:
                _surf_plt = _axes_lst[0].plot_surface(x, y, P[_sel_prt, _sel_com], linewidth=0, cmap=cm.jet)
                limits = lim_P[_sel_prt, _sel_com]
                _axes_lst[0].axis('on')
                _axes_lst[0].set_xlabel('u')
                _axes_lst[0].set_ylabel('v')
                _axes_lst[0].set_zlabel('|E|')
            elif _sel_typ == 1:
                _surf_plt = _axes_lst[0].plot_surface(x, y, L[_sel_prt, _sel_com], linewidth=0, cmap=cm.jet)
                limits = lim_L[_sel_prt, _sel_com]
                _axes_lst[0].axis('on')
                _axes_lst[0].set_xlabel('u')
                _axes_lst[0].set_ylabel('v')
                _axes_lst[0].set_zlabel('log|E|')
            elif _sel_typ == 2:
                _surf_plt = _axes_lst[0].plot_surface(X_P[_sel_prt, _sel_com],
                                                      Y_P[_sel_prt, _sel_com],
                                                      Z_P[_sel_prt, _sel_com], linewidth=0,
                                                      facecolors=fc_P[_sel_prt, _sel_com])
                limits = lim_P[_sel_prt, _sel_com]
                _axes_lst[0].axis('off')
            else:
                _surf_plt = _axes_lst[0].plot_surface(X_L[_sel_prt, _sel_com],
                                                      Y_L[_sel_prt, _sel_com],
                                                      Z_L[_sel_prt, _sel_com], linewidth=0,
                                                      facecolors=fc_P[_sel_prt, _sel_com])
                limits = lim_L_n[_sel_prt, _sel_com]
                _axes_lst[0].axis('off')
            # Now the Color Bar
            _clr_bar = clrbr.ColorbarBase(_clr_axis, cmap=cm.jet, norm=clrs.Normalize(*limits))

        # Selectively Update Polar
        if polar_plot:
            phi_indx_l = int(_sel_phi / phi_step)
            phi_indx_r = phi_indx_l+(180/phi_step)
            if _sel_pol == 0:
                data = np.concatenate((P[_sel_prt, _sel_com, ::-1, phi_indx_r],
                                       P[_sel_prt, _sel_com, 1::, phi_indx_l]))
            else:
                data = np.concatenate((L_n[_sel_prt, _sel_com, ::-1, phi_indx_r],
                                       L_n[_sel_prt, _sel_com, 1::, phi_indx_l]))
            if _plr_plt is None:
                _plr_plt, = _axes_lst[1].plot(T, data)
                _axes_lst[1].set_theta_zero_location("N")
            else:
                _plr_plt.set_ydata(data)
                _axes_lst[1].relim()
                _axes_lst[1].autoscale()
        # Finally Draw
        plt.draw()

    # Generate Plot Axes
    gs = gspec.GridSpec(1, 2, width_ratios=[2, 1])
    _axes_lst[0] = plt.subplot(gs[0], projection='3d')
    _axes_lst[1] = plt.subplot(gs[1], projection='polar')
    _figure.subplots_adjust(left=0.04, right=0.95, wspace=0.25, bottom=0.05, top=0.900)
    _clr_axis = _figure.add_axes([0.575, 0.2, 0.02, 0.6])
    update_plot_sep()

    def on_Port_sep(polaris):
        global _sel_prt
        _sel_prt = ports.index(int(polaris))
        update_plot_sep() # No need to update raw-data

    def on_Comp_sep(comp):
        global _sel_com
        _sel_com = components.index(comp)
        update_plot_sep() # No need to update raw-data

    def on_Type_sep(label):
        global _sel_typ
        _sel_typ = surf_type.index(label)
        update_plot_sep(polar_plot=False)

    def on_Phi(phi):
        global _sel_phi
        _sel_phi = float(phi)
        update_plot_sep(surf_plot=False)

    def on_Pol_scale(scale):
        global _sel_pol
        _sel_pol = polar_type.index(scale)
        update_plot_sep(surf_plot=False)

    # Polarisation Select
    _pol_rb_sep = RadioButtons(_figure.add_axes([0.02, 0.6, 0.03, 0.075], facecolor='lightgoldenrodyellow'),
                               ports, active=0, activecolor='red')
    plt.title('PORT')
    _pol_rb_sep.on_clicked(on_Port_sep)

    # Component Selection
    _com_rb_sep = RadioButtons(_figure.add_axes([0.02, 0.4, 0.03, 0.125], facecolor='lightgoldenrodyellow'),
                               components, active=0, activecolor='red')
    plt.title('COM')
    _com_rb_sep.on_clicked(on_Comp_sep)

    # Style Selection
    _typ_select_sep = HorRadioButtons(_figure.add_axes([0.150, 0.90, 0.300, 0.030], fc='lightgoldenrodyellow'),
                                      surf_type, 'Style', active=0, activecolor='red')
    _typ_select_sep.on_clicked(on_Type_sep)

    # Phi Selection (slider)
    _phi_sld = Slider(_figure.add_axes([0.65, 0.15, 0.3, 0.03], facecolor='lightgoldenrodyellow'), label='Phi',
                      valmin = 0.0, valmax=179.0, valinit=_sel_phi, valfmt='%d')
    _phi_sld.on_changed(on_Phi)

    # Polar Type Selection
    _pol_select_sep = HorRadioButtons(_figure.add_axes([0.700, 0.90, 0.200, 0.030], fc='lightgoldenrodyellow'),
                                      polar_type, 'Scale', active=0, activecolor='red')
    _pol_select_sep.on_clicked(on_Pol_scale)


    plt.show()
