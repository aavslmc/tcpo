import logging

import numpy as np
import numpy.polynomial.polynomial as nppol
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons, CheckButtons

from pytcpo.Core.Common import MovingAverage, TCPOCode
from pytcpo.Core.PipeLine import PipelineManager, AbstractProcessor
from pytcpo.Modules.Archivers import FilePersistor, RedisPersistor

import pytcpo.Modules.IO
import pytcpo.Modules.Archivers
import pytcpo.Modules.RawFilters
import pytcpo.Modules.BandPassModel
import pytcpo.Modules.AnomalyDetectors

# Original 'Storage-Specific' Definitions
O_Samples = (0,20000)                # Sample Range in original file
O_Antennas = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)  # Which are the valid Antennas
O_Polarisations = (0,1)                   # Number of Polarisations
O_Tile = 1                                # Which Tile
O_DownSample = 200                        # Down-Sample Factor (skipping values)
O_Model = 'PPF_log'
O_Outliers = []
O_Anomalies = []
O_Logs = True
O_Orders = [5,7]
O_Buffer = 15
ma_length = 5

# Plotting Specifications
P_DownSample = 1                          # DownSample Rate to plot at (relative to original)

# Computations
Freq_Offs = 0
Freq_Scale = 0.78125
Num_Samples = (O_Samples[1] - O_Samples[0])//(O_DownSample * P_DownSample)
P_Channels = np.arange(74,448)
P_Frequencies = P_Channels*Freq_Scale + Freq_Offs # Re-Compute Frequencies

# Change this to suit your needs... i.e. were the data is stored
raw_location = '/home/xandru/Documents/Data/NewData2'
src_location = '/home/xandru/Documents/ScratchPiecewise/TestDetectors2'

show_derivatives = False
show_squared = False
show_averages = False
averages_fits_channels = [np.arange(74,125), np.arange(125,448)] # Partition P_Channels

persistor = FilePersistor
serialiser = 'CSVSerialiser'

class SampleConsumer(AbstractProcessor):

    def __init__(self):
        super(SampleConsumer, self).__init__()
        self.store = None
        self.index = None

    def _setup(self, filter_model):
        self.store = filter_model
        return True

    def _will_start(self):
        self.index = 0
        return True

    def _new_sample(self, message, forward):
        self.store[:,:,self.index,:] = message.Default
        self.index += 1
        return True

    def _clean_up(self, reason):
        pass

PipelineManager.registerModule(SampleConsumer)


if __name__=='__main__':

    dearchiver = persistor()

    logging.basicConfig(level=logging.INFO)

    # Pre-Allocate
    raw_data = np.zeros((2, len(O_Antennas), len(O_Polarisations), Num_Samples, len(P_Channels)), dtype=np.float)
    polycoefs = np.empty((len(O_Antennas), len(O_Polarisations), Num_Samples, (2*len(O_Orders))-1), dtype=np.object)
    outliers = np.empty((len(O_Antennas), len(O_Polarisations), len(O_Outliers), Num_Samples), dtype=np.object)
    anomalies = np.zeros((len(O_Antennas), len(O_Polarisations), len(O_Anomalies), Num_Samples), dtype=np.bool)
    if show_averages:
        mov_avg = np.empty((len(O_Antennas), len(O_Polarisations), len(O_Anomalies), len(O_Orders), Num_Samples), dtype=np.object)
    if show_squared:
        squared = np.empty((len(O_Antennas), len(O_Polarisations), Num_Samples, len(P_Channels)), dtype=np.float)

    print ' Reading Raw Samples... '
    manager = PipelineManager()

    # Reader
    manager.add_root('ChannelStageReader', {'path': raw_location, 'time': None, 'tiles': O_Tile, 'sample_rate': 0,
                                            'block_rate': 2048, 'samples': O_Samples, 'stop_on_end': True})
    # Down-Sampler
    manager.add_module('AliasingDownSampler', {'rate': (O_DownSample * P_DownSample), 'start': O_Samples[0]})
    # Antenna Selector
    manager.add_module('AntennaSelector', {'in_antennas': np.arange(16), 'out_antennas': np.asarray(O_Antennas)})
    # PolarisationSelector
    manager.add_module('PolarisationSelector', {'in_polarisations': np.arange(2), 'out_polarisations': np.asarray(O_Polarisations)})
    # ChannelSelector
    manager.add_module('ChannelSelector', {'in_channels': np.arange(512), 'out_channels': P_Channels})
    # CScaler
    scaler = manager.add_module('CScaler', {'input': (0, 65535), 'output': (0, 65535), 'type': float})
    # Log-Scale
    manager.add_module('PowerLog', {'multiplier': 10, 'log': '10'})
    # Samplers
    manager.add_module('SampleConsumer', raw_data[1]) # Power Log values
    manager.add_module('SampleConsumer', raw_data[0], (scaler,))  # Linear values

    # Build
    manager.build()

    # Run Pipeline
    assert (manager.start() == TCPOCode.Status.ACTIVE)
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)

    # Calculate Diffs
    if show_derivatives:
        diff_data = np.pad(np.diff(raw_data), ((0,0), (0,0), (0,0), (0,0), (1,0)), 'edge') + 30.0

    print ' Reading Coefficients... '
    assert (dearchiver.setup(FilePersistor.M_READ, {'source':src_location + '/' + O_Model, 'keys':{'polyfit':np.ndarray}}, serialiser, None))
    # assert (dearchiver.setup(FilePersistor.M_READ, {'host': 'localhost', 'port': 1500, 'name': 'simple-pipeline', 'auth': 'tcpo.test.2oi7', 'keys': {'polyfit': np.ndarray}}, serialiser, None))
    for s in range(Num_Samples):
        # Skip the first Num_Samples - 1 lines
        assert (dearchiver.SkipNext('polyfit', P_DownSample - 1))
        # Now Read in the next sample
        ts, sample = dearchiver.ReadNext('polyfit')
        for a_i in range(len(O_Antennas)):
            for p_i in range(len(O_Polarisations)):
                squared_channels = np.array([])
                for f_i in range((2*len(O_Orders))-1):
                    polycoefs[a_i,p_i,s,f_i] = sample[a_i,p_i,f_i]

                    if show_squared:
                        squared_channels = np.concatenate((squared_channels,
                                                           np.multiply(nppol.polyval(sample[a_i,p_i,f_i].ret_chans(),
                                                                                     sample[a_i,p_i,f_i].ret_coeff()),
                                                                       nppol.polyval(sample[a_i,p_i,f_i].ret_chans(),
                                                                                     sample[a_i,p_i,f_i].ret_coeff()))))

                if show_squared:
                    squared[a_i,p_i,s,:] = squared_channels[:]

    print ' Reading Outliers'
    for d_i, d_v in enumerate(O_Outliers):
        assert (dearchiver.setup(FilePersistor.M_READ, {'source': src_location + '/' + O_Model, 'keys': {'out_'+d_v: np.ndarray}}, serialiser, None))
        # assert (dearchiver.setup(FilePersistor.M_READ, {'host': 'localhost', 'port': 1500, 'name': 'simple-pipeline', 'auth': 'tcpo.test.2oi7', 'keys': {'out_' + d_v: np.ndarray}}, serialiser, None))
        for s in range(Num_Samples):
            # Skip the first Num_Samples - 1 lines
            assert (dearchiver.SkipNext('out_' + d_v, P_DownSample - 1))
            # Now Read in the next sample
            ts, sample = dearchiver.ReadNext('out_' + d_v)
            for failure in sample.ravel():
                x = failure._anomaly
                outliers[failure._ant, failure._pol, d_i, s] = x - P_Channels[0]

    print ' Reading Anomalies ... '
    for a_i, a_v in enumerate(O_Anomalies):
        assert (dearchiver.setup(FilePersistor.M_READ, {'source': src_location + '/' + O_Model, 'keys': {'anom_' + a_v: np.ndarray}}, serialiser, None))
        # assert (dearchiver.setup(FilePersistor.M_READ, {'host': 'localhost', 'port': 1500, 'name': 'simple-pipeline', 'auth': 'tcpo.test.2oi7', 'keys': {'anom_' + a_v: np.ndarray}}, serialiser, None))

        if a_v in ['IOD', 'OWOD', 'PADD']: # Handling buffering cases
            Sample_Range = Num_Samples - O_Buffer
        else:
            Sample_Range = Num_Samples

        for s in range(Sample_Range):
            # Skip the first Num_Samples - 1 lines
            assert (dearchiver.SkipNext('anom_' + a_v, P_DownSample - 1))
            # Now Read in the next sample
            ts, sample = dearchiver.ReadNext('anom_' + a_v)
            for failure in sample.ravel():
                anomalies[failure._ant, failure._pol, a_i, s] = True

    if show_averages:
        print ' Computing Averages... '
        for n_i in range(len(O_Anomalies)):
            _buffers = np.empty((len(O_Antennas), len(O_Polarisations), len(O_Orders)), np.object)
            # Build Models
            for a in range(len(O_Antennas)):
                for p in range(len(O_Polarisations)):
                    for o, o_v in enumerate(O_Orders):
                        _buffers[a,p,o] = MovingAverage(np.zeros(o_v + 1), ma_length)
            # Now compute sample averages
            for a in range(len(O_Antennas)):
                for p in range(len(O_Polarisations)):
                    for o, o_v in enumerate(O_Orders):
                        mov_avg[a, p, n_i, o, 0] = np.zeros(o_v + 1)
                    for s in range(Num_Samples - 1):
                        if anomalies[a,p,n_i,s]:
                            mov_avg[a,p,n_i,:,s+1] = mov_avg[a,p,n_i,:,s]
                        else:
                            for o in range(len(O_Orders)):
                                mov_avg[a,p,n_i,o,s+1] = _buffers[a,p,o].add(polycoefs[a,p,s,2*o].ret_coeff())

    print ' Plotting ... '

    # Globals
    _figure = plt.figure()
    _axes = np.empty((len(O_Polarisations),), dtype=np.object)    # Polarisations all in same figure
    _marks = np.empty(len(O_Anomalies), dtype=np.object)
    _lines = np.empty((len(O_Polarisations), 6), dtype=np.object)  # Lines per Polarisation
    _sel_ant = 0    # Antenna Selection
    _sel_ord = 0    # Order Selection
    _sel_mod = 0    # Model Selection
    _sel_smp = 0    # Sample Selection
    _sel_out = 0    # Outlier Detection Mode
    _scale_ax = True # Scale Axes

    # Define Some Helper Functions
    def set_axes_color(axis, colour, width):
        for name in ['top', 'bottom', 'left', 'right']:
            axis.spines[name].set_color(colour)
            axis.spines[name].set_linewidth(width)

    def update_plot(update_raw):
        plt.suptitle(O_Model + '  with Outliers and Anomaly Detection for Antenna ' + str(O_Antennas[_sel_ant]))
        for p_i, p_v in enumerate(O_Polarisations):

            # First Update Graphs
            if update_raw:
                _lines[p_i, 0].set_ydata(raw_data[O_Logs * 1, _sel_ant, p_i, _sel_smp])
                if show_derivatives:
                    _lines[p_i, 3].set_ydata(diff_data[O_Logs * 1, _sel_ant, p_i, _sel_smp])

            chan_idx = 0
            evaluated = np.empty((len(P_Channels),), dtype=np.float64)
            for fit in polycoefs[_sel_ant, p_i, _sel_smp]:
                chan_cnt = len(fit.ret_chans())
                evaluated[chan_idx:(chan_idx + chan_cnt)] = nppol.polyval(fit.ret_chans(), fit.ret_coeff())
                chan_idx += chan_cnt
            _lines[p_i, 1].set_ydata(evaluated)

            if len(O_Outliers) == 0:
                _lines[p_i, 2], = plt.plot(0, 0)
            elif outliers[_sel_ant, p_i, _sel_out, _sel_smp] is None:
                _lines[p_i, 2], = plt.plot(0, 0)
            else:
                _lines[p_i, 2].set_data(P_Frequencies[outliers[_sel_ant, p_i, _sel_out, _sel_smp]],
                                        raw_data[O_Logs * 1, _sel_ant, p_i, _sel_smp,
                                        outliers[_sel_ant, p_i, _sel_out, _sel_smp]])
            if show_averages:
                chan_idx = 0
                evaluated = np.empty((len(P_Channels),), dtype=np.float64)
                for c, chans in enumerate(averages_fits_channels):
                    chan_cnt = len(chans)
                    evaluated[chan_idx:(chan_idx + chan_cnt)] = nppol.polyval(chans, mov_avg[_sel_ant,p_i,0,o,_sel_smp])
                    chan_idx += chan_cnt

                _lines[p_i, 4].set_ydata(evaluated)

            if show_squared:
                _lines[p_i, 5].set_ydata(squared[_sel_ant, p_i, _sel_smp])

            for m_i, m_v in enumerate(_marks):
                m_v[p_i].set_height(1 if anomalies[_sel_ant, p_i, m_i, _sel_smp] else 0)

            # Scale
            if _scale_ax:
                _axes[p_i].relim()
                _axes[p_i].autoscale()

        plt.draw()

    def on_Antenna(antenna):
        global _sel_ant
        _sel_ant = O_Antennas.index(int(antenna))
        update_plot(True)

    def on_Outlier(order):
        global _sel_out
        _sel_out = O_Outliers.index(order)
        update_plot(False)

    def on_Sample(sample):
        global _sel_smp
        _sel_smp = int(sample)
        update_plot(True)

    def on_Scale(label):
        global _scale_ax
        if label == 'Scale':
            _scale_ax = not(_scale_ax)
            if _scale_ax:
                for ax in _axes:
                    ax.relim()
                    ax.autoscale()
                plt.draw()

    def on_Sample_Sub(event):
        global _sel_smp, _smp_sld
        _sel_smp = max(_sel_smp - 1, 0)
        _smp_sld.set_val(_sel_smp)

    def on_Sample_Add(event):
        global _sel_smp, _smp_sld
        _sel_smp = min(_sel_smp + 1, Num_Samples-1)
        _smp_sld.set_val(_sel_smp)

    # Create Plot(s)
    plt.suptitle(O_Model + '  with Outliers and Anomaly Detection for Antenna ' + str(O_Antennas[_sel_ant]))
    for p_i, p_v in enumerate(O_Polarisations):  # Iterate over Polarisations (in same Figure)

        # Sharing of Axes
        _share = _axes[0] if p_i > 0 else None

        # Create SubPlot & plot
        _axes[p_i] = _figure.add_subplot((len(O_Polarisations)+1)/2, 2,  p_i + 1, sharex=_share, sharey=_share)
        _axes[p_i].set_title('Polar. ' + str(p_v))
        _lines[p_i, 0], = plt.plot(P_Frequencies, raw_data[O_Logs * 1, _sel_ant, p_i, _sel_smp])

        chan_idx = 0
        evaluated = np.empty((len(P_Channels),), dtype=np.float64)
        for fit in polycoefs[_sel_ant, p_i, _sel_smp]:
            chan_cnt = len(fit.ret_chans())
            evaluated[chan_idx:(chan_idx + chan_cnt)] = nppol.polyval(fit.ret_chans(), fit.ret_coeff())
            chan_idx += chan_cnt
        _lines[p_i,1], = plt.plot(P_Frequencies, evaluated)

        if len(O_Outliers) == 0:
            _lines[p_i, 2], = plt.plot(0,0)
        elif outliers[_sel_ant, p_i, _sel_out, _sel_smp] is None:
            _lines[p_i, 2], = plt.plot(0,0)
        else :
            _lines[p_i, 2], = plt.plot(P_Frequencies[outliers[_sel_ant, p_i, _sel_out, _sel_smp]],
                                       raw_data[O_Logs * 1, _sel_ant, p_i, _sel_smp,
                                       outliers[_sel_ant, p_i, _sel_out, _sel_smp]],'x')

        if show_derivatives:
            _lines[p_i, 3], = plt.plot(P_Frequencies, diff_data[O_Logs * 1, _sel_ant, p_i, _sel_smp])

        if show_averages:
            chan_idx = 0
            evaluated = np.empty((len(P_Channels),), dtype=np.float64)
            for c, chans in enumerate(averages_fits_channels):
                chan_cnt = len(chans)
                evaluated[chan_idx:(chan_idx + chan_cnt)] = nppol.polyval(chans, mov_avg[_sel_ant, p_i, 0, o, _sel_smp])
                chan_idx += chan_cnt

            _lines[p_i, 4], = plt.plot(P_Frequencies,evaluated)

        if show_squared:
            _lines[p_i, 5], = plt.plot(P_Frequencies, squared[_sel_ant, p_i, _sel_smp])

        # Labels
        if p_i % 2 == 0:
            plt.ylabel('Magnitude')
        else:
            plt.setp(_axes[p_i].get_yticklabels(), visible=False)

        plt.xlabel('Frequency')
        labels = ['Raw (Log) Data', 'ModelFit', 'Outliers']
        lins = [_lines[0,0], _lines[0,1], _lines[0,2]]

        if show_derivatives:
            labels.append('Derivative')
            lins.append(_lines[0,3])

        if show_averages:
            labels.append('Averages')
            lins.append(_lines[0,4])
        plt.legend(lins, labels)

    # Arrange Space and add Controls
    _figure.subplots_adjust(left=0.165, wspace=0.025, right=0.975, bottom=0.15, top=0.925)

    # Anomaly Detections
    for a_i in range(len(O_Anomalies)):
        _figure.add_axes([0.025, 0.55 - 0.08*a_i, 0.075, 0.05])
        _marks[a_i] = plt.bar(O_Polarisations,
                              np.ones(len(O_Polarisations)) * anomalies[_sel_ant, :, a_i, _sel_smp],
                              color='red')
        plt.gca().get_yaxis().set_ticks([])
        plt.ylabel(O_Anomalies[a_i])
        plt.ylim([0,1])

        if a_i == 0:
            plt.gca().xaxis.tick_top()
            plt.xticks(O_Polarisations,['P'+str(pol) for pol in O_Polarisations])
        else:
            plt.gca().get_xaxis().set_visible(False)

    # Outlier Selection
    _out_rb = RadioButtons(_figure.add_axes([0.075, 0.75, 0.05, 0.2], facecolor='lightgoldenrodyellow'),
                           O_Outliers, active=0, activecolor='red')
    plt.title('OUT'); _out_rb.on_clicked(on_Outlier)

    # Antenna-Selection
    _ant_rb = RadioButtons(_figure.add_axes([0.035, 0.75, 0.02, 0.2], facecolor='lightgoldenrodyellow'),
                           O_Antennas, active=0, activecolor='red')
    plt.title('ANT'); _ant_rb.on_clicked(on_Antenna)

    # Axes Scaling Selection
    _axis_scaler = CheckButtons(_figure.add_axes([0.02, 0.65, 0.1, 0.05], facecolor='lightgoldenrodyellow'),
                                ('Scale',), (True,))
    _axis_scaler.on_clicked(on_Scale)

    # Sample Selection (slider)
    _smp_sld = Slider(_figure.add_axes([0.05, 0.06, 0.9, 0.03], facecolor='lightgoldenrodyellow'), label='Sample',
                      valmin=0, valmax=Num_Samples-1, valinit=_sel_smp, valfmt='%0.0fx'+str(P_DownSample*O_DownSample))
    _smp_sld.on_changed(on_Sample)

    # Sample Selection (Buttons)
    _smp_add = Button(_figure.add_axes([0.15, 0.02, 0.1, 0.03]), 'Sample (-)', color='lightgoldenrodyellow',
                      hovercolor='0.975')
    _smp_add.on_clicked(on_Sample_Sub)
    _smp_sub = Button(_figure.add_axes([0.70, 0.02, 0.1, 0.03]), 'Sample (+)', color='lightgoldenrodyellow',
                      hovercolor='0.975')
    _smp_sub.on_clicked(on_Sample_Add)

    plt.show()