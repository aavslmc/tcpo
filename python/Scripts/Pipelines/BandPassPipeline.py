import os
import logging

from pytcpo.Core.Common import TCPOCode
from pytcpo.Pipelines import SimpleAnomalyPipelineBuilder
from pytcpo.Modules.Archivers import FilePersistor, CSVSerialiser

# Change this to suit your needs... i.e. were the data is stored
src_location = '/media/michael/Elements/MichaelC/Data/SKA-Run (March 2017)/raw'
dst_root_loc = '/media/michael/Elements/MichaelC/Data/Scratch/Test/DPF_log/ORD_9'
# src_location = '/home/michael/Downloads'
# src_location = '/home/xandru/Documents/Data/TestDataFromPolyGen'
# dst_root_loc = '/home/xandru/Documents/Scratch/Test/DPF_log/ORD_0'

if __name__=='__main__':

    # Set Logging for all loggers
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # Note that a server must be running on localhost:1500

    # Create and Setup Pipeline
    pipeline = SimpleAnomalyPipelineBuilder()
    pipeline.setup(params= {'archive_coeffs': True,                     # Archive all coefficients (debug mode)
                            'source': src_location,                     # Source Location (for Data)
                            'redis_server': ('localhost', 1500),        # Redis Configuration
                            'station': 0,                               # Station Number
                            'tile_config': {0: (0,1,2,3,8,9,10,11)},    # The Tile/Antenna specification
                            'range_samples': (0,100),                   # How Many Samples to read
                            'polyn_order': 9,                           # The Polynomial Order
                            'noise_prefilter': 1.0,                     # The Gradient for detecting and ignoring spikes
                            'devs_multiple': 5.0,                       # 5 Standard Deviations for Outlier
                            'deriv_ignore': 0.5,                        # Ignore First Derivatives below 0.5
                            'morph_window': 2,                          # Morpholigical Window Size
                            'outlier_ratio': 0.10,                      # Outlier Ratio Cutoff
                            'min_overlap': 0.95,                        # Minimum Overlap to Tolerate
                            'avg_length': 5,                            # Length of Moving Average
                            'mean_length': 8,                           # Mean is over each 8-channels
                            'detrend_mean':300.0},                      # De-Trend everything to around 300
                   offline=True)

    # Build and get manager
    manager = pipeline.build()

    # Now start the reader, doing all
    result = manager.start()
    if result is not TCPOCode.Status.ACTIVE:
        print 'Error in Starting Pipeline - ' + TCPOCode._print(result)
        exit(-1)

    # Wait for completion
    assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)
