import logging
import numpy as np
from datetime import datetime
from distutils import util
from configparser import ConfigParser
import subprocess
import time
import tempfile as tmp

from pytcpo.Pipelines.ModelVisibilitiesPipeline import ModelVisPipelineBuilder
from pytcpo.Pipelines.RealVisibilitiesPipeline import RealVisPipelineBuilder
from pytcpo.Core.PipeLine.PipelineParallelisation import PipelineParallelisation
from pytcpo.Core.DataModels.TelescopeModel import TelescopeModel as TM

# Temporary json file creator
json_script = '/home/josef/Documents/tcpo/python/TestingTools/TelescopeModel_JSON_Test_Builder.py'
subprocess.call(['python', json_script])

# Define config file path
config_file = '/home/josef/Documents/tcpo/obs_config.ini'
config = ConfigParser()
config.read(config_file)
json_file = config.get('Configuration', 'json_file').encode('ascii', 'ignore')

# Create TM instance and load json
main_dir = tmp.tempdir
tm = TM.Instance()
tm.from_file(json_file)

# Setup SkyModel Param
local_sky_model = main_dir + '/local_sky_model.osm'

# Commence logging
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

# Define telescope name
telescope = config.get('Configuration', 'telescope').encode('ascii', 'ignore')

# Setup Pipelines
model_generation = bool(util.strtobool(config.get('Configuration', 'model_generation').encode('ascii', 'ignore')))

# Setup ModelVisCal
model_file = main_dir + '/model_vis.txt'

# Setup RealVisCal
real_vis_dir = config.get('Configuration', 'real_vis_dir').encode('ascii', 'ignore')
calib_coeffs_dir = real_vis_dir
real_vis_file = config.get('Configuration', 'real_vis_file').encode('ascii', 'ignore')
transit_run_file = real_vis_dir + real_vis_file
obs_file = config.get('Configuration', 'obs_file_path').encode('ascii', 'ignore')
calib_check_path = real_vis_dir + 'calib_plot.png'
model_vis_dir = main_dir + '/MSets'
model_vis_generated = main_dir + '/model_vis.txt'
coeffs_out = main_dir + '/coeffs_pointing.txt'

time_steps = int(tm.ObsLength / tm.TimeAverage)
no_of_antennas = tm.antennas.shape[0]

calibration_dec = np.float(config.get('Configuration', 'calibration_dec').encode('ascii', 'ignore'))
selfcal = bool(util.strtobool(config.get('Configuration', 'selfcal').encode('ascii', 'ignore')))
gaincal = bool(util.strtobool(config.get('Configuration', 'gaincal').encode('ascii', 'ignore')))
phasecal = bool(util.strtobool(config.get('Configuration', 'phasecal').encode('ascii', 'ignore')))
stefcal = bool(util.strtobool(config.get('Configuration', 'stefcal').encode('ascii', 'ignore')))
fescal = bool(util.strtobool(config.get('Configuration', 'fescal').encode('ascii', 'ignore')))
transit_run = bool(util.strtobool(config.get('Configuration', 'transit_run').encode('ascii', 'ignore')))
coeff_test = bool(util.strtobool(config.get('Configuration', 'coeff_test').encode('ascii', 'ignore')))
auto_corr = bool(util.strtobool(config.get('Configuration', 'auto_corr').encode('ascii', 'ignore')))

bas_ant_no = None
if auto_corr is True:
    cr = 0
    no_of_baselines = np.int(0.5 * ((no_of_antennas ** 2) + no_of_antennas))
    bas_ant_no = np.zeros((no_of_baselines, 2), dtype=np.int)
    for i in range(no_of_antennas):
        for j in range(no_of_antennas):
            if i <= j:
                bas_ant_no[cr, 0] = i
                bas_ant_no[cr, 1] = j
                cr += 1

if auto_corr is False:
    cr = 0
    no_of_baselines = np.int(0.5 * ((no_of_antennas ** 2) - no_of_antennas))
    bas_ant_no = np.zeros((no_of_baselines, 2), dtype=np.int)
    for i in range(no_of_antennas):
        for j in range(no_of_antennas):
            if i < j:
                bas_ant_no[cr, 0] = i
                bas_ant_no[cr, 1] = j
                cr += 1

# ModelVisGenPipeline process creation
model_vis_pipeline = ModelVisPipelineBuilder.ModelVisPipelineBuilder()
model_vis_pipeline.setup(params={'TM_instance': tm, 'source': main_dir, 'model_file': model_file,
                                 'local_sky_model': local_sky_model})

model_vis_manager = model_vis_pipeline.build()
model_vis_process = PipelineParallelisation(model_vis_manager)

# RealVisGenPipeline process creation
real_vis_pipeline = RealVisPipelineBuilder.RealVisPipelineBuilder()
real_vis_pipeline.setup(params={'actual_vis_directory': real_vis_dir, 'model_vis_directory': model_vis_dir,
                                'model_vis_list_path': model_vis_generated, 'total_time_samples': tm.ObsLength,
                                'selfcal': selfcal, 'stefcal': stefcal, 'fescal': fescal, 'antennas': tm.antennas,
                                'coeffs_filepath': coeffs_out, 'transit_run': transit_run, 'baseline_no': bas_ant_no,
                                'coeff_test_run': coeff_test, 'main_dir': main_dir, 'calibration_dec': calibration_dec,
                                'pointing_ra': tm.PCRA, 'pointing_dec': tm.PCDec, 'no_of_antennas': no_of_antennas,
                                'longitude': tm.cen_lon, 'latitude': tm.cen_lat, 'calib_check_path': calib_check_path,
                                'frequency': tm.StartFreq, 'bandwith': tm.Bandwith, 'transit_file': transit_run_file,
                                'gaincal': gaincal, 'phasecal': phasecal, 'model_run': model_generation,
                                'telescope': telescope, 'auto_corr': auto_corr, 'calib_coeffs_dir': calib_coeffs_dir,
                                'obs_file': obs_file, 'obs_time': tm.StartTime})

real_vis_manager = real_vis_pipeline.build()
real_vis_process = PipelineParallelisation(real_vis_manager)

# Starting and parallelising pipeline process

if __name__ == '__main__':

    if model_generation is True:
        model_vis_process.start()
        time.sleep(10)
    real_vis_process.start()
    real_vis_process.join()

    #shutil.rmtree(main_dir, ignore_errors=True)
