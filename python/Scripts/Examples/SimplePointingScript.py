import Queue as qu
from astropy.time.core import Time
import logging
import numpy as np

from pytcpo.Core.Common import TCPOCode
from pytcpo.Pipelines.PointingPipeline import PointingPipelineBuilder
from pytcpo.Core.DataModels.TelescopeModel import TelescopeModel as TM

# Required Queue onto which pointing requests are to be pushed
queue = qu.Queue()
# Pushing a TCPOUpdate containing the request details
queue.put(np.array([5, Time('2000-01-02T00:00:00.123456789', scale='utc'), '12h53m23.9s', '-4d19m12.4s']))
queue.put(np.array([1, Time('2000-01-02T00:00:00.123456789', scale='utc'), '12h53m23.9s', '-4d19m12.4s']))
queue.put(np.array([2, Time('2000-01-02T00:00:00.123456789', scale='utc'), '12h53m23.9s', '-4d19m12.4s']))
queue.put(np.array([3, Time('2000-01-02T00:00:00.123456789', scale='utc'), '12h53m23.9s', '-4d19m12.4s']))

src_loc = '/home/xandru/Documents/tm_data.json'

if __name__ == '__main__':

    # Set Logging for all loggers
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    tm = TM.Instance()
    tm.from_file(src_loc)

    pipeline = PointingPipelineBuilder.PointingPipelineBuilder()
    pipeline.setup(params={'TM_instance': tm, 'request_queue': queue})

    manager = pipeline.build()

    result = manager.start()
    if result is not TCPOCode.Status.ACTIVE:
        print 'Error in Starting Pipeline - ' + TCPOCode._print(result)
        exit(-1)

    # Wait for completion
    assert (manager.wait_until_ready())
