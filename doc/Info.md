# INFO #

This directory will provide up-to-date architecture-level documentation of the TCPO Library Modules, organised by 
package. This should be supported by the docstrings in the individual python scripts which focus on the nitty gritty of
implementation details - indeed, reference is often suggested using the (*rd*) acronym

For an introductory overview of the functionality of the library, refer to the global [README](../README.md)

## Organisation of the Library

The library is logically organised into three main packages (under `pytcpo`):

 * The **Core** package defines the abstract interfaces and base functionality of the framework,
 * The **Modules** package implements a number of concrete modules which are used to build processing pipelines,
 * The **Pipelines** package on the other hand defines some pre-structured pipelines which may be used as is.
 
Additionally, unittests are organised within the **Test** Directory (outside the *pytcpo* library), while the
**Scripts** package contains examples of python runnables.

## Contents

 1. [Core Library](Core/Core.md)
      1. [Base Functionality](Core/Common.md)
      1. [Pipeline Architecture](Core/Pipeline.md)
      1. [Archiving Functionality](Core/Archiving.md)
      
 1. [Modules (Implementations)](Modules/Modules.md)
      1. [Anomaly Detectors](Modules/AnomalyDetectors.md])
      1. [Archivers](Modules/Archivers.md)
      1. [Data Modeling](Modules/BandPassModel.md)
      1. [Data Emulation](Modules/DataEmulators.md)
      1. [Equalisation](Modules/Equalisers.md)
      1. [Input/Output](Modules/IO.md)
      1. [Other Filters](Modules/RawFilters.md)
      
 1. [Pre-Built Pipelines](Pipelines.md)
      
 1. [Testing](Testing.md)

