# Input/Output Modules

This Package deals with the input (and output) to/from the pipelines. Note that this is separate from the archiving
paradigm, and generally serves as the front-end/back-end to the pipeline (such as dealing with hdf5 files).

Readers are typically implemented as Generator Modules (as is the case with the ChannelStageReader), while writers take
the form of normal processor modules.

<[Go Back](Modules.md)>
