# The Modules Library

The Modules library encapsulates all concrete implementations of the pipeline modules, organised by their general
functionality.

This documentation will serve only to give an overview of the global functionality of each package, together with
certain key design decisions. A more detailed description is present within the docstrings for each module. This
includes specifying for each module:
  
  * Requirements : The type/format of data to expect from its parent(s). This is omitted for Generator Modules.
  * Outputs : The type/format of the output data children should expect from this module.
  * Archives : The type/format of the archive data (if any)
  
Additionally, options (parameters) are listed within the docstring for the `_setup()` method.

<[Go Back](../Info.md)>