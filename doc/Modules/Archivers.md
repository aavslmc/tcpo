# Archiving Implementations

This Package organises the concrete **ASerialiser** and **APersistor** instances (in appropriate sub-packages).

There are currently two persistors and two serialisers, which are cross-compatible with each other.

|    Persistor   |       Module      |           Description          | Random Reads | Read-Write |
|:--------------:|:-----------------:|:-------------------------------|:------------:|:----------:|
|  FilePersistor |  FilePersistor.py | Text File with line per sample and separate files per key |      No      |     No     |
| RedisPersistor | RedisPersistor.py | Redis-based dbase with samples in lists, and separate list per key |      Yes      |     Yes     |

|   Serialiser   |       Module      |           Description          | String/Binary |
|:--------------:|:-----------------:|:-------------------------------|:------------:|
|  CSVSerialiser |  CSVSerialiser.py | Space-Separated Values |      String      |
| JSONSerialiser | JSONSerialiser.py | JSON formatting, with ASerialiseables converted to JSON dicts | String |
   
<[Go Back](Modules.md)>
