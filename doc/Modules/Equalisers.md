# Equalisation

This package includes a number of modules for performing equalisation/de-trending of the channel band-pass power
spectrum.

<[Go Back](Modules.md)>