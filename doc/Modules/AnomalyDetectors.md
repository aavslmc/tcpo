# Anomaly Detections

This package encapsulates all modules which are related to detecting anomalies/outliers (currently in integrated
bandpass data).

### Types of Detectors

Within the context of the TCPO architecture, we distinguish between two high-level anomaly types:

 * **Antenna Anomaly** Detection. These detectors serve to flag any antennas (and specific polarisations) which may have
   failed and hence be giving incorrect signals. This family of detectors is further subdivided based on whether the
   algorithm operates on *cooperative* statistics (i.e. comparison with neighbouring antennas/polarisations) or on some
   characterisation (such as history) of the *independent* antenna.
   
 * **Channel Outlier** Detection. These detectors flag individual frequency channels which may be corrupted by RFI
   (Radio-Frequency Interference) or other forms of noise.

### Common Interface

In order to provide a common user-interface, all anomaly detectors typically follow the paradigm that:
  
  * The Output is typically a Boolean Matrix indicating whether the Antenna/Polarisation/Channel is anomalous.
  
  * The Archive data is a list of `FlaggedAnomaly` objects indicating which Antennas/Polarisations/Channels are 
    anomalous.
    
Additionally, they should be able to accept NaN inputs for specific Antennas/Polarisations/Channels, in which case, an
instant anomaly is typically flagged.

### The FlaggedAnomaly Wrapper

The **FlaggedAnomaly** object serves as a condensed representation of an anomaly, based on the assumption that under
normal operating conditions, it is more efficient to index anomalies rather than enumerate all with a flag (since there
should be far less anomalies then there are antennas/polarisations/channels).

The object (which is `ASerialiseable`) encapsulates 3 attributes:

 * `_ant` : Absolute index of Anomalous Antenna
 
 * `_pol` : Absolute index of Polarisation which generated the Anomaly
 
 * `_anomaly` : This is different based on whether we are flagging antenna or channel anomalies:
    * For Antenna Anomalies, this is an additional measure of the anomaly (such as a deviation from norm). This is
      specific to the module and is documented in the docstring.
    * For Channel Outliers, this is the absolute indices of outlying channels within that Antenna/Polarisation.
    
Note that in all cases, the indices are **Absolute** indices relative only to the particular tile. This means that if
say the pipeline operates only on a subset of the antennas/polarisations/channels in the tile, nonetheless, the indices
reported are (should be) absolute values.

<[Go Back](Modules.md)>