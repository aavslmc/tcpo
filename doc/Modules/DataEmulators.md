# TODO

## Implementation Details

This will provide a more detailed discussion of the abstract implementation, for the sake of future maintainers. Discussion
of concrete implementations of data generating solutions is deferred to other documents.

### Design Considerations

The Data Emulation Module (DE) is design to simulate the output from the Channelizer stage of the TPM. In this regards,
the conceptual output of the DE is a three-dimensional matrix sampling the frequency response of each channel, in each
polarisation per Antenna.

#### Requirements
* Flexible Antenna models, with the possibility of modeling noise.
* Flexibility in allowing changing model specification
* Flexibility in the output type and format

#### Architectural Decisions
Due to the above requirements, as well as the fact that the antenna models are envisioned to change (and increase in
complexity) over time, it was decided to decouple the responsibility of formatting the output from the actual generation.
To this end, the current architecture implements a two-tiered approach.

The Abstract Interface, `AbstractDataEmulator` is responsible for exposing a common public interface and above all for
formatting the output into the file-based system (currently hdf5). In so doing, it is also responsible for specification
of the telescope model (in the sense that it handles references to its specification) and the timing parameters of data
generation (samples per block and sample rate) - basically the 'engineering' decisions.

The concrete implementations then handle only the generation of the data for a given number of antennas, polarisations and
channels, for a particular sample index (potentially allowing for time-varying models). This decoupling is achieved through
the concrete model implementing the `_data(self, sample)` method which is requested by the abstract interface on a regular
basis.