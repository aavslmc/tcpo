# BandPass Modeling

This package defines a number of bandpass-characterisation tools. These generally take the form of fitting a polynomial
across the bandpass power spectrum, albeit with some exotic pre-filtering to reduce the impact of outlier channels.

Note that the modules in this package output model coefficients/parameters: additional modules may also be supplied
to evaluate the coefficients to generate the model data.

### Numpy Dependencies

Where applicable, the modules in this package use the newer `numpy.polynomial.polynomial` as opposed to the
`numpy.poly1d` package.

### Signaling Invalid Models

In certain cases, the module may be unable to generate a characterisation of the bandpass (due for example to the
presence of too many outliers, or ill-conditioned data). In this case, the output should be signalled using NaN values.
Subsequent modules must be able to handle such invalid models.

<[Go Back](Modules.md)>