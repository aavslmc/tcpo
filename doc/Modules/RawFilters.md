# Raw Filters

This package contains a number of filters which operate on typically raw antenna data. The package is further subdivided
into a number of modules containing related items:

 * **Casts**: Data Casting. This is often required due to the nature of the TPM's input which is typically integer-based
    while operations should be performed in floating-point.
    
 * **Exponentials**: Logarithms and exponents.

 * **Samplers**: For Down-Sampling. Two variants are implemented.

 * **Selectors**: Used to select a portion of the input, typically, which antennas/channels to pass through.
 

Note that the `MovingAverageModule`, in `Statistics.py` should not be used as it is obsolete

<[Go Back](Modules.md)>