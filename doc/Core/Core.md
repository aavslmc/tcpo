# The Core Library

The Core Library Package is the repository of the base components for the TCPO pipeline, and consists mostly of
abstract base classes and associated utilities. The Package itself is organised into 3 sub-packages:

 * [Common](Common.md): Includes some common definitions and utility packages
 * [Archiving](Archiving.md): Organises the abstract archiving framework used by the library to allow communication with outside
    entities and (optionally) persistence.
 * [Pipeline](Pipeline.md): Defines the Pipeline Framework specification.


<[Go Back](../Info.md)>