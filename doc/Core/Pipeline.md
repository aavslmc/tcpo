# The TCPO Pipeline Architecture

This document describes the design and implementation of the pipeline architecture within the TCPO framework. The
framework provides not only a base implementation for abstracting away the data transfer process between different
processing components but also a suite of management tools.

## Overview

The nature of the Tile Processing Module lends itself to a pipeline implementation, in that typically, data flows from a
source (the antennas) through a number of processing components. This is especially true for the calibration process
which typically needs to be staggered between channels. At the same time, the same data may need to be acted upon by
more than one operation, possibly in parallel. To this end, the pipeline implementation follows a producer/consumer
architecture, based on a number of callbacks, and this, together with a number of other common features shared by all,
are abstracted away in the `AbstractPipelineModule` base class. Additionally, specific types of modules are provided for 
data generators and processors.

The unit of transfer between each component is encapsulated in a Message, which contains all the information associated
with the sample such as index, time-stamp and 'selection' information (see below), as well as the data itself, organised
in a python dictionary for easy access. The message object provides a common interface for data access (see below).

Finally, in an effort to increase flexibility, the user only interfaces with the pipeline either through a pipeline
manager, or more abstractly through one of several pipeline builders. The former acts as a factory class for
instantiating the different types of modules, as well as handling their lifetime and general error handling. On the
other hand, the builders provide a preset structure for the pipeline, with the user able to tweak options for the
modules themselves. Builders also allow easy replication of the pipeline structure (and configuration once setup).

### General Usage

Typically, using the TCPO Library entails:

1. Implementing concrete pipeline components, derived from the `AbstractPipelineModule` base class (or rather one of its
   two direct descendants).
1. Connecting a succession of these components in any order desired:
    1. preferably using a pipeline manager
    1. manually (but this is discouraged)
1. Alternatively, using a preset pipeline builder and only specifying options for the pipeline as a whole
1. Running the pipeline.

#### Implementing Pipeline Components
Pipeline components should not inherit directly from `AbstractPipelineModule`: rather, depending on the nature of the
processing to be carried out in the module, they should inherit from either `AbstractGenerator` or `AbstractProcessor`.
The former is used when the module is actually the generator of the data (and will be the root module in a pipeline), 
while all other modules should inherit from the second variant.

In each case, implementation requires defining some key methods, which differ slightly based on the parent. 

###### Implementing a Processor Module
```python
from pytcpo.Core.Common import TCPOCode
from pytcpo.Core.PipeLine import AbstractProcessor

class IdentityFilter(AbstractProcessor):

    def __init__(self):
        super(IdentityFilter, self).__init__()
        self._samples_read = None
        
    def _setup(self, filter_model):
        self._samples_read = 0
        return TCPOCode.Status.SETUP
        
    def _will_start(self):
        return TCPOCode.Status.OK

    def _new_sample(self, message, forward):
        self._samples_read += 1
        forward[0] = message.Default
        forward[1] = self._samples_read
        return TCPOCode.Status.OK

    def _clean_up(self, reason):
        print TCPOCode._print(reason)
```
The example shows an Identity filter, in that the data is forwarded on without any changes, logging to archive the 
current sample number. At a minimum, such an object will need to implement the above four methods, and optionally a 
**default** initialiser.

The `_setup` method is called before the data generation process begins, and can be used to initialise any 
model-specific parameters. In our case, this just initialises the number of samples read to 0. In line with the error-
handling paradigm of the library ([see here](Common.md)), any failure should be signalled through an exception. The
method may optionally return a status indicator.

Before data starts flowing, the object will also be informed using `_will_start`. Again, this should raise an exception
if due to an error it is unable to start processing data: this will automatically halt the entire process.

Data is delivered to the object through the `_new_sample` callback. This method is given the data matrix, and should 
return the data to pass on to the next item in turn, as well as any data to be archived, through the second `forward`
parameter. This is a list, with the data to pipeline and the data to archive respectively. In our case, we simply pass 
on the data (default member, see below) as is downstream, together with the number of samples read to the archiver.

If the pipeline is stopped for any reason (including an error), the module is informed through the `__clean_up`
callback. This will include the reason for stopping, enumerated in the `TCPOCode` class.


###### Implementing a Generator
The following is a simple random data generator:
```python
import time as tm
import numpy as np

from pytcpo.Core.Common import TCPOCode
from pytcpo.Core.PipeLine import AbstractGenerator, Message

class RandomGenerator(AbstractGenerator):
    
    def __init__(self):
        super(RandomGenerator, self).__init__()
        self.Size = None
        self.__index = None
        
    def _setup(self, filter_model):
        self.Size = filter_model['size']
        self.__index = 0
        return TCPOCode.Status.SETUP
            
    def _generator_loop(self):
        while not self._UserStop.wait(1.0):
            msg = Message((Message.SelType.NONE, 0), self.__index, tm.time(), self.Name)
            msg.Default = np.random.uniform(0, 1, self.Size)
            self.__index += 1
            self._push(msg, None)
```
Unlike with processor modules, generators require only the implementation of two methods. The first is the `_setup`
which serves the same function and follows the same semantics as for the equivalent processor method. In this case,
using the preferred method of passing parameters (i.e. through a dictionary), the size of the data to be generated is
specified.

The other method is the `_generator_loop`, which as the name implies is the function which consumes the generator thread
(i.e. it is run in its own thread). Signaling that it needs to terminate is indicated by the `_UserStop` event (refer to
the threading module documentation). In our case, this is generating a new message every second. Note how the indexing
is incremented and the message is named appropriately (to allow the Default member to be accessible, see below).


###### Supporting Dynamic Instantiation
In order to allow for the user to automatically instantiate modules by name, the implementation(s) must be registered
with the pipeline manager. This must be done from the outermost file which will feature in the import of the class. This
is typically the package `__init__.py` file for the package. All that is needed is as follows (assuming our generator/
processor are both stored in `dummy.py`, within package `Dummy`):

```python
from dummy.py import IdentityFilter, RandomGenerator

from pytcpo.Core.PipeLine import PipelineManager
PipelineManager.registerModule(IdentityFilter)
PipelineManager.registerModule(RandomGenerator)
```

Additionally, the package itself must be imported from the script using the modules.

#### Constructing a Pipeline

Construction of the pipeline can be in one of two ways: the recomended method uses the PipelineManager to dynamically
instantiate and manage the individual modules: it also handles certain connective structures behind the scenes. There is
also the option of manually instantiating and setting up the modules. However, this is discouraged except for debugging.

This example will utilise the manager. Assuming the above classes have been declared and registered, we will construct a
simple pipeline consisting of a tree structure as shown below:

![alt_text](../resources/Pipeline_Sample.jpg "Sample Pipeline Architecture")

Note that `Front` will be a *RandomGenerator* and the rest *IdentityFilter* instances. We first instantiate the Manager:
```python
manager = PipelineManager()
```

In order to use the archiving functionality (see [Archiver](Archiving.md)), we also need to set it up within the manager
(which conveniently takes care of everything) before defining any module which requires it:
```python
manager.set_archiver(persistor={'type': 'FilePersistor', 'setup': {'keys': {'Int1':int, 'Int2':int}}, 'append': False},
                     serialiser={'type': 'CSVSerialiser', 'setup': None})
```

In this case we have used a FilePersistor/CSVSerialiser combo: in the first case, we need to specify the type, any setup
parameters (in this case the list of keys/types, refer to appropriate docstring), and whether we wish to append or erase
existing data. Similarly, for the serialiser, we specify the type and any associated setup.

From then on, the process is simply that of adding elements, starting with a root node:
```python
manager.add_root('RandomGenerator', {'size': [5,5]})                               # Front
front = manager.Last # Keep Reference

manager.add_module('IdentityFilter', None, archive_key = 'Int1')                   # Int_1
manager.add_module('IdentityFilter', None)                                         # Back
manager.add_module('IdentityFilter', None, parents=front, archive_key = 'Int2')    # Int_2
```

Adding modules involves specifying the type and the associated setup (in our cases None, refer to each module's
docstring), and for non-root modules the parents: by default, the last added module will be set as the parent for the
new node. Additionaly, we may specify an archive key to which the module will push data. In the case where multiple
nodes must be specified as children of the same node, you need to maintain a reference to the specific parent, as we
have done with the front (generator) module. 

The last operation is to call build. That is all that is needed: there is no need to keep track of names, explicitly
register the connections or call setup individually - this is all handled by the manager. If anything fails down the
line an exception is raised and can be handled appropriately.
```python
manager.build()
```

An ever easier option for pipeline construction is through Pipeline Builders. This option skips the need to setup the
pipeline structure, as this is handled by the specific builder itself, requiring only the options to be specified. For
example, assume we have a `DummyPipelineBuilder` which generates the above structure. Then all we would need to do is as
follows:
```python
builder = DummyPipelineBuilder()

builder.setup(params={}, offline=True)

manager = builder.build()
```
The process involves only instantiating the builder, setting it up (in our case, there are no options to specify) and
building it, which returns a pipeline manager.

Irrespective of how the pipeline is created, running and managing it remains the realm of the pipeline manager. To start
the Pipeline:
```python
assert (manager.start() == TCPOCode.Status.ACTIVE)
assert (manager.wait_until_ready() == TCPOCode.Status.FILE_END)
```
The first command starts the actual pipeline (including handling cascading of the start command through the pipeline,
starting at its leaves). This returns immediately, since the generator operates on a different thread: hence, a
convenience method is provided to wait until the generator stops: alternatively, the user him/herself may request stop
via the `stop` method.


## Implementation Details

The motivation to go for a pipeline architecture is spurred by the nature of processing envisioned within the Tile
Processing Module. Specifically:

1. The TPM can be envisioned as a set of sequential modules connected end to end.

1. Data flows from modules in one direction only (there are no directed cycles).

1. Arrival of data is in general spurious, but can be acted upon immediately with no explicit synchronisation (i.e.
   synchronisation happens automatically by the data flowing).
   
1. Each component may send data to multiple processing components down-stream. Conversely the same component may require
   data from multiple components.

These considerations lend naturally to a pipeline implementation. This is especially the case with the calibration
process which is envisioned to be staggared by channel. The other alternative would have been to implement a 
parallelised system (i.e. just operate on the data sequentially, and fork multiple threads), but a pipeline architecture
also aids in modular design of the code, which would allow a plug and play approach to inserting/removing components and
in general constructing the processing.

During the course of this discussion, reference will often be made to 2 potential users of the library. *Module
designers* or *programmers* refers to the act of inheriting from the abstract classes to extend concrete implementations
of specific algorithms: *pipeline users* refers to using pre-existing modules and packaging them in a script for running
a specific task.

### Design Requirements

The core idea of the pipeline is to abstract away core functionality and allow concrete implementations to only deal
with the data processing aspect. To this end, we seek the following requirements:

1. Allow the potential for multi-threading: i.e. a true pipeline, whereby each component does not block the modules
   upstream. This has not been currently implemented but the design supports such an update.

1. Handle the archiving of data from each component of the pipeline.

1. Handle registration of (multiple) consumers to the component (fan-out) and conversely a consumer subscribing to
   multiple components (fan-in)

1. Abstract away error handling and thread-safety

1. All modules should be structure-agnostic, in that they should not need to care which elements are ahead of them or
   after them in the pipeline, as long as the data is of the correct format. This also means that naming should be
   independent of the structure.


### Message Passing

For the sake of efficiency and abstraction, it was decided to encapsulate the data in its own **Message** object. The
class not only encapsulates selector, index and time-stamp information, but is also instrumental in situations of fan-in 
identification.

##### Meta-Data

Since the data will be acted upon by multiple components (modules), a form of indexing is required. This is achieved
through an index field, as well as a time-stamp field. Generation of these values lies with the Data Generator module
(see below), and it must ensure that:

 1. Indices never repeat, and are always ascending at regular intervals.
 1. Timestamps are always ascending
 
Other than that, there is no limitation into how they are generated, and no direct interdependency between the two.

##### Message Selection

An important field in the Message meta-data is the `Selector` attribute. This is used to select certain messages from
others. For example, for the Band-Pass Characterisation & Anomaly Detection pipeline, the pipeline operates on a
per-tile bases: hence the 'Selector' field may be used to specify the tile. The issue arises in that not all pipelines
follow this scheme: the Beam-pointing script conversely deals with beams and has no notion of tiles.
 
In order to reduce confusion in this regards, the Selection mechanism is actually based on two attributes: the
aforementioned `Selector` and a `SelectorMask`. The latter is an enumeration (actually, due to limitations of Python
2.7, it is a set of integer constants defined within a nested class), which lists the type of selection this message
supports. The BaseSelector filter (an implemented module), can then be used to divert messages to the specific branch.
In particular, this selector enforces that the message selection mechanism is of the correct type. **N.B.** it is up to
the generator to populate both fields with the correct value.

The current selection only allows selection on one type. However, the enumerations are bitwise exclusive, meaning that
in a future refactoring, such capability may be supported. In effect, this is more the realm of the generators and 
selector-modules themselves rather than the Message instance.

  
##### Data Identification

The main problem with a unified message object for the pipeline is the storage of the data item. This is not an issue
when one subscriber gets its data from one component upstream: neither is it with a fan-out process. However, in the
case of a fan-in, when data from multiple sources must be consolidated into one message, a means is needed to identify
the different components. However, in the interest of structure-agnosticity, (which is needed to  support the plug'n
play approach), this needs to be as independent of the pipeline itself as possible.

The solution to this lies in the naming scheme employed. Basically, each pipeline module is given a unique name by the 
pipeline designer - this is guaranteed when using the Pipeline Manager (stressing the benefit of its use). This name is
used to tag the data item in the message originating from that module (which data items are organised in a dictionary). 
This allows each message to potentially contain data from multiple sources: and by using a dictionary, this allows it to
be flexibly defined at initialisation time rather than hardcoded in the implementation.

While this solves one problem (identifying different sources), it introduces a dependency by the subscriber on knowing
what the pipeline component upfront is called. This cannot be avoided entirely when a module requires data from multiple
sources: in this case the setup stage must accept a name-map to the data items (which in any case decouples the
dependency during module design). However, it can be circumvented when only one data item is required. To this end, each
message has a single **Default** data component, whose name is specified at message-initialisation stage. Hence, for
single-input subscriptions, the down-stream module need not know anything about the structure of the pipeline upstream
to utilise the data.

##### Consistency

An issue arises in passing data from one module to another. On the one hand, for absolute thread-safety and security, it
would be preferable to copy the data to prevent accidental modification: however, this will incur a lot of overhead
given the voluminous data being handled. The design process employed puts the responsibility on the module itself to
never modify the data it receives in place: if it needs to work directly on the input, it should do a copy of it.

In order to help with this, and also with the renaming of default data items, the message object implements three levels
of copying:

  1. ***Partial*** Copy : This is intended to pass on the message with new data to the next component in line: only the
                          meta-information of the message is copied (tile, index and timestamp).
  1. ***Shallow*** Copy : This creates a ***new*** message (with the same management information apart from the default 
                          name) but the data itself is only shallow copied, albeit in a new dictionary.
  1. ***Deep*** Copy : This performs a full deep copy of the message, including the data.


### Pipelining

The single-unit of processing within the pipeline is the module which operates exclusively on one sample at a time.
There are two main types of user-accessible abstract base classes for defining operations, Generator and Processor
modules. Each of these will be discussed in turn. However, in effect, (and hidden to the module developer), both these
abstract classes inherit from the `AbstractPipelineModule`, which implements shared functionality. In particular, the
abstract base class:

 1. Keeps track of the Module Name
 1. Ensures thread-safety (responsibility shared with direct descendants)
 1. Handles the archiving of data and access to the archiver.
 1. Handles registration and management of consumers (including forwarding of data)
 1. Handles communication with the Pipeline Manager through thread-queues (see below), including logging all
    communication.
    
##### Generator Modules

Generator Modules inherit from `AbstractGenerator`, and serve as the front end (roots) of pipelines. They operate
independently of other modules, pushing data down the line as soon as it is generated. The generation process occurs
within a `_generator_loop` method which is run automatically on a separate thread. Additionally, it also allows a static
start, in which case, although still running on its own thread, the main thread blocks until the generation of data has
finished on its own. 


##### Processor Modules

Processor Modules inherit from `AbstractProcessor` and operate in an event-driven fashion. Data from module(s) upstream
is ingested, processed and the result forwarded to other modules down-stream. This is achieved through a user-overridden
callback which is called whenever a new sample arrives, and which should relay back the data to forward downstream (as
well as any archiving).


##### Module Life-Cycle

The typical lifetime of a pipeline module passes through a setup-phase, a data-flow phase and a tear-down, as
illustrated by the following figure:

![alt_text](../resources/Pipeline_Module_Life-Cycle.png "Typical Pipeline Life-Cycle")

In particular note that:

 1. A run-loop (data-flow phase) may be executed more than once for the same call to `setup()`: any state-initialisation
    must therefore happen in the start stage.
 1. Setup has no associated tear-down method, meaning that again, any resource allocation should happen within the start
    stage.

##### Call Order

The usage of the modules **through the manager** (below) ensures that the internal methods are always called in the
order defined below. 

For processor modules, developers inherit appropriate `_will_start()`, `_new_sample()` and
`_clean_up()` methods which correspond to the two stages of the data-flow phase and the tear-down phase. Calls are
guaranteed to happen in the following order:

 1. **_setup** - First called - will only be ever called once.
 1. **_will_start** - Called only if `_setup` completed successfully.
 1. **_new_sample** - Called only if `_will_start` completed successfully and `_clean_up` has not been called and the
        previous `_new_sample` did not fail.
 1. **_clean_up** - Can be called anytime after `_will_start`: however, guaranteed to be called only once per
        corresponding call to `_will_start` (including if `_will_start` is unsuccessful).


In the case of Generator Modules, the 3 'blue' sub-stages must all happen within the `_generator_loop`: i.e. it is the
responsibility of the generator designer to follow a proper starting, data generation and cleanup stage. However, it is
also guaranteed that:

 1. **_setup** - First called - will only be ever called once.
 1. **_generator_loop** - This will only be called if `_setup` was successful.

##### Cleanup

In the case of generators, there is no termination callback: rather, the `_generator_loop` should repetitively check the 
*_UserStopped* event, and perform appropriate clean-up before returning. For Processor modules, this is automatically
handled via the `_clean_up` callback.

##### Pipeline Structures

Apart from the typicaly single-input single-output communication, the pipeline modules support arbitrary fan-in/fan-out
configurations.

Fan-out is easily handled by the subscription process: each module keeps a list of interested subscribers and forwards
the result of its computations to each of them in turn. As already stated, to prevent accidental modification of the
data between siblings, it is paramount that any module operating in-place on the data, make a copy of it.

A Fan-in process is slightly more challenging. The source-identification issue has already been discussed (above), but
there remains the problem of consolidating the recieved messages and synchronising data (since the legs of the pipeline
may be of different lengths and, if parallelised, may take different durations to spit out the data). To this end, an
Aggregator Module acts as the front-end for fan-in pipeline structures. It serves a dual purpose, that of consolidating
messages from multiple sources into a singular message and as a buffering point, using the sample index to synchronise
the data from the same sample.

##### Thread-Safety

Thread-Safety is of paramount importance for the scenario in which the pipeline operates. In this case, thread-safety
is shared between the `AbstractPipelineModule` and its direct descendants (`AbstractGenerator` and `AbstractProcessor`)
who have access to the single mutex object.

All public methods are locked: protected and private methods are not locked, since they are designed to be used from
within the overrides (which are themselves called from within the public API). In particular, note that the
`_generator_loop` within Generator Modules is not locked in any way (although it is guaranteed that a new loop will not
be started while one is running, since `start()` is locked).
 
##### Consumer Chaining

Pipeline Connections follows a callback mechanism: i.e. each pipeline module exposes a registration capability as well
as the sample-callback (for processor modules). In addition, commands and/or status updates trickle up and down the
pipeline through the Pipeline Manager (below). In effect, it is guaranteed that:

 1. No parent is started before all of its children have been started.
 1. No child is stopped before all of its parents have been stopped.
 1. An Error in a module stops the entire pipeline

##### Error Handling

In line with the error-handling paradigm of the entire library, all error codes are signaled through exceptions. This
is especially the case during the initialisation, setup and start phases. An exception should be caught and handled
only at that point where doing so can solve the issue, which is typically at the top-level script (where the user
interacts).

However, due to the multi-threading (and potentially future multi-processing) scenario of the architecture, exceptions
during runtime will violate the guarantee that the pipeline remains in a defined state. This is because an exception
in a particular thread cannot be caught by the main thread and handled to clean up the remaining modules. To this end,
during the data-flow phase, all exceptions must be caught and relayed back to the Pipeline Manager via the management
queue which is provided (see below). The Processor Module's new-sample handler automatically traps and forwards all
exceptions in this manner, meaning the module designer may throw exceptions as deemed fit. However, the Generator
Module's `_generator_loop()` method itself is not wrapped in any helper method: hence the programmer **must** trap all
exceptions. A convenience function is however provided to `_inform()` the manager of an error.

Apart from error handling, the management queue may also be used for status updates where necessary. The `_inform()`
helper should be used in this regards (for all types of modules), since it abstracts away the communication and also
logs all updates/errors including the line of code which generated them.

Additionally, for Processor Modules, the cleanup method must be guaranteed not to throw exceptions.


##### Parallelisation Design

The question of performance is a tough nut to crack. The current implementation's callback nature implies that the 
pipeline structure is traversed (by the data) in a depth-first manner, and all in a sequential nature. This obviously
defeats the staggering functionality of a pipeline, and as is, provides no performance advantage (it does however
achieve modularity, which was also a major goal in the pipeline design).

It is envisioned however, that the pipeline architecture will be extended such that each module operates in its own
thread (or process). In this case, the design will continue to guarantee that a call to the `_new_sample()` callback
will only be issued after the previous one has returned. This ensures that the designer of the concrete module need not
explicitly handle thread-safety and race conditions, beyond ensuring that the data processing is self-contained within
the `_new_sample()` callback itself and that any forwarded data (for modules down the line and archiving) is stateless.

If further parallelism is desired, it can always be achieved via the selection mechanism, where each subset of modules
operates on different parts of the data (say replicating the pipeline in different processes per tile).


### Pipeline Manager

Due to the callback nature and threading scenario, lifetime management of the modules needs to be handled externally.
To ensure consistent and controlled access, as well as abstracting certain tedious functionality via helpers, a
Pipeline Manager is provided. The Pipeline Manager is the preferred way of interacting with the pipeline since it
ensures proper structure generation, module setup/teardown, and clean error handling.

##### Instantiation

The Pipeline Manager acts as a factory class for the different module types. This allows the modules to be referenced
only by their type as a string, and potentially allow instantiation from some text-file in the future. As in the case
of archiving, all modules must be appropriately registered with the pipeline manager in the top-level `__init__.py`.

##### Structure Definition

The Manager provides the methods to structure the pipeline through the addition of modules to the specified parent(s).
This ensures a consistent structure definition, through specification of a root module (currently only one root is
supported), handling consumer registration as necessary and inserting Aggregator Modules automatically where more than
one parent is specified.

In addition, it guarantees that if an error occurs during the setup of a module, the state of the pipeline is
consistent with how it was before the module creation was attempted: i.e. the already specified modules and associated
structure is not affected in any way.

##### Naming and Referencing

The Manager deals with automatic naming of the modules for the sake of structure referencing and message attribution.
It guarantees that all modules have unique names, currently the module type and a numerically increasing index.

From the user's end, there is little need to access the modules themselves, since even when adding modules, the new
processor is added to the last added module by default. However, in cases where some other structure is desired, or
when a module has multiple parents, the user can gain access to a module's name (reference) in one of two ways:

 1. The `add_module()` (and `add_root()`) method automatically returns the reference (name) of the module which can
    be retained for later use.
 1. The `Manager.Last` property gives access to the last added leaf node.

The naming scheme (rather than returning access to the module itself) also achieves a level of security and thread-
safety, since the user can never access the modules directly.

##### Synchronisation

The Manager ensures state synchronisation (that operations are called in order) as well as status updates and
lifetime management of each module. This includes starting of all modules in synchronisation and monitoring sent
messages on its management queue (which is accessible to all modules). When a module requests termination
(explicitly or implicitly due to an error), or if the user terminates, it takes care of stopping each and every
module (in the order specified previously) ensuring appropriate cleanup.

A note regarding the building process is in order. The current build method is a dummy, since the pipeline is
actually constructed (and the modules setup) as it is being defined. However, the build method acts as a break
point beyond which no further structure modification is performed. It also supports the possibility that in the
future, structure specification is separate from module setup. The Manager thus follows the same life-cycle
paradigm as the modules, meaning that once built, the pipeline may be run multiple times (each time invoking
start).

Running of the pipeline is executed on the main thread (since generator modules are expected to implement their
own processing thread), but pipeline management is spawned on a separate thread. The Manager allows the user to
start/stop the pipeline at will and optionally block until the pipeline has terminated. It also deals with
notifying the user (through a callback) when the pipeline terminates.

##### Archiving

The manager also maintains the archiving system. The current scheme calls for a single archiver to be used
throughout the pipeline, with different modules distinguishing themselves by writing to different keys (refer to
the [Archiving](Archiving.md) documentation). This is specified typically at the start of the structuring
process, but definitely before any module using archiving is defined.

The Manager allows full flexibility in archiving, based on the hierarchical separation between responsibilities
of the serialiser and persistor. Individual modules themselves may choose whether or not to archive data by
specifying the archive-key when archiving is desired.


### Pipeline Builders

The Pipeline Manager provides a complete and safe interface for dealing with pipelines. However, in certain cases
it is necessary to package a pipeline of a certain structure (for example for beam-pointing), allowing the user
to only specify options to tune parameters.

This is the realm of the pipeline builders, which as the name implies, serve to structure pipelines. Builders
themselves do not replace the role of the manager: pipeline management and execution remains the responsibility of
the Manager, however, they allow a structure to be 'hard-coded' and easy replication of both the structure and
specific setups.

##### Option Specification

While the specific pipeline builder inherently defines the pipeline structure, it should allow the user to fine-tune the
parameters for the individual modules. To support this, an option-specification paradigm similar to the argparse module
is implemented.

Specifically, the builder implementer must specify the options that the builder itself will expose to users. This is
performed within the `_specification()` abstract method. The programmer adds options using the `_add_option()` helper,
specifying the name, description and whether it is required or not (with optional default value and possible accepted
types for type checking). The helper adds the options as member attributes of the specific builder (which is why the 
`_specification` is an object method).

`_specification()` is called from within the class initialiser, meaning that all options are available for setting
immediately after object instantiation. The __user__ of the specific builder then passes a dictionary of key-value pairs
to the public `setup()` method, which amongst others ensures that all required parameters are provided, and if specified
by the programmer, that they are of the correct type.

##### Pipeline Structuring

The structure of the pipeline is defined in the abstract `_structure()` method. This method is passed a new `manager`
instance, which the __programmer__ must build the specified pipeline on. At this point, the programmer has access to all
user-specified options (as member attributes, accessed using `self`).

The `_structure()` method is itself called from within the public `build()` function. This method allows the user (after
having setup the builder) to create a new pipeline using the specified setup. The result is a structured and built
pipeline manager which can be used in the normal way (see above).

The `build()` method also allows the Pipeline Builder to act as a quasi-static definition of a pipeline with a specific
setup configuration, which can be replicated: each call to the method creates and returns a new (independent) manager
instance.

##### Error Handling

Since the Pipeline Builder is invoked only in the states leading up to building, the normal error-handling paradigm
(that is through exceptions) can be easily followed. This is especially the case since the `_specification()` of options
actually occurs within the initialiser.


## TODO
* Support for Multi-Processing.

## Issues
* The Builder option specification does not support specifying the type of possible none-type arguments

<[Go Back](Core.md)>