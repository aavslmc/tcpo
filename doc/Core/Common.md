# Common Functionality

This package is actually a conglomeration of some base functionality as well as utilities which are useful library wide
but do not fit directly under a specific heading. Apart from the Error Handling Module, the modules contained herein are
not tied to the pipeline architecture.


## Error / Status Handling

In line with proper pythonic programming, errors originating within modules are signalled through exceptions. This
ensures that any errors will immediately be signaled and will not escape untreated. The only exception to this is while
the pipeline is running, which due to the multi-threading scenario requires messages published to python Queues (more details within the 
[Pipeline Manager](Pipeline.md) documentation) to allow for a cleaner shutdown when the error happens.


#### TCPOCode

For the sake of consolidating all status messaging, it was decided to wrap all error/ warning/ request/ status codes
into one class, the TCPOCode. This emulates the missing 'enum' functionality in Python 2.7 by assigning class-attributes
to unique numbers - indeed, TCPOCode is an entirely 'static' class in C++ nomenclature.

The different 'families' of codes are themselves packaged in 4 separate nested-classes, Error, Warning, Status and
Request, occupying different ranges of values (< -100, -99 - -1, 1 - 100 and > 100 respectively). This allows for quick
checking of the type (handled through the class method `_type(cls, int)`).

The current code list should be quite verbose enough for most major cases, but can be expanded upon if need be.


#### TCPOException

Errors are signalled through the TCPOException class. The error itself is stored as an integer code: the exception also
allows for an optional string message to further specify the error. The exception, at the time of instantiation, also
stores the module of the callee, the function name and the line in the file where it was thrown. These may be accessed
by the exception handler through appropriately named attributes.


#### TCPOUpdate

This is a wrapper class for sending messages between threads while the TCPO pipelines are running (where exceptions
cannot be thrown while ensuring a clean stop) or to signal non-critical states (where an exception is inapropriate). The
object encapsulates the code value (integer), the identifier of the module which generated it (refer to the Pipeline
architecture, usually the name) together with additional information, which is typically a string message.


## npalt

As the name implies, this serves as an alternative (or rather extension) to Numpy. All the below methods are implemented
as module functions (classless)

  * **exp10** : Exponentiation function to powers of 10.
  
  * **get_nearest_idx** : Method for retrieving the index of the value in the numpy array which most closely matches the
    desired value.
    
  * **array_nan_equal** : Method for comparing numpy arrays which might contain NaN values (and require them to equate)
  
  * **rms** : Computes the Root-Mean-Square over an array.


## Data Filters

This module contains a number of objects for performing signal processing operations.

#### Moving Average Filter
The `MovingAverage` implements a variable-sized moving average operating on numpy arrays. 

#### Connectedness Filter
This is a morphological operator which operates on boolean numpy arrays. The `ConnectorFiller` is used to join together
true instances with false-gaps in between if the distance between the true-values is less than a length threshold.

#### Polynomial Comparison
The class provides a means of comparing polynomials using the 'overlap'-area between them as a measure of similarity.
The class is able to recursively compute the overlap between arbitrary pairs of polynomials in a list, or between just
two polynomials, yielding a ratio between -1.0 (opposite functions) and 1.0 (perfect equality)


## Utilities

`Utilities.py` contains some more general tools.

  * **string** : This is a subclass of the `str` base-type which implements functionality for truncating a string up to
    a certain length, as well as an acronym generator (which returns the upper-case letters in a string).
  
  * **tupler** : Converts scalar values (i.e. not of type list/tuple/np.ndarray) to a tuple, retaining None values as
    None.
    
  * **namer** : Returns the name of the object type (including None types)
  
  * **moduler** : Parses the name of the module, returning only the last part.
  
  * **directory** : Method for automatically checking if a directory exists and creating it if not.


## Graphing

Implements a horizontal radio button feature, based on matplotlib.

<[Go Back](Core.md)>