# Data Archiver #

This package defines the project-wide solution to archiving of diagnostic and summary data for the TCPO Library. To this
end, it is actually independent of a specific module and seeks to support the demands of the project as a whole.

## Overview ##

The Archiving process is mainly designed to allow sharing of pipeline data with external software packages and/or 
persistence for offline processing. In order to maintain flexibility in what can be stored/shared and how this is done,
archiving is divided into a three-step process:

 1. The object to be archived specifies what components should be serialised
 
 1. A Serialiser deals with representing hierarchical objects into a flattened string (text or binary)
 
 1. The Persistor specifies where the data is shared/transmitted

Each of these responsibilities are clearly defined in the `ASerialiseable`, `ASerialiser` and `APersistor` interfaces
respectively. A number of Serialiser and Persistor implementations already exist and may be used. In effect, the second
step above is generally transparent to the user, who deals only with implementing ASerialiseable for objects which s/he
needs to archive and calling on the appropriate persistor/serialiser combination to store/share them.


#### Implementing ASerialiseable

The first step in supporting archiving is to have the desired data wrapped inside an object which inherits from
`ASerialiseable` and implements the abstract method `serial(self)`. This method takes no argument and should return a
dictionary mapping attribute names (python `str` ) to attirbute type (python `type`). A simple implementation for a
class with two attributes follows:

```python
from pytcpo.Core.Archiving import ASerialiseable
from pytcpo.Core.Archiving import ASerialiser

class SimpleSerialiseable(ASerialiseable):
    def __init__(self):
        self._x = 0.0
        self._y = None
        
    def serial(self):
        return {'_x':float, '_y':float}
        
ASerialiser.registerSerialiseable(SimpleSerialiseable)
```
The `serial` function will specify which object-attributes will be serialised and in what manner (type). Note that the
attribute in question need not be initialised to the correct type in the constructor (`_y` is None), as long as the
correct type is specified. Note that the supported attributes can be of the following types only:

* Python core data types (`int`, `float`, `bool` or `str`) as well as numpy scalars,
* Other `ASerialiseable` objects,
* Numpy `ndarrays` of either the above core data types or `ASerialiseable` objects.

This is all the implementation required. However, in order to allow the archiver to deserialise instances of the object,
it must be registered with it through the `registerSerialiseable` class method, thereby acting as a factory class for it.
 
#### Using the Archiver
 
The rest of the process is the realm of the archiver, which is a Persistor/Serialiser combination. For the sake of this
example, the `FilePersistor`, together with the 'CSVSerialiser' will be used. This stores objects in CSV-style text
files. The first thing to do is to instantiate and initialise the persistor.

```python
from pytcpo.Modules.Archivers import FilePersistor
from pytcpo.Modules.Archivers import CSVSerialiser

archiver = FilePersistor()
assert (archiver.setup(mode=FilePersistor.M_RDWRT, p_setup={'source':'.', 'keys': {'value':SimpleSerialiseable}},
                       serialiser='CSVSerialiser', s_setup=None))

```
The `setup` method takes 4 arguments: the archive open mode (*rd*), the setup parameters for the persistor (typically a
dict, (*rd*)), the (string) type of serialiser to associate with and the serialiser-specific setup. In our case, the
FilePersistor requires the source directory and a dictionary of keys mapping to data types to store.

With the archiver setup, one can read or write samples as required (depending on the mode the archiver was initialised
in). Writing a sample is as simple as passing in the sample object to write:
```python
# Instantiate a SimpleSerialiseable object (and initialise)
ss = SimpleSerialiseable()
ss._x = 1.0
ss._y = 2.0

# Write the sample with time-stamp
archiver.WriteSample('value', 100.0, ss)
```
`WriteSample` takes, in addition to the sample object, the key to write to, which allows writing to multiple channels
(in this case multiple files) and the time-stamp to set. Reading is generally even simpler:

```python
tm, ss = archiver.ReadNext('value')
```
Again the function uses the key parameter to specify from which channel to read. This generally also dictates the type
of the sample to retrieve, since this was part of the persistor setup. The function returns a tuple containing the time
associated with the sample, as well as the sample itself.

The `ReadNext()` method recovers the next sample in turn: some archiver implementations also support reading arbitrary
samples using an index (the `FilePersistor` does not and will throw an exception if the following is attempted):

```python
ts, ss = archiver.ReadSample('value', 5)
```

The same return scheme is followed (time-stamp, sample).

Once the required operations have been completed, it is important to close the archiver (and release system resources):

```python
archiver.Close()
```

## Implementation Details ##

This will provide a more detailed discussion of the abstract implementation, for the sake of future maintainers. The API
itself, as well as a discussion of the concrete implementations is deferred to the appropriate docstrings.

The Data Archiver is a complete (and independent) sub-system within the TCPO library. It serves the need of he project
in persisting both sample statistics and diagnostic data, as well as a means of sharing data with external software
modules. Hence, an analysis of the design considerations needs to start with the requirements of the system.

### Design Requirements ###
The use of the Archiver is envisioned to happen in the following conditions:
 * Archiving of Diagnostics/Sample Summary along the pipeline
 * Transmiting of intermediary outputs along the pipeline to external components
 * Data from the same module is always of a fixed type
 * TimeStamping of all data
 * Generally serial access to the data

Given the above usage, we can identify the following requirements:

1. Flexibility in the type of data to be stored: i.e. flexibility in defining what a single sample represents.

1. Flexibility in how data is represented when serialised (at the very least, string vs binary)

1. Flexibility in the format of storage to use: i.e. flexibility in the archiving scheme, including whether it is to
   secondary storage or memory.
   
1. Modularity: i.e. the ability to change schemes down the road, especially since the needs of the project are
   continuously changing.
   
1. Efficiency: although not paramount, the volume of data does warrant some consideration of efficiency.


### Architectural Decisions ###

Given the above specifications and requirements, it was decided to employ a separation between the **what**, **how** and
**where** components of archiving: i.e.:
  * The ASerialiseable interface defines **what** is to be stored
  
  * The ASerialiser implementation controls **how** data is to be stored/represented in a linear (serialised) fashion.
  
  * Finally, the APersistor controls **where** the serialised data is archived/transmitted to.
  
This ensures that each responsibility is orthogonal to the others: of crucial importance is the separation between
defining what nees to be serialised with how and where it is to be archived, as this allows changing the data/schemas as
needed.

#### Sample Definition - ASerialiseable Interface

The first task is definition of which parts of the sample object need to be stored. This is trivial for the builtin
types (int, float, bool or str), but is a bit more involved for composite types, most notably user-defined objects. The
solution lies in the object being itself responsible for determining which attributes make it up.

The ASerialiseable Abstract Base Class acts as the interface which all objects that may be archived need to implement.
Concretely, this involves defining a `serial()` method which returns a python dictionary, consisting of the names of the
relevant attributes (as strings) together with their python type. The reason for also specifying the type is that the
object may not have initialised the attributes to the correct type (some may initially be defined None within the 
initialiser), and hence their type cannot be safely determined.

It was decided to make the specification explicit, rather than enumerating all non-callable attributes using python's
`__dict__` member of each class, in the interest of consistency and flexibility. Due to the need for dynamic
instantiation, the derived class must implement the Default Initialiser (i.e. it must need no parameters during
construction)

#### Serialisation - ASerialiser Interface

The next step in the hierarchy is the task flattening of composite objects and representing them in some serial form
(typically a string). This implies having access to the `ASerialiseable`'s dictionary and the ability to parse it to
query the specified attributes for writing or setting them during reading.

This is the realm of the `ASerialiser` interface. The interface itself implements little functionality beyond a factory
pattern for `ASerialiseable`'s (see below) and exposing a common interface. The concrete implementation is itself
responsible for parsing the `ASerialiseable` dictionary and flattening it out or building it. This is achieved via two
virtual methods, `serialise()` and `deserialise()`.

In theory any imaginable structure could be built using combinations of the core types, and encapsulated into 
`ASerialiseable`'s which can be further chained so supporting these should in theory be enough. However, given the
demands of the project which deals with matrices on a regular basis, it was decided that persistors should explicitly
support numpy arrays. This is no small task, as it involves dealing with N-Dimensional arrays, with the possibility of scalar or even empty
arrays. In summary, each `ASerialiser` implementation must be able to deal with:

 * All primitive data types (int, float, complex, bool, str),
 * All numpy numeric types (as subclassed by np.number)
 * ASerialiseable instances
 * N-dimensional Numpy Arrays (including scalars/empty arrays) of the above types.

Note that lists, tuples and dictionaries themselves are not part of the current requirement: the former two can be
generally emulated using numpy, while dictionaries can be to some extent replaced by cascading `ASerialiseable`
attributes.

To support Numpy Arrays of arbitrary ASerialiseables (see below), a factory pattern is implemented to enable Serialisers
to instantiate Serialiseables dynamically from their string name. This is handled entirely by the abstract
implementation, but it is the responsibility of the `ASerialiseable`-derived-writer to register the type with the
factory.

###### Some Subtleties

In the case of Numpy Arrays, the specification of **what** to store blends into **how** it is stored: this is because,
the array needs to know the shape of its dimensions and the type of object it is encapsulating: these are serialised as
part of the numpy array storage.

The avid programmer may argue that for proper orthogonality, the type of the array should be specified as part of the
python dict. However, not only is this more difficult, but it precludes certain efficiency considerations. Specifically,
while in most situations, the size of the array is fixed (and can be easily specified), in others it may not. Hence,
some archivers may deal with a pre-specified structure while others will need to encode certain structure (which is not
limited to numpy arrays but even the type of sample itself) with the data itself. This aspect is more controllable at
the serialiser level rather than the sample object (serialiseable) level.

Anoter key design decision was how much of the functionality to implement at the abstract base class level and which to
delegate to the concrete implementation. A case in point concerned the parsing of the attribute dictionary. In the end,
it was decided that for efficiency reasons, this would be the responsibility of the derived class. The reason is that
due to the different ways in which the data can be serialised (such as storing structure itself, the attribute names or
just the binary data, as discussed above), the abstract-level implementation was going to end up defining too many
fine-grained abstract methods, most of which would not be used by any one concrete implementation.

#### Persistence - APersistor

The final aspect is the actual persisting of serial data to some storage/transmission medium. This is the realm of the
`APersistor`, or rather the concrete implementations of it.

Given the demands of the project, persistence needs to maintain a level of indexing. At the same time, in the interest
of efficiency, the design calls for a single archiver for the entire pipeline. Data originating from different modules
(and hence generally of different types) is identified through unique keys. Within the same key, samples are further
indexed by a time-stamp. Handling both is the responsibility of the concrete implementation, and, especially for saving
the time-stamp, presents another subtlety where the realms of **how** and **where** overlap. 

The Abstract Base Class itself is limited in its responsibilities, dealing mainly with:

 1. Exposing a consistent public API to the user: this includes setup and tear-down and sample writing/readout,
 
 1. Ensuring consistent life-cycle control, including thread-safety and preventing illegal operations,

 1. Instantiating and managing the associated `ASerialiser` (which type is passed as parameter), and
 
 1. Acting as a Factory for `ASerialiser`'s.


###### Thread-Safety

The issue of thread-safety is generally handled by the Abstract `APersistor`, but presents certain challenges.
Certainly, operations such as setup/tear-down of the archiver cannot happen simultaneously, or while readouts/writeouts
are happening. However, depending on the type of archiver itself, readouts/writeouts themselves may happen
simultaneously to different keys or even to the same key.

To solve this conundrum, the abstract class implements a read-write lock mechanism around its public methods. Setup and
cleanup methods must acquire a write lock before executing (ensuring exclusive access): writing and/or reading require
only a read lock: it is then up to the concrete implementation to determine thread-safety between them. This is based on
the premise that setup/cleanup affect the status of the object as a whole, while the other methods only affect local
(at most key-specific) states, which can be dealt with at the level of the concrete class.


## Issues ##
* The `ASerialiseable` dictionary scheme has not been tested to understand how well it plays with:

  1. Class Attributes (as opposed to object attributes)
  
  1. Name Mangled attributes (by naming convention, those with a leading double underscore`__`)
 
* There is a limitation in the data types which can be supported. Most notably, there is no support for dictionary types.

* There is limited error checking in serialisation to ensure that the callee does not attempt to serialise callables for
  example.
  
* There is still the gray area with regards to each concrete implementation having to implement its own parsing of the
  hierarchical structure... Now this is necessary in a way, to allow different archive types to store different
  structures but what about code reuse?

<[Go Back](Core.md)>