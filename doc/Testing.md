# Testing Framework

The testing framework for the project is based on python's [unittest](https://docs.python.org/2/library/unittest.html)
module. Testing provides a consistent methodology for ensuring that updates and changes to the library do not break
existing packages.

## Methodology ##
For details on using the [unittest](https://docs.python.org/2/library/unittest.html) framework refer to the appropriate
documentation. Here, we discuss only the use of the module with regards to the TCPO Library.

### Architecture ###
All testcases should inherit from the **TCPOTester** base class (TCPOTests.py). Besides consolidating the framework, the
class implements added functionality:

 * Assertion for No Exception
 * Checks-style assertions, which do not fail immediately but only at the end of the test-case (method). This allows
    testing in a for-loop for example when failures do not depend on each-other, and indicating which one failed.

All the major modules have corresponding tests, and future developers should adhere to this requirement. The Tests
should validate the key features of each functionality. To do so, it may be necessary to implement concrete classes of
abstract interfaces: these should be defined as *dummy* classes within the Test script itself, and should make the least
possible assumptions about the concrete implementation, focusing instead (where possible) on the functionality and 
responsibilities of the abstract interface.

Another key requirement is that tests should be self-contained, and not depend on the results of other tests. This is
not only because tests may be run in any order, but also because it is preferable that broken modules do not affect
other modules. In most cases this can be achieved by implementing dummy classes which emulate the functionality of the
required dependency. Obviously this is not possible in all scenarios, but must be contained wherever possible and
appropriately documented if not the case.

### Current Tests ###

Most of the modules have their own testing scripts, some of which are organised in respective packages whose names are
self-explanatory. Additionally, **Test000.py** contains a single test case to ensure that the environment is set up
correctly by checking for the required modules.

### Caveats ###

1. One aspect that is central to the testing framework is comparing equality of results. This is a problem in comparing
   objects, since python defaults to comparing identities (i.e. the 'memory-address') of the instances. Hence, any
   classes which require comparison using the **assertEqual** should override the equality (`__eq__`) operator: 
   likewise, if using **assertNotEqual**, the inequality operator (`__ne__`) should be overridden. In particular, note
   that overriding one does not imply the other. The ASerialiseable base class does override this functionality for the
   serialiseable portion of the class.
   
1. Numpy arrays are not directly comparable using the **assertEqual** functionality. There are two options:
   1. Iterate in a for loop
   1. Use `(a == b).all()` and test for truth
   1. Use `np.array_equal(a, b)` and test for truth.
   
1. NaN values by default do not compare equal (or unequal for that matter). For this reason, the npalt module defines an
    array_nan_equal equivalent to np.array_equal which equates NaN values.

## Testing ##

All tests can be executed from the command line by running the main script:

```bash
python TCPOTests.py
```
    
This will use unittest's auto-discovery feature to search for and execute all tests in the *pytcpotests* package. If the
library is not installed as a system-wide dependency, the above should be executed from the *python* project directory
rather than the *pytcpotests* directory itself, to ensure that the other modules are discovereable:
 
```bash
python pytcpotests/TCPOTests.py
```

<[Go Back](Info.md)>