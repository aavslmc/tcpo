% Abstract Processor Module TCPO Template

\subsection{OutlierWeightedOverlapDetector}

	\textbf{AbstractProcessor Type} \\
	
	This class implements a historical overlap based anomaly detector, where the moving average computation in the case of an anomalous sample is weighted according to the number of outliers detected (typically by a smoothness-based outlier detector):
	\[\text{ma} \ += \ \left\{\begin{array}{ll}
	\text{msg.default for f in no. of poly fits} & \text{if non-anomalous} \\
	\text{none} & \text{if msg.default is nan} \\
	\text{ma * alpha + msg.default * (1 - alpha)} & \text{if anomalous}
	\end{array}\right\}\]
	
	where :
	\begin{enumerate}
		\item $+=$ is the normal moving average update
		\item none indicates no update to the moving average
		\item alpha = 1 - min(1.0, msg.outliers/max\_outliers)
		\item max\_outliers is the maximum allowed outliers to consider a sample at all (calculated from ratio to \# of
		channels in the computation)
	\end{enumerate}

	This ensures that while anomalous samples do not corrupt the average, the history-based scheme is able to recover from a wrong start. \\
	
	The module deals with invalid models (containing NaN's) in the following ways:
	\begin{enumerate}
		\item A model containing at least one FitStorage object set to check\_nan=True is treated as an anomaly
		\item While the buffer for some piecewise fit is filling up (if ignore is set), then if it encounters an nan-model, its contribution is replaced with the 'start' vector corresponding to the piecewise fit - the antenna / polarisation is not signalled at this stage however
		\item During normal operation, nan'd models are treated like any other anomaly
	\end{enumerate}

	Note that the module assumes that the Outlier data does not contain np.NaN (since it is a boolean matrix).
	
	\subsubsection{Input Dependencies}
	
	\begin{enumerate}
		\item FitStorage objects within a 3D Numpy array (order Antennas / Polarisations / FitStorage), with coefficients within the FitStorage objects
		\item Outlying Channels per antenna / polarisation, within a boolean 3D Numpy array, order Antennas / Polarisations / Channels)
	\end{enumerate}
	
	\subsubsection{Output}
	
		\begin{itemize}
			\item Succeeding Modules - 2D Numpy array (order Antennas / Polarisations) of type boolean, flagging which antennas/polarisations are anomalous
			\item Archiver - List of FlaggedAnomaly objects indicating which Antennas / Polarisations are anomalous, with the anomaly field including the overlap ratios with the historical average for each anomalous fit
		\end{itemize}
	
	\subsubsection{Setup Requirements}
	
		\begin{enumerate}
			\item antennas - [Required] : The indices of selected antennas in the tile
			\item polarisations - [Required] : The indices of selected polarisations per antenna
			\item channels - [Required] : The (complete) list of channels fitted over (incl. interpolated regions) to evaluate over
			\item fits - [Required] : The number of polynomial fits considered
			\item start - [Required] : Numpy array of starting vectors (one for each fit). This will also dictate the
			shape of the model coefficients which will be received. It is also used as a stand-in during the initialisation phase, if an anomalous piecewise fit entry is detected due to nan's in which case this value is input instead to the moving average. Vectors must be supplied in the same order that the corresponding FitStorage objects are supplied
			\item length - [Required] : The length of the Moving Average over which to smooth comparisons
			\item min\_overlap - [Required] : Minimum overlap ratio to consider for a valid antenna (used for all piecewise polynomial fits)
			\item max\_outlier - [Required] : The maximum number of outliers (ratio of number of channels) to allow for consideration of sample in Moving Average
			\item outlier\_gen - [Required] : Reference to the outlier detection module used
			\item form - [Optional] : When True (defaults to False) indicates that the comparator should only consider the general shape (form) rather than the overall area. This is achieved effectively by
			zeroing out the constant term.
			\item ignore - [Optional] : If True (default False), does not signal anomalies until after the moving average buffer for each piecewise fit has been filled up completely with real data. This option is typically used when the start vectors are non-informative (such as all zero vectors)
		\end{enumerate}
	
	\subsubsection{Limitations}
	
	The system may still be affected negatively by a long stream of high-magnitude anomalous antennas, which will shift the 'normal' state towards it. This can happen especially if the anomalous state does not exhibit considerable noise, but is just a magnitude shift. However, the possibilities of this should be remote.
