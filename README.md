# README #

This is the improved TCPO Calibration Library developed by Michael Camilleri (Ascent), Josef Borg (Ascent) and Xandru
Mifsud (Ascent) in collaboration with the University of Oxford. This is only an introductory document. More advanced
information is provided within the [doc](doc/Info.md)umentation folder. 

## Introduction ##

* The Python Repository is organised into 3 Packages:

    * The **pytcpo** Package defines the framework to build the pipelines, as well as a number of modules ready-built
        for the same purpose. The library is detailed in the [doc](doc/Info.md) scripts.
    
    * The **Scripts** Package includes a number of example usages and a number of visualisers.
      
    * Finally, the **pytcpotests** Directory contains a number of testing scripts.

* Unless otherwise stated, the library is thread-safe.

## Setting up the Project ##

### Requirements ###
The required APT packages are listed in requirements.apt. Note that the library is compatible with Python 2.7
***(not 3)***

Additionally, the pip file, found in python/requirements.pip, can be used to automatically install required python
packages. Note that this pip file contains two types of requirements:
 * Standard PIP requirements
 * Libraries which are not in the pip repository and must be manually installed to the python path. These are indicated
   by a single comment (#). These are necessary to allow the Tester to search for them when testing.
 
### Setup Procedure ###

1. Install any requirements as specified in the *.apt* file.

1. In most cases, you may wish to install the library in a virtual python environment. This is especially the case on
   newer system since the proejct requires python 2.7. Refer to 
   [Anaconda](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/) for a step-by step
   walkthrough.

1. In any case, install the pip requirements for the library. This can be done by executing 

    ```bash
    pip install requirements.pip
    ```
       
1. You will need to install the aavs-daq library, upon which this object is built. This is not a pip library and hence
    must be manually installed by executing 

    ```bash
    python setup.py install 
    ```
    from within the *aavs-daq/python* directory.
    
1. You are done! Just import the modules from the relative directory, and run the tests to ensure that everything is set
    up correctly (see below).

###Testing Methodology ###
The library uses the standard python unittest framework.
Typically, to run all the tests in the *pytcpotests* package, just execute:

```bash
python TCPOTests.py
```
    

### Directory Structure ###
All python code is packaged under the *python* directory, while the *doc* directory contains more verbose documentation.

Key: [P] - Package / [M] - Module

    project
     |__ doc : Contains Documentation Files
     |
     |__ matlab : Contains some (obsolete) Matlab visualisation scripts
     |
     |__ Simulation: Contains Simulation Data for testing
     |
     |__ python
          |
          |__ pytcpo [P] : The TCPO Library
          |     |
          |     |__ Core [P] : Abstract Library
          |     |     |
          |     |     |__ Archiving [P] : Archiving Framework
          |     |     |     |
          |     |     |     |__ Persistor [M] : Abstract Persistor Definition & Associated Persistor Factory
          |     |     |     |__ Serialiseable [M] : Abstract Serialiseable Interface
          |     |     |     |__ Serialiser [M] : Abstract Serialiser Interface
          |     |     |
          |     |     |__ Common [P] : Base Utilities & Helpers
          |     |     |     |
          |     |     |     |__ DataFilters [M] : MovingAverage, ConnectorFiller & PolynomialCompare
          |     |     |     |__ ErrorHandling [M] : TCPOCode, TCPOUpdate & TCPOException
          |     |     |     |__ Graphing [M] : HorRadioButton
          |     |     |     |__ npalt [M] : Addition to Numpy: exp10, get_nearest_idx, array_nan_equal, rms
          |     |     |     |__ Utilities [M] : string (wrapper), namer, tupler, moduler, directory
          |     |     |
          |     |     |__ Pipeline [P] : Base Pipeline Definitions
          |     |           |
          |     |           |__ Message [M] : The Messaging infrastracture
          |     |           |__ PipelineBuilder [M]: Abstract Pipeline Builder Interface
          |     |           |__ PipelineManager [M]: Concrete Pipeline Manager
          |     |           |__ PipelineModule [M]: AbstractProcess, AbstractGenerator and Aggregator Modules
          |     |
          |     |__ Modules [P] : Concrete Implementations
          |     |
          |     |__ Pipelines [P] : 
          |
          |
          |__ Scripts [P] : A Number of executable Scripts for running the pipeline
          |     |
          |     |__ Examples [P] : A Number of (Obsolete) Examples. May be retired
          |     |
          |     |__ Pipelines [P] : Pipeline Executables
          |     |     |
          |     |     |__ BandPassPipeline [M] : Executable around the Simple Anomaly Detection Pipeline
          |     |     |__ ModelFittingRuns [M] : Executable for testing multiple configurations of the bandpass pipeline
          |     |     |__ PolyDataEmulationPipeline [M] : Executable for the polynomial data emulator
          |     |
          |     |__ Visualisers [P] : Scripts for graphing of pipeline results
          |           |
          |           |__ Antenna-EM-Verification [P] : EM Model visualisation Scripts (replace Matlab Code)
          |           |     |
          |           |     |__ SingleAntennaView [M] : Visualiser for single-antennas (with selections for scale/component etc...)
          |           |     |__ SynthesizedAntennaView [M] : Visualiser for the Synthesized Antenna View
          |           |
          |           |__ RealTime_Plot [M] : Grapher for the results of the ModelFittingRuns & BandPassPipeline
          |
          |
          |__ Test [P] : Testing Packages
          
          
          
          
          
          
          
          
          
     |    |__ Core : [P]
     |    |    |
     |    |    |__ Algorithmics : [P]
     |    |    |      |__ DataFilters.py [M] - Moving Average Filter
     |    |    |
     |    |    |__ Archiving : [P]
     |    |    |      |__ Modules.py [M] - Serialiseable and Archiver Interfaces
     |    |    |
     |    |    |__ Emulators : [P]
     |    |    |      |__ Common.py [M] - Abstract Data Emulator Definition
     |    |    |
     |    |    |__ Pipeline : [P]
     |    |           |__ Common.py [M] - Definition of the Pipeline Message and Error Handling
     |    |           |__ Modules.py [M] - Abstract pipeline module and helper classes
     |    |
     |    |__ Tools : [P]
     |    |    |
     |    |    |__ AnomalyDetectors : [P]
     |    |    |      |__ Common.py [M] - Anomaly Abstract Type Specification
     |    |    |      |__ Implementations : [P]
     |    |    |            |__ CooperativeDetectors.py [M] - Detection based on inter-antenna differences
     |    |    |            |__ IndependentDetectors.py [M] - Detection based on intrinsic Antenna
     |    |    |
     |    |    |__ Archivers : [P]
     |    |    |      |__ FileStream.py [M] - Text based file archiver
     |    |    |
     |    |    |__ DataEmulators : [P]
     |    |    |      |__ PolynEmulator.py [M] - Channel-stage emulators based on polynomial with noise
     |    |    |
     |    |    |__ Equalisers : [P]
     |    |    |      |__ DeTrender.py : [M] - De-Trending of signal based on polynomial model
     |    |    |      |__ SimpleEqualisers.py [M] - Inter-Antenna/Polarisation mean equalisation
          

 
## TODO ##
 * Complete the Folder Structure
    
## Queries ##

* Michael Camilleri: michael.camilleri@ascent.software