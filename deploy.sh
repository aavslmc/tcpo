#!/bin/bash

echo -e "======================================================\n"
echo -e "====== Configuring TCPO Framework and Utilities ======\n\n"
echo -e " Note that the AAVS-DAQ Library must be preinstalled.\n"
echo -e "Also, make sure that you are connected to the internet\n\n"
echo -e "------------------------------------------------------\n\n"

# Installing required system packages
echo -e "------------------------------------------------------\n\n"
echo -e " Installing required system (synaptic) packages\n"
sudo apt-get -q install --force-yes --yes $(grep -vE "^\s*#" requirements.apt  | tr "\n" " ")
echo -e "\n\n Packages Installed.\n"


# Go into Python Directory
pushd python

# Install required python packages
echo -e "------------------------------------------------------\n\n"
echo -e " Installing Python Dependencies\n"
pip install -r requirements.pip         # Package-Managed Dependencies
echo -e "\n\n Python Dependencies Installed.\n"

# Install DAQ python library
echo -e "------------------------------------------------------\n\n"
echo -e " Installing pytcpo library\n"
python setup.py install
echo -e "\n\n Done.\n"

# Prompt to run tests
echo -e "Installation Done. Do you wish to run Tests? (yes/no) [ENTER]:"
read run_tests

while [["$run_tests" != "yes" && "$run_tests" != "no"]]; do
    echo -e "Wrong Input - (yes/no) [ENTER]:"
done

if "$run_tests" == "yes"; then
    python python/pytcpotests/TCPOTests.py
    echo -e "\nAll Done. GoodBye!"

else "$run_tests" == "no"
    echo -e "GoodBye!"

fi

# Finished with python
popd